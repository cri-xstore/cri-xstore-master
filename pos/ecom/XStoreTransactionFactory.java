// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   XStoreTransactionFactory.java

package com.micros_retail.xcenter.serenade.order.lookup;

import com.micros_retail.util.*;
import com.micros_retail.util.logging.Logger;
import com.micros_retail.xcenter.bootstrap.XcenterProperties;
import com.micros_retail.xcenter.XcenterException;
import com.micros_retail.xcenter.serenade.databeans.CWOrderOut;
import dtv.data2.AbstractPersistenceDefaults;
import dtv.data2.access.DataFactory;
import dtv.data2.access.ObjectNotFoundException;
import dtv.xst.dao.crm.IParty;
import dtv.xst.dao.crm.PartyId;
import dtv.xst.dao.itm.IItem;
import dtv.xst.dao.itm.ItemId;
import dtv.xst.dao.tax.*;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trn.PosTransactionId;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.micros_retail.xcenter.serenade.order.lookup:
//            CwOrderId

public class XStoreTransactionFactory
{

    public XStoreTransactionFactory(CwOrderId argOrderId, CWOrderOut argCWOrderOut)
    {
        Assert.notNull(argOrderId, "argOrderId cannot be null.");
        Assert.notNull(argCWOrderOut, "argCWOrderOut cannot be null.");
        orderId = argOrderId;
        cwOrderOut = argCWOrderOut;
    }

    public IRetailTransaction createTransaction()
    {
        matchShipTo();
        PosTransactionId id = new PosTransactionId();
        id.setOrganizationId(Long.valueOf(getOrganizationId()));
        
        //id.setRetailLocationId(Long.valueOf(0L));
        // Replacing WebOrder Store No from 0 to new value 993 - Value is configured in Xcenter.properties
        String webOrderStoreNo = getConfigValue("xcenter.IBMWebService.WebOrderStoreNo", "993");
        LOG.warn("XStoreTransactionFactory:: createTransaction: webOrderStoreNo "+ webOrderStoreNo);
        id.setRetailLocationId(Long.valueOf(webOrderStoreNo));
        LOG.warn("XStoreTransactionFactory:: createTransaction: TEST "+ id.getRetailLocationId()+"Second ID"+Long.valueOf(webOrderStoreNo));
        id.setWorkstationId(Long.valueOf(0L));
        id.setBusinessDate(cwOrderOut.getHeader().getOrderDate());
        id.setTransactionSequence(Long.valueOf(cwOrderOut.getHeader().getOrderId()));
        transId = id;
        buildHeader();
        buildLines();
        return trans;
    }

    protected BigDecimal round(BigDecimal argNumber)
    {
        return NumberUtils.nonNull(argNumber).setScale(2, RoundingMode.HALF_UP);
    }

    private void buildHeader()
    { 
    	String webOrderStoreNo = getConfigValue("xcenter.IBMWebService.WebOrderStoreNo", "993");
    	LOG.warn("XStoreTransactionFactory:: createTransaction: webOrderStoreNo "+ webOrderStoreNo);
    	
        trans = (IRetailTransaction)DataFactory.createTransientObject(dtv.xst.dao.trl.IRetailTransaction.class);
        trans.setObjectId(transId);
        trans.setRetailLocationId(Long.valueOf(webOrderStoreNo));
        trans.setSubtotal(shipTo.getSubTotal());
        trans.setTaxAmount(shipTo.getTax());
        trans.setTotal(shipTo.getOrderTotal());
        trans.setTransactionStatusCode("COMPLETE");
        trans.setTransactionTypeCode("RETAIL_SALE");
        trans.setStringProperty("CROSSCHANNEL_SERENADE_COMPANY", orderId.getCompanyCode());
        trans.setStringProperty("CROSSCHANNEL_SERENADE_SHIP_TO", String.valueOf(orderId.getShipToNumber()));
        setOperatorParty();
        setCustomer();
    }

    private void buildLines()
    {
        buildSaleReturnLines();
    }

    private void buildSaleReturnLines()
    {
        com.micros_retail.xcenter.serenade.databeans.CWOrderOut.Header.ShipTo.Detail cwLine;
        ISaleReturnLineItem line;
        for(Iterator i$ = shipTo.getDetails().iterator(); i$.hasNext(); line.setRetailTransactionLineItemSequence(cwLine.getLineSeqNumber()))
        {
            cwLine = (com.micros_retail.xcenter.serenade.databeans.CWOrderOut.Header.ShipTo.Detail)i$.next();
            line = (ISaleReturnLineItem)DataFactory.createTransientObject(dtv.xst.dao.trl.ISaleReturnLineItem.class);
            line.setLineItemTypeCode("ITEM");
            line.setSaleReturnLineItemTypeCode("SALE");
            BigDecimal availableToReturn = getAvailableToReturn(cwLine);
            line.setItemId(cwLine.getItemId());
            line.setScannedItemId(cwLine.getItemId());
            line.setQuantity(availableToReturn);
            line.setReturnedQuantity(cwLine.getReturnQuantity());
            BigDecimal basePrice = NumberUtils.nonNull(cwLine.getActualPrice());
            line.setExtendedAmount(round(basePrice.multiply(availableToReturn)));
            line.setGrossAmount(basePrice);
            line.setRegularBasePrice(basePrice);
            line.setUnitPrice(basePrice);
            line.setBaseUnitPrice(basePrice);
            line.setNetAmount(basePrice);
            line.setGrossQuantity(availableToReturn);
            line.setNetQuantity(availableToReturn);
            loadItem(line);
            setTaxModifiers(cwLine, line);
            trans.addRetailTransactionLineItem(line);
        }

    }

    private BigDecimal getAvailableToReturn(com.micros_retail.xcenter.serenade.databeans.CWOrderOut.Header.ShipTo.Detail argCwLine)
    {
        BigDecimal availableToReturn = NumberUtils.nonNull(argCwLine.getShipQuantity()).subtract(NumberUtils.nonNull(argCwLine.getReturnQuantity())).max(BigDecimal.ZERO);
        return availableToReturn;
    }

    private long getOrganizationId()
    {
        return AbstractPersistenceDefaults.getInstance().getOrganizationId().longValue();
    }

    private void loadItem(ISaleReturnLineItem argLine)
    {
        ItemId id = new ItemId();
        id.setOrganizationId(Long.valueOf(getOrganizationId()));
        id.setItemId(argLine.getItemId());
        try
        {
            argLine.setItem((IItem)DataFactory.getObjectById(id));
        }
        catch(ObjectNotFoundException ex)
        {
            LOG.warn((new StringBuilder()).append("Couldn't match serenade item id ").append(argLine.getItemId()).append(" to the Xcenter item master.").toString(), ex);
            throw ex;
        }
    }

    private void matchShipTo()
    {
        for(Iterator i$ = cwOrderOut.getHeader().getShipTos().iterator(); i$.hasNext();)
        {
            com.micros_retail.xcenter.serenade.databeans.CWOrderOut.Header.ShipTo shipToCandidate = (com.micros_retail.xcenter.serenade.databeans.CWOrderOut.Header.ShipTo)i$.next();
            LOG.warn("Inside XStoreTransactionFactory::matchShipTo :orderId.getShipToNumber() "+orderId.getShipToNumber());
            LOG.warn("Inside XStoreTransactionFactory::matchShipTo :shipToCandidate.getShipToNumber() "+shipToCandidate.getShipToNumber());
            if(orderId.getShipToNumber() == shipToCandidate.getShipToNumber())
            {
                shipTo = shipToCandidate;
                return;
            }
        }

        throw new XcenterException("Couln't find requested shipTo number %s in order %s", new Object[] {
            Integer.valueOf(orderId.getShipToNumber()), orderId.getOrderId()
        });
    }

    private void setCustomer()
    {
        IParty party = (IParty)DataFactory.createTransientObject(dtv.xst.dao.crm.IParty.class);
        party.setFirstName(cwOrderOut.getHeader().getSoldToFname());
        party.setLastName(cwOrderOut.getHeader().getSoldToLname());
        trans.setCustomerParty(party);
    }

    private void setOperatorParty()
    {
        PartyId id = new PartyId();
        id.setOrganizationId(Long.valueOf(getOrganizationId()));
        long XSTORE_SYSTEM_USER = 0L;
        id.setPartyId(Long.valueOf(XSTORE_SYSTEM_USER));
        IParty xstoreSystemUser = (IParty)DataFactory.createTransientObject(dtv.xst.dao.crm.IParty.class);
        xstoreSystemUser.setObjectId(id);
        xstoreSystemUser.setLastName("Xstore system user.");
        trans.setOperatorPartyId(XSTORE_SYSTEM_USER);
        trans.setOperatorParty(xstoreSystemUser);
    }

    private void setTaxModifiers(com.micros_retail.xcenter.serenade.databeans.CWOrderOut.Header.ShipTo.Detail argCwLine, ISaleReturnLineItem argLine)
    {
        //String taxGroupId = "SERENADE_UNKNOWN_TAX_GROUP";
        //String taxGroupId = argLine.getTaxGroupId();
        String taxGroupId = argLine.getItem().getTaxGroupId();
        LOG.warn("XStoreTransactionFactory:: setTaxModifiers: taxGroupId Set from Item"+ taxGroupId);
        String taxAuthorityId = "SERENADE_UNKNOWN_TAX_AUTHORITY";
        String taxLocationId = "SERENADE_UNKNOWN_TAX_LOCATION";
        ITaxGroup taxGroup = (ITaxGroup)DataFactory.createTransientObject(dtv.xst.dao.tax.ITaxGroup.class);
        taxGroup.setTaxGroupId(taxGroupId);
        argLine.setTaxGroup(taxGroup);
        BigDecimal availableToReturn = getAvailableToReturn(argCwLine);
        if(NumberUtils.isZero(availableToReturn))
        {
            return;
        } else
        {
            BigDecimal taxAmount = NumberUtils.nonNull(argCwLine.getTax());
            ITaxGroupRule taxGroupRule = (ITaxGroupRule)DataFactory.createTransientObject(dtv.xst.dao.tax.ITaxGroupRule.class);
            ITaxAuthority taxAuthority = (ITaxAuthority)DataFactory.createTransientObject(dtv.xst.dao.tax.ITaxAuthority.class);
            taxAuthority.setTaxAuthorityId(taxAuthorityId);
            taxAuthority.setRoundingDigitsQuantity(2);
            taxAuthority.setRoundingCode("HALF_UP");
            taxAuthority.setName("_webOrderUnknownTaxAuthority");
            taxGroupRule.setTaxAuthority(taxAuthority);
            taxGroupRule.setTaxLocationId(taxLocationId);
            taxGroupRule.setTaxGroupId(taxGroupId);
            taxGroupRule.setTaxedAtTransLevel(true);
            ITaxRateRule rateRule = (ITaxRateRule)DataFactory.createTransientObject(dtv.xst.dao.tax.ITaxRateRule.class);
            rateRule.setTaxLocationId(taxLocationId);
            rateRule.setAmount(taxAmount);
            rateRule.setTaxGroupId(taxGroupId);
            rateRule.setTaxRuleSequence(0);
            rateRule.setEffectiveDatetimestamp(DateUtils.getFarPastDate());
            rateRule.setExpirationDatetimestamp(DateUtils.getFarFutureDate());
            ISaleTaxModifier taxModifier = (ISaleTaxModifier)DataFactory.createTransientObject(dtv.xst.dao.trl.ISaleTaxModifier.class);
            taxModifier.setTaxableAmount(argLine.getExtendedAmount());
            taxModifier.setTaxAmount(taxAmount);
            taxModifier.setSaleTaxGroupRule(taxGroupRule);
            taxModifier.getSaleTaxGroupRule().addTaxRateRule(rateRule);
            taxModifier.setTaxOverride(true);
            taxModifier.setTaxOverrideAmount(taxAmount);
            argLine.addSaleTaxModifier(taxModifier);
            return;
        }
    }

	private String getConfigValue(String propertyKey, String defaultValue) {
		String value = XcenterProperties.getProperty(propertyKey);
		return StringUtils.isEmpty(value) ? defaultValue : value;
	}
	
    private static final Logger LOG = Logger.getLogger();
    private CWOrderOut cwOrderOut;
    private CwOrderId orderId;
    private com.micros_retail.xcenter.serenade.databeans.CWOrderOut.Header.ShipTo shipTo;
    private IRetailTransaction trans;
    private PosTransactionId transId;

}
