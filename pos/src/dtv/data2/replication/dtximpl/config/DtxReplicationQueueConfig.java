//$Id: DtxReplicationQueueConfig.java 26626 2010-12-07 21:19:32Z dtvdomain\jweiss $
package dtv.data2.replication.dtximpl.config;

import java.util.ArrayList;
import java.util.List;

import dtv.util.config.*;

/**
 * Replication config that defines parameters for the replication queue<br>
 * 
 * Copyright (c) 2005 Datavantage Corporation
 * 
 * @author mmichalek
 * @version $Revision: 26626 $
 * @created Oct 26, 2005
 */
public class DtxReplicationQueueConfig
    extends AbstractParentConfig {

  private static final long serialVersionUID = 1L;
  private static final int DEFAULT_CYCLE_INTERVAL = 10000;
  private static final int DEFAULT_MAX_RECRORDS = 50;
  private static final int DEFAULT_FAILURE_COUNT_RESET_INTERVAL = 600000; // 10 minutes
  private static final int DEFAULT_ERROR_NOTIFICATION_INTERVAL = 30;

  private String dataSource_;
  private int cycleInterval_;
  private int maxRecsPerCycle_;
  private int offlineFailureCountResetInterval_ = DEFAULT_FAILURE_COUNT_RESET_INTERVAL;
  private int errorFailureInterval_;
  private final List<RelegationLevelConfig> relegationLevels_ = new ArrayList<RelegationLevelConfig>(4);

  public int getCycleInterval() {
    return cycleInterval_;
  }

  public String getDataSource() {
    return dataSource_;
  }

  public int getMaxRecsPerCycle() {
    return maxRecsPerCycle_;
  }

  public int getOfflineFailureCountResetInterval() {
    return offlineFailureCountResetInterval_;
  }

  public List<RelegationLevelConfig> getRelegationLevels() {
    return relegationLevels_;
  }

  public int getErrorFailureInterval() {
    return errorFailureInterval_;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setConfigObject(String argKey, IConfigObject argValue) {
    if ("dataSource".equalsIgnoreCase(argKey)) {
      dataSource_ = argValue.toString();
    }
    else if ("cycleInterval".equalsIgnoreCase(argKey)) {
      cycleInterval_ = ConfigUtils.toInt(argValue, DEFAULT_CYCLE_INTERVAL);
    }
    else if ("maxRecsPerCycle".equalsIgnoreCase(argKey)) {
      maxRecsPerCycle_ = ConfigUtils.toInt(argValue, DEFAULT_MAX_RECRORDS);
    }
    else if ("offlineFailureCountResetInterval".equalsIgnoreCase(argKey)) {
      offlineFailureCountResetInterval_ = ConfigUtils.toInt(argValue, DEFAULT_FAILURE_COUNT_RESET_INTERVAL);
    }
    else if ("errorFailureNotificationInterval".equalsIgnoreCase(argKey)) {
      errorFailureInterval_ = ConfigUtils.toInt(argValue, DEFAULT_ERROR_NOTIFICATION_INTERVAL);
    }
    else if (argValue instanceof RelegationLevelConfig) {
      relegationLevels_.add((RelegationLevelConfig) argValue);
    }
    else {
      warnUnsupported(argKey, argValue);
    }
  }

}
