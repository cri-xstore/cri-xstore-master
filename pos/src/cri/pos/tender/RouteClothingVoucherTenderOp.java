//$Id: RouteClothingVoucherTenderOp.java 785 2014-05-23 01:40:51Z suz.bshi $
package cri.pos.tender;

import static cri.pos.common.CriCommonHelper.hasWestVirginiaTaxExempt;
import static dtv.pos.framework.action.type.XstChainActionType.START;
import static dtv.pos.iframework.op.OpStatus.COMPLETE;

import dtv.pos.common.OpChainKey;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.util.config.IConfigObject;

/**
 * Route to clothing voucher tender.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Bob.Shi
 * @created May 20, 2014
 * @version $Revision: 785 $
 */
public class RouteClothingVoucherTenderOp
    extends Operation {

  private static final long serialVersionUID = 1L;
  private IOpChainKey chainKey_ = null;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {

    if (hasWestVirginiaTaxExempt(argCmd)) {
      return HELPER.getRunChainResponse(COMPLETE, getChainKey(argCmd), argCmd, START);
    }
    return HELPER.completeResponse();
  }

  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("ChainKey".equalsIgnoreCase(argName)) {
      setChainKey(OpChainKey.valueOf(argValue.toString()));
    }
  }

  public IOpChainKey getChainKey(IXstCommand argCmd) {
    return chainKey_;
  }

  public void setChainKey(IOpChainKey argChainKey) {
    chainKey_ = argChainKey;
  }

}
