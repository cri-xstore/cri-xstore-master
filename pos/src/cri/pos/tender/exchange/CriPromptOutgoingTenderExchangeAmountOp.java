//$Id: CriPromptOutgoingTenderExchangeAmountOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.tender.exchange;

import java.util.List;

import dtv.i18n.*;
import dtv.pos.common.PromptKey;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.pos.iframework.validation.IValidationKey;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.exchange.PromptOutgoingTenderExchangeAmountOp;
import dtv.xst.dao.trl.IRetailTransactionLineItem;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Jul 5, 2012
 * @version $Revision: 1105 $
 */
public class CriPromptOutgoingTenderExchangeAmountOp
    extends PromptOutgoingTenderExchangeAmountOp {

  private static final long serialVersionUID = 1L;

  private static final String ISD_GIFT_CARD = "ISD_GIFT_CARD";
  private static final String ISD_GIFT_ECARD = "ISD_GIFT_ECARD";
  private boolean giftCardCashout;

  private static final FormattableFactory FF = FormattableFactory.getInstance();

  /**
   * @param argCmd current command
   * @return The IValidationKeys for this Op. These keys are chosen according to the Tender.
   */
  @Override
  public IValidationKey[] getValidationKeys(IXstCommand argCmd) {
    if (isISDGiftCardCashOut() && (((ISaleTenderCmd) argCmd).getTenderAmount() != null)) {
      return null;
    }
    else {
      return super.getValidationKeys(argCmd);
    }
  }

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    init(argCmd, argEvent);
    return super.handleOpExec(argCmd, argEvent);
  }

  /**
   * @param argCmd current command
   * @return The IPromptKey for this Op (OUTGOING_TENDER_EXCHANGE_AMOUNT_NOTIFY) or super class
   * result.
   */
  @Override
  protected IPromptKey getPromptKey(IXstCommand argCmd) {

    if (isISDGiftCardCashOut()) {
      return PromptKey.valueOf("OUTGOING_TENDER_EXCHANGE_AMOUNT_NOTIFY");
    }
    else {
      return super.getPromptKey(argCmd);
    }
  }

  private boolean isISDGiftCardCashOut() {
    return giftCardCashout;
  }

  protected void init(IXstCommand argCmd, IXstEvent argEvent) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    IPosTransaction tran = cmd.getTransaction();
    List<IRetailTransactionLineItem> lineItems = tran.getTenderLineItems();

    //Currently single gift card is allowed for cash out in a transaction
    if ((lineItems != null) && (lineItems.size() == 1)) {

      ITenderLineItem tenderLine = (ITenderLineItem) lineItems.get(0);
      String tenderId = tenderLine.getTenderId();
      if ((ISD_GIFT_CARD.equals(tenderId) || ISD_GIFT_ECARD.equals(tenderId))) {
        giftCardCashout = true;
      }
    }
  }

  /**
   * Passes the given IPromptKey and PromptArgs to the HELPER's getPromptResponse method. This is
   * overridden so that we can set the default value for the Tender amount.
   * 
   * @param argCmd The Command that holds state data for this OpChain.
   * @param argEvent The Event that triggered this Operation.
   * @param promptKey The IPromptKey for which a response should be returned.
   * @param promptArgs The Arguments for the given PromptKey.
   * @return The response from the HELPER.
   */
  @Override
  protected IOpResponse getPromptResponse(IXstCommand argCmd, IXstEvent argEvent, IFormattable[] promptArgs) {

    if (isISDGiftCardCashOut()) {

      ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
      IPosTransaction tran = cmd.getTransaction();
      cmd.setTenderAmount(tran.getAmountDue());
      IFormattable amount = FF.getSimpleFormattable(cmd.getTenderAmount(), FormatterType.MONEY);

      return HELPER.getPromptResponse(getPromptKey(cmd), new IFormattable[] {amount});
    }
    else {
      return super.getPromptResponse(argCmd, argEvent, getPromptArgs(argCmd, argEvent));
    }

  }
}
