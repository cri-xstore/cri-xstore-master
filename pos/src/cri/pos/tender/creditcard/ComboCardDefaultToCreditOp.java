//$Id: ComboCardDefaultToCreditOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.tender.creditcard;

import org.apache.log4j.Logger;

import dtv.hardware.HardwareMgr;
import dtv.hardware.IHardwareMgr;
import dtv.hardware.events.IHardwareInputEvent;
import dtv.pos.common.ConfigurationMgr;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.TenderHelper;
import dtv.xst.dao.tnd.TenderStatus;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Customization for CRI to default to CREDIT when a combo card is used for return<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Jun 8, 2012
 * @version $Revision: 1105 $
 */
public class ComboCardDefaultToCreditOp
    extends Operation {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(ComboCardDefaultToCreditOp.class);

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {

    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ICreditDebitTenderLineItem tenderLine = (ICreditDebitTenderLineItem) cmd.getTenderLineItem();
    String argTenderId =
        TenderHelper.getInstance().getNonDebitTenderId((IHardwareInputEvent<?>) tenderLine.getInputEvent());
    TenderHelper.getInstance().setTenderIdType(tenderLine, argTenderId);
    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {

    if (ConfigurationMgr.isPinpadRequiredForDebit()) {
      IHardwareMgr hm = HardwareMgr.getCurrentHardwareMgr();
      if (!hm.getPinPad().isPresent()) {
        logger_.info("no PIN pad present ergo prompt not applicable");
        return false;
      }
    }

    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ITenderLineItem tenderLine = cmd.getTenderLineItem();
    Object inputEvent = tenderLine.getInputEvent();
    if (!(inputEvent instanceof IHardwareInputEvent)) {
      logger_.info("no hardware input event ergo prompt not applicable");
      return false;
    }
    if (!TenderHelper.getInstance().isCreditDebitAmbiguous((IHardwareInputEvent<?>) inputEvent)) {
      logger_.info("not ambiguous ergo prompt not applicable");
      return false;
    }
    if (!cmd.getTenderStatus().equals(TenderStatus.REFUND)) {
      return false;
    }
    return true;

  }

}
