//$Id$
package cri.pos.tender.voucher;

import cri.pos.register.CriValueKey;

import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.voucher.GatherVoucherTenderInformationOp;
import dtv.util.config.*;

/**
 * Extends base functionality by parsing customer-specific parameters in <tt>ActionConfig.xml</tt>.
 * <br>
 * <br>
 * Copyright (c) 2013 MICROS Retail
 *
 * @author Administrator
 * @created Mar 27, 2013
 * @version $Revision$
 */
public class CriGatherVoucherTenderInformationOp
    extends GatherVoucherTenderInformationOp {

  private static final long serialVersionUID = 1L;

  /** {@inheritDoc} */
  @Override
  protected void setParameter(IXstCommand argCmd, IXstEvent argEvent, ParameterConfig argConfig) {
    String name = argConfig.getName();
    if ("CriPromptCustomer".equalsIgnoreCase(name)) {
      IConfigObject value = argConfig.getValue();
      Boolean prompt = ConfigUtils.toBoolean(value, false);
      CriValueKey.PROMPT_CUSTOMER.set(argCmd, prompt);
    }
    else {
      super.setParameter(argCmd, argEvent, argConfig);
    }
  }

}
