//$Id: CriPromptTenderAmtOp.java 785 2014-05-23 01:40:51Z suz.bshi $
package cri.pos.tender;

import static cri.pos.common.CriConstants.CLOTHING_VOUCHER_AMOUNT;
import static dtv.pos.framework.action.type.XstChainActionType.START;
import static dtv.pos.iframework.op.OpStatus.COMPLETE_HALT;

import java.math.BigDecimal;

import dtv.pos.common.OpChainKey;
import dtv.pos.common.PromptKey;
import dtv.pos.framework.op.OpState;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.PromptTenderAmtOp;
import dtv.util.NumberUtils;
import dtv.util.config.IConfigObject;

/**
 * Prompt tender amount.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Bob.Shi
 * @created May 20, 2014
 * @version $Revision: 785 $
 */
public class CriPromptTenderAmtOp
    extends PromptTenderAmtOp {

  private static final long serialVersionUID = 894104937718271399L;
  private IPromptKey tenderAmountPromptKey_ = null;
  private IPromptKey tenderAmountGreaterThanVoucherAmtPromptKey_ = null;
  private IOpChainKey backChainKey_ = null;
  private final IOpState CRI_ERROR_PROMPT = new OpState(this, "CRI_ERROR_PROMPT");

  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IOpState state = argCmd.getOpState();
    if (state == CRI_ERROR_PROMPT) {
      return HELPER.getRunChainResponse(COMPLETE_HALT, getBackChainKey(argCmd), argCmd, argEvent, START);
    }
    else {
      return super.handleOpExec(argCmd, argEvent);
    }
  }

  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    BigDecimal tenderAmount = getTenderAmount(argEvent);
    BigDecimal voucherAmount =
        new BigDecimal(cmd.getTenderLineItem().getStringProperty(CLOTHING_VOUCHER_AMOUNT));

    if (NumberUtils.isGreaterThan(tenderAmount, voucherAmount)) {
      argCmd.setOpState(CRI_ERROR_PROMPT);
      return HELPER.getPromptResponse(getTenderAmountGreaterThanVoucherAmtPromptKey(argCmd));
    }
    return super.handlePromptResponse(argCmd, argEvent);
  }

  @Override
  protected IPromptKey getPromptKey(IXstCommand argCmd) {
    return getTenderAmountPromptKey(argCmd);
  }

  public IPromptKey getTenderAmountPromptKey(IXstCommand argCmd) {
    return tenderAmountPromptKey_;
  }

  public void setTenderAmountPromptKey(IPromptKey argTenderAmountPromptKey) {
    tenderAmountPromptKey_ = argTenderAmountPromptKey;
  }

  public IOpChainKey getBackChainKey(IXstCommand argCmd) {
    return backChainKey_;
  }

  public void setBackChainKey(IOpChainKey argBackChainKey) {
    backChainKey_ = argBackChainKey;
  }

  public IPromptKey getTenderAmountGreaterThanVoucherAmtPromptKey(IXstCommand argCmd) {
    return tenderAmountGreaterThanVoucherAmtPromptKey_;
  }

  public void setTenderAmountGreaterThanTransAmtPromptKey(
      IPromptKey argTenderAmountGreaterThanTransAmtPromptKey) {
    tenderAmountGreaterThanVoucherAmtPromptKey_ = argTenderAmountGreaterThanTransAmtPromptKey;
  }

  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("tenderAmountPromptKey".equalsIgnoreCase(argName)) {
      setTenderAmountPromptKey(PromptKey.valueOf(argValue.toString()));
    }
    else if ("tenderAmountGreaterThanVoucherAmtPromptKey".equalsIgnoreCase(argName)) {
      setTenderAmountGreaterThanTransAmtPromptKey(PromptKey.valueOf(argValue.toString()));
    }
    else if ("BackChainKey".equalsIgnoreCase(argName)) {
      setBackChainKey(OpChainKey.valueOf(argValue.toString()));
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

}
