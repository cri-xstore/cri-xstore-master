//$Id: ClothingVoucherTenderAmountValidationRule.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.tender;

import java.math.BigDecimal;

import cri.pos.common.CriConstants;

import dtv.pos.iframework.validation.*;
import dtv.pos.tender.validation.TenderValidationData;
import dtv.pos.util.validation.AbstractValidationRule;
import dtv.util.NumberUtils;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Validate clothing voucher tender amount.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Bob.Shi
 * @created May 22, 2014
 * @version $Revision: 1105 $
 */
public class ClothingVoucherTenderAmountValidationRule
    extends AbstractValidationRule {

  /** {@inheritDoc} */
  @Override
  public IValidationResult validate(IValidationData argObject) {
    IValidationResult result = IValidationResult.SUCCESS;
    final TenderValidationData data = (TenderValidationData) argObject;
    final BigDecimal enteredAmount = data.getEnteredAmount();
    IPosTransaction trans = (IPosTransaction) data.getSuppliedData()[0];
    ITenderLineItem tender = data.getTenderLineItem();
    if ((tender != null) && CriConstants.CLOTHING_VOUCHER.equalsIgnoreCase(tender.getTenderId())
        && !NumberUtils.equivalent(enteredAmount, trans.getAmountDue())) {
      result = SimpleValidationResult.getFailed("_criInvalidClothingVoucherTenderAmountMsg");
    }
    return result;
  }
}
