//$Id: VoucherOnReturnRule.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.shared.visibilityrules;

import static dtv.pos.iframework.visibilityrules.AccessLevel.DENIED;
import static dtv.pos.iframework.visibilityrules.AccessLevel.GRANTED;
import static dtv.pos.register.returns.ReturnType.UNVERIFIED;
import static dtv.util.NumberUtils.isZero;

import dtv.pos.iframework.visibilityrules.IAccessLevel;
import dtv.pos.register.returns.ReturnManager;
import dtv.pos.shared.visibilityrules.TenderOnReturnRule;
import dtv.xst.dao.tnd.TenderCategory;
import dtv.xst.dao.trl.IRetailTransaction;
import dtv.xst.dao.trl.ISaleReturnLineItem;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2016 MICROS Retail
 *
 * @author aguddissa
 * @created Feb 18, 2016
 * @version $Revision: 1105 $
 */
public class VoucherOnReturnRule
    extends TenderOnReturnRule {

  /** {@inheritDoc} */
  @Override
  protected IAccessLevel checkVisibilityImpl()
      throws Exception {

    IRetailTransaction trans = getCurrentRetailTransaction();

    if ((trans == null) || isZero(trans.getAmountDue())) {
      return DENIED;
    }

    boolean hasReturn = false;
    for (ISaleReturnLineItem saleLine : trans.getLineItems(ISaleReturnLineItem.class)) {
      if (saleLine.getReturn() && !saleLine.getVoid()) {
        hasReturn = true;

        /* If there's a customer account modifier on the return line, assume that the latter is involved in
         * the cancellation of a customer item account. As such, access should be granted even in cases where
         * the return is verified. */
        if ((saleLine.getCustomerAccountModifier() != null) //
            || (saleLine.getReturnTypeCode() == null) //
        /*
         * CRI Act 457634/ FB 369690 -Don't allow refund to CreditCards on Blind return
         * || BLIND.matches(saleLine.getReturnTypeCode())
         */
            || UNVERIFIED.matches(saleLine.getReturnTypeCode())) {

          return GRANTED;
        }

      }
    }

    if (!hasReturn) {
      return GRANTED;
    }

    for (IRetailTransaction origTrans : ReturnManager.getInstance().getAllOrigTransaction()) {
      for (ITenderLineItem tenderLine : origTrans.getLineItems(ITenderLineItem.class)) {
        if (TenderCategory.VOUCHER.matches(tenderLine.getTender())) {
          return GRANTED;
        }
      }
    }
    for (ISaleReturnLineItem saleLine : trans.getLineItems(ISaleReturnLineItem.class)) {

      if (saleLine.getReturn() && !saleLine.getVoid() && saleLine.getReturnedWithGiftReceipt()) {
        return GRANTED;
      }
    }
    return DENIED;
  }
}
