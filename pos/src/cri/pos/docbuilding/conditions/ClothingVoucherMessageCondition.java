//$Id: ClothingVoucherMessageCondition.java 785 2014-05-23 01:40:51Z suz.bshi $
package cri.pos.docbuilding.conditions;

import static cri.pos.common.CriCommonHelper.getClothingVoucherNumber;

import dtv.docbuilding.conditions.AbstractCondition;
import dtv.util.StringUtils;
import dtv.xst.dao.trl.IRetailTransaction;

/**
 * Check to display clothing voucher message.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Bob.Shi
 * @created May 21, 2014
 * @version $Revision: 785 $
 */
public class ClothingVoucherMessageCondition
    extends AbstractCondition {

  /** {@inheritDoc} */
  @Override
  public boolean conditionMet(Object argSource) {
    IRetailTransaction trans = (IRetailTransaction) argSource;
    return !StringUtils.isEmpty(getClothingVoucherNumber(trans));
  }

}
