//$Id: CriPromptInventoryShippingItemQuantityOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.inventory.op;

import static dtv.hardware.types.HardwareType.KEYBOARD;

import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.inventory.op.IInventoryCmd;
import dtv.pos.inventory.op.PromptInventoryItemQuantityOp;

/**
 * Prompt the shipping item quantity. If the item is scanned don't prompt to enter the quantity.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author shy xie
 * @created Oct 3, 2012
 * @version $Revision: 1105 $
 */
public class CriPromptInventoryShippingItemQuantityOp
    extends PromptInventoryItemQuantityOp {

  /**
   * 
   */
  private static final long serialVersionUID = 3540983097563735466L;

  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    IInventoryCmd cmd = (IInventoryCmd) argCmd;
    boolean isScanned = !KEYBOARD.equals(cmd.getScanEntryMethod());

    if (isScanned) {
      return false;
    }

    else {
      return super.isOperationApplicable(argCmd);
    }
  }

}
