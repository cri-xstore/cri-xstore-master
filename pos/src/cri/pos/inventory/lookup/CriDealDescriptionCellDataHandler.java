//$Id: CriDealDescriptionCellDataHandler.java 777 2014-03-27 02:15:29Z suz.jliu $
package cri.pos.inventory.lookup;

import java.awt.Color;
import java.awt.Font;

import dtv.pos.framework.ui.listview.DefaultCellDataHandler;
import dtv.pos.framework.ui.listview.config.ListViewColumnConfig;
import dtv.ui.layout.ViewCellData;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Jerny.Liu
 * @created Mar 19, 2014
 * @version $Revision: 777 $
 */
public class CriDealDescriptionCellDataHandler
    extends DefaultCellDataHandler {

  @Override
  public ViewCellData.CellColumn buildCellColumn(ListViewColumnConfig argColConfig, Object argModel,
      Color argDefaultRowTextColor, Font argDefaultRowFont) {
    StringBuffer displayText = new StringBuffer();

    if (argModel instanceof CriItemPromotionalData) {
      CriItemPromotionalData dealData = (CriItemPromotionalData) argModel;

      for (String description : dealData.getDescriptionList()) {
        displayText.append(description).append("\n");
      }

      if (displayText.length() > 0) {
        displayText.delete(displayText.length() - 1, displayText.length());
      }
    }

    return new ViewCellData.CellColumn(displayText.toString(), null, argDefaultRowTextColor,
        argDefaultRowFont, argColConfig.getAlignment().getSwingAlignment(), argColConfig.getStart(),
        argColConfig.getWidth(), argColConfig.getRenderer(), argColConfig.isTextWrapped(),
        argColConfig.isSearchedOn());
  }
}
