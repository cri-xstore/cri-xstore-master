//$Id: CriCapturedCustSignStoreRule.java 1085 2016-09-16 17:51:17Z olr.jgaughn $
package cri.pos.hardware.rcptbuilding.copyrules;

import static cri.pos.common.CriCommonHelper.getClothingVoucherNumber;

import dtv.hardware.rcptbuilding.copyrules.AbstractRcptCopyRule;
import dtv.pos.tender.TenderHelper;
import dtv.util.StringUtils;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * To always save a Store Copy receipts when signature is captured <br>
 * <br>
 * Copyright (c) 2013 MICROS Retail
 * 
 * @author abalane
 * @created Apr 23, 2013
 * @version $Revision: 1085 $
 */
public class CriCapturedCustSignStoreRule
    extends AbstractRcptCopyRule {

  /** {@inheritDoc} */
  @Override
  protected boolean doesRuleApply(Object argSource) {
    if (!(argSource instanceof IPosTransaction)) {
      return false;
    }

    IPosTransaction tran = (IPosTransaction) argSource;

    if (!StringUtils.isEmpty(getClothingVoucherNumber(tran))) {
      return true;
    }

    for (ICreditDebitTenderLineItem lineItem : tran.getLineItems(ICreditDebitTenderLineItem.class)) {
      if (!lineItem.getVoid() && TenderHelper.getInstance().isSignatureRequired(lineItem)
          && (lineItem.getSignature() != null)
          && (!StringUtils.isEmpty(lineItem.getSignature().getSignature()))) {
        return true;
      }
    }
    return false;
  }

}
