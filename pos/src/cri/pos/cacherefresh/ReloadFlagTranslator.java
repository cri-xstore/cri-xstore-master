//$Id: ReloadFlagTranslator.java 119 2012-07-16 15:12:24Z suz.sxie $
package cri.pos.cacherefresh;

import static cri.pos.common.CriConstants.DATALOADER_PATH;
import static cri.pos.common.CriConstants.REFRESH_FLAG_FILE;

import java.io.*;
import java.util.Date;

import org.apache.log4j.Logger;

import dtv.data2.dataloader.fileprocessing.FileLine;
import dtv.data2.dataloader.valuetranslator.AbstractValueTranslator;

/**
 * Responsible to create RELOAD_CACHE.flg file<br>
 * <br>
 * Copyright (c) 2011 MICROS Retail
 * 
 * @author pgolli
 * @created May 12, 2011
 * @version $Revision: 119 $
 */
public class ReloadFlagTranslator
    extends AbstractValueTranslator {

  private final static Logger logger_ = Logger.getLogger(ReloadFlagTranslator.class);

  @Override
  public String translate(String argCurrentValue, FileLine argCurrentLine) {
    try {
      StringBuffer archivePathStringBuffer = new StringBuffer(System.getProperty(DATALOADER_PATH));

      File refrshFlagFile = new File(archivePathStringBuffer.toString() + REFRESH_FLAG_FILE);

      Writer w = new BufferedWriter(new FileWriter(refrshFlagFile));
      w.write(new Date().toString());
      w.close();
    }
    catch (Exception ex) {
      logger_.warn("Refresh Flag file not created.", ex);
    }

    logger_.info("Refresh Flag file created.");

    return "1";
  }

}
