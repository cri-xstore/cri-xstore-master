//$Id$
package cri.pos.register;

import dtv.pos.common.OpChainKey;
import dtv.pos.iframework.cmd.ITransactionCmd;
import dtv.pos.iframework.op.IOpChainKey;
import dtv.pos.register.CheckSaleCompleteOp;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 *
 * @author pgolli
 * @created Feb 6, 2014
 * @version $Revision$
 */
public class CriCheckSaleCompleteOp
    extends CheckSaleCompleteOp {

  /**
   * 
   */
  private static final long serialVersionUID = 1321784105567900365L;

  /**
   * Get the OpChain key for refunding a tender on a verified return.
   * @return IOpChainKey
   */
  @Override
  protected IOpChainKey getVerifiedRefundTenderOpChainKey(ITransactionCmd argCmd) {
    return OpChainKey.valueOf("REFUND_TENDER");
  }

  /**
   * Determine if this operation needs to call the verified return tender chain.
   * 
   * @param argCmd the current command.
   * @return <code>true</code> if this operation should call the chain, <code>false</code> if not.
   */
  @Override
  protected boolean shouldRunVerifiedReturnChain(ITransactionCmd argCmd) {

    return false;
  }

}
