//$Id: SalePriceHistorySearchResult.java 772 2014-03-18 09:33:33Z suz.dhu $
package cri.pos.register.returns;

import java.math.BigDecimal;

import dtv.pos.register.returns.customerhistory.ReturnableTransSearchResult;

/**
 * Query result for ULT_SALE_PRICE_HISTORY query. Wraps a pos transaction and the price of item
 * after deal.<br>
 * Copyright (c) 2014 MICROS Retail
 *
 * @author David.Hu
 * @created Mar 14, 2014
 * @version $Revision: 772 $
 */
public class SalePriceHistorySearchResult
    extends ReturnableTransSearchResult {

  private static final long serialVersionUID = 1L;

  private BigDecimal price;
  private long lineItemSequence;

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal argPrice) {
    price = argPrice;
  }

  public long getLineItemSequence() {
    return lineItemSequence;
  }

  public void setLineItemSequence(long argLineItemSequence) {
    lineItemSequence = argLineItemSequence;
  }

}
