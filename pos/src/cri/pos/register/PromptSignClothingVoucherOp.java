//$Id: PromptSignClothingVoucherOp.java 785 2014-05-23 01:40:51Z suz.bshi $
package cri.pos.register;

import static cri.pos.common.CriCommonHelper.getClothingVoucherNumber;

import dtv.pos.common.AbstractValidationOp;
import dtv.pos.iframework.cmd.ITransactionCmd;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.validation.IValidationData;
import dtv.pos.iframework.validation.IValidationKey;
import dtv.util.StringUtils;
import dtv.xst.dao.trn.IPosTransaction;

/**
 * Prompt to ensure the voucher is signed by the customer and the cashier.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Bob.Shi
 * @created May 21, 2014
 * @version $Revision: 785 $
 */
public class PromptSignClothingVoucherOp
    extends AbstractValidationOp {

  private static final long serialVersionUID = 7069741738405497200L;

  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    IPosTransaction trans = ((ITransactionCmd) argCmd).getTransaction();
    return !StringUtils.isEmpty(getClothingVoucherNumber(trans));
  }

  /** {@inheritDoc} */
  @Override
  public IValidationData getValidationData(IXstCommand argCmd, IXstEvent argEvent, IValidationKey argKey) {
    return null;
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    return HELPER.completeResponse();
  }

}
