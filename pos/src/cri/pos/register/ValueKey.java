//$Id: ValueKey.java 1101 2016-09-29 18:47:38Z olr.jgaughn $
package cri.pos.register;

import org.apache.commons.lang.builder.HashCodeBuilder;

import dtv.pos.iframework.op.IXstCommand;

/**
 * A value key is used to store and retrieve objects in a command's generic storage. This is useful
 * when the customer layer needs to store data on the command, but there is no built-in method on
 * the command's interface.<br>
 * <br>
 * When moving to a modern Xstore base version, this class should be deleted. There is a different
 * version in the newer bases that accomplishes the same task in the command-less framework.<br>
 * <br>
 * Copyright (c) 2016 MICROS Retail
 *
 * @author johgaug
 * @created Sep 29, 2016
 * @version $Revision: 1101 $
 */
public final class ValueKey<T> {

  private final String name;
  private final Class<T> type;

  /** Constructs a <code>ValueKey</code>. */
  public ValueKey(Class<T> argType, String argName) {
    type = argType;
    name = argName;
  }

  /** Get the value from the command. */
  public T get(IXstCommand argCmd) {
    return type.cast(argCmd.getValue(name));
  }

  /** Set the value on the command. */
  public void set(IXstCommand argCmd, T argValue) {
    argCmd.setValue(name, argValue);
  }

  /** {@inheritDoc} */
  @Override
  public boolean equals(Object argOther) {
    final boolean equal;
    if (this == argOther) {
      equal = true;
    }
    else if (!(argOther instanceof ValueKey)) {
      equal = false;
    }
    else {
      ValueKey<?> o = (ValueKey<?>) argOther;
      equal = (name.equals(o.name) && type.equals(o.type));
    }
    return equal;
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(name).append(getClass().getName()).toHashCode();
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    return getClass().getName() + "[" + type.getName() + "," + name + "]";
  }

}
