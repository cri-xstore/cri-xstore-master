//$Id: TaxPercentageIterator.java 6508 2012-12-10 18:34:42Z dtvdomain\anayar $
package cri.pos.register.tax.docbuilding;

import static dtv.util.NumberUtils.ZERO;
import static java.math.RoundingMode.HALF_UP;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

import org.apache.log4j.Logger;

import dtv.docbuilding.*;
import dtv.pos.register.tax.TaxHelper;
import dtv.util.ObjectUtils;
import dtv.xst.dao.tax.ITaxGroupRule;
import dtv.xst.dao.tax.ITaxRateRule;
import dtv.xst.dao.trl.*;

/**
 * Tax Iterator for Receipts<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author pgolli
 * @created May 28, 2014
 * @version $Revision$
 */
public class TaxPercentageIterator
    extends DocBuilderIterator {

  private static final Logger logger_ = Logger.getLogger(TaxPercentageIterator.class);

  protected String type_ = null;

  @Override
  protected void iterate(IPosDocument argDoc, IDocElementFactory argElementFactory, Object argObject)
      throws IOException {
    Object obj = argObject;
    // Assumptions: for any tax authority, the same percent applies to all items in this section
    //                                     (and we exclude zero tax items)

    Map<String, TaxInfoHolder> taxInfoMap = new HashMap<String, TaxInfoHolder>();

    if (obj instanceof IRetailTransaction) {
      IRetailTransaction trans = (IRetailTransaction) obj;
      obj = trans.getSaleLineItems();
    }

    if (obj instanceof ITaxLineItem) {
      setDBTaxPercentage((ITaxLineItem) obj);
      processTaxLine(taxInfoMap, (ITaxLineItem) obj);

    }
    else if (obj instanceof List) {
      processList(taxInfoMap, (List<?>) obj);
    }
    else {
      if (obj != null) {
        logger_.warn("expected " + IRetailTransaction.class.getName() + " but instead got "
            + ObjectUtils.getClassNameFromObject(obj));
      }
      return;
    }

    // Custom grouping of receipt tax lines by authority id
    // Note: percents of receipt lines can only be summed once lines have been summed into Tax Holders.
    Map<String, TaxInfoHolder> receiptLineMap = buildReceiptLineMap(taxInfoMap);

    // Custom ordering of grouped receipt tax lines
    // Note: can only be done after all groups summed
    //       which determines each group's final sorting key
    super.iterate(argDoc, argElementFactory, receiptLineMap.values());
  }

  private Map<String, TaxInfoHolder> buildReceiptLineMap(Map<String, TaxInfoHolder> taxInfoMap) {
    Map<String, TaxInfoHolder> receiptLineMap = new HashMap<String, TaxInfoHolder>();
    for (TaxInfoHolder taxInfoHolder : taxInfoMap.values()) {
      String receiptLineKey = taxInfoHolder.getAuthorityId();
      TaxInfoHolder receiptLineInfo = receiptLineMap.get(receiptLineKey);
      if (receiptLineInfo == null) {
        receiptLineMap.put(receiptLineKey, taxInfoHolder);
      }
      else {
        receiptLineInfo.addToReceiptLine(taxInfoHolder);
      }
    }
    return receiptLineMap;
  }

  private void setDBTaxPercentage(ITaxLineItem taxData) {
    BigDecimal percent = ZERO;
    ITaxRateRule[] rules =
        TaxHelper.getInstance().getRateRules().get(taxData.getSaleTaxGroupRule(), taxData.getTaxableAmount());
    for (ITaxRateRule rule : rules) {
      percent = percent.add(rule.getPercent());
    }
    taxData.setTaxPercentage(percent);
  }

  private void setDBTaxPercentage(ISaleTaxModifier taxData) {
    BigDecimal percent = ZERO;
    //we need to send the transaction total taxable so we can get the rule that have a amt minimum.
    ITaxRateRule[] rules = TaxHelper.getInstance().getRateRules().get(taxData.getSaleTaxGroupRule(),
        taxData.getTranTaxableAmt());
    for (ITaxRateRule rule : rules) {
      percent = percent.add(rule.getPercent());
    }
    taxData.setTaxPercentage(percent);
  }

  private void processList(Map<String, TaxInfoHolder> taxInfoMap, List<?> taxData) {

    for (Object o : taxData) {
      if (o instanceof ISaleReturnLineItem) {
        ISaleReturnLineItem sli = (ISaleReturnLineItem) o;

        if (!sli.getVoid()) {
          processSaleLine(taxInfoMap, sli);
        }
      }
    }
  }

  private void processSaleLine(Map<String, TaxInfoHolder> taxInfoMap, ISaleReturnLineItem taxData) {
    if (type_ != null) {
      if (type_.equalsIgnoreCase("RETURN")) {
        if (!taxData.getReturn()) {
          return;
        }
      }
      else if (type_.equalsIgnoreCase("SALE")) {
        if (taxData.getReturn()) {
          return;
        }
      }
      else {
        logger_.warn("Unexpected Type of " + type_);
      }
    }

    for (ISaleTaxModifier taxMod : taxData.getTaxModifiers()) {
      if (!taxMod.getVoid()) {
        setDBTaxPercentage(taxMod);
        processTaxLine(taxInfoMap, taxMod);
      }
    }
  }

  private void processTaxLine(Map<String, TaxInfoHolder> taxInfoMap, ITaxLineItem taxData) {
    if (logger_.isDebugEnabled()) {
      logger_.debug("Processing tax line for " + taxData.getAuthorityId() + " (" + taxData.getAuthorityName()
          + "), " + taxData.getTaxPercentage() + ", " + taxData.getTaxAmount());
    }
    if (!taxData.getVoid()) {
      processTaxLine(taxInfoMap, new TaxInfoHolder(taxData));
    }
  }

  private void processTaxLine(Map<String, TaxInfoHolder> taxInfoMap, ISaleTaxModifier taxData) {
    if (logger_.isDebugEnabled()) {
      logger_.debug("Processing tax mod for " + taxData.getAuthorityId() + " (" + taxData.getAuthorityName()
          + "), " + taxData.getTaxPercentage() + ", " + taxData.getTaxAmount());
    }
    if (!taxData.getVoid()) {
      processTaxLine(taxInfoMap, new TaxInfoHolder(taxData));
    }
  }

  private void processTaxLine(Map<String, TaxInfoHolder> taxInfoMap, TaxInfoHolder taxInfoHolder) {
    String taxKey = taxInfoHolder.getTaxHolderKey();
    TaxInfoHolder oldTaxInfoHolder = taxInfoMap.get(taxKey);
    if (oldTaxInfoHolder == null) {
      taxInfoMap.put(taxKey, taxInfoHolder);
    }
    else {
      oldTaxInfoHolder.addToHolder(taxInfoHolder);
    }
  }

  protected static class TaxInfoHolder {

    private final String authorityId_;
    private final String authorityDescription_;
    private BigDecimal taxAmount_;
    private BigDecimal percentage_;
    private boolean compound_;

    private String taxHolderKey_;
    private String receiptLineKey_;
    private String sortingKey_;
    private final String taxGroupRuleDescription_;

    private TaxInfoHolder(BigDecimal argTaxAmount, String argAuthorityId, String argAuthorityDescription,
        ITaxGroupRule argTaxGroupRule, BigDecimal argTaxPercentage) {
      taxAmount_ = argTaxAmount;
      authorityId_ = argAuthorityId;
      authorityDescription_ = argAuthorityDescription;

      compound_ = argTaxGroupRule.getCompound();
      taxGroupRuleDescription_ = argTaxGroupRule.getDescription();

      if ((argTaxPercentage != null) && (argTaxPercentage.compareTo(ZERO) == 0)) {
        percentage_ = ZERO; // only display percentage if available
      }
      else {
        percentage_ = argTaxPercentage;
      }

      taxHolderKey_ = null;
      receiptLineKey_ = null;
      sortingKey_ = null;

      if (logger_.isDebugEnabled()) {
        logger_.debug("Creating tax holder for " + authorityId_ + " (" + authorityDescription_ + "), "
            + percentage_ + ", " + taxAmount_ + ", " + compound_);
      }
    }

    public TaxInfoHolder(ITaxLineItem taxData) {
      this(taxData.getTaxAmount(), taxData.getAuthorityId(), taxData.getAuthorityName(),
          taxData.getSaleTaxGroupRule(), taxData.getTaxPercentage());
    }

    public TaxInfoHolder(ISaleTaxModifier taxData) {
      this(taxData.getTaxAmount(), taxData.getAuthorityId(), taxData.getAuthorityName(),
          taxData.getSaleTaxGroupRule(), taxData.getTaxPercentage());
    }

    public BigDecimal getTaxAmount() {
      return taxAmount_;
    }

    public String getAuthorityId() {
      return authorityId_;
    }

    public String getAuthorityDescription() {
      return authorityDescription_;
    }

    public String getTaxGroupRuleDescription() {
      return taxGroupRuleDescription_;
    }

    public String getDescription() {
      return (taxGroupRuleDescription_ != null ? taxGroupRuleDescription_ : authorityDescription_);
    }

    public String getPercentage() {
      // only display percentage if available
      return percentage_ == null ? ""
          : percentage_.scaleByPowerOfTen(2).setScale(3, HALF_UP).toString() + "%";
    }

    public boolean getCompound() {
      return compound_;
    }

    public String getTaxHolderKey() {
      if (taxHolderKey_ == null) {
        taxHolderKey_ = authorityId_ + "~" + authorityDescription_;
      }
      return taxHolderKey_;
    }

    public String getReceiptLineKey() {
      if (receiptLineKey_ == null) {
        receiptLineKey_ = authorityDescription_;
      }
      return receiptLineKey_;
    }

    public String getSortingKey() {
      if (sortingKey_ == null) {
        String sortingPrefix;
        if (compound_) {
          sortingPrefix = "1"; // puts compound in a top grouping
        }
        else {
          sortingPrefix = "5"; // a common middle grouping for everything else
        }
        String sortingType;
        // specific to J.Jill/Talbots, use the third character of the authority id
        // as a tax type identifier
        if (authorityId_.length() >= 3) {
          sortingType = authorityId_.substring(2, 3); // the tax type identifier
        }
        else {
          sortingType = "~"; // otherwise force at end
        }
        sortingKey_ = sortingPrefix + sortingType + "~" + authorityDescription_;
      }
      return sortingKey_;
    }

    public void addToHolder(TaxInfoHolder taxInfoHolder) {
      if (logger_.isDebugEnabled()) {
        logger_.debug("To tax holder of " + authorityId_ + " (" + authorityDescription_ + "), " + percentage_
            + ", " + taxAmount_ + ", " + compound_);
        logger_.debug("  add tax holder " + taxInfoHolder.authorityId_ + " ("
            + taxInfoHolder.authorityDescription_ + "), " + taxInfoHolder.percentage_ + ", "
            + taxInfoHolder.taxAmount_ + ", " + taxInfoHolder.compound_);
      }
      taxAmount_ = taxAmount_.add(taxInfoHolder.taxAmount_);
      // if percentages do not match, then we cannot logically display percentage to the user
      // normally percentages should match (but they may not for line tax overrides)
      if ((percentage_ != null) && (percentage_.compareTo(ZERO) != 0)
          && ((taxInfoHolder.percentage_ == null) || ((taxInfoHolder.percentage_.compareTo(percentage_) != 0)
              && (taxInfoHolder.percentage_.compareTo(ZERO) != 0)))) {
        percentage_ = null;
      }
      else if ((((percentage_ != null) && (percentage_.compareTo(ZERO) == 0))
          || ((taxInfoHolder.percentage_ != null) && (taxInfoHolder.percentage_.compareTo(ZERO) == 0)))
          && (percentage_.compareTo(taxInfoHolder.percentage_) == -1)) {
        BigDecimal temp = taxInfoHolder.percentage_;
        percentage_ = temp;
      }
      // won't normally be summing non-compound groupings with compound groupings
      // but just in case, force compound to be true (causing them to sort to top of list)
      if (taxInfoHolder.compound_) {
        compound_ = true;
      }
      if (logger_.isDebugEnabled()) {
        logger_.debug(" = tax holder of " + authorityId_ + " (" + authorityDescription_ + "), " + percentage_
            + ", " + taxAmount_ + ", " + compound_);
      }
    }

    public void addToReceiptLine(TaxInfoHolder taxInfoHolder) {
      // guarantee sorting keys have been set
      getSortingKey();
      taxInfoHolder.getSortingKey();
      if (logger_.isDebugEnabled()) {
        logger_.debug("To tax holder receipt line of " + sortingKey_ + ", " + percentage_ + ", " + taxAmount_
            + ", " + compound_);
        logger_.debug("  add tax holder receipt line " + taxInfoHolder.sortingKey_ + ", "
            + taxInfoHolder.percentage_ + ", " + taxInfoHolder.taxAmount_ + ", " + taxInfoHolder.compound_);
      }
      taxAmount_ = taxAmount_.add(taxInfoHolder.taxAmount_);
      // if any tax override (without percentage) added to tax line, don't display percentage
      if (percentage_ != null) {
        if (taxInfoHolder.percentage_ == null) {
          percentage_ = null;
        }
        else {
          percentage_ = percentage_.add(taxInfoHolder.percentage_);
        }
      }
      // use smallest sorting id in group for ordering
      if (sortingKey_.compareToIgnoreCase(taxInfoHolder.sortingKey_) > 0) {
        sortingKey_ = taxInfoHolder.sortingKey_;
      }
      // won't normally be summing non-compound groupings with compound groupings
      // but just in case, force compound to be true (causing them to sort to top of list)
      if (taxInfoHolder.compound_) {
        compound_ = true;
      }
      if (logger_.isDebugEnabled()) {
        logger_.debug(" = tax holder receipt line of " + sortingKey_ + ", " + percentage_ + ", " + taxAmount_
            + ", " + compound_);
      }
    }
  }

}
