//$Id: CriEditTaxExemptionOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.register.tax;

import static cri.pos.common.CriConstants.*;
import static dtv.pos.PosConstants.SELECTED_REASON_CODE;

import dtv.pos.iframework.form.IEditModel;
import dtv.pos.iframework.form.IEditModelCmd;
import dtv.pos.register.tax.EditTaxExemptionOp;
import dtv.pos.register.tax.TaxExemptionMaintenanceModel;
import dtv.xst.dao.com.IReasonCode;

/**
 * 427124 - Clothing Voucher Tender<br>
 * 
 * If casher selected the West Virginia Tax Exempt then certificate # should be default to
 * 55-6000771 and state is WV
 * 
 * <br>
 * Copyright (c) 2014 MICROS Retail
 *
 * @author Bob.Shi
 * @created May 19, 2014
 * @version $Revision: 1105 $
 */
public class CriEditTaxExemptionOp
    extends EditTaxExemptionOp {

  private static final long serialVersionUID = 1L;

  @Override
  protected IEditModel getModel(IEditModelCmd argCmd) {
    TaxExemptionMaintenanceModel model = (TaxExemptionMaintenanceModel) super.getModel(argCmd);
    IReasonCode taxExemptReason = (IReasonCode) argCmd.getValue(SELECTED_REASON_CODE);
    if (WEST_VIRGINIA_TAX_EXEMPT_REASON_CODE.equals(taxExemptReason.getReasonCode())) {
      model.setCertificateNbr(WEST_VIRGINIA_CERTIFICATE_NUMBER);
      model.setCertificateState(WEST_VIRGINIA_CERTIFICATE_STATE_CODE);
    }

    return model;
  }
}
