//$Id: CriConstants.java 1105 2017-01-11 20:41:13Z olr.jgaughn $

package cri.pos.common;

/**
 * Constants values used for CRI.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author Dani V Nedumpurath
 * @created July 13, 2012
 * @version $Revision: 1105 $
 */
public class CriConstants {

  public static final String DATALOADER_PATH = "cri.cacheRefresh.flagPath";
  public static final String REFRESH_FLAG_FILE = "/RELOAD_CACHE.flg";
  public static final String XSTORE_REFRESH_FLAG_FILE = "/RELOAD_XSTORE_CACHE.flg";

  public static final String WEST_VIRGINIA_TAX_EXEMPT_REASON_CODE = "TE13";
  public static final String WEST_VIRGINIA_CERTIFICATE_NUMBER = "55-6000771";
  public static final String WEST_VIRGINIA_CERTIFICATE_STATE_CODE = "WV";
  public static final String CLOTHING_VOUCHER_NUMBER = "CLOTHING_VOUCHER_NUMBER";
  public static final String CLOTHING_VOUCHER_AMOUNT = "CLOTHING_VOUCHER_AMOUNT";
  public static final String CLOTHING_VOUCHER = "CLOTHING_VOUCHER";

  public static final String CRI_STOREDOWN_ENTRYMODE = "STOREDOWN_ENTRYMODE";

  private CriConstants() {
    // prevent instantiation
  }

}
