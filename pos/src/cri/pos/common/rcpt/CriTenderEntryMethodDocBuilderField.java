//$Id: CriTenderEntryMethodDocBuilderField.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.common.rcpt;

import cri.ajb.fipay.impl.DebitCreditEntryMethodCode;

import dtv.docbuilding.AbstractDocBuilderField;
import dtv.docbuilding.IDocElementFactory;
import dtv.docbuilding.types.DocBuilderAlignmentType;
import dtv.hardware.types.HardwareFamilyType;
import dtv.i18n.FormattableFactory;
import dtv.i18n.OutputContextType;
import dtv.i18n.formatter.output.IOutputFormatter;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2016 OLR Retail
 *
 * @author johgaug
 * @created Jul 8, 2016
 * @version $Revision: 1105 $
 */
public class CriTenderEntryMethodDocBuilderField
    extends AbstractDocBuilderField {

  private static final FormattableFactory FF = FormattableFactory.getInstance();
  private final String SWIPED = "_entryMethodSwiped";
  private final String KEYED = "_entryMethodKeyed";
  private final String SCANNED = "_entryMethodScanned";
  private final String CHIP = "_entryMethodChip";
  private final String CONTACTLESS = "_entryMethodContactless";

  public CriTenderEntryMethodDocBuilderField(String argContents, String argStyle, Integer argLocation,
      DocBuilderAlignmentType argAlignment, int argPriority, IOutputFormatter argFormatter) {
    super(argContents, argStyle, argLocation, argAlignment, argPriority, argFormatter);
  }

  /** {@inheritDoc} */
  @Override
  public String getContents(Object argSource, IDocElementFactory argFactory) {
    String result = "";
    if (argSource instanceof ICreditDebitTenderLineItem) {
      ICreditDebitTenderLineItem ccLine = (ICreditDebitTenderLineItem) argSource;
      if (isSwiped(ccLine)) {
        result = FF.getTranslatable(SWIPED).toString(OutputContextType.RECEIPT);
      }
      else if (isScanned(ccLine)) {
        result = FF.getTranslatable(SCANNED).toString(OutputContextType.RECEIPT);
      }
      else if (isChipRead(ccLine)) {
        result = FF.getTranslatable(CHIP).toString(OutputContextType.RECEIPT);
      }
      else if (isRfiRead(ccLine)) {
        result = FF.getTranslatable(CONTACTLESS).toString(OutputContextType.RECEIPT);
      }
      else {
        result = FF.getTranslatable(KEYED).toString(OutputContextType.RECEIPT);
      }
    }
    return result;
  }

  private boolean isChipRead(ICreditDebitTenderLineItem argLineItem) {
    return (argLineItem.getEntryMethodCode() != null)
        && argLineItem.getEntryMethodCode().equals(DebitCreditEntryMethodCode._CEM_INSERT.getMapping());
  }

  private boolean isRfiRead(ICreditDebitTenderLineItem argLineItem) {
    return (argLineItem.getEntryMethodCode() != null)
        && argLineItem.getEntryMethodCode().equals(DebitCreditEntryMethodCode._CONTACTLESS.getMapping());
  }

  private boolean isScanned(ICreditDebitTenderLineItem argLineItem) {
    return (argLineItem.getEntryMethodCode() != null)
        && argLineItem.getEntryMethodCode().startsWith(HardwareFamilyType.SCANNER.getName());
  }

  private boolean isSwiped(ICreditDebitTenderLineItem argLineItem) {
    if ((argLineItem.getEntryMethodCode() != null)
        && argLineItem.getEntryMethodCode().equals(DebitCreditEntryMethodCode._CEM_SWIPED.getMapping())) {
      return true;
    }
    return false;
  }

}
