// $Id: CriCustGroupRefreshStrategy.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.pricing;

import java.util.*;

import dtv.data2.access.DataFactory;
import dtv.pos.pricing.CustGroupRefreshStrategy;
import dtv.util.DateUtils;
import dtv.xst.dao.prc.IDeal;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2013 MICROS Retail
 *
 * @author shy xie
 * @created May 14, 2013
 * @version $Revision: 1105 $
 */
public class CriCustGroupRefreshStrategy
    extends CustGroupRefreshStrategy {

  /** {@inheritDoc} */
  @Override
  protected List<IDeal> loadSingles() {
    List<IDeal> result = new ArrayList<IDeal>();

    // Retrieve the deals which have no grouping associated with them (which
    // do not exclude other deals by their application)
    TreeMap<String, Object> params = new TreeMap<String, Object>();
    params.put(LOAD_DEALS_QUERY_ARG_DEFERRED, Boolean.FALSE);
    // do not load the expired deal in to cache
    params.put("argBusinessDate", DateUtils.getNewDate());

    List<IDeal> s = DataFactory.getObjectByQueryNoThrow(LOAD_DEALS_QUERY, params);
    result.addAll(s);

    return result;
  }
}
