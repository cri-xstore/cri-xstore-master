//$Id: GetIsdKeyManagementOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.isd.km;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import cri.tenderauth.impl.isd.km.IsdKMResponse;
import imsformat.km.KMResponse;

import dtv.data2.access.*;
import dtv.hardware.events.IHardwareInputEvent;
import dtv.pos.assistance.AssistanceHelper;
import dtv.pos.common.ConfigurationMgr;
import dtv.pos.common.PersistablesBag;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.*;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.event.AuthEventType;
import dtv.util.DateUtils;
import dtv.util.crypto.*;
import dtv.xst.dao.com.ICodeValue;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Sep 20, 2012
 * @version $Revision: 1105 $
 */
public class GetIsdKeyManagementOp
    extends Operation
    implements IXstEventObserver {

  private static final long serialVersionUID = 1L;
  private final static Logger logger_ = Logger.getLogger(GetIsdKeyManagementOp.class);
  protected static final IXstEventType[] EVENTS = new IXstEventType[] {AuthEventType.RESPONSE};
  protected static final Class<?>[] EVENT_INTERFACES = new Class<?>[] {IHardwareInputEvent.class};

  IQueryKey<ICodeValue> ISD_KM_KEY = new QueryKey<ICodeValue>("CRI_ISD_KM_KEY", ICodeValue.class);
  private static final String ISD_COM_CODE_CATEGORY = "ISD_KM";

  // IQueryKey<ICodeValue> ISD_KM_CURR_KEY = new QueryKey<ICodeValue>("CRI_ISD_KM_CURR_KEY", ICodeValue.class);

  private final String cipherName_ = "config";

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCommand, IXstEvent argEvent) {

    // handle training mode up front so we don't use the live processors
    if (AssistanceHelper.getInstance().isTrainingMode()) {
      return HELPER.completeResponse();
    }

    // handle any responses from the auth process
    if (argEvent instanceof IAuthResponse) {
      handleAuthResponse((IAuthResponse) argEvent);
      return HELPER.completeResponse();
      //return handleAuthResponse(cmd, (IAuthResponse) argEvent);
    }

    IAuthProcess process = AuthFactory.getInstance().getAuthProcess("ISD_KM_MESSAGE");
    IAuthRequest authRequest = AuthFactory.getInstance().makeAuthRequest("ISD_KM_MESSAGE",
        AuthRequestType.forName("GET_KEY"), null, true);
    // when no appropriate authorization request exists then consider the op complete.
    if (authRequest == null) {
      return HELPER.completeResponse();
    }
    process.processRequest(authRequest);
    return HELPER.waitResponse();
  }

  /**
   * @param argResponse
   */
  private void handleAuthResponse(IAuthResponse argResponse) {
    ICodeValue keyId = getKMKeyByCode(KMResponse.KEY_ID);
    ICodeValue currKey = getKMKeyByCode(KMResponse.CURR_KEY);

    ISDKeyManagementHelper keyMgr = ISDKeyManagementHelper.getInstance();

    if ((keyId != null) && (currKey != null)) {
      //Setting the keys from the database
      keyMgr.setKeyId(decrypt(keyId.getDescription()));
      keyMgr.setCurrKeyValue(decrypt(currKey.getDescription()));
    }

    if (!(argResponse instanceof IsdKMResponse) || !((IsdKMResponse) argResponse).isSuccess()) {
      return; //failed to get the keys from ISD
    }

    IsdKMResponse response = (IsdKMResponse) argResponse;
    /* if (response.getKeyIdentifier().equalsIgnoreCase(keyMgr.getKeyId())
        && response.getCurrentActiveKeyValue().equalsIgnoreCase(keyMgr.getCurrKeyValue())) {
      return; //Already have the latest key no need to persist in the DB
    }*/

    //Setting news keys obtained from KMResponse
    keyMgr.setKeyId(response.getKeyIdentifier());
    keyMgr.setCurrKeyValue(response.getCurrentActiveKeyValue());

    PersistablesBag changeObjects = new PersistablesBag();

    //Persisting keys to the database
    if (keyId != null) {
      keyId.setDescription(encrypt(response.getKeyIdentifier()));
      keyId.setUpdateUserId("BaseData");
      keyId.setUpdateDate(DateUtils.getNewDate());
      keyId.setHidden(true);
      changeObjects.addObject(keyId);
    }
    else {
      ICodeValue newKeyId = DataFactory.createObject(ICodeValue.class);
      newKeyId.setOrganizationId(ConfigurationMgr.getOrganizationId());
      newKeyId.setCategory(ISD_COM_CODE_CATEGORY);
      newKeyId.setCode(KMResponse.KEY_ID);
      newKeyId.setDescription(encrypt(response.getKeyIdentifier()));
      newKeyId.setCreateUserId("BaseData");
      newKeyId.setCreateDate(DateUtils.getNewDate());
      newKeyId.setHidden(true);
      changeObjects.addObject(newKeyId);
    }

    if (currKey != null) {
      currKey.setDescription(encrypt(response.getCurrentActiveKeyValue()));
      currKey.setUpdateUserId("BaseData");
      currKey.setUpdateDate(DateUtils.getNewDate());
      currKey.setHidden(true);
      changeObjects.addObject(currKey);
    }
    else {
      ICodeValue newKeyValue = DataFactory.createObject(ICodeValue.class);
      newKeyValue.setOrganizationId(ConfigurationMgr.getOrganizationId());
      newKeyValue.setCategory(ISD_COM_CODE_CATEGORY);
      newKeyValue.setCode(KMResponse.CURR_KEY);
      newKeyValue.setDescription(encrypt(response.getCurrentActiveKeyValue()));
      newKeyValue.setCreateUserId("BaseData");
      newKeyValue.setCreateDate(DateUtils.getNewDate());
      newKeyValue.setHidden(true);
      changeObjects.addObject(newKeyValue);
    }

    changeObjects.persist();

  }

  /** {@inheritDoc} */
  @Override
  public IXstEventType[] getObservedEvents() {
    return EVENTS;
  }

  /** {@inheritDoc} */
  @Override
  public Class<?>[] getObservedEventInterfaces() {
    return EVENT_INTERFACES;
  }

  protected ICodeValue getKMKeyByCode(String code) {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("argCategory", ISD_COM_CODE_CATEGORY);
    params.put("argCode", code);

    IQueryResultList<ICodeValue> list = DataFactory.getObjectByQueryNoThrow(ISD_KM_KEY, params);

    if ((list != null) && !list.isEmpty()) {
      ICodeValue record = (ICodeValue) (list.toArray()[0]);
      return record;
    }
    else {
      return null;
    }
  }

  private String decrypt(String argEncryptedValue) {
    DtvDecrypter dc;
    try {
      logger_.debug("argCipherName " + cipherName_);
      dc = (DtvDecrypter) DtvDecrypter.getInstance(cipherName_);
      String decryptedValue = dc.decrypt(argEncryptedValue);
      return decryptedValue;
    }
    catch (SecretKeyCipherStoreException e) {
      logger_.error("Error decrypting   : " + e.getMessage());
    }
    catch (Exception e) {
      logger_.error("Error decrypting   : " + e.getMessage());
    }
    return "NotEvaluated";
  }

  private String encrypt(String argValue) {
    DtvEncrypter de;
    try {
      logger_.debug("argCipherName " + cipherName_);
      de = (DtvEncrypter) DtvEncrypter.getInstance(cipherName_);
      String encyptedValue = de.encrypt(argValue);
      return encyptedValue;
    }
    catch (SecretKeyCipherStoreException e) {
      logger_.error("Error encrypting   : " + e.getMessage());
    }
    return "NotEvaluated";
  }

}
