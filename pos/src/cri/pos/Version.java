//$Id: Version.java 1264 2018-10-12 20:03:44Z sorpete $
package cri.pos;

import java.util.Date;

import dtv.util.DateUtils;

/**
 * A class which reports version information for the base customer overlay.<br>
 * <br>
 * Copyright (c) 2007 Datavantage Corporation
 * 
 * @author jweiss
 * @created Jun 1, 2007
 * @version $Revision: 1264 $
 */
public class Version
    extends dtv.pos.Version {

  private static final String CUSTOMER_VERSION = "16.1.2";
  private static final String PATCH_VERSION = "0.0";
  private static final String BUILD_DATE = "2018-10-01T14:30:00-0400";

  /** {@inheritDoc} */
  @Override
  protected Date getCustomerBuildDateImpl() {
    return DateUtils.parseIso(BUILD_DATE);
  }

  /** {@inheritDoc} */
  @Override
  protected String getCustomerVersionImpl() {
    return CUSTOMER_VERSION;
  }

  /** {@inheritDoc} */
  @Override
  protected String getPatchVersionImpl() {
    return PATCH_VERSION;
  }
}
