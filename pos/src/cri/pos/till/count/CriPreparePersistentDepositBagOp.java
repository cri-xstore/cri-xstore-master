//$Id: CriPreparePersistentDepositBagOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;

/**
 * Put the selected count bag items to the persistent queue.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 26, 2012
 * @version $Revision: 1105 $
 */
public class CriPreparePersistentDepositBagOp
    extends Operation {
  /**
   * 
   */
  private static final long serialVersionUID = -1808979136252467817L;

  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    for (CriBagSummaryItem countItem : cmd.getSelectedBagItems()) {
      cmd.addPersistable(countItem.getCountBag());
    }

    return HELPER.completeResponse();
  }
}
