//$Id: CriPromptStoreSafeEndCountOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import dtv.pos.till.count.PromptTillCountOp;
import dtv.pos.till.types.TenderControlTransTypeCode;

/**
 * Prompt to the store safe end count.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author shy xie
 * @created Jul 23, 2012
 * @version $Revision: 1105 $
 */
public class CriPromptStoreSafeEndCountOp
    extends PromptTillCountOp {

  /**
   * 
   */
  private static final long serialVersionUID = -1180938627521155433L;

  /** {@inheritDoc} */
  @Override
  protected TenderControlTransTypeCode getTenderCountTransactionType() {
    return TenderControlTransTypeCode.BANK_DEPOSIT;
  }
}
