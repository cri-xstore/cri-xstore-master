//$Id: CriDepositBagEditModel.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import static dtv.pos.iframework.form.IEditModelFieldMetadata.ATTR_NEW;

import java.math.BigDecimal;

import cri.pos.till.CriTillHelper;

import dtv.pos.framework.form.BasicEditModel;
import dtv.pos.framework.form.EditModelField;
import dtv.pos.iframework.form.ICardinality;
import dtv.pos.tender.TenderHelper;
import dtv.pos.till.count.CountSummaryItem;
import dtv.pos.till.count.TillCountModel;
import dtv.util.NumberUtils;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.tsn.ISession;

/**
 * Deposit bag edit model.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author shy xie
 * @created Jul 23, 2012
 * @version $Revision: 1105 $
 */
public class CriDepositBagEditModel
    extends BasicEditModel {
  private TillCountModel countModel_;
  private String cashBagNumber_;
  private String checkBagNumber_;
  private BigDecimal totalCashAmount_;
  private BigDecimal totalCheckAmount_;
  private String bagTypeCash_;
  private String bagTypeCheck_;

  public static final String CASH_BAG_NUMBER = "cashBagNumber";
  public static final String CHECK_BAG_NUMBER = "checkBagNumber";
  public static final String TOTAL_CASH_AMOUNT = "totalCashAmount";
  public static final String TOTAL_CHECK_AMOUNT = "totalCheckAmount";
  public static final String BAG_TYPE_CASH = "bagTypeCash";
  public static final String BAG_TYPE_CHECK = "bagTypeCheck";

  protected CriTillHelper TILL_HELPER = CriTillHelper.getInstance();

  /**
   * Constructs a <code>CriDepositBagEditModel</code>.
   * @param argCountModel
   */
  public CriDepositBagEditModel(TillCountModel argCountModel) {
    super(FF.getTranslatable("_criDepositBagFormTitle"), FF.getTranslatable("_criDepositBagFormMsg"));
    bagTypeCash_ = CriBagTypes.CASH.getDescription();
    bagTypeCheck_ = CriBagTypes.CHECKS.getDescription();
    countModel_ = argCountModel;
    totalCashAmount_ = getCashDepositAmount(argCountModel);
    totalCheckAmount_ = getCheckDepositAmount(argCountModel);
    addFields();
    initializeFieldState();
  }

  /**
   * 
   * @param argCountModel
   * @return
   */
  public BigDecimal getCashDepositAmount(TillCountModel argCountModel) {
    BigDecimal registerFloatAmount = NumberUtils.nonNull(TILL_HELPER.getTotalRegisterCashRemaining());
    ISession storeSafeSession = argCountModel.getCurrentTillCountSession();
    BigDecimal storeSafeFloatAmount =
        NumberUtils.nonNull(storeSafeSession.getTenderRepository().getDefaultClosingCashAmt());
    //    BigDecimal totalFloatAmount = registerFloatAmount.add(storeSafeFloatAmount);
    BigDecimal totalStoreSafeAmount = getTotalCashCountAmount(argCountModel).add(registerFloatAmount);

    return totalStoreSafeAmount.subtract(storeSafeFloatAmount);
  }

  /**
   * 
   * @param argCountModel
   * @return
   */
  protected BigDecimal getTotalCashCountAmount(TillCountModel argCountModel) {
    BigDecimal totalCountAmount = BigDecimal.ZERO;
    for (CountSummaryItem countItem : argCountModel.getCountSummaryList()) {
      ITender tender = (ITender) countItem.getCountSummaryObject();
      if (TenderHelper.getInstance().getLocalCurrencyTenderId().equals(tender.getTenderId())) {
        totalCountAmount = totalCountAmount.add(countItem.getTotalDeclaredAmount());
      }
    }

    return totalCountAmount;
  }

  /**
   * 
   * @param argCountModel
   * @return
   */
  public BigDecimal getCheckDepositAmount(TillCountModel argCountModel) {
    return getTotalCheckCountAmount(argCountModel);
  }

  /**
   * 
   * @param argCountModel
   * @return
   */
  protected BigDecimal getTotalCheckCountAmount(TillCountModel argCountModel) {
    BigDecimal totalCountAmount = BigDecimal.ZERO;
    for (CountSummaryItem countItem : argCountModel.getCountSummaryList()) {
      ITender tender = (ITender) countItem.getCountSummaryObject();
      if (TILL_HELPER.isCheckTender(tender)) {
        totalCountAmount = totalCountAmount.add(countItem.getTotalDeclaredAmount());
      }
    }

    return totalCountAmount;
  }

  /**
   * 
   */
  protected void addFields() {
    if (NumberUtils.isPositive(getCashDepositAmount(getCountModel()))) {
      addField(CASH_BAG_NUMBER, String.class);
    }
    else {
      addField(EditModelField.makeFieldDef(this, CASH_BAG_NUMBER, String.class, ATTR_NEW,
          ICardinality.NOT_AVAILABLE));
    }
    if (NumberUtils.isPositive(getCheckDepositAmount(getCountModel()))) {
      addField(CHECK_BAG_NUMBER, String.class);
    }
    else {
      addField(EditModelField.makeFieldDef(this, CHECK_BAG_NUMBER, String.class, ATTR_NEW,
          ICardinality.NOT_AVAILABLE));
    }

    addField(TOTAL_CASH_AMOUNT, BigDecimal.class);
    addField(TOTAL_CHECK_AMOUNT, BigDecimal.class);
    addField(BAG_TYPE_CASH, String.class);
    addField(BAG_TYPE_CHECK, String.class);
  }

  /**
   * 
   * @return
   */
  public TillCountModel getCountModel() {
    return countModel_;
  }

  /**
   * 
   * @param argCountModel
   */
  public void setCountModel(TillCountModel argCountModel) {
    countModel_ = argCountModel;
  }

  /**
   * 
   * @return
   */
  public String getCashBagNumber() {
    return cashBagNumber_;
  }

  /**
   * 
   * @param argCashBagNumber
   */
  public void setCashBagNumber(String argCashBagNumber) {
    cashBagNumber_ = argCashBagNumber;
  }

  /**
   * 
   * @return
   */
  public String getCheckBagNumber() {
    return checkBagNumber_;
  }

  /**
   * 
   * @param argCheckBagNumber
   */
  public void setCheckBagNumber(String argCheckBagNumber) {
    checkBagNumber_ = argCheckBagNumber;
  }

  /**
   * 
   * @return
   */
  public BigDecimal getTotalCashAmount() {
    return NumberUtils.isNegative(totalCashAmount_) ? BigDecimal.ZERO : totalCashAmount_;
  }

  /**
   * 
   * @param argTotalCashAmount
   */
  public void setTotalCashAmount(BigDecimal argTotalCashAmount) {
    totalCashAmount_ = argTotalCashAmount;
  }

  /**
   * 
   * @return
   */
  public BigDecimal getTotalCheckAmount() {
    return NumberUtils.isNegative(totalCheckAmount_) ? BigDecimal.ZERO : totalCheckAmount_;
  }

  public void setTotalCheckAmount(BigDecimal argTotalCheckAmount) {
    totalCheckAmount_ = argTotalCheckAmount;
  }

  /**
   * 
   * @return
   */
  public String getBagTypeCash() {
    return bagTypeCash_;
  }

  /**
   * 
   * @param argBagTypeCash
   */
  public void setBagTypeCash(String argBagTypeCash) {
    bagTypeCash_ = argBagTypeCash;
  }

  /**
   * 
   * @return
   */
  public String getBagTypeCheck() {
    return bagTypeCheck_;
  }

  /**
   * 
   * @param argBagTypeCheck
   */
  public void setBagTypeCheck(String argBagTypeCheck) {
    bagTypeCheck_ = argBagTypeCheck;
  }
}
