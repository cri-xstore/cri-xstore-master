//$Id: CriBagTypes.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import dtv.pos.iframework.type.AbstractCodeEnum;

/**
 * Store money deposit bag types.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author shy xie
 * @created Jul 23, 2012
 * @version $Revision: 1105 $
 */
public class CriBagTypes
    extends AbstractCodeEnum<CriBagStatus> {

  private static final Logger logger_ = Logger.getLogger(CriBagStatus.class);
  private static Map<String, CriBagTypes> _values;
  private static CriBagStatus[] _instances;

  public static final CriBagTypes CASH = new CriBagTypes("CASH");
  public static final CriBagTypes CHECKS = new CriBagTypes("CHECKS");

  /**
   * Get an array of all values of this enumeration.
   * 
   * @return an array of all values of this enumeration
   */
  public static CriBagStatus[] getInstances() {
    if (_instances == null) {
      _instances = _values.values().toArray(new CriBagStatus[0]);
    }

    return _instances;
  }

  /**
   * Get the CriBagTypes by name.
   * 
   * @param argName
   * @return
   */
  public static CriBagTypes forName(String argName) {
    if (argName == null) {
      return null;
    }

    CriBagTypes found = _values.get(argName.trim().toUpperCase());

    if (found == null) {
      logger_.warn("There is no instance of [" + CriBagStatus.class.getName() + "] named [" + argName + "].");
    }

    return found;
  }

  /**
   * Constructs a <code>CriBagTypes</code>.
   * @param argName
   */
  private CriBagTypes(String argName) {
    super(CriBagTypes.class, argName);

    if (_values == null) {
      _values = new HashMap<String, CriBagTypes>();
    }

    _values.put(getCode(), this);
  }

}
