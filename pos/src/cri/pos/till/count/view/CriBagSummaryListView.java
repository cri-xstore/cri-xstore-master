//$Id: CriBagSummaryListView.java 1105 2017-01-11 20:41:13Z olr.jgaughn $

package cri.pos.till.count.view;

import javax.swing.JComponent;

import cri.pos.till.count.CriBagSummaryModel;

import dtv.pos.common.ViewElementType;
import dtv.pos.framework.form.component.AbstractCustomFormComponent;
import dtv.pos.framework.ui.component.XstList;
import dtv.pos.framework.ui.component.XstViewComponentFactory;
import dtv.pos.iframework.ui.IViewElementType;
import dtv.pos.iframework.ui.RendererDef;
import dtv.pos.iframework.ui.model.IFormModel;
import dtv.pos.till.count.view.CountableView;
import dtv.ui.UIServices;

/**
 * A view for the deposit bag summary<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 15, 2012
 * @version $Revision: 1105 $
 * @param <T>
 */
public class CriBagSummaryListView<T extends IFormModel>
    extends AbstractCustomFormComponent<T> {

  private final CountableView<T> countableView_;
  final XstList bagList_;
  private CriBagSummaryModel summaryModel_;

  /**
   * Constructs a <code>CriBagSummaryListView</code>.
   */
  public CriBagSummaryListView() {
    super();
    bagList_ = XstViewComponentFactory.getInstance().createList();
    countableView_ = new CountableView<T>(this);
    countableView_.setContent(bagList_);
  }

  /**
   * Returns the actual object meant to be added to another GUI container as part of a larger view
   * layout. Often, this component may simply be the custom component instance itself. For some
   * components meant to exist within a managing container (e.g. text areas may exist within a
   * scroll pane), this component may be that container.
   * 
   * @return the GUI component meant for inclusion as part of a larger view
   */
  @Override
  public JComponent getDisplayComponent() {
    return countableView_.getDisplayComponent();
  }

  /**
   * Returns the component within this component wrapper that should receive focus when the
   * non-visual wrapper either requests or is assigned focus.
   * 
   * @return the component within this component wrapper to which focus requests should be directed
   */
  @Override
  public JComponent getFocusComponent() {
    return bagList_.getFocusComponent();
  }

  /**
   * Assigns the specified form model to this view.
   * @param model IModel the new form model for this view
   */
  @Override
  public void setModel(T model) {
    summaryModel_ = (model != null) ? (CriBagSummaryModel) model.getEditModel() : null;
    bagList_.setModel(summaryModel_);
    update();
  }

  /**
   * Returns the list element type used to determine the display and formatting of the countable
   * tenders.
   */
  protected IViewElementType getListViewElementType() {
    return ViewElementType.valueOf("CRI.BAGSUMMARY_LIST_DETAILED");
  }

  /**
   * Updates this view in response to the assignment of a new model or a meaningful change to the
   * existing one.
   */
  private void update() {
    UIServices.invoke(new Runnable() {
      /** {@inheritDoc} */
      @Override
      public void run() {
        RendererDef def = new RendererDef(getListViewElementType());
        bagList_.setColumnHeaderRendererDef(def);
        bagList_.setCellRendererDef(def);
      }
    }, true, false);
  }
}
