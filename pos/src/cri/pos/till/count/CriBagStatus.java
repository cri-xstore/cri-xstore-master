//$Id: CriBagStatus.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import dtv.pos.iframework.type.AbstractCodeEnum;

/**
 * Deposit bag status.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 15, 2012
 * @version $Revision: 1105 $
 */
public class CriBagStatus
    extends AbstractCodeEnum<CriBagStatus> {

  private static final Logger logger_ = Logger.getLogger(CriBagStatus.class);
  private static Map<String, CriBagStatus> _values;
  private static CriBagStatus[] _instances;

  public static final CriBagStatus CONVEYED = new CriBagStatus("CONVEYED");
  public static final CriBagStatus AVAILABLE = new CriBagStatus("AVAILABLE");

  /**
   * Get an array of all values of this enumeration.
   * 
   * @return an array of all values of this enumeration
   */
  public static CriBagStatus[] getInstances() {
    if (_instances == null) {
      _instances = _values.values().toArray(new CriBagStatus[0]);
    }

    return _instances;
  }

  /**
   * Get the CriBagStatus by name.
   * 
   * @param argName
   * @return
   */
  public static CriBagStatus forName(String argName) {
    if (argName == null) {
      return null;
    }

    CriBagStatus found = _values.get(argName.trim().toUpperCase());

    if (found == null) {
      logger_.warn("There is no instance of [" + CriBagStatus.class.getName() + "] named [" + argName + "].");
    }

    return found;
  }

  /**
   * Constructs a <code>CriBagStatus</code>.
   * @param argName
   */
  private CriBagStatus(String argName) {
    super(CriBagStatus.class, argName);

    if (_values == null) {
      _values = new HashMap<String, CriBagStatus>();
    }

    _values.put(getCode(), this);
  }

}
