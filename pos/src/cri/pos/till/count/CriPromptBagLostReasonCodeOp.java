//$Id: CriPromptBagLostReasonCodeOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import dtv.pos.common.PromptKey;
import dtv.pos.common.op.AbstractPromptReasonCodeOp;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.xst.dao.com.IReasonCode;

/**
 * Prompt the deposit bag lost reason code.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Sep 26, 2012
 * @version $Revision: 1105 $
 */
public class CriPromptBagLostReasonCodeOp
    extends AbstractPromptReasonCodeOp {

  /**
   * 
   */
  private static final long serialVersionUID = -8872553064937521923L;

  public CriPromptBagLostReasonCodeOp() {
    super("BAG_LOST");
  }

  @Override
  protected IPromptKey getCommentPromptKey(IXstCommand argCmd) {
    return PromptKey.valueOf("CRI.BAG_LOST_REASON_COMMENT");
  }

  @Override
  protected void setComment(IXstCommand argCmd, String comment) {
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    if (cmd.getSelectedBagItems() != null) {
      for (CriBagSummaryItem item : cmd.getSelectedBagItems()) {
        item.getCountBag().setLostComments(comment);
      }
    }
  }

  @Override
  protected void setSelectedCode(IXstCommand argCmd, IReasonCode reason) {
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    if ((cmd.getSelectedBagItems() != null) && (reason != null)) {
      for (CriBagSummaryItem item : cmd.getSelectedBagItems()) {
        item.setLostReasonCode(reason.getReasonCode());
      }
    }
  }

  @Override
  protected IPromptKey getReasonCodePromptKey(IXstCommand argCmd) {
    return PromptKey.valueOf("CRI.REASON_CODE_LIST");
  }
}
