//$Id: CriDepositBagConveyanceOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import cri.xst.dao.tsn.ICriDepositBag;

import dtv.pos.framework.op.Operation;
import dtv.pos.framework.security.SecurityMgr;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.storecalendar.StoreCalendar;
import dtv.pos.till.TillHelper;
import dtv.xst.dao.crm.IParty;
import dtv.xst.dao.tsn.ITenderControlTransaction;

/**
 * Convey the store deposit bag.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author shy xie
 * @created Jul 29, 2012
 * @version $Revision: 1105 $
 */
public class CriDepositBagConveyanceOp
    extends Operation {

  /**
   * 
   */
  private static final long serialVersionUID = 8456064626639455385L;

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    ICriDepositBagCmd cmd = (ICriDepositBagCmd) argCmd;
    return !cmd.getSelectedDepositItems().isEmpty();
  }

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    ICriDepositBagCmd cmd = (ICriDepositBagCmd) argCmd;
    IParty currentEmployee = SecurityMgr.getCurrentUser().getOperatorParty();

    ITenderControlTransaction trans = TillHelper.getInstance().createTenderControlTransaction();
    trans.setTransactionTypeCode("CONVEYED_BAGS");
    trans.setTransactionStatusCode("COMPLETE");
    trans.setTypeCode("DEPOSIT_BAG_CONVEYANCE");
    trans.setOperatorParty(currentEmployee);

    for (ICriDepositBag bag : cmd.getSelectedDepositItems()) {
      bag.setStatus(CriBagStatus.CONVEYED.getCode());
      bag.setConveyanceDateTime(StoreCalendar.getBusinessDateTimeStamp());
      bag.setLastChangeEmployeeId(currentEmployee.getEmployeeId());
      bag.setBusinessDate(StoreCalendar.getCurrentBusinessDate());
    }

    cmd.addPersistable(cmd.getSelectedDepositItems());
    cmd.setTransaction(trans);
    return HELPER.completeResponse();
  }
}
