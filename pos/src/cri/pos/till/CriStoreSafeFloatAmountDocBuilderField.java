// $Id: CriStoreSafeFloatAmountDocBuilderField.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till;

import java.math.BigDecimal;

import dtv.docbuilding.AbstractDocBuilderField;
import dtv.docbuilding.IDocElementFactory;
import dtv.docbuilding.types.DocBuilderAlignmentType;
import dtv.i18n.formatter.output.IOutputFormatter;
import dtv.pos.till.TillHelper;
import dtv.xst.dao.tsn.ITenderControlTransaction;

/**
 * Store sale float amount log builder field.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Sep 29, 2012
 * @version $Revision: 1105 $
 */
public class CriStoreSafeFloatAmountDocBuilderField
    extends AbstractDocBuilderField {

  public CriStoreSafeFloatAmountDocBuilderField(String argContents, String argStyle, Integer argLocation,
      DocBuilderAlignmentType argAlignment, int argPriority, IOutputFormatter argFormatter) {
    super(argContents, argStyle, argLocation, argAlignment, argPriority, argFormatter);
  }

  @Override
  public String getContents(Object argObj, IDocElementFactory argDocElementFactory) {
    StringBuilder sb = new StringBuilder(100);
    if (argObj instanceof ITenderControlTransaction) {
      if ("STOREBANK".equals(
          ((ITenderControlTransaction) argObj).getOutboundTenderRepository().getTenderRepositoryId())) {
        BigDecimal defaultFloatAmount =
            ((ITenderControlTransaction) argObj).getOutboundTenderRepository().getDefaultClosingCashAmt();
        BigDecimal defaultRegisterFloatAmount = TillHelper.getInstance().getTotalRegisterCashRemaining();
        BigDecimal storeSafeFloatAmount = defaultFloatAmount.subtract(defaultRegisterFloatAmount);

        sb.append("<dtv:StoreSafeFloatAmount>");
        sb.append("<![CDATA[");
        sb.append(storeSafeFloatAmount);
        sb.append("]]>");
        sb.append("</dtv:StoreSafeFloatAmount>");
      }
    }

    return getFormatter().format(sb);
  }
}
