//$Id$
package cri.pos.till;

import java.math.BigDecimal;
import java.util.*;

import org.apache.log4j.Logger;

import dtv.data2.access.*;
import dtv.data2.access.query.QueryRequest;
import dtv.data2.dataloader.DataLoaderUtils;
import dtv.data2.dataloader.fileprocessing.FileLine;
import dtv.data2.dataloader.valuetranslator.AbstractValueTranslator;
import dtv.pos.till.TillHelper;
import dtv.pos.till.types.TenderRepositoryTypeCode;
import dtv.xst.dao.tsn.ITenderRepository;

/**
 * This calls lookup current float amounts and increase/decrease by the amount specified in
 * UPDATE_STORE_FLOATS download record<br>
 * <br>
 * Copyright (c) 2013 MICROS Retail
 *
 * @author pgolli
 * @created Aug 30, 2013
 * @version $Revision$
 */
public class UpdateFloatsTranslator
    extends AbstractValueTranslator {

  private final static Logger logger_ = Logger.getLogger(UpdateFloatsTranslator.class);

  // private String positionOrganizationId_;
  private String positionRetailLocationId_;
  private String positionTillFloatInc_;
  private String positionStoreFloatInc_;

  private static final IQueryKey<Object[]> SP_RESET_FLOAT =
      new QueryKey<Object[]>("SP_RESET_FLOAT", Object[].class);

  @Override
  public String translate(String argCurrentValue, FileLine argCurrentLine) {

    Long retailLocationId = getRetailLocationId(argCurrentLine);
    String tillFloatIncStr = getTillFloatInc(argCurrentLine);
    String storeFloatIncStr = getStoreFloatInc(argCurrentLine);

    BigDecimal tillFloatInc = BigDecimal.ZERO;
    BigDecimal storeFloatInc = BigDecimal.ZERO;

    try {
      tillFloatInc = new BigDecimal(tillFloatIncStr);
      storeFloatInc = new BigDecimal(storeFloatIncStr);
    }
    catch (Exception ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }

    List<ITenderRepository> repositories =
        TillHelper.getInstance().getTenderRepositoryListByType(TenderRepositoryTypeCode.TILL);

    ITenderRepository tillRepository = repositories.get(0);
    BigDecimal currentTillFloat = tillRepository.getDefaultClosingCashAmt();
    BigDecimal newTillFloat = currentTillFloat.add(tillFloatInc);
    // int numOfTills = repositories.size();

    ITenderRepository storeBankRepository =
        TillHelper.getInstance().getTenderRepositoryListByType(TenderRepositoryTypeCode.STOREBANK).get(0);

    BigDecimal currentStoreFloat = storeBankRepository.getLastClosingCashAmt();
    BigDecimal newStoreFloat = currentStoreFloat.add(storeFloatInc);

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("argRetailLocationId", Long.valueOf(retailLocationId));
    params.put("argTillFloat", newTillFloat);
    params.put("argStoreFloat", newStoreFloat);

    try {
      executeFloatUpdate(params);
    }
    catch (Exception ex) {
      logger_.warn("Floats not updated.", ex);
    }

    return "1";
  }

  /* protected Long getOrganizationId(FileLine argCurrentLine) {
    String value = DataLoaderUtils.getValueForValueSpecifier(positionOrganizationId_, argCurrentLine);
    return Long.valueOf(value);
  }*/

  protected Long getRetailLocationId(FileLine argCurrentLine) {
    String value = DataLoaderUtils.getValueForValueSpecifier(positionRetailLocationId_, argCurrentLine);
    return Long.valueOf(value);
  }

  protected String getTillFloatInc(FileLine argCurrentLine) {
    return DataLoaderUtils.getValueForValueSpecifier(positionTillFloatInc_, argCurrentLine);
  }

  protected String getStoreFloatInc(FileLine argCurrentLine) {
    return DataLoaderUtils.getValueForValueSpecifier(positionStoreFloatInc_, argCurrentLine);
  }

  private void executeFloatUpdate(Map<String, Object> params) {
    List<QueryRequest> request = new ArrayList<QueryRequest>();

    QueryRequest query = new QueryRequest();
    query.setQueryKey(SP_RESET_FLOAT);
    query.setParams(params);

    request.add(query);
    DataFactory.makePersistent(request);
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, String argValue) {
    if ("RetailLocationId".equalsIgnoreCase(argName)) {
      positionRetailLocationId_ = argValue;
    }
    else if ("TillFolatInc".equalsIgnoreCase(argName)) {
      positionTillFloatInc_ = argValue;
    }
    else if ("StoreFloatInc".equalsIgnoreCase(argName)) {
      positionStoreFloatInc_ = argValue;
    }

    else {
      super.setParameter(argName, argValue);
    }
  }
}
