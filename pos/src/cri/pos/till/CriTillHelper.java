//$Id: CriTillHelper.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till;

import static dtv.util.NumberUtils.nonNull;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.util.*;

import org.apache.log4j.Logger;

import cri.pos.common.CriConfigurationMgr;
import cri.pos.till.count.*;
import cri.xst.dao.tsn.*;

import dtv.data2.access.*;
import dtv.pos.common.ConfigurationMgr;
import dtv.pos.framework.security.SecurityMgr;
import dtv.pos.storecalendar.StoreCalendar;
import dtv.pos.tender.TenderHelper;
import dtv.pos.till.TillHelper;
import dtv.pos.till.count.CountSummaryItem;
import dtv.pos.till.count.TillCountModel;
import dtv.util.NumberUtils;
import dtv.util.sequence.SequenceFactory;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.tsn.ISession;
import dtv.xst.query.results.RegisterRemainingCashResult;

/**
 * Till count function related methods.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author shy xie
 * @created Jul 16, 2012
 * @version $Revision: 1105 $
 */
public class CriTillHelper
    extends TillHelper {
  private static final Logger logger_ = Logger.getLogger(CriTillHelper.class);

  private static final IQueryKey<ICriDepositBag> CRI_DEPOSIT_BAG =
      new QueryKey<ICriDepositBag>("CRI.DEPOSIT_BAG", ICriDepositBag.class);
  private static final IQueryKey<RegisterRemainingCashResult> TOTAL_REGISTER_FLOAT_AMT =
      new QueryKey<RegisterRemainingCashResult>("TOTAL_REGISTER_FLOAT_AMT",
          RegisterRemainingCashResult.class);
  private static final IQueryKey<RegisterRemainingCashResult> TOTAL_REGISTER_DEFAULT_CASH =
      new QueryKey<RegisterRemainingCashResult>("CRI.TOTAL_REGISTER_DEFAULT_CASH",
          RegisterRemainingCashResult.class);

  /**
   * Get the instance.
   * 
   * @return
   */
  public static CriTillHelper getInstance() {
    return (CriTillHelper) TillHelper.getInstance();
  }

  /**
   * Get the store deposit bag list by the bag status code.
   * 
   * @param argStatusCode
   * @return
   */
  public List<ICriDepositBag> getDepositBags(String argStatusCode) {
    List<ICriDepositBag> result = new ArrayList<ICriDepositBag>();

    try {
      Map<String, Object> params = new HashMap<String, Object>();
      if (argStatusCode != null) {
        params.put("argStatusCode", argStatusCode);
      }
      params.put("argRetailLocId", ConfigurationMgr.getRetailLocationId());
      result = DataFactory.getObjectByQuery(CRI_DEPOSIT_BAG, params);
    }
    catch (ObjectNotFoundException ex) {
      logger_.debug("Deposit Bag is not found!", ex);
    }

    return result;
  }

  /**
   * Get the conveyed deposit bags.
   * 
   * @param argConveyanceDateFrom
   * @param argConveyanceDateTo
   * @return
   */
  public List<ICriDepositBag> getConveyedDepositBags(Date argConveyanceDateFrom, Date argConveyanceDateTo) {
    List<ICriDepositBag> result = new ArrayList<ICriDepositBag>();

    try {
      Map<String, Object> params = new HashMap<String, Object>();
      params.put("argStatusCode", CriBagStatus.CONVEYED.getCode());
      params.put("argRetailLocId", ConfigurationMgr.getRetailLocationId());
      params.put("argConveyanceStartDate", argConveyanceDateFrom);
      params.put("argConveyanceEndDate", argConveyanceDateTo);
      result = DataFactory.getObjectByQuery(CRI_DEPOSIT_BAG, params);
    }
    catch (ObjectNotFoundException ex) {
      logger_.debug("Deposit Bag is not found!", ex);
    }

    return result;
  }

  /**
   * Check the tender is an configured check tender or not.
   * 
   * @param argTender
   * @return
   */
  public boolean isCheckTender(ITender argTender) {
    List<String> checkTenderId = CriConfigurationMgr.getCheckTenderIds();
    if (checkTenderId.contains(argTender.getTenderId())) {
      return true;
    }

    return false;
  }

  public List<ICriDepositBag> createDepositBags(CriDepositBagEditModel argEditModel) {
    List<ICriDepositBag> result = new ArrayList<ICriDepositBag>();

    if (NumberUtils.isPositive(argEditModel.getTotalCashAmount())) {
      ICriDepositBag bag = createDepositBag();
      bag.setNumber(argEditModel.getCashBagNumber());
      bag.setAmount(argEditModel.getTotalCashAmount());
      bag.setType(argEditModel.getBagTypeCash());

      for (CountSummaryItem countItem : argEditModel.getCountModel().getLocalCurrencyCountSummaryItemList()) {
        ITender tender = (ITender) countItem.getCountSummaryObject();
        if (TenderHelper.getInstance().isLocalCurrency(tender)) {
          ICriDepositBagDetailItem detail = DataFactory.createObject(ICriDepositBagDetailItem.class);
          detail.setAmount(argEditModel.getTotalCashAmount());
          detail.setDescription(countItem.getDescription());
          bag.addCriDepositBagDetailItem(detail);
          break;
        }
      }

      result.add(bag);
    }

    if (NumberUtils.isPositive(argEditModel.getTotalCheckAmount())) {
      ICriDepositBag bag = createDepositBag();
      bag.setNumber(argEditModel.getCheckBagNumber());
      bag.setAmount(argEditModel.getTotalCheckAmount());
      bag.setType(argEditModel.getBagTypeCheck());

      for (CountSummaryItem countItem : argEditModel.getCountModel().getCountSummaryList()) {
        ITender tender = (ITender) countItem.getCountSummaryObject();
        if (isCheckTender(tender)) {
          ICriDepositBagDetailItem detail = DataFactory.createObject(ICriDepositBagDetailItem.class);
          detail.setAmount(countItem.getTotalDeclaredAmount());
          detail.setDescription(countItem.getDescription());
          bag.addCriDepositBagDetailItem(detail);
        }
      }

      result.add(bag);
    }

    return result;
  }

  /**
   * Create the deposit bag.
   * 
   * @return
   */
  public ICriDepositBag createDepositBag() {
    CriDepositBagId newId = new CriDepositBagId();
    newId.setRetailLocationId(Long.valueOf(ConfigurationMgr.getRetailLocationId()));
    newId.setBagSequence(SequenceFactory.getNextLongValue("DEPOSIT_BAG"));

    ICriDepositBag depositBag = DataFactory.createObject(newId, ICriDepositBag.class);
    depositBag.setStatus(CriBagStatus.AVAILABLE.getCode());
    depositBag.setCreateDateTime(StoreCalendar.getBusinessDateTimeStamp());
    depositBag.setLastChangeEmployeeId(SecurityMgr.getCurrentUser().getOperatorParty().getEmployeeId());
    depositBag.setEmployeeId(SecurityMgr.getCurrentUser().getOperatorParty().getEmployeeId());
    depositBag.setBusinessDate(StoreCalendar.getCurrentBusinessDate());
    return depositBag;
  }

  /**
   * Get the total deposit amount by the deposit bag types.
   * 
   * @param argStoreSafeSession
   * @param argCountModel
   * @param argBagType
   * @return
   */
  public BigDecimal getTotalDepositAmount(ISession argStoreSafeSession, TillCountModel argCountModel,
      CriBagTypes argBagType) {
    String currencyTenderId = TenderHelper.getInstance().getLocalCurrencyTenderId();
    BigDecimal totalRegisterFloatAmount = getTotalRegisterCashRemaining();

    if ((argStoreSafeSession == null) || (currencyTenderId == null)) {
      return BigDecimal.ZERO;
    }

    //    BigDecimal totalRegisterFloatAmount = getTotalRegisterCashRemaining();
    BigDecimal totalStoreSafeFloatAmount =
        NumberUtils.nonNull(argStoreSafeSession.getTenderRepository().getDefaultClosingCashAmt());
    //    BigDecimal totalFloatAmount = totalRegisterFloatAmount.add(totalStoreSafeFloatAmount);
    BigDecimal depositAmount = null;

    if (CriBagTypes.CASH == argBagType) {
      depositAmount = getTotalCashCountAmount(argCountModel).add(totalRegisterFloatAmount);
      depositAmount = depositAmount.subtract(totalStoreSafeFloatAmount);
    }
    else if (CriBagTypes.CHECKS == argBagType) {
      depositAmount = getTotalChecksCountAmount(argCountModel);
    }
    else {
      depositAmount = getTotalCashCountAmount(argCountModel);
      depositAmount = depositAmount.add(getTotalChecksCountAmount(argCountModel));
      depositAmount = depositAmount.subtract(totalStoreSafeFloatAmount);
    }

    return depositAmount;

  }

  /**
   * Get the total cash count amount.
   * 
   * @param argCountModel
   * @return
   */
  protected BigDecimal getTotalCashCountAmount(TillCountModel argCountModel) {
    BigDecimal countAmount = BigDecimal.ZERO;
    for (CountSummaryItem countItem : argCountModel.getLocalDepositableCountSummaryItems()) {
      countAmount = countAmount.add(countItem.getTotalDeclaredAmount());
    }

    return countAmount;
  }

  /**
   * Get the total check count amount.
   * 
   * @param argCountModel
   * @return
   */
  protected BigDecimal getTotalChecksCountAmount(TillCountModel argCountModel) {
    BigDecimal countAmount = BigDecimal.ZERO;
    for (CountSummaryItem countItem : argCountModel.getLocalDepositableCountSummaryItems()) {
      if (isCheckTender((ITender) countItem.getCountSummaryObject())) {
        countAmount = countAmount.add(countItem.getTotalDeclaredAmount());
      }
    }

    return countAmount;
  }

  public BigDecimal getTotalRegisterFloatAmt() {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("argRetailLocId", getRetailLocationId());

    List<RegisterRemainingCashResult> results =
        DataFactory.getObjectByQueryNoThrow(TOTAL_REGISTER_FLOAT_AMT, params);

    return nonNull((results.isEmpty()) ? ZERO : results.get(0).getAmount());
  }

  /**
   * Get the total register configured default closing amount.
   *
   * @return
   */
  public BigDecimal getTotalRegisterDefaultClosingAmt() {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("argRetailLocId", getRetailLocationId());

    List<RegisterRemainingCashResult> results =
        DataFactory.getObjectByQueryNoThrow(TOTAL_REGISTER_DEFAULT_CASH, params);

    return nonNull((results.isEmpty()) ? ZERO : results.get(0).getAmount());
  }

  /**
   * Get the configured default store safe float amount.
   *
   * @return
   */
  public BigDecimal getConfiguredStoreSafeAmount() {
    ISession storeSafeSession = getStoreBankSession();
    BigDecimal storeSafeFloatAmount =
        NumberUtils.nonNull(storeSafeSession.getTenderRepository().getDefaultClosingCashAmt());
    BigDecimal totalRegisterFloatAmount = getTotalRegisterDefaultClosingAmt();

    return storeSafeFloatAmount.subtract(totalRegisterFloatAmount);
  }

  /**
   * Get the last store safe count amount. This amount will be updated after do the store bank
   * count.
   *
   * @return
   */
  public BigDecimal getLastCountAmount() {
    ISession storeSafeSession = getStoreBankSession();
    BigDecimal bankLastCountAmount =
        NumberUtils.nonNull(storeSafeSession.getTenderRepository().getLastClosingCashAmt());

    return bankLastCountAmount;
  }
}
