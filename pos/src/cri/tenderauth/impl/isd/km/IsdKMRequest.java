//$Id: IsdKMRequest.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.tenderauth.impl.isd.km;

import imsformat.credit.CCRequest;

import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.impl.isd.AbstractIsdRequest;
import dtv.xst.dao.trl.IAuthorizableLineItem;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Sep 20, 2012
 * @version $Revision: 1105 $
 */
public class IsdKMRequest
    extends AbstractIsdRequest {

  /**
   * Constructs a <code>IsdKMRequest</code>.
   * @param argType
   * @param argLine
   */
  public IsdKMRequest(AuthRequestType argType, IAuthorizableLineItem argLine) {
    this(argType, argLine, null);
  }

  /**
   * Constructs a <code>IsdKMRequest</code>.
   * @param argType
   * @param argLine
   * @param argTenderUsageCode
   */
  protected IsdKMRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
      TenderUsageCodeType argTenderUsageCode) {
    super(argType, argLine, argTenderUsageCode);

  }

  @Override
  public void prepRequest() {
    super.prepRequest();
    setParameter(CCRequest.SYSTEM_TRACE_AUDIT_NUMBER, null);
  }
}
