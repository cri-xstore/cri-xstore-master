//$Id: IsdKMResponse.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.tenderauth.impl.isd.km;

import static dtv.util.StringUtils.isEmpty;

import java.util.Map;

import imsformat.km.KMResponse;

import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.impl.isd.IsdResponse;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Sep 20, 2012
 * @version $Revision: 1105 $
 */
public class IsdKMResponse
    extends IsdResponse {

  private static final long serialVersionUID = 1L;

  /**
   * Constructs a <code>IsdKMResponse</code>.
   * @param argRequest
   * @param argMap
   */
  public IsdKMResponse(IAuthRequest argRequest, Map<String, Object> argMap) {
    super(argRequest, argMap);
    if (!isEmpty(getKeyIdentifier()) && !isEmpty(getCurrentActiveKeyValue())) {
      setSuccess(true);
    }
  }

  public String getKeyIdentifier() {
    return get(KMResponse.KEY_ID);
  }

  public String getCurrentActiveKeyValue() {
    return get(KMResponse.CURR_KEY);
  }
}
