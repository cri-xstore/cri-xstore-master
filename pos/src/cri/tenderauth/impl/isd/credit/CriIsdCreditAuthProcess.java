//$Id: CriIsdCreditAuthProcess.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.tenderauth.impl.isd.credit;

import java.math.BigDecimal;
import java.util.*;

import dtv.i18n.FormatterType;
import dtv.i18n.IFormattable;
import dtv.tenderauth.*;
import dtv.tenderauth.impl.isd.credit.IsdCreditAuthProcess;
import dtv.util.config.IConfigObject;
import dtv.util.config.ParameterConfig;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Oct 12, 2012
 * @version $Revision: 1105 $
 */
public class CriIsdCreditAuthProcess
    extends IsdCreditAuthProcess {

  private String _criBankId;

  @Override
  public void setParameter(ParameterConfig argConfig) {
    String name = argConfig.getName();
    IConfigObject value = argConfig.getValue();

    if ("CRIBankID".equalsIgnoreCase(name)) {
      _criBankId = value.toString();
    }
    else {
      super.setParameter(argConfig);
    }
  }

  @Override
  protected IAuthInfoField[] getManualAuthInfoFields(IAuthRequest argRequest) {
    List<IAuthInfoField> fields = new LinkedList<IAuthInfoField>();

    IFormattable merchantNumber = getMerchantNumber();
    if ((merchantNumber != IFormattable.EMPTY) && (merchantNumber != null)) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.MERCHANT_NUMBER, //
          FF.getTranslatable("_authManualMerchantNumber"), //
          merchantNumber));
    }

    if (_criBankId != null) {
      fields.add(
          new AuthInfoField("bankId", FF.getTranslatable("_criVisaMCBankID"), FF.getLiteral(_criBankId)));
    }

    String transitNumber = getTransitNumber(argRequest);
    if (transitNumber != null) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.TRANSIT_NUMBER, //
          FF.getTranslatable("_authManualTransitNumber"), //
          FF.getLiteral(transitNumber)));
    }

    String accountNumber = getAccountNumber(argRequest);
    if (getShowAccountNumber(accountNumber)) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.ACCT_NUMBER, //
          FF.getTranslatable("_authManualAccountNumber"), //
          FF.getLiteral(accountNumber)));
    }

    String checkNumber = getCheckNumber(argRequest);
    if (checkNumber != null) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.SERIAL_NUMBER, //
          FF.getTranslatable("_authManualCheckNumber"), //
          FF.getLiteral(checkNumber)));
    }

    Date expirationDate = getExpirationDate(argRequest);
    if (getShowExpirationDate(expirationDate)) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.EXP_DATE, //
          FF.getTranslatable("_authManualExpDate"), //
          FF.getSimpleFormattable(expirationDate, FormatterType.DATE_EXP_LONG)));
    }

    if (!isManualAuthAmountEditable()) {
      BigDecimal amount = getAmount(argRequest);
      if (amount != null) {
        fields.add(new AuthInfoField(AuthInfoFieldKey.AMOUNT, //
            FF.getTranslatable("_authManualAmount"), //
            FF.getSimpleFormattable(amount, FormatterType.MONEY)));
      }
    }
    return fields.toArray(new IAuthInfoField[fields.size()]);
  }
}
