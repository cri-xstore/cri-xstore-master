//$Id: AccessCodeCalculator.java 517 2012-10-26 19:34:52Z dtvdomain\aabdul $
package cri.tenderauth.storedvalue.svs;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>
 * Access Code calculator for SVS Manual authorizations
 * </p>
 * 
 * <p>
 * Copyright (c) 2008 MICROS-Retail
 * </p>
 * 
 * @author lren
 * @created May 15, 2008
 * @version $Revision: 517 $
 */
public class AccessCodeCalculator {
  private static AccessCodeCalculator INSTANCE = new AccessCodeCalculator();
  private final DateFormat julianFormat_ = new SimpleDateFormat("D");

  private AccessCodeCalculator() {}

  public static AccessCodeCalculator getInstance() {
    return INSTANCE;
  }

  public String calculate(String argCardNumber, BigDecimal argTranAmount, long argRtlLocId) {
    int day = getJulianDay();
    int cardNum = getLastThreeDigitCardNumber(argCardNumber);
    long amount = getWholeNumberTranAmount(argTranAmount);

    String accessCode = Long.toString(cardNum * amount * argRtlLocId * day);
    int length = accessCode.length();

    if (length < 6) {
      StringBuffer sbAccessCode = new StringBuffer("0");
      sbAccessCode.append(accessCode);
      while (sbAccessCode.length() < 6) {
        sbAccessCode.insert(0, "0");
      }
      accessCode = sbAccessCode.toString();
    }
    else if (length > 6) {
      accessCode = accessCode.substring(0, 6);
    }

    return accessCode;
  }

  private int getLastThreeDigitCardNumber(String argCardNumber) {
    String cardNumber = argCardNumber.trim();
    int length = cardNumber.length();

    if (length > 3) {
      return Integer.parseInt(cardNumber.substring(length - 3));
    }
    else {
      return Integer.parseInt(cardNumber);
    }
  }

  private long getWholeNumberTranAmount(BigDecimal argTranAmount) {
    // converting to double and casting to long is losing the last decimal value.
    //return (long)((argTranAmount.doubleValue())*100);
    BigDecimal amount = argTranAmount.multiply(new BigDecimal(100));
    return amount.longValue();

  }

  private int getJulianDay() {
    Date currentDay = new Date();
    String julianDayString = julianFormat_.format(currentDay);
    return Integer.parseInt(julianDayString);
  }
}
