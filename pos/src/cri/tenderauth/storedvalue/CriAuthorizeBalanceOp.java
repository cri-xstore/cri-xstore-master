//$Id: CriAuthorizeBalanceOp.java 343 2012-09-05 20:37:23Z dtvdomain\aabdul $
package cri.tenderauth.storedvalue;

import dtv.pos.tender.voucher.IVoucherCmd;
import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.op.IAuthorizationCmd;
import dtv.tenderauth.storedvalue.AuthorizeBalanceOp;
import dtv.tenderauth.storedvalue.IStoredValueAuthRequest;

/**
 * Customized to send the pin for balance inquiry request<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Sep 5, 2012
 * @version $Revision: 343 $
 */
public class CriAuthorizeBalanceOp
    extends AuthorizeBalanceOp {

  private static final long serialVersionUID = 1L;

  @Override
  protected IAuthRequest getAuthRequest(IAuthorizationCmd argCmd) {

    IVoucherCmd cmd = (IVoucherCmd) argCmd;
    IStoredValueAuthRequest request = (IStoredValueAuthRequest) super.getAuthRequest(argCmd);
    request.setPIN(cmd.getPINNumber());
    return request;
  }
}
