//$Id: ISupportsManualEntry.java 1167 2017-09-26 16:41:46Z johgaug $
package cri.ajb.fipay.reqeust;

/**
 * Interface for a request that supports prompting the customer for manual entry.<br>
 * <br>
 * Copyright (c) 2017 OLR Retail
 *
 * @author johgaug
 * @created Sep 21, 2017
 * @version $Revision: 1167 $
 */
public interface ISupportsManualEntry {

  /** Set the CEM_Manual flag on the request. */
  void setManual();
}
