//$Id: AjbFiPayPingRequest.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.reqeust;

import cri.ajb.fipay.response.AjbFiPayPingResponse;

import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.event.IAuthResponse;

/**
 * AJB ping request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayPingRequest
    extends AjbFiPayGenericRequest {

  /**
   * Constructor method
   * 
   * @param argType
   * @param argData
   */
  public AjbFiPayPingRequest(AuthRequestType argType, Object[] argData) {
    super(argType, argData);
  }

  /** {@inheritDoc} */
  @Override
  public IAuthResponse getResponse(String[] argFields, String argResponseMessageId) {
    return new AjbFiPayPingResponse(this, argFields);
  }
}
