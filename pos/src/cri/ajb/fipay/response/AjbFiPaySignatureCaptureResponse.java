//$Id: AjbFiPaySignatureCaptureResponse.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.response;

import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.impl.ajb.response.AjbCreditResponse;

/**
 * AJB signature capture response.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPaySignatureCaptureResponse
    extends AjbCreditResponse
    implements IAjbFiPaySignatureCaptureResponse {

  /**
   * 
   */
  private static final long serialVersionUID = -8683439944757962385L;
  protected static final int SIGNATURE_NBR = 17;
  protected static final int FOCUS_AUTH_CODE_NBR = 19;

  /**
   * Constructor
   * 
   * @param argRequest the request that triggered this response.
   * @param argFields the raw response fields
   * @param argAuthorizationCode authorization code
   */
  public AjbFiPaySignatureCaptureResponse(IAuthRequest argRequest, String[] argFields,
      String argAuthorizationCode) {
    super(argRequest, argFields, null);
  }

  /** {@inheritDoc} */
  @Override
  public String getSignature() {
    return getField(SIGNATURE_NBR);
  }

  /** {@inheritDoc} */
  @Override
  public String getResponseCode() {
    return getActionCodeString();
  }

  /** {@inheritDoc} */
  @Override
  public String getResponseText() {
    return getField(FOCUS_AUTH_CODE_NBR);
  }

}
