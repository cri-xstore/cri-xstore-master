//$Id: AjbFiPayGiftCardResponse.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.response;

import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.storedvalue.ajb.AjbGiftCardResponse;
import dtv.util.StringUtils;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.trl.IVoucherLineItem;

/**
 * AJB FiPay gift card auth response.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayGiftCardResponse
    extends AjbGiftCardResponse {

  /**
   * 
   */
  private static final long serialVersionUID = -5423801604543793381L;
  private static final int ACCOUNT_NBR = 13;
  protected static final int OPTIONS_NBR = 21;
  protected static final int SAF_FLAG = 89;

  /**
   * Constructor
   * 
   * @param argRequest the request that triggered this response.
   * @param argFields the raw response fields
   * @param argAuthorizationCode authorization code
   */
  public AjbFiPayGiftCardResponse(IAuthRequest argRequest, String[] argFields, String argAuthorizationCode) {
    super(argRequest, argFields, argAuthorizationCode);
    generateCardInfo(argRequest.getLineItem(), this);
  }

  /**
   * 
   * @param argLineItem
   * @param argResponse
   */
  protected void generateCardInfo(IAuthorizableLineItem argLineItem, IAuthResponse argResponse) {
    if (argLineItem != null) {
      IVoucherLineItem voucherLineItem = (IVoucherLineItem) argLineItem;
      voucherLineItem.setEntryMethodCode("MSR.CUST_MSR");

      String accountNbr = getAccountNumber();
      voucherLineItem.setSerialNumber(accountNbr);
      voucherLineItem.getVoucher().setSerialNumber(accountNbr);

      if (argResponse.isSuccess()) {
        voucherLineItem.setVoid(false);
      }

    }
  }

  /** {@inheritDoc} */
  @Override
  public String getAmountString() {
    String amount = getField(AMOUNT);
    if (StringUtils.isEmpty(amount)) {
      return "0";
    }
    else {
      return amount;
    }
  }

  /**
   * 
   * @return
   */
  public String getEnterMethod() {
    return getField(OPTIONS_NBR);
  }

  /**
   * 
   * @return
   */
  public String getAccountNumber() {
    return getField(ACCOUNT_NBR);
  }

  /** {@inheritDoc} */
  @Override
  public String getResponseCode() {
    return getActionCodeString();
  }

  public boolean isSAFAvailable() {
    String flag = getField(SAF_FLAG);
    return (!StringUtils.isEmpty(flag) && flag.contains("SAFable"));
  }

}
