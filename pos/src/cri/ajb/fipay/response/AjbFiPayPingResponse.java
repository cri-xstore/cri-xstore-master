//$Id: AjbFiPayPingResponse.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.response;

import dtv.tenderauth.IAuthRequest;

/**
 * AJB ping response.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayPingResponse
    extends AjbFiPayGenericResponse
    implements IAjbFiPayPingResponse {

  /**
   * 
   */
  private static final long serialVersionUID = -6298757885867165142L;
  private boolean offlineFlag_ = false;

  /**
   * Constructor
   * 
   * @param argRequest the request that triggered this response.
   * @param argFields the raw response fields
   */
  public AjbFiPayPingResponse(IAuthRequest argRequest, String[] argFields) {
    super(argRequest, argFields);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isOffline() {
    return offlineFlag_;
  }

  /** {@inheritDoc} */
  @Override
  public void setOffline(boolean argOffline) {
    offlineFlag_ = argOffline;
  }
}
