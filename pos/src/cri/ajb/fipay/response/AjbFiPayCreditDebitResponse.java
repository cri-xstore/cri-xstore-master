//$Id: AjbFiPayCreditDebitResponse.java 1263 2018-10-11 23:00:21Z johgaug $
package cri.ajb.fipay.response;

import java.util.*;

import org.apache.log4j.Logger;

import cri.ajb.fipay.impl.DebitCreditEntryMethodCode;
import cri.ajb.fipay.reqeust.AjbFiPayCreditDebitRequest;
import cri.pos.tender.CriTenderHelper;

import dtv.i18n.*;
import dtv.pos.tender.TenderHelper;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.impl.ajb.response.AjbCreditResponse;
import dtv.util.ObjectUtils;
import dtv.util.StringUtils;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * AJB FiPay credit/debit auth response.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1263 $
 */
public class AjbFiPayCreditDebitResponse
    extends AjbCreditResponse {

  /**
   * 
   */
  private static final long serialVersionUID = -4322271697319137557L;

  private static final Logger _logger = Logger.getLogger(AjbFiPayCreditDebitResponse.class);

  protected static final int CARD_TYPE_NBR = 12;
  protected static final int FOCUS_AUTH_CODE_NBR = 19;
  protected static final int OPTIONS_NBR = 21;
  protected static final int TOKEN_NBR = 34;
  protected static final int RECEIPT_DISPLAY_NBR = 38;
  protected static final int PRODUCT_INFO = 56;
  protected static final int CUSTOMER_NAME = 31;
  protected static final int SAF_FLAG = 89;

  protected static final CriTenderHelper TENDER_HELPER = (CriTenderHelper) TenderHelper.getInstance();
  protected static final AuthRequestType TENDER_MANUAL_ENTRY = AuthRequestType.forName("TENDER_MANUAL_ENTRY");

  protected static final Set<AuthRequestType> _types =
      Collections.unmodifiableSet(new HashSet<AuthRequestType>(Arrays.asList(new AuthRequestType[] {
          AuthRequestType.TENDER, TENDER_MANUAL_ENTRY, AuthRequestType.REFUND_TENDER})));

  /**
   * Constructor
   * 
   * @param argRequest the request that triggered this response.
   * @param argFields the raw response fields
   * @param argAuthorizationCode authorization code
   */
  public AjbFiPayCreditDebitResponse(IAuthRequest argRequest, String[] argFields,
      String argAuthorizationCode) {
    super(argRequest, argFields, argAuthorizationCode);
    generateCardInfo((AjbFiPayCreditDebitRequest) argRequest);
  }

  protected void generateCardInfo(AjbFiPayCreditDebitRequest argRequest) {
    //only tender the card type and info decide by FiPay have to copy it to xstore tender line model.
    if (_types.contains(argRequest.getRequestType())) {
      ICreditDebitTenderLineItem lineItem = (ICreditDebitTenderLineItem) argRequest.getLineItem();
      ITender tender = CriTenderHelper.getInstance().getTender(this);
      lineItem.setTender(tender);

      String options = getOptions();
      if (!StringUtils.isEmpty(options)) {
        DebitCreditEntryMethodCode entryMethod = DebitCreditEntryMethodCode.forOptions(options);
        if (entryMethod != null) {
          lineItem.setEntryMethodCode(entryMethod.getMapping());
        }
        else if (options.contains("CEM_Manual")) {
          lineItem.setEntryMethodCode("KEYBOARD.KEYBOARD");
        }
        else if (options.contains("CEM_Swiped")) {
          lineItem.setEntryMethodCode("MSR.CUST_MSR");
        }
      }

      String accountNbr = getAccountNumber();
      lineItem.setAccountNumber(accountNbr);
      lineItem.setMaskAccountNumberDao(maskField(accountNbr));

      String exprDateStr = getExpirationDateString();
      lineItem.setExpirationDateString(exprDateStr);

      String token = getAuthorizationToken();
      lineItem.setAuthorizationToken(token);

      TENDER_HELPER.setSignatureRequired(lineItem, isSignRequired());

      if (isSuccess()) {
        _logger.warn("Response is successful");
        lineItem.setVoid(false);
      }
      else {
        _logger.warn("Response is not successful");
      }
      _logger.warn("Void flag is [" + lineItem.getVoid() + "] for line item: "
          + ObjectUtils.getClassNameFromObject(lineItem));
    }
  }

  public String getOptions() {
    return getField(OPTIONS_NBR);
  }

  public String getCardType() {
    return getField(CARD_TYPE_NBR);
  }

  /** {@inheritDoc} */
  @Override
  public String getAuthorizationToken() {
    return getField(TOKEN_NBR);
  }

  /** Check the auth amount is over than the FiPay floor limit amount. */
  public boolean isAboveFloorLimit() {
    String value = getField(FOCUS_AUTH_CODE_NBR);
    return StringUtils.isEmpty(value);
  }

  /** Get the signature required flag. */
  public boolean isSignRequired() {
    String value = getField(OPTIONS_NBR);
    return !StringUtils.isEmpty(value) && value.contains("*SIGNATURE");
  }

  /** {@inheritDoc} */
  @Override
  public String getResponseText() {
    return getField(RECEIPT_DISPLAY_NBR);
  }

  /** {@inheritDoc} */
  @Override
  public String getResponseCode() {
    return getActionCodeString();
  }

  protected static String maskField(String value) {
    if (value == null) {
      return null;
    }

    FormatterFactory ff = FormatterFactory.getInstance();
    IFormatter f = ff.getFormatter(FormatterType.forName("CreditCard"));

    return f.format(value, OutputContextType.DOCUMENT);
  }

  public boolean isCreditCard() {
    //AC#457634-FB#365653 - CRI - 9.1.5 - Offline Debit.
    return !StringUtils.isEmpty(getCardType()) && !getCardType().equalsIgnoreCase("Debit");
  }

  public boolean isSAFAvailable() {
    String flag = getField(SAF_FLAG);
    return (!StringUtils.isEmpty(flag) && flag.contains("SAFable"));
  }

  /** Get the product info from field 56, which contains EMV data. */
  public String getProductInfo() {
    return getField(PRODUCT_INFO);
  }
  
  /** Get the customer name. */
  public String getCustomerName() {
    return getField(CUSTOMER_NAME);
  }

}
