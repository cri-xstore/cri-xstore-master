//$Id: AjbFiPayPromptOutgoingVoucherTenderExchangeAmtOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.exchange.op;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

import org.apache.log4j.Logger;

import cri.ajb.fipay.op.AjbFiPayPromptVoucherTenderAmtOp;

import dtv.i18n.IFormattable;
import dtv.pos.common.PromptKey;
import dtv.pos.common.ValidationKey;
import dtv.pos.framework.ui.config.DataFieldConfig;
import dtv.pos.framework.ui.config.PromptConfig;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.pos.iframework.validation.IValidationKey;
import dtv.pos.tender.TenderConstants;
import dtv.pos.tender.exchange.TenderExchangeCmd;
import dtv.util.NumberUtils;
import dtv.xst.dao.tnd.ITender;

/**
 * This operation handles prompting the user to enter the voucher tender amount and validation on
 * the amount entered.<br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created June 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayPromptOutgoingVoucherTenderExchangeAmtOp
    extends AjbFiPayPromptVoucherTenderAmtOp {

  /**
   * 
   */
  private static final long serialVersionUID = 6307684824469679339L;
  private static final Logger logger_ =
      Logger.getLogger(AjbFiPayPromptOutgoingVoucherTenderExchangeAmtOp.class);

  /** {@inheritDoc} */
  @Override
  public IValidationKey[] getValidationKeys(IXstCommand argCmd) {
    return new IValidationKey[] {ValidationKey.valueOf("TENDER_EXCHANGE_OUT_NOT_GREATER_THAN_IN"),
        ValidationKey.valueOf("TENDER_EXCHANGE_OUT_NOT_GREATER_THAN_AVAILABLE"),
        TenderConstants.TENDER_EXCHANGE_MULTIPLE_OUTGOING};
  }

  /** {@inheritDoc} */
  @Override
  protected IPromptKey getPromptKey(IXstCommand argCmd) {
    return PromptKey.valueOf("OUTGOING_TENDER_EXCHANGE_AMOUNT");
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse getPromptResponse(IXstCommand argCmd, IXstEvent argEvent, IPromptKey promptKey,
      IFormattable[] promptArgs) {

    TenderExchangeCmd cmd = (TenderExchangeCmd) argCmd;
    ITender tender = cmd.getTender();
    PromptConfig config = new PromptConfig();
    config.setDataFieldConfig(new DataFieldConfig());
    BigDecimal maxAllowed = cmd.getTransaction().getAmountDue();
    maxAllowed = maxAllowed.min(cmd.getTransaction().getAmountDue().abs());
    maxAllowed = maxAllowed.max(NumberUtils.ZERO);
    int roundingDigits = 2;

    try {
      final String cid = tender.getCurrencyId();
      final Currency c = Currency.getInstance(cid);
      roundingDigits = c.getDefaultFractionDigits();
    }
    catch (Exception ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }

    config.getDataFieldConfig().setDefaultValue(maxAllowed.setScale(roundingDigits, RoundingMode.HALF_EVEN));

    return HELPER.getPromptResponse(promptKey, promptArgs, config);
  }

}
