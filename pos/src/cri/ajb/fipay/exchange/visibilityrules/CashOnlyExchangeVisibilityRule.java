//$Id: CashOnlyExchangeVisibilityRule.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.exchange.visibilityrules;

import java.util.ArrayList;
import java.util.List;

import dtv.pos.framework.visibilityrules.AbstractVisibilityRule;
import dtv.pos.iframework.visibilityrules.AccessLevel;
import dtv.pos.iframework.visibilityrules.IAccessLevel;
import dtv.util.config.IConfigObject;
import dtv.xst.dao.tnd.TenderStatus;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Cash only exchange.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created June 17, 2015
 * @version $Revision: 1105 $
 */
public class CashOnlyExchangeVisibilityRule
    extends AbstractVisibilityRule {
  private final List<String> tenderIds_ = new ArrayList<String>();

  /** {@inheritDoc} */
  @Override
  protected IAccessLevel checkVisibilityImpl()
      throws Exception {
    IPosTransaction trans = getCurrentPosTransaction();
    if (trans == null) {
      return AccessLevel.DENIED;
    }

    for (ITenderLineItem tenderLineItem : trans.getLineItems(ITenderLineItem.class)) {
      if (!tenderLineItem.getVoid() && tenderIds_.contains(tenderLineItem.getTenderId())
          && TenderStatus.TENDER.matches(tenderLineItem.getTenderStatusCode())) {
        return AccessLevel.DENIED;
      }
    }

    return AccessLevel.GRANTED;
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("TenderId".equalsIgnoreCase(argName)) {
      tenderIds_.add(argValue.toString());
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

}
