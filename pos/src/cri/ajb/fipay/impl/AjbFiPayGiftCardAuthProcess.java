//$Id: AjbFiPayGiftCardAuthProcess.java 1133 2017-06-14 09:17:39Z krimoha $
package cri.ajb.fipay.impl;

import java.math.BigDecimal;
import java.util.*;

import org.apache.log4j.Logger;

import cri.tenderauth.storedvalue.svs.AccessCodeCalculator;
import cri.ajb.fipay.response.AjbFiPayGiftCardResponse;

import dtv.i18n.FormatterType;
import dtv.i18n.IFormattable;
import dtv.pos.common.ConfigurationMgr;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.storedvalue.IStoredValueAuthRequest;
import dtv.tenderauth.storedvalue.ajb.AjbGiftCardAuthProcess;

/**
 * An authorization process for gift cards via AJB.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1133 $
 */
public class AjbFiPayGiftCardAuthProcess
    extends AjbGiftCardAuthProcess {
  
  private static final Logger logger_ = Logger
      .getLogger(AjbFiPayGiftCardAuthProcess.class);

  /** {@inheritDoc} */
  @Override
  protected IAuthResponse handleOffline(ITenderAuthRequest argRequest) {

    IAuthResponse returnResponse =
        getFailedResponse(argRequest, argRequest, getMessageKeyOffline(argRequest));
    IAuthResponse response = argRequest.getResponses()[argRequest.getResponses().length - 1];
    List<String> ids = new ArrayList<String>();
    ids.addAll(Arrays.asList(argRequest.getMessageKeys(response)));
    ids.add(getUnknownResponseMessageId());
    returnResponse.setMessage(getMessage(response, ids));
    returnResponse.setAvailableActions(response.getAvailableActions());

    return returnResponse;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isManualAuthAllowed(IAuthResponse argResponse) {
    boolean result = super.isManualAuthAllowed(argResponse);
    if (result && (argResponse instanceof AjbFiPayGiftCardResponse)) {
      result = ((AjbFiPayGiftCardResponse) argResponse).isSAFAvailable();
    }
    return result;
  }

  /** CRU INC0045299 */
  @Override
  protected IAuthInfoField[] getManualAuthInfoFields(IAuthRequest argRequest) {
    logger_.info("Begin --> getManualAuthInfoFields(IAuthRequest argRequest)");
    IStoredValueAuthRequest request = (IStoredValueAuthRequest) argRequest;
    List<IAuthInfoField> fields = new LinkedList<IAuthInfoField>();
    IFormattable merchantNumber = getMerchantNumber();
    logger_.info("merchantNumber --> " + merchantNumber);
    if (merchantNumber != null && merchantNumber != IFormattable.EMPTY) {
        fields.add(new AuthInfoField("merchantNumber", FF
                .getTranslatable("_ajbGiftCardAuthMerchantNumber"),
                merchantNumber));
    }
    
    String transitNumber = getTransitNumber(request);
    logger_.info("transitNumber --> " + transitNumber);
    if (transitNumber != null) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.TRANSIT_NUMBER, //
          FF.getTranslatable("_authManualTransitNumber"), //
          FF.getLiteral(transitNumber)));
    }

    //String accountNumber = getAccountNumber(request);
    String accountNumber = request.getAccountId();
    logger_.info("accountNumber --> " + accountNumber);
    if (getShowAccountNumber(accountNumber)) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.ACCT_NUMBER, //
          FF.getTranslatable("_authManualAccountNumber"), //
          FF.getLiteral(accountNumber)));
    }

    String checkNumber = getCheckNumber(request);
    logger_.info("checkNumber --> " + checkNumber);
    if (checkNumber != null) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.SERIAL_NUMBER, //
          FF.getTranslatable("_authManualCheckNumber"), //
          FF.getLiteral(checkNumber)));
    }

    Date expirationDate = getExpirationDate(request);
    logger_.info("expirationDate --> " + expirationDate);
    if (getShowExpirationDate(expirationDate)) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.EXP_DATE, //
          FF.getTranslatable("_authManualExpDate"), //
          FF.getSimpleFormattable(expirationDate, FormatterType.DATE_EXP_LONG)));
    }

    IFormattable accessCode =
        getAccessCode(accountNumber, request.getAmount(), ConfigurationMgr.getRetailLocationId());
    logger_.info("accessCode --> " + accessCode);
    if ((accessCode != IFormattable.EMPTY) && (accessCode != null)) {
      fields.add(new AuthInfoField("accessCode", FF.getTranslatable("_authOfflineAccessCode"), accessCode));
    }

    if ((accessCode != IFormattable.EMPTY) && (accessCode != null)) {
      logger_.info("password --> " + FF.getTranslatable("_authOfflinePassword"));
      fields.add(new AuthInfoField("password", FF.getTranslatable("_authOfflinePasswordLbl"),
          FF.getTranslatable("_authOfflinePassword")));
    }
    
    if(!isManualAuthAmountEditable()) {
      logger_.info("isManualAuthAmountEditable is set to false");
      BigDecimal amount = getAmount(request);
      logger_.info("amount --> " + amount);
       fields.add(new AuthInfoField("amount", FF.getTranslatable("_ajbGiftCardManualAuthAmount"), FF.getSimpleFormattable(amount, FormatterType.MONEY)));
     }
    logger_.info("End --> getManualAuthInfoFields(IAuthRequest argRequest)");
    return fields.toArray(new IAuthInfoField[fields.size()]);
}
  
  /** CRU INC0045299 */
  protected final IFormattable getAccessCode(String argCardNumber, BigDecimal argTranAmount,
      long argRtlLocId) {
    logger_.info("Card Number --> " + argCardNumber);
    logger_.info("Tran Amount --> " + argTranAmount);
    logger_.info("Location Id --> " + argRtlLocId);
    AccessCodeCalculator calculator = AccessCodeCalculator.getInstance();
    String accessCode = calculator.calculate(argCardNumber, argTranAmount, argRtlLocId);
    return FF.getLiteral(accessCode);
  }

}
