package cri.ajb.fipay.impl;

import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.impl.ajb.AjbRequestConverter;

/**
 * AJB ping process.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 *
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPaySignatureCaptureRequestConverter
    extends AjbRequestConverter {

  @Override
  public Object convertRequest(IAuthRequest argAuthRequest) {
    return convertRequestImpl(argAuthRequest);
  }
}
