//$Id: AjbFiPayDisplayDevice.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.impl;

import org.apache.log4j.Logger;

import cri.ajb.*;
import cri.ajb.state.DeviceTranState;
import cri.ajb.state.IStateChange;

import dtv.hardware.sigcap.state.DefaultLineItemRenderer;
import dtv.tenderauth.*;

/**
 * AJB display device.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayDisplayDevice
    implements IDisplayChangeListener {
  private TransactionDisplayModel displayModel_;
  private static final AjbFiPayDisplayDevice INSTANCE = new AjbFiPayDisplayDevice();
  private DeviceTranState currentDeviceTranState_;

  private static final Logger logger_ = Logger.getLogger(AjbFiPayDisplayDevice.class);

  /**
   * Initialize the display device.
   * 
   * @return
   */
  public boolean initialize() {
    return initializeImpl();
  }

  /**
   * Get the device instance.
   * 
   * @return
   */
  public static AjbFiPayDisplayDevice getInstance() {
    return INSTANCE;
  }

  /**
   * implementation of device initialization.
   * 
   * @return
   */
  protected boolean initializeImpl() {
    displayModel_ = new TransactionDisplayModel(AjbFiPayDisplayDevice.this, new LineItemRenderer(57),
        new LineItemFilter());
    currentDeviceTranState_ = new DeviceTranState(new DefaultLineItemRenderer(0), null);
    return true;
  }

  /** {@inheritDoc} */
  @Override
  public void handleDisplayStateChange(DeviceTranState argTransState) {
    if (anyChanges(argTransState)) {
      IAuthProcess process = getAuthProcessor();
      if (process != null) {
        process.processRequest(makeRequest(argTransState));
      }
      currentDeviceTranState_ = argTransState;
    }
  }

  /** {@inheritDoc} */
  @Override
  public void handleTenderLineCanceled() {

  }

  /** {@inheritDoc} */
  @Override
  public void handleDisplayStateClear() {
    IAuthProcess process = getAuthProcessor();
    if (process != null) {
      IAuthRequest request =
          AuthFactory.getInstance().makeAuthRequest(getAuthMethodCode(), AuthRequestType.get("CLEAR"), null);
      process.processRequest(request);
    }
  }

  /**
   * Get the auth processor.
   * 
   * @return
   */
  protected IAuthProcess getAuthProcessor() {
    if (AuthFactory.getInstance().getAuthProcess(getAuthMethodCode()) instanceof AjbFiPayGenericProcess) {
      return AuthFactory.getInstance().getAuthProcess(getAuthMethodCode());
    }
    else {
      logger_
          .warn("There is no special display process be found by auth method [" + getAuthMethodCode() + "].");
      return null;
    }
  }

  /**
   * Create auth request.
   * 
   * @param argState
   * @return
   */
  protected IAuthRequest makeRequest(DeviceTranState argState) {
    IAuthRequest request = AuthFactory.getInstance().makeAuthRequest(getAuthMethodCode(),
        AuthRequestType.get("RECEIPT"), new Object[] {argState});
    return request;
  }

  /**
   * Get auth method code.
   * 
   * @return
   */
  protected String getAuthMethodCode() {
    return "AJB_ROLLING_RECEIPT";
  }

  protected boolean anyChanges(DeviceTranState argState) {
    IStateChange[] changes = argState.getChanges(currentDeviceTranState_);
    return changes.length > 0;
  }

}
