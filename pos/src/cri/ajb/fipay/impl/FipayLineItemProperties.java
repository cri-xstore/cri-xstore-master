//$Id: FipayLineItemProperties.java 1263 2018-10-11 23:00:21Z johgaug $
package cri.ajb.fipay.impl;

import java.util.ArrayList;
import java.util.List;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2016 OLR Retail
 *
 * @author johgaug
 * @created Jun 27, 2016
 * @version $Revision: 1263 $
 */
public enum FipayLineItemProperties {
  ACCOUNT_BALANCE("ACCOUNT_BALANCE"), SAFd("SAF_STATUS"), CERTIGY_UID("CERTEGY_UID"),
  MASKED_ACCOUNT_NUMBER("MASKED_ACCOUNT_NUMBER"), CHECK_CODE("CHECK_RESPONSE_CODE"), CHECK_FEE("CHECK_FEE"),
  TRACE_NUMBER("TRACE_NUMBER"), SIGNATURE_CAPTURE("SIGNATURE_CAPTURED"), INVALID("INVALID"),
  CODE_4F("4F", "AID", true), CODE_8A("8A", "ARC", true), CODE_95("95", "TVR", true),
  CODE_9B("9B", "TSI", true), CODE_9F10("9F10", "IAD", true), CODE_9F34("9F34", "9F34"),;

  public static FipayLineItemProperties forCode(String argCode) {
    FipayLineItemProperties result = INVALID;
    for (FipayLineItemProperties v : values()) {
      if (v.matches(argCode)) {
        result = v;
        break;
      }
    }
    return result;
  }

  public static List<FipayLineItemProperties> listEmv() {
    List<FipayLineItemProperties> candidates = new ArrayList<FipayLineItemProperties>();
    for (FipayLineItemProperties v : values()) {
      if (v.isEmvTag()) {
        candidates.add(v);
      }
    }
    return candidates;
  }

  public static List<FipayLineItemProperties> listEmvReceipt() {
    List<FipayLineItemProperties> candidates = new ArrayList<FipayLineItemProperties>(values().length);
    for (FipayLineItemProperties v : values()) {
      if (v.isEmvReceipt()) {
        candidates.add(v);
      }
    }
    return candidates;
  }

  private final String code_;
  private final String name_;
  private final boolean emv_;
  private final boolean receipt_;

  /** Constructs a <code>FipayLineItemProperties</code>. */
  private FipayLineItemProperties(String argCode) {
    this(argCode, argCode, false, false);
  }

  private FipayLineItemProperties(String argCode, String argName) {
    this(argCode, argName, true, false);
  }

  private FipayLineItemProperties(String argCode, String argName, Boolean argIsReceipt) {
    this(argCode, argName, true, argIsReceipt);

  }

  private FipayLineItemProperties(String argCode, String argName, Boolean argIsEmv, Boolean argIsReceipt) {
    code_ = argCode;
    name_ = argName;
    emv_ = argIsEmv;
    receipt_ = argIsReceipt;
  }

  /** Get the code. */
  public String getCode() {
    return code_;
  }

  /** Get the Name */
  public String getName() {
    return name_;
  }

  public boolean isEmvReceipt() {
    return receipt_;
  }

  public boolean isEmvTag() {
    return emv_;
  }

  /** Get whether this instances matches the provided error code. */
  public boolean matches(String argCode) {
    return code_.equals(argCode);
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    return code_;
  }

}
