//$Id: AjbFiPayVerifySignatureOp.java 874 2015-07-13 19:24:07Z suz.sxie $
package cri.ajb.fipay.op;

import dtv.pos.tender.ISaleTenderCmd;
import dtv.tenderauth.*;
import dtv.tenderauth.op.IAuthorizationCmd;
import dtv.tenderauth.storedvalue.VoidTenderAuthorizationOp;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.ttr.*;

/**
 * Operation to void a previous authorization.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 *
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 875 $
 */
public class AjbFiPayVoidTenderAuthorizationOp
    extends VoidTenderAuthorizationOp {

  /**
   * 
   */
  private static final long serialVersionUID = -6049474912729166764L;

  /** {@inheritDoc} */
  @Override
  protected IAuthRequest getAuthRequest(IAuthorizationCmd argCmd) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    IAuthorizableTenderLineItem tli = (IAuthorizableTenderLineItem) cmd.getTenderLineItem();
    String authMethodCode = getAuthMethodCode(tli);
    AuthRequestType authReqType = getAuthReqType(tli);

    boolean isVoucher = (tli instanceof IVoucherTenderLineItem);
    IAuthRequest request =
        AuthFactory.getInstance().makeAuthRequest(authMethodCode, authReqType, tli, isVoucher);

    return request;
  }

  /** {@inheritDoc} */
  @Override
  protected String getAuthMethodCode(ITenderLineItem argLineItem) {
    ITender tender = argLineItem.getTender();
    return (tender == null) ? null : tender.getAuthMethodCode();
  }
}
