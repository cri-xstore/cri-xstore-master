//$Id: AjbFiPaySafAuthorizeOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import java.util.Arrays;
import java.util.List;

import cri.ajb.fipay.response.AjbFiPayCreditDebitResponse;
import cri.pos.tender.CriSaleTenderCmd;
import cri.tenderauth.CriAuthFactory;

import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.op.AbstractAuthorizeOp;
import dtv.tenderauth.op.IAuthorizationCmd;

/**
 * AJB SAF request approval when the bank is offline get the temp token key<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Sep 24, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPaySafAuthorizeOp
    extends AbstractAuthorizeOp {
  /**
   * 
   */
  private static final long serialVersionUID = 1169842028787380529L;
  protected static final ITenderAuthHelper AUTH_HELPER = TenderAuthHelper.getInstance();
  protected static final int ACTION_CODE_SAF_REQUEST = 2;
  protected static final int ACTION_CODE_BANK_DOWN = 3;
  protected static final int ACTION_CODE_TIMED_OUT = 10;

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    if (!(argCmd instanceof CriSaleTenderCmd)) {
      return false;
    }
    IAuthResponse response = getAuthResponse((CriSaleTenderCmd) argCmd);

    if (response instanceof AjbFiPayCreditDebitResponse) {
      return isSAFApplicable((AjbFiPayCreditDebitResponse) response);
    }

    return false;
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthProcess getAuthProcessor(IAuthorizationCmd argCmd) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    return AUTH_HELPER.getAuthProcess(cmd.getTender());
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthRequest getAuthRequest(IAuthorizationCmd argCmd) {
    CriSaleTenderCmd cmd = (CriSaleTenderCmd) argCmd;
    IAuthRequest preRequest = cmd.getAuthRequest();
    IAuthResponse response = getAuthResponse(cmd);

    String authCode = null;
    if (preRequest.getMoreAuthInfo() != null) {
      authCode = preRequest.getMoreAuthInfo().getInputFieldValue(AuthInfoFieldKey.AUTH_NUMBER, String.class);
    }

    return ((CriAuthFactory) AuthFactory.getInstance()).makeAuthRequest("AJB_CREDIT_DEBIT",
        AuthRequestType.forName("SAF"), response, authCode);
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleTrainingMode(IAuthorizationCmd argCmd) {
    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleVoid(IAuthorizationCmd argCmd) {
    return HELPER.completeResponse();
  }

  /**
   * Get the Auth Response.
   * 
   * @param argCmd
   * @return
   */
  protected IAuthResponse getAuthResponse(CriSaleTenderCmd argCmd) {
    return argCmd.getSAFAuthRequiredResponse();
  }

  /**
   * Check if we should send SAF request
   * 
   * @param response
   * @return
   */
  protected boolean isSAFApplicable(AjbFiPayCreditDebitResponse response) {
    int actionCode = response.getActionCode();
    List<Integer> safActionCodes =
        Arrays.asList(ACTION_CODE_SAF_REQUEST, ACTION_CODE_BANK_DOWN, ACTION_CODE_TIMED_OUT);

    if (response.isSAFAvailable() && safActionCodes.contains(actionCode)) {
      return true;
    }

    return false;
  }

}
