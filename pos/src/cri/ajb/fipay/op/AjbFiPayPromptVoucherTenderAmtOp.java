//$Id: AjbFiPayPromptVoucherTenderAmtOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import static dtv.pos.iframework.type.VoucherActivityCodeType.ISSUED;
import static dtv.util.NumberUtils.nonNull;

import java.math.BigDecimal;

import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.voucher.PromptVoucherTenderAmtOp;
import dtv.xst.dao.ttr.IVoucher;
import dtv.xst.dao.ttr.IVoucherTenderLineItem;

/**
 * This operation handles prompting the user to enter the voucher tender amount and validation on
 * the amount entered.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayPromptVoucherTenderAmtOp
    extends PromptVoucherTenderAmtOp {

  /**
   * 
   */
  private static final long serialVersionUID = 3437085813383614671L;

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    IOpResponse response = super.handlePromptResponse(argCmd, argEvent);

    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    IVoucher voucher = cmd.getVoucher();

    BigDecimal unspentBalance =
        nonNull(voucher.getUnspentBalanceAmount()).add(cmd.getTenderLineItem().getAmount().negate());

    voucher.setUnspentBalanceAmount(unspentBalance);

    if (cmd.getVoucherActivityCodeType().equals(ISSUED)) {
      if (cmd.getTenderLineItem() instanceof IVoucherTenderLineItem) {
        ((IVoucherTenderLineItem) cmd.getTenderLineItem()).setFaceValueAmount(unspentBalance);
      }
      voucher.setFaceValueAmount(cmd.getTenderLineItem().getAmount());
    }

    return response;
  }
}