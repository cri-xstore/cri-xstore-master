//$Id: AjbFiPayPingOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.register.SaleItemCmd;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.op.AbstractAuthorizeOp;
import dtv.tenderauth.op.IAuthorizationCmd;
import dtv.tenderauth.voiding.IReverseTenderAuthCmd;
import dtv.xst.dao.trl.IVoucherLineItem;
import dtv.xst.dao.trl.IVoucherSaleLineItem;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * FiPay connection test OP.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayPingOp
    extends AbstractAuthorizeOp {

  /**
   * 
   */
  private static final long serialVersionUID = -1229108299059535280L;

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    if (argCmd instanceof ISaleTenderCmd) {
      ITenderLineItem tenderLineItem = ((ISaleTenderCmd) argCmd).getTenderLineItem();
      return tenderLineItem.getTender().getAuthRequired();
    }

    if (argCmd instanceof SaleItemCmd) {
      IVoucherLineItem voucherLineItem = ((SaleItemCmd) argCmd).getVoucherLineItem();
      return voucherLineItem instanceof IVoucherSaleLineItem;
    }

    if (argCmd instanceof IReverseTenderAuthCmd) {
      return ((IReverseTenderAuthCmd) argCmd).getTenderLine() != null;
    }

    return false;
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthProcess getAuthProcessor(IAuthorizationCmd argCmd) {
    return AuthFactory.getInstance().getAuthProcess(getAuthMethodCode(argCmd));
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthRequest getAuthRequest(IAuthorizationCmd argCmd) {
    return AuthFactory.getInstance().makeAuthRequest(getAuthMethodCode(argCmd), AuthRequestType.get("PING"),
        new Object[] {null});
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleTrainingMode(IAuthorizationCmd argCmd) {
    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleVoid(IAuthorizationCmd argCmd) {
    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleAuthResponse(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    if (argResponse.isSuccess()) {
      return handleSuccess(argCmd, argResponse);
    }
    if (isNeedingMoreInfo(argResponse)) {
      return handleNeedMoreInfo(argCmd, argResponse);
    }
    else {
      return handleFailed(argCmd, argResponse);
    }
  }

  protected String getAuthMethodCode(IXstCommand argCmd) {
    return "AJB_FIPAY_PING";
  }
}
