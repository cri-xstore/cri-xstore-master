//$Id: AjbFiPayCreateVoucherSaleLineItemOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.register.ISaleItemCmd;
import dtv.pos.register.nonphysical.CreateVoucherSaleLineItemOp;

/**
 * AJB voucher sale line item creation OP.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 *
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayCreateVoucherSaleLineItemOp
    extends CreateVoucherSaleLineItemOp {

  /**
   * 
   */
  private static final long serialVersionUID = -3329022671057033038L;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    ISaleItemCmd cmd = (ISaleItemCmd) argCmd;
    IOpResponse response = super.handleOpExec(argCmd, argEvent);
    cmd.getVoucherLineItem().setVoid(true);
    return response;
  }
}
