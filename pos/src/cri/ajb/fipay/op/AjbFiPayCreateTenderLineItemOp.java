//$Id: AjbFiPayCreateTenderLineItemOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.CreateTenderLineItemOp;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * AJB tender line item creation OP.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayCreateTenderLineItemOp
    extends CreateTenderLineItemOp {

  /**
   * 
   */
  private static final long serialVersionUID = -2327744487263933826L;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IOpResponse response = super.handleOpExec(argCmd, argEvent);

    //don't enable the credit/debit tender until get the approved response from AJB FiPay.
    ITenderLineItem lineItem = ((ISaleTenderCmd) argCmd).getTenderLineItem();
    if (lineItem != null) {
      lineItem.setVoid(true);
    }
    return response;
  }

}
