//$Id: TransactionDisplayModel.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb;

import static dtv.hardware.custdisplay.IDtvCustDisplayDevice.STATION_MODEL_EVENT_DESCRIPTOR;

import java.util.List;

import cri.ajb.fipay.op.AjbFiPayRollingReceiptStartOp;
import cri.ajb.state.DeviceTranState;
import cri.pos.common.CriConfigurationMgr;

import dtv.data2.access.IDataModel;
import dtv.event.*;
import dtv.event.constraint.*;
import dtv.hardware.sigcap.state.ILineItemRenderer;
import dtv.pos.iframework.type.AttributeKey;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.daocommon.ILineItemFilter;
import dtv.xst.daocommon.IRetailTransactionManager;

/**
 * Rolling receipt data model.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class TransactionDisplayModel
    implements IEventSource {
  private static final IEventConstraint currentTransConstraint_ =
      Constraints.and(new NameConstraint(AttributeKey.CURRENT_TRANSACTION), PayloadConstraint.empty());
  private static final IEventConstraint transUpdateConstraint_ =
      new NameConstraint(IRetailTransactionManager.TRANSACTION_UPDATED);
  private static final IEventConstraint tenderLineCancelConstraint_ =
      Constraints.and(new NameConstraint(AttributeKey.TENDER_INPUT_EVENT), PayloadConstraint.empty());
  private static final IEventConstraint transLineAddedConstraint_ =
      new NameConstraint(IPosTransaction.ADD_RETAILTRANSACTIONLINEITEMS);

  private final IDisplayChangeListener listener_;
  private final ILineItemRenderer lineRenderer_;
  private final ILineItemFilter lineItemFilter_;

  /**
   * Constructor method.
   * 
   * @param argListener
   * @param argLineRenderer
   * @param argLineFilter
   */
  public TransactionDisplayModel(IDisplayChangeListener argListener, ILineItemRenderer argLineRenderer,
      ILineItemFilter argLineFilter) {

    listener_ = argListener;
    lineRenderer_ = argLineRenderer;
    lineItemFilter_ = argLineFilter;

    // register for notifications from the station model
    EventManager.registerEventHandler(currentTransHandler_, STATION_MODEL_EVENT_DESCRIPTOR,
        currentTransConstraint_);
    EventManager.registerEventHandler(currentTransHandler_,
        AjbFiPayRollingReceiptStartOp.EMPTY_TRANSACTION_UPDATE, false);
    EventManager.registerEventHandler(transUpdatedHandler_, STATION_MODEL_EVENT_DESCRIPTOR,
        transUpdateConstraint_);
    EventManager.registerEventHandler(tenderLineCancelHandler_, STATION_MODEL_EVENT_DESCRIPTOR,
        tenderLineCancelConstraint_);
  }

  // An event handler for processing transaction assignments.
  private final EventHandler currentTransHandler_ = new EventHandler() {
    /** {@inheritDoc} */
    @Override
    protected void handle(Event argEvent) {
      handleTransactionUpdated(argEvent, null);
    }
  };

  // An event handler for processing changes to the current transaction.
  private final EventHandler transUpdatedHandler_ = new EventHandler() {
    /** {@inheritDoc} */
    @Override
    protected void handle(Event argEvent) {
      handleTransactionUpdated(argEvent, argEvent.getSource(IPosTransaction.class));
    }
  };

  // An event handler for processing the cancellation of tender lines.
  private final EventHandler tenderLineCancelHandler_ = new EventHandler() {
    /** {@inheritDoc} */
    @Override
    protected void handle(Event argEvent) {
      handleTenderLineCanceled(argEvent, argEvent.getSource(IPosTransaction.class));
    }
  };

  /*  private final EventHandler transCompleteHandler_ = new EventHandler() {
    *//** {@inheritDoc} *//*
                          @Override
                          protected void handle(Event argEvent) {
                          handleTransactionCompleted(argEvent, null);
                          }
                          };*/

  /**
   * Handle the transaction data update response.
   * 
   * @param argEvent
   * @param transaction
   */
  protected void handleTransactionUpdated(Event argEvent, IPosTransaction transaction) {
    DeviceTranState state = new DeviceTranState(lineRenderer_, transaction);
    if (transaction != null) {
      EventManager.registerEventHandler(transUpdatedHandler_, transaction, transLineAddedConstraint_);
      List<? extends IDataModel> items = transaction.getRetailTransactionLineItems();
      items = lineItemFilter_.filter(items);

      //AC#470900 - Update Pin Pad Display to Show Last 30 Items Scanned.
      int i = 0;
      int verifoneDisplayItemCount = CriConfigurationMgr.getMaxVerifoneDisplayItemCount();
      if (items.size() > verifoneDisplayItemCount) {
        i = items.size() - verifoneDisplayItemCount;
      }

      for (; i < items.size(); i++ ) {
        state.addLineItem(items.get(i));
      }
      state.setSubtotal(transaction.getSubtotal());
      state.setTotal(transaction.getTotal());
      state.setTax(transaction.getTaxAmount());
      listener_.handleDisplayStateChange(state);
    }
    else {
      state.setSubtotal(null);
      listener_.handleDisplayStateClear();
    }
  }

  protected void handleTransactionCompleted(Event argEvent, IPosTransaction transaction) {
    listener_.handleDisplayStateClear();
  }

  /**
   * Handle the tender cancel response.
   * 
   * @param argEvent
   * @param transaction
   */
  protected void handleTenderLineCanceled(Event argEvent, IPosTransaction transaction) {
    listener_.handleTenderLineCanceled();
  }
}
