//$Id: IDisplayChangeListener.java 870 2015-07-07 16:59:17Z suz.sxie $
package cri.ajb;

import cri.ajb.state.DeviceTranState;

/**
 * Transaction data change listener.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 *
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 870 $
 */
public interface IDisplayChangeListener {

  /**
   * handle the transaction change event.
   *
   * @param argTransState
   */
  public void handleDisplayStateChange(DeviceTranState argTransState);

  /**
   * handle the tender cancel event.
   */
  public void handleTenderLineCanceled();

  /**
   * Clean the device.
   *
   */
  public void handleDisplayStateClear();
}
