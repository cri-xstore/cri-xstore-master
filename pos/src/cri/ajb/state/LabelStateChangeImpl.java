//$Id: LabelStateChangeImpl.java 872 2015-07-08 19:39:02Z suz.sxie $
package cri.ajb.state;

/**
 * Label state change impl.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 872 $
 */
public class LabelStateChangeImpl
    extends AbstractStateChange {

  /**
   * Constructor method.
   * 
   * @param argNewText
   */
  protected LabelStateChangeImpl(String argNewText) {
    super(argNewText);
  }
}
