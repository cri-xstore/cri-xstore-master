PRINT '$Id: CRIModelUpdate.sql 902 2015-07-30 19:26:41Z suz.sxie $';
PRINT '$URL: https://node1000.gspann.local:8443/svn/20181115-cri/branches/fipay_cust_name/pos/database/CRIModelUpdate.sql $';

-- ************************************************************************************************
--
-- This script contains updates to the database schema such as tables, columns, views, triggers and
-- procedures. It should apply data model changes only without changing underlying data.
--
-- ************************************************************************************************

IF OBJECT_ID('cri_deposit_bag') IS NULL
BEGIN
	PRINT '  - Creating table: cri_deposit_bag';
	CREATE TABLE dbo.cri_deposit_bag(
      organization_id           int             NOT NULL,
      rtl_loc_id                int             NOT NULL,
      bag_seq                   bigint          NOT NULL,
      number                    varchar(60),
      amount                    decimal(17, 6),
      type                      varchar(30),
      status                    varchar(30),
      lost_reason_code          varchar(30),
      lost_comments             varchar(255),
      conveyance_date_timestamp datetime,
      employee_id               varchar(30),
      last_change_employee_id   varchar(30),
      wkstn_id                  bigint,
      create_date_timestamp     datetime,
      create_date               datetime,
      create_user_id            varchar(20),
      update_date               datetime,
      update_user_id            varchar(20),
      record_state              varchar(30),
		CONSTRAINT pk_cri_deposit_bag PRIMARY KEY CLUSTERED (organization_id, rtl_loc_id, bag_seq));
END
GO

IF NOT EXISTS (Select * From sys.syscolumns Where name = 'business_date' And id = OBJECT_ID('cri_deposit_bag'))
  ALTER TABLE cri_deposit_bag ADD business_date datetime;
GO


IF OBJECT_ID('cri_deposit_bag_detail') IS NULL
BEGIN
	PRINT '  - Creating table: cri_deposit_bag_detail';
	CREATE TABLE dbo.cri_deposit_bag_detail(
      organization_id           int             NOT NULL,
      rtl_loc_id                int             NOT NULL,
      bag_seq                   bigint          NOT NULL,
      bag_tender_seq            int             NOT NULL,
      amount                    decimal(17, 6),
      description               varchar(60),
      create_date               datetime,
      create_user_id            varchar(20),
      update_date               datetime,
      update_user_id            varchar(20),
      record_state              varchar(30),
		CONSTRAINT pk_cri_deposit_bag_detail PRIMARY KEY CLUSTERED (organization_id, rtl_loc_id, bag_seq, bag_tender_seq));
END
GO

--alter table rpt_sales_by_hour

IF NOT EXISTS (Select * From sys.syscolumns Where name = 'cri_price_override' And id = OBJECT_ID('rpt_sales_by_hour'))
  ALTER TABLE rpt_sales_by_hour ADD cri_price_override decimal(17,6);
GO

IF NOT EXISTS (Select * From sys.syscolumns Where name = 'cri_discount_amt' And id = OBJECT_ID('rpt_sales_by_hour'))
  ALTER TABLE rpt_sales_by_hour ADD cri_discount_amt decimal(17,6);
GO


--alter table rpt_sale_line

IF NOT EXISTS (Select * From sys.syscolumns Where name = 'cri_price_override_amt' And id = OBJECT_ID('rpt_sale_line'))
  ALTER TABLE rpt_sale_line ADD cri_price_override_amt decimal(17,6);
GO

-- Activity # 391035
-- Alter table rpt_sales_by_hour
-- Add this column to save price manual change amount
IF NOT EXISTS (Select * From sys.syscolumns Where name = 'cri_price_manual_change' And id = OBJECT_ID('rpt_sales_by_hour'))
  ALTER TABLE rpt_sales_by_hour ADD cri_price_manual_change decimal(17,6);
GO

IF OBJECT_ID('cri_dsc_discount_type') IS NULL
BEGIN
	PRINT '  - Creating table: cri_dsc_discount_type';
	CREATE TABLE dbo.cri_dsc_discount_type(
    organization_id  int             NOT NULL,
    discount_code    varchar(30)     NOT NULL,
    discount_type    varchar(30)     NOT NULL,
    org_code         varchar(30)     NOT NULL,
    org_value		 	   varchar(60)     NOT NULL,
    create_date      datetime        NULL,
    create_user_id   varchar(20)     NULL,
    update_date      datetime        NULL,
    update_user_id   varchar(20)     NULL,
    record_state     varchar(30)     NULL,
    CONSTRAINT pk_cri_dsc_discount_type PRIMARY KEY CLUSTERED (organization_id, discount_code, discount_type, org_code, org_value));
END
GO

IF EXISTS (Select * From sysobjects Where name = 'sp_ins_upd_hourly_sales' and type = 'P')
DROP PROCEDURE dbo.sp_ins_upd_hourly_sales;
GO
CREATE PROCEDURE dbo.sp_ins_upd_hourly_sales (
    @argOrgId int,
    @argRtlLocId int,
    @argBusinessDate datetime,
    @argWkstnId bigint,
    @argHour datetime,
    @argQty decimal(11, 2),
    @argNetAmt decimal(17, 6),
    @argGrossAmt decimal(17, 6),
    @argTransCount int,
    @argCurrencyId varchar(3) = 'USD',
    @argPriceOverrideAmt decimal(17, 6), -- Activity # 391035
    @argDiscountAmt decimal(17, 6), -- Activity # 391035
    @argManualPriceChangeAmt decimal(17, 6)) -- Activity # 391035
AS
  UPDATE rpt_sales_by_hour
    SET qty = qty + @argQty,
        trans_count = trans_count + @argTransCount,
        net_sales = net_sales + @argNetAmt,
        gross_sales = gross_sales + @argGrossAmt,
        update_date = getdate(),
        update_user_id = user,
        cri_price_override = cri_price_override + @argPriceOverrideAmt,
        cri_discount_amt = cri_discount_amt + @argDiscountAmt,
        cri_price_manual_change = cri_price_manual_change + @argManualPriceChangeAmt
    WHERE organization_id = @argOrgId
      AND rtl_loc_id = @argRtlLocId
      AND wkstn_id = @argWkstnId
      AND business_date = @argBusinessDate
      AND hour = datepart(hh, @argHour);

IF @@ROWCOUNT = 0
  INSERT INTO rpt_sales_by_hour (organization_id, rtl_loc_id, wkstn_id, hour, qty, trans_count,
      net_sales, business_date, gross_sales, currency_id, create_date, create_user_id, cri_price_override, cri_discount_amt, cri_price_manual_change)
    VALUES
      (@argOrgId, @argRtlLocId, @argWkstnId, datepart(hh, @argHour), @argQty, @argTransCount,
      @argNetAmt ,@argBusinessDate, @argGrossAmt, @argCurrencyId, getdate(), user, @argPriceOverrideAmt, @argDiscountAmt, @argManualPriceChangeAmt);

GO

IF EXISTS (Select * From sysobjects Where name = 'sp_flash' and type = 'P')
DROP PROCEDURE dbo.sp_flash;
GO
CREATE PROCEDURE dbo.sp_flash (
    @argOrgId int,
    @argRtlLocId int,
    @argBusinessDate datetime,
    @argWrkstnId bigint,
    @argTransSeq bigint)
AS
  DECLARE -- Quantities
      @vActualQuantity decimal(11, 2),
      @vGrossQuantity decimal(11, 2),
      @vQuantity decimal(11, 2),
      @vTotQuantity decimal(11, 2);

  DECLARE -- Amounts
      @vNetAmount decimal(17, 6),
      @vGrossAmount decimal(17, 6),
      @vTotGrossAmt decimal(17, 6),
      @vTotNetAmt decimal(17, 6),
      @vDiscountAmt decimal(17, 6),
      @vCriPriceOverrideAmt decimal(17, 6), -- Activity # 391035
      @vTotCriPriceOverrideAmt decimal(17, 6), -- Activity # 391035
      @vTotCriDiscountAmt decimal(17, 6), -- Activity # 391035
      @vTotCriManualPriceChangeAmt decimal(17, 6), -- Activity # 391035
      @vOverrideAmt decimal(17, 6),
      @vPaidAmt decimal(17, 6),
      @vTenderAmt decimal(17, 6),
      @vForeign_amt decimal(17, 6),
      @vLayawayPrice decimal(17, 6),
      @vUnitPrice decimal(17, 6);


  DECLARE -- Non Physical Items
      @vNonPhys varchar(30),
      @vNonPhysSaleType varchar(30),
      @vNonPhysType varchar(30),
      @vNonPhysPrice decimal(17, 6),
      @vNonPhysQuantity decimal(11, 2);

  DECLARE -- Status codes
      @vTransStatcode varchar(30),
      @vTransTypcode varchar(30),
      @vSaleLineItmTypcode varchar(30),
      @vTndrStatcode varchar(60),
      @vLineitemStatcode varchar(30);

  DECLARE -- Misc.
      @vTransTimeStamp datetime,
      @vTransCount int,
      @vTndrCount int,
      @vPostVoidFlag bit,
      @vReturnFlag bit,
      @vTaxTotal decimal(17, 6),
      @vPaid varchar(30),
      @vLineEnum varchar(150),
      @vTndrId varchar(60),
      @vItemId varchar(60),
      @vRtransLineItmSeq int,
      @vDepartmentId varchar(90),
      @vCurrencyId varchar(3);

  DECLARE
      @vSerialNbr varchar(60),
      @vPriceModAmt decimal(17, 6),
      @vPriceModExtAmt decimal(17, 6),
      @vPriceModReascode varchar(60),
      @vNonPhysExcludeFlag bit,
      @vCustPartyId varchar(60),
      @vCustLastName varchar(90),
      @vCustFirstName varchar(90),
      @vItemDesc varchar(120),
      @vBeginTimeInt int;

  --load transaction data
  SELECT @vTransStatcode = tt.trans_statcode,
         @vTransTypcode = tt.trans_typcode,
         @vTransTimeStamp = tt.begin_datetime,
         @vTaxTotal = tt.taxtotal,
         @vPostVoidFlag = tt.post_void_flag,
         @vBeginTimeInt = tt.begin_time_int,
         @vCurrencyId =
			CASE
				WHEN rl.currency_id IS NOT NULL THEN rl.currency_id
				ELSE 'USD'
			END
    FROM trn_trans tt with (nolock)
    LEFT JOIN loc_rtl_loc rl on tt.organization_id = rl.organization_id and tt.rtl_loc_id = rl.rtl_loc_id
    WHERE tt.organization_id = @argOrgId
      AND tt.rtl_loc_id = @argRtlLocId
      AND tt.wkstn_id = @argWrkstnId
      AND tt.business_date = @argBusinessDate
      AND tt.trans_seq = @argTransSeq;

  IF @@ROWCOUNT = 0
    RETURN;  -- Invalid transaction

  SET @vTransCount = 1;

  -- update trans
  UPDATE trn_trans
    SET flash_sales_flag = 1
    WHERE organization_id = @argOrgId
      AND rtl_loc_id = @argRtlLocId
      AND wkstn_id = @argWrkstnId
      AND trans_seq = @argTransSeq
      AND business_date = @argBusinessDate;

  -- collect transaction data
  IF ABS(@vTaxTotal) > 0 AND (@vTransTypcode <> 'POST_VOID' AND @vPostVoidFlag = 0) AND @vTransStatcode = 'COMPLETE'
    EXEC sp_ins_upd_flash_sales
      @argOrgId, @argRtlLocId, @argBusinessDate,
      @argWrkstnId, 'TotalTax', 1, @vTaxTotal, @vCurrencyId;

  IF @vTransTypcode = 'TENDER_CONTROL' AND @vPostVoidFlag = 0 BEGIN -- process for paid in paid out
    SELECT @vPaid = typcode,
           @vPaidAmt = amt
      FROM tsn_tndr_control_trans WITH (NOLOCK)
      WHERE typcode like 'PAID%'
        AND organization_id = @argOrgId
        AND rtl_loc_id = @argRtlLocId
        AND wkstn_id = @argWrkstnId
        AND trans_seq = @argTransSeq
        AND business_date = @argBusinessDate;

    IF @@ROWCOUNT = 1 BEGIN    -- Paid In/Out
      IF @vPaid = 'PAID_IN' OR @vPaid = 'PAIDIN'
        SET @vLineEnum = 'paidin';
      ELSE
        SET @vLineEnum = 'paidout';

      -- update flash sales
      IF @vTransStatcode = 'COMPLETE'
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, @vLineEnum, 1, @vPaidAmt, @vCurrencyId;
    END
  END

  -- collect tenders  data
  IF @vPostVoidFlag = 0 BEGIN
    DECLARE tenderCursor CURSOR FOR
      SELECT t.amt, t.foreign_amt, t.tndr_id, t.tndr_statcode, r.currency_id
        FROM ttr_tndr_lineitm t WITH (NOLOCK)
          INNER JOIN trl_rtrans_lineitm r WITH (NOLOCK)
              ON t.organization_id = r.organization_id
              AND t.rtl_loc_id = r.rtl_loc_id
              AND t.wkstn_id = r.wkstn_id
              AND t.trans_seq = r.trans_seq
              AND t.business_date = r.business_date
              AND t.rtrans_lineitm_seq = r.rtrans_lineitm_seq
        WHERE t.organization_id = @argOrgId
          AND t.rtl_loc_id = @argRtlLocId
          AND t.wkstn_id = @argWrkstnId
          AND t.trans_seq = @argTransSeq
          AND t.business_date = @argBusinessDate
          AND r.void_flag = '0';

    OPEN tenderCursor;

    WHILE 1 = 1 BEGIN
      FETCH NEXT FROM tenderCursor INTO @vTenderAmt, @vForeign_amt, @vTndrid, @vTndrStatcode, @vCurrencyId;
      IF @@FETCH_STATUS <> 0
        BREAK;

      IF @vTndrStatcode <> 'Change'
        SET @vTndrCount = 1;  -- only for original tenders
      ELSE
        SET @vTndrCount = 0;

      IF @vLineEnum = 'paidout' BEGIN
        SET @vTenderAmt = COALESCE(@vTenderAmt, 0) * -1;
        SET @vForeign_amt = COALESCE(@vForeign_amt, 0) * -1;
      END

      -- update flash sales
      IF @vTransStatcode = 'COMPLETE'
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, @vTndrid, @vTndrCount, @vTenderAmt, @vCurrencyId;

      IF @vTenderAmt > 0 AND @vTransStatcode = 'COMPLETE'
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, 'TendersTakenIn', 1, @vTenderAmt, @vCurrencyId;
      ELSE
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, 'TendersRefunded', 1, @vTenderAmt, @vCurrencyId;
    END

    CLOSE tenderCursor;
    DEALLOCATE tendercursor;
  END

  -- collect post void info
  IF @vTransTypcode = 'POST_VOID' OR @vPostVoidFlag = 1 BEGIN
    SET @vTransCount = -1;

    IF @vPostVoidFlag = 0 BEGIN
      SET @vPostVoidFlag = 1;

      -- get the original post voided transaction and set it as original parameters
      SELECT @argOrgId = voided_org_id,
             @argRtlLocId = voided_rtl_store_id,
             @argWrkstnId = voided_wkstn_id,
             @argBusinessDate = voided_business_date,
             @argTransSeq = voided_trans_id
        FROM trn_post_void_trans WITH (NOLOCK)
        WHERE organization_id = @argOrgId
          AND rtl_loc_id = @argRtlLocId
          AND wkstn_id = @argWrkstnId
          AND business_date = @argBusinessDate
          AND trans_seq = @argTransSeq

      /* NOTE: From now on the parameter value carries the original post voided information rather
         than the current transaction information in case of post void trans type. This will apply
         for sales data processing. */

      IF @@ROWCOUNT = 0   -- don't know the original post voided record
        RETURN;
    END

    -- update the rpt sale line for post void
    UPDATE rpt_sale_line
      SET trans_statcode='VOID'
      WHERE organization_id = @argOrgId
        AND rtl_loc_id = @argRtlLocId
        AND wkstn_id = @argWrkstnId
        AND business_date = @argBusinessDate
        AND trans_seq = @argTransSeq;

    -- reverse paidin/paidout
    SELECT @vPaid = typcode,
           @vPaidAmt = amt
      FROM tsn_tndr_control_trans WITH (NOLOCK)
      WHERE typcode like 'PAID%'
        AND organization_id = @argOrgId
        AND rtl_loc_id = @argRtlLocId
        AND wkstn_id = @argWrkstnId
        AND trans_seq = @argTransSeq
        AND business_date = @argBusinessDate;

    IF @@ROWCOUNT = 1 BEGIN      -- Paid In/Out
      IF @vPaid = 'PAID_IN' OR @vPaid = 'PAIDIN'
        SET @vLineEnum = 'paidin'
      ELSE
        SET @vLineEnum = 'paidout';

      SET @vPaidAmt = @vPaidAmt * -1;

      -- update flash sales
      IF @vTransStatcode = 'COMPLETE'
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, @vLineEnum, -1, @vPaidAmt, @vCurrencyId;
    END

    -- reverse tax
    SELECT @vTaxTotal = taxtotal
      FROM trn_trans WITH (NOLOCK)
      WHERE organization_id = @argOrgId
        AND rtl_loc_id = @argRtlLocId
        AND wkstn_id = @argWrkstnId
        AND business_date = @argBusinessDate
        AND trans_seq = @argTransSeq;

    IF ABS(@vTaxTotal) > 0 AND @vTransStatcode = 'COMPLETE' BEGIN
      SET @vTaxTotal = @vTaxTotal * -1;
      EXEC sp_ins_upd_flash_sales
          @argOrgId, @argRtlLocId, @argBusinessDate,
          @argWrkstnId, 'TotalTax', -1, @vTaxTotal, @vCurrencyId;
    END

    -- reverse tenders
    DECLARE postVoidTenderCursor CURSOR FOR
      SELECT t.amt,
             t.foreign_amt,
             t.tndr_id,
             t.tndr_statcode,
             r.currency_id
        FROM ttr_tndr_lineitm t WITH (NOLOCK)
          INNER JOIN trl_rtrans_lineitm r WITH (NOLOCK)
              ON t.organization_id = r.organization_id
              AND t.rtl_loc_id = r.rtl_loc_id
              AND t.wkstn_id = r.wkstn_id
              AND t.trans_seq = r.trans_seq
              AND t.business_date = r.business_date
              AND t.rtrans_lineitm_seq = r.rtrans_lineitm_seq
        WHERE t.organization_id = @argOrgId
          AND t.rtl_loc_id = @argRtlLocId
          AND t.wkstn_id = @argWrkstnId
          AND t.trans_seq = @argTransSeq
          AND t.business_date = @argBusinessDate
          AND r.void_flag = '0';

    OPEN postVoidTenderCursor;

    WHILE 1 = 1 BEGIN
      FETCH NEXT FROM postVoidTenderCursor
        INTO @vTenderAmt, @vForeign_amt, @vTndrid, @vTndrStatcode, @vCurrencyId;

      IF @@FETCH_STATUS <> 0
        BREAK;

      IF @vTndrStatcode <> 'Change'
        SET @vTndrCount = -1;
      ELSE
        SET @vTndrCount = 0;

      -- update flash
      SET @vTenderAmt = @vTenderAmt * -1;

      IF @vTransStatcode = 'COMPLETE'
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, @vTndrid, @vTndrCount, @vTenderAmt, @vCurrencyId;

      IF @vTenderAmt < 0 AND @vTransStatcode = 'COMPLETE'
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, 'TendersTakenIn', -1, @vTenderAmt, @vCurrencyId;
      ELSE
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, 'TendersRefunded', -1, @vTenderAmt, @vCurrencyId;
    END

    CLOSE postVoidTenderCursor;
    DEALLOCATE postVoidTenderCursor;
  END

  -- collect sales data
  -- loop through all the line item inside the transaction sequence

  DECLARE saleCursor CURSOR FOR
    SELECT tsl.rtrans_lineitm_seq, tsl.quantity, tsl.net_quantity, tsl.unit_price, tsl.net_amt,
           tsl.gross_amt, tsl.item_id, tsl.serial_nbr, tsl.return_flag, tsl.sale_lineitm_typcode,
           tsl.gross_quantity, r.currency_id
      FROM trl_sale_lineitm tsl WITH (NOLOCK)
        INNER JOIN trl_rtrans_lineitm r WITH (NOLOCK)
            ON tsl.organization_id = r.organization_id
            AND tsl.rtl_loc_id = r.rtl_loc_id
            AND tsl.wkstn_id = r.wkstn_id
            AND tsl.trans_seq = r.trans_seq
            AND tsl.business_date = r.business_date
            AND tsl.rtrans_lineitm_seq = r.rtrans_lineitm_seq
            AND r.rtrans_lineitm_typcode = 'ITEM'
      WHERE tsl.organization_id = @argOrgId
        AND tsl.rtl_loc_id = @argRtlLocId
        AND tsl.wkstn_id = @argWrkstnId
        AND tsl.business_date = @argBusinessDate
        AND tsl.trans_seq = @argTransSeq
        AND r.void_flag = 0;

  OPEN saleCursor;

  WHILE 1 = 1 BEGIN
    FETCH NEXT FROM saleCursor
      INTO @vRtransLineItmSeq, @vActualQuantity, @vQuantity, @vUnitPrice, @vNetAmount, @vGrossAmount,
           @vItemId, @vSerialNbr, @vReturnFlag, @vSaleLineitmTypcode, @vGrossQuantity, @vCurrencyId;

    IF @@FETCH_STATUS <> 0
      BREAK;

    -- initialize
    SET @vOverrideAmt = 0;
    SET @vDiscountAmt = 0;
    SET @vCriPriceOverrideAmt = 0; -- Activity # 391035

    -- get discounts / overrides
    DECLARE cur_rpt_price_mod CURSOR FOR
      SELECT amt, extended_amt, rtl_price_mod_reascode
        FROM trl_rtl_price_mod WITH (NOLOCK)
        WHERE organization_id = @argOrgId
          AND rtl_loc_id = @argRtlLocId
          AND business_date = @argBusinessDate
          AND wkstn_id = @argWrkstnId
          AND trans_seq = @argTransSeq
          AND rtrans_lineitm_seq = @vRtransLineItmSeq
          AND void_flag = 0
        ORDER BY rtl_price_mod_seq_nbr;

    OPEN cur_rpt_price_mod;

    WHILE 1 = 1 BEGIN
      FETCH NEXT FROM cur_rpt_price_mod INTO @vPriceModAmt, @vPriceModExtAmt, @vPriceModReascode;

      IF @@FETCH_STATUS <> 0
        BREAK;

      IF @vPriceModReascode = 'PRICE_OVERRIDE'
        SET @vOverrideAmt = @vPriceModAmt * @vQuantity; -- always replace so to get the last price override amount
      ELSE IF @vPriceModReascode = 'LINE_ITEM_DISCOUNT' OR @vPriceModReascode = 'TRANSACTION_DISCOUNT'
        SET @vDiscountAmt = COALESCE(@vDiscountAmt, 0) + @vPriceModExtAmt;
      ELSE IF @vPriceModReascode = 'NEW_PRICE_RULE' OR @vPriceModReascode = 'DEAL'
        SET @vCriPriceOverrideAmt = COALESCE(@vCriPriceOverrideAmt, 0) + @vPriceModExtAmt; -- Activity # 391035
      END  -- end of price modifier

    CLOSE cur_rpt_price_mod;
    DEALLOCATE cur_rpt_price_mod;

    -- Get item info
    SELECT @vItemDesc = description,
           @vDepartmentId = department_id
      FROM itm_item WITH (NOLOCK)
      WHERE organization_id = @argOrgId
        AND item_id = @vItemId;

    IF @@ROWCOUNT = 0
      SET @vItemDesc = '';

    -- department id defaulting
    IF @vDepartmentId IS NULL
      SET @vDepartmentId = 'DEFAULT';

    IF @vPostVoidFlag = 0 BEGIN -- dont do it for rpt sale line
      -- Ignore for non phys type payment and deposit
      SELECT @vNonPhysExcludeFlag = exclude_from_net_sales_flag
        FROM itm_non_phys_item WITH (NOLOCK)
        WHERE organization_id = @argOrgId
          AND item_id = @vItemId;

      IF @@ROWCOUNT = 0 OR @vNonPhysExcludeFlag IS NULL
        SET @vNonPhysExcludeFlag = 0;

      -- Get customer info
      SELECT @vCustPartyId = cust_party_id
        FROM trl_rtrans WITH (NOLOCK)
        WHERE organization_id = @argOrgId
          AND rtl_loc_id = @argRtlLocId
          AND business_date = @argBusinessDate
          AND wkstn_id = @argWrkstnId
          AND trans_seq = @argTransSeq;

      IF @@ROWCOUNT = 1 BEGIN -- found customer
        SELECT @vCustLastName = last_name,
               @vCustFirstName = first_name
          FROM crm_party
          WHERE organization_id = @argOrgId
            AND party_id = @vCustPartyId;

        IF @@ROWCOUNT = 0 BEGIN
          SET @vCustLastName = '';
          SET @vCustFirstName = '';
        END
      END
      ELSE BEGIN
        -- no customer selected for this sale
        SET @vCustLastName = '';
        SET @vCustFirstName = '';
      END

      IF @vNonPhysExcludeFlag = 0 -- avoid non physical item with exclude flag
        INSERT into rpt_sale_line
            (organization_id, rtl_loc_id, business_date, wkstn_id, trans_seq, rtrans_lineitm_seq,
            quantity, actual_quantity, gross_quantity, unit_price, net_amt, gross_amt, item_id,
            serial_nbr, return_flag, trans_statcode, last_name, first_name, item_desc, override_amt,
            discount_amt, department_id, sale_lineitm_typcode, trans_timestamp, begin_time_int,
            cust_party_id, currency_id, cri_price_override_amt)
          VALUES
            (@argOrgId, @argRtlLocId, @argBusinessDate, @argWrkstnId, @argTransSeq,
            @vRtransLineItmSeq, @vQuantity, @vActualQuantity, @vGrossQuantity, @vUnitPrice,
            @vNetAmount, @vGrossAmount, @vItemId, @vSerialNbr, @vReturnFlag, @vTransStatcode,
            @vCustLastName, @vCustFirstName, @vItemDesc, @vOverrideAmt, @vDiscountAmt,
            @vDepartmentId, @vSaleLineitmTypcode, @vTransTimeStamp,@vBeginTimeInt, @vCustPartyId,
            @vCurrencyId, @vCriPriceOverrideAmt);    -- Activity # 391035
    END

    IF @vTransStatcode = 'COMPLETE' BEGIN   -- only when complete populate flash sales
      -- figure out the numbers
      IF @vPostVoidFlag = 1 BEGIN
        IF @vReturnFlag = 1 BEGIN
          -- reversing the returns makes it positive
          SET @vNetAmount = @vNetAmount * -1;
          SET @vGrossAmount = @vGrossAmount * -1;
          SET @vQuantity = @vQuantity * 1;
          SET @vGrossQuantity = @vGrossQuantity * 1;
          SET @vActualQuantity = COALESCE(@vActualQuantity, 0) * 1;
          SET @vDiscountAmt = @vDiscountAmt * 1; -- discounts and overrides should be (-) when voiding a return,
          SET @vOverrideAmt = @vOverrideAmt * 1; --  but but the discount amt is (-), so do not negate
          SET @vCriPriceOverrideAmt = @vCriPriceOverrideAmt * 1;  -- Activity # 391035
        END
        ELSE BEGIN
          -- reversing since its void
          SET @vNetAmount = @vNetAmount * -1;
          SET @vGrossAmount = @vGrossAmount * -1;
          SET @vQuantity = @vQuantity * -1;
          set @vGrossQuantity = @vGrossQuantity * -1;
          set @vActualQuantity = COALESCE(@vActualQuantity, 0) * -1;
          set @vDiscountAmt = @vDiscountAmt * 1; -- discounts and overrides should be (+) when voiding a sale
          set @vOverrideAmt = @vOverrideAmt * 1;
          SET @vCriPriceOverrideAmt = @vCriPriceOverrideAmt * 1;  -- Activity # 391035
        END
      END
      ELSE BEGIN      -- for sale items
        IF @vReturnFlag = 0 BEGIN     -- sale
          SET @vNetAmount = @vNetAmount;
          SET @vGrossAmount = @vGrossAmount;
          SET @vQuantity = @vQuantity;
          SET @vGrossQuantity = @vGrossQuantity;
          SET @vActualQuantity = COALESCE(@vActualQuantity, 0);
          SET @vDiscountAmt = @vDiscountAmt * -1; -- discounts and overrides should be (-) during a sale
          SET @vOverrideAmt = @vOverrideAmt * -1;
          SET @vCriPriceOverrideAmt = @vCriPriceOverrideAmt * -1;  -- Activity # 391035
        END
        ELSE BEGIN    -- for return items
          SET @vNetAmount = @vNetAmount;
          SET @vGrossAmount = @vGrossAmount;
          SET @vQuantity = @vQuantity * -1;
          SET @vGrossQuantity = @vGrossQuantity * -1;
          SET @vActualQuantity = COALESCE(@vActualQuantity, 0);
          SET @vDiscountAmt = @vDiscountAmt * -1;  -- discounts and overrides should be (+) during a return,
          SET @vOverrideAmt = @vOverrideAmt * -1;  --   but the discount amt is (-), so negate
          SET @vCriPriceOverrideAmt = @vCriPriceOverrideAmt * -1;  -- Activity # 391035
        END
      END

      IF @vReturnFlag = 1
        -- populate now to flash tables
        -- returns
        -- Activity #391035 (Flash Sales Report)
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, 'Returns', @vQuantity, @vGrossAmount, @vCurrencyId;

      -- Gross Sales update
      IF ABS(@vGrossAmount) > 0 AND (NOT @vReturnFlag = 1) -- Activity #391035 (Flash Sales Report)
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, 'GrossSales', @vGrossQuantity, @vGrossAmount, @vCurrencyId;

      -- Net Sales update
      IF ABS(@vNetAmount) > 0
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, 'NetSales', @vQuantity, @vNetAmount, @vCurrencyId;

      -- Overrides
      IF ABS(@vOverrideAmt) > 0
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, 'Overrides', @vQuantity, @vOverrideAmt, @vCurrencyId;

      -- Discounts
      IF ABS(@vDiscountAmt) > 0
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, 'Discounts', @vQuantity, @vDiscountAmt, @vCurrencyId;

	    -- Price Override Activity #391035
      IF ABS(@vCriPriceOverrideAmt) > 0
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, 'Promotions', @vQuantity, @vCriPriceOverrideAmt, @vCurrencyId;

      -- non merchandise
      -- Non Merchandise (returns after processing)
      SELECT @vNonPhysType = non_phys_item_typcode
        FROM itm_non_phys_item WITH (NOLOCK)
        WHERE item_id = @vItemId
          AND organization_id = @argOrgId;

      IF @@ROWCOUNT = 1 BEGIN      -- check for layaway or sp. order payment / deposit
        IF @vPostVoidFlag = 1 BEGIN
          SET @vNonPhysPrice = @vUnitPrice * -1;
          SET @vNonPhysQuantity = @vActualQuantity * -1;
        END
        ELSE BEGIN
          SET @vNonPhysPrice = @vUnitPrice;
          SET @vNonPhysQuantity = @vActualQuantity;
        END

        IF @vNonPhysType = 'LAYAWAY_DEPOSIT'
          SET @vNonPhys = 'LayawayDeposits';
        ELSE IF @vNonPhysType = 'LAYAWAY_PAYMENT'
          SET @vNonPhys = 'LayawayPayments';
        ELSE IF @vNonPhysType = 'SP_ORDER_DEPOSIT'
          SET @vNonPhys = 'SpOrderDeposits';
        ELSE IF @vNonPhysType = 'SP_ORDER_PAYMENT'
          SET @vNonPhys = 'SpOrderPayments';
        ELSE BEGIN
          SET @vNonPhys = 'NonMerchandise';
          SET @vNonPhysPrice = @vGrossAmount;
          SET @vNonPhysQuantity = @vGrossQuantity;
        END

        -- update flash sales for non physical payments / deposits
        EXEC sp_ins_upd_flash_sales
            @argOrgId, @argRtlLocId, @argBusinessDate,
            @argWrkstnId, @vNonPhys, @vNonPhysQuantity, @vNonphysPrice, @vCurrencyId;

        -- Gross Sales update
        -- Gross Sales exclude NP Activity #391035
		    IF @vNonPhys = 'NonMerchandise' BEGIN
						SET @vNonPhysQuantity = @vNonPhysQuantity * -1;
						SET @vNonphysPrice = @vNonphysPrice * -1;
						EXEC sp_ins_upd_flash_sales
						  @argOrgId, @argRtlLocId, @argBusinessDate,
						  @argWrkstnId, 'GrossSales', @vNonPhysQuantity, @vNonphysPrice, @vCurrencyId;
		    END
      END
      ELSE
        SET @vNonPhys = '';   -- reset

      -- process layaways and special orders (not sales)
      IF @vSaleLineitmTypcode = 'LAYAWAY' OR @vSaleLineitmTypcode = 'SPECIAL_ORDER' BEGIN
        IF (NOT (@vNonPhys = 'LayawayDeposits'
              OR @vNonPhys = 'LayawayPayments'
              OR @vNonPhys = 'SpOrderDeposits'
              OR @vNonPhys = 'SpOrderPayments'))
          AND ((@vLineitemStatcode IS NULL) OR (@vLineitemStatcode <> 'CANCEL'))
        BEGIN
          SET @vNonPhysSaleType = 'SpOrderItems';
          IF @vSaleLineitmTypcode = 'LAYAWAY'
            SET @vNonPhysSaleType = 'LayawayItems';

          -- update flash sales for layaway items
          SET @vLayawayPrice = @vUnitPrice * COALESCE(@vActualQuantity, 0);

          EXEC sp_ins_upd_flash_sales
              @argOrgId, @argRtlLocId, @argBusinessDate,
              @argWrkstnId, @vNonPhys, @vActualQuantity, @vLayawayPrice, @vCurrencyId;
        END
      END
      -- end flash sales update

      -- department sales
      EXEC sp_ins_upd_dept_sales
          @argOrgId, @argRtlLocId, @argBusinessDate,
          @argWrkstnId, @vDepartmentId, @vQuantity, @vNetAmount, @vGrossAmount, @vCurrencyId;

      -- Hourly sales updates (add for all the line items in the transaction)
      SET @vTotQuantity = coalesce(@vTotQuantity, 0) + @vQuantity;
      SET @vTotNetAmt = coalesce(@vTotNetAmt, 0) + @vNetAmount;
      IF NOT @vNonPhys = 'NonMerchandise' BEGIN -- Activity # 391035
      	SET @vTotGrossAmt = coalesce(@vTotGrossAmt, 0) + @vGrossAmount;
      END
      SET @vTotCriPriceOverrideAmt = coalesce(@vTotCriPriceOverrideAmt, 0) + @vCriPriceOverrideAmt; -- Activity # 391035
      SET @vTotCriDiscountAmt = coalesce(@vTotCriDiscountAmt, 0) + @vDiscountAmt; -- Activity # 391035
      SET @vTotCriManualPriceChangeAmt = coalesce(@vTotCriManualPriceChangeAmt, 0) + @vOverrideAmt; -- Activity # 391035
    END
  END

  CLOSE saleCursor;
  DEALLOCATE saleCursor;

  -- update hourly sales
  IF (ABS(@vTotNetAmt) > 0 OR ABS(@vTotGrossAmt) > 0 OR ABS(@vTotquantity) > 0 OR ABS(@vTotCriPriceOverrideAmt) > 0 OR ABS(@vTotCriDiscountAmt) > 0 OR ABS(@vTotCriManualPriceChangeAmt) > 0)
    EXEC sp_ins_upd_hourly_sales
        @argOrgId, @argRtlLocId, @argBusinessDate, @argWrkstnId, @vTransTimeStamp,
        @vTotquantity, @vTotNetAmt, @vTotGrossAmt, @vTransCount, @vCurrencyId, @vTotCriPriceOverrideAmt, @vTotCriDiscountAmt, @vTotCriManualPriceChangeAmt; -- Activity # 391035

GO

IF EXISTS (Select * From sysobjects Where name = 'sp_reset_float' and type = 'P')
DROP PROCEDURE dbo.sp_reset_float;
GO
CREATE PROCEDURE [dbo].[sp_reset_float] (
    @argOrgId int,
    @argRtlLocId int,
    @argTillFloatAmount decimal(17, 6),
    @argStoreSafeFloatAmount decimal(17, 6) )
AS

update tsn_tndr_repository 
set dflt_closing_cash_balance_amt = @argTillFloatAmount,
dflt_opening_cash_balance_amt = @argTillFloatAmount, 
 last_closing_cash_balance_amt = @argTillFloatAmount,
update_date = GETDATE(),
update_user_id = 'DATALOADER' 
 where typcode='TILL' and organization_id=@argOrgId  and rtl_loc_id=@argRtlLocId;
 
update tsn_tndr_repository set dflt_closing_cash_balance_amt = @argStoreSafeFloatAmount+ ((select count(*) from tsn_tndr_repository where typcode='TILL' and organization_id=@argOrgId  and rtl_loc_id=@argRtlLocId)*@argTillFloatAmount), 
dflt_opening_cash_balance_amt = @argStoreSafeFloatAmount + ((select count(*) from tsn_tndr_repository where typcode='TILL' and organization_id=@argOrgId  and rtl_loc_id=@argRtlLocId)*@argTillFloatAmount), 
last_closing_cash_balance_amt = @argStoreSafeFloatAmount, update_date = GETDATE(),
update_user_id = 'DATALOADER'  where typcode='STOREBANK' and organization_id=1  and rtl_loc_id= @argRtlLocId;
 
update tsn_session_tndr set actual_media_amt = @argStoreSafeFloatAmount + ((select count(*) from tsn_tndr_repository where typcode='TILL' and organization_id=@argOrgId  and rtl_loc_id=@argRtlLocId)*@argTillFloatAmount),update_date = GETDATE(),
update_user_id = 'DATALOADER'  where session_id=0 and rtl_loc_id=@argRtlLocId and tndr_id='USD_CURRENCY';


GO

IF OBJECT_ID('cri_token_update_queue') IS NULL
BEGIN
	PRINT '  - Creating table: cri_token_update_queue';
  CREATE TABLE [dbo].[cri_token_update_queue](
      [organization_id]          int             NOT NULL,
      [invoice_nbr]              varchar(30)     NOT NULL,
      [rtl_loc_id]               int             NOT NULL,
      [business_date]            datetime        NOT NULL,
      [wkstn_id]                 bigint          NOT NULL,
      [trans_seq]                bigint          NOT NULL,
      [rtrans_lineitm_seq]       int             NOT NULL,
      [updated_flag]             bit             DEFAULT ((0)) NULL,
      [create_date]              datetime        NULL,
      [create_user_id]           varchar(30)     NULL,
      [update_date]              datetime        NULL,
      [update_user_id]           varchar(30)     NULL,
      [record_state]             varchar(30)     NULL,
      CONSTRAINT [pk_cri_token_update_queue] PRIMARY KEY CLUSTERED ([organization_id], [invoice_nbr]) WITH (FILLFACTOR = 80)
  )
END
GO
