<?xml version="1.0" encoding="UTF-8"?>
<!-- $Id$ -->
<!-- $URL$ -->
<!-- see http://ant.apache.org/manual/index.html for the Ant manual -->
<project name="customer-build" default="dtx-and-reports-dev" basedir=".">

  <!-- read from property file -->
  <property file="buildoverrides.properties"/>

  <!-- read in the environment variables -->
  <property environment="env" />

  <!-- turn it blank if not specified -->
  <property name="env.PROPS_PREFIX" value="" />

  <!-- read from ant.install.property file -->
  <property file="installx/builder/${env.PROPS_PREFIX}ant.install.properties"/>

  <!-- set properties that are not overridden in properties file or from a call into this file -->
  <property name="jar.name" value="${cust}-pos.jar"/>
  <property name="zip.name" value="${cust}.zip" />
  <property name="version.file.path" value="${cust}/pos" />
  <property name="config" location="config"/>
  <property name="gen" location="gen"/>
  <property name="src" value="src"/>
  <property name="src.bridged" value="src-bridged"/>
  <property name="src.reports" value="reports"/>
  <property name="src.test" value="test"/>
  <property name="temp.reports" location="${src.reports}/report.temp"/>
  <property name="classes" value="classes"/>
  <property name="classes.bridged" value="classes"/>
  <property name="classes.test" value="classes-test"/>
  <property name="wrapper.dist" value="wrapper-dist"/>
  <property name="cust_config" value="cust_config"/>
  <property name="javadoc" location="javadoc" />
  <property name="lib" value="../lib/lib"/>
  <property name="cust.lib" value="lib"/>
  <property name="ecom.lib" value="ecom"/>
  <property name="dist" value="dist"/>
  <property name="platform" value="windows"/>
  <property name="build" value="build"/>
  <property name="net.sf.jasperreports.compiler.{language}" value="net.sf.jasperreports.engine.design.JRJdtCompiler"/>
  <property name="workspace" value=".." />
  <property name="distro.installx" value="${workspace}/distro"/>
  <property name="distro" value="${workspace}/distro-full"/>
  <property name="ixwork" value="${workspace}/../ixwork" />
  <property name="installx-dist-dir" value="../../../InstallX/workspace/installx/dist/"/>
  <property name="full.version" value="${x.version}_${cust.version}"/>
  <property name="copy-to-archive.disabled" value="true"/>

  <!-- read in the environment variables -->
  <property environment="env" />

  <path id="config.validation.classpath">
    <fileset dir="${lib}" erroronmissingdir="false">
      <include name="**/*.jar"/>
    </fileset>
    <fileset dir="${basedir}/lib" erroronmissingdir="false">
      <include name="**/*.jar"/>
    </fileset>
    <pathelement location="${classes}" />
    <pathelement location="${basedir}/target/xstore-validation-tasks-1.0.jar" />
  </path>

  <!-- determine if there are any reports -->
  <available file="${src.reports}" type="dir" property="has.reports" />

  <!-- determine if there are any dtx -->
  <available file="${config}" type="dir" property="has.dtx" />

  <!-- determine if we should combine some MNTs into a download.zip -->
  <condition property="should.zip.mnts">
    <and>
      <!--are there any MNTs?-->
      <resourcecount when="greater" count="0">
       <fileset dir="download" includes="**/*.mnt" />
      </resourcecount>
      <!-- and we don't have a download.zip checked in -->
      <not><available file="${download}/download.zip" type="file" /></not>
      <!-- and we didn't specifically disable the auto-zip -->
      <not><isset property="${disable.mnt.to.download.zip}"/></not>
    </and>
  </condition>

  <!-- determine if there are any dtx -->
  <available file="${src.test}" type="dir" property="has.test" />

  <!-- determine if we are doing an INSTALL -->
  <condition property="installx.INSTALL">
    <contains string="${x.type}" substring="INSTALL" casesensitive="false" />
  </condition>

  <!-- determine if we are doing an UPDATE -->
  <condition property="installx.UPDATE">
    <contains string="${x.type}" substring="UPDATE" casesensitive="false" />
  </condition>

  <!-- determine if we are doing an UPGRADE -->
  <condition property="installx.UPGRADE">
    <contains string="${x.type}" substring="UPGRADE" casesensitive="false" />
  </condition>

  <!-- determine if we are doing something else -->
  <condition property="installx.DEFAULT">
    <not>
      <or>
        <isset property="installx.INSTALL" />
        <isset property="installx.UPDATE" />
        <isset property="installx.UPGRADE" />
      </or>
    </not>
  </condition>
  
  <!-- ****************************** -->
  <!-- ** start of targets ********** -->
  <!-- ****************************** -->

  <target name="test-setup" description="test that you are set up correctly to build with this file">
    <fail unless="cust" message="the 'cust' must be set" />
  </target>

  <target name="clean-all" description="clean up">
    <antcall target="clean"/>
    <antcall target="clean-dtx"/>
    <antcall target="clean-reports"/>
  </target>

  <target name="clean" description="Cleans everything except DTX and reports">
    <delete dir="${classes}" failonerror="false"/>
    <delete dir="${classes.bridged}" failonerror="false"/>
    <delete dir="${classes.test}" failonerror="false"/>
    <delete dir="${build}" failonerror="false"/>
    <delete dir="${dist}" failonerror="false"/>
    <delete dir="${javadoc}" failonerror="false"/>
    <delete file="${jar.name}" failonerror="false"/>
    <delete file="${zip.name}" failonerror="false"/>
    <delete file="./FormatterClassMap.properties" failonerror="false"/>
  </target>

  <target name="clean-dtx" description="Clean temporary files for DTX objects">
    <!-- Removes contents of "gen" without removing the directory itself. -->
    <delete includeemptydirs="true" failonerror="false">
      <fileset dir="${gen}" includes="**/*"/>
    </delete>
  </target>

  <target name="clean-reports" if="has.reports" description="Clean temporary files for reports">
    <delete includeemptydirs="true" failonerror="false">
      <fileset dir="${src.reports}" includes="**/*.jasper"/>
    </delete>
    <delete dir="${temp.reports}" failonerror="false"/>
    <delete file="./FormatterClassMap.properties" failonerror="false"/>
  </target>

  <target name="update-version-class" depends="test-setup" description="mark the new build number in the Version.java file">
    <tstamp>
      <format pattern="yyyy-MM-dd&apos;T&apos;HH:mm:ssZ" property="build-date-time" />
    </tstamp>
    <replaceregexp byline="true"
          file="${src}/${version.file.path}/Version.java"
          match="String BUILD_DATE =(.)+"
          replace="String BUILD_DATE = &quot;${build-date-time}&quot;;" />
    <replaceregexp byline="true"
          file="${src}/${version.file.path}/Version.java"
          match="String CUSTOMER_VERSION =(.)+"
          replace="String CUSTOMER_VERSION = &quot;${cust.version}&quot;;" />
    <replaceregexp byline="true"
          file="${src}/${version.file.path}/Version.java"
          match="String PATCH_VERSION =(.)+"
          replace="String PATCH_VERSION = &quot;${patch.version}&quot;;" />
    <echo message="updated ${src}/${version.file.path}/Version.java to BUILD_DATE = &quot;${build-date-time}&quot;, CUSTOMER_VERSION = &quot;${cust.version}&quot;, and PATCH_VERSION = &quot;${patch.version}&quot;" />

  </target>

  <target name="-compile-test-if" if="has.test" description="Compiles test source if necessary">
    <property environment="env" />
    <echo message="JAVA_HOME=${env.JAVA_HOME}"/>
    <mkdir dir="${classes.test}" />
    <javac srcdir="${src.test}" destdir="${classes.test}" deprecation="true" debug="true">
      <classpath>
        <pathelement location="${classes}" />
        <pathelement location="${classes.bridged}" />
        <pathelement location="${classes.test}" />
        <fileset dir="${cust.lib}">
          <include name="**/*.jar"/>
        </fileset>
        <fileset dir="${lib}">
          <include name="**/*.jar"/>
        </fileset>
      </classpath>
    </javac>
    <!-- copy over test resources like eclipse does -->
    <copy todir="${classes.test}">
      <fileset dir="${src.test}">
        <exclude name="**/*.java"/>
      </fileset>
    </copy>
  </target>

  <target name="-compile-test" depends="compile" description="Determines if test source exists and delegates its compilation">
    <available file="${src.test}/${cust}" type="dir" property="has.test"/>
    <antcall target="-compile-test-if" inheritall="true" inheritrefs="true"/>
  </target>

  <target name="generation" depends="clean-dtx" description="Generates DTX objects.">
    <antcall target="-generation-if" inheritall="true" inheritrefs="true"/>
  </target>

  <target name="-generation-if" if="has.dtx">
    <taskdef name="dao-generation" classname="dtv.data2.access.impl.daogen.DAOGenAnt">
      <classpath>
        <pathelement location="${classes.bridged}" />
        <fileset dir="${cust.lib}">
          <include name="**/*.jar"/>
        </fileset>
        <fileset dir="${lib}">
          <include name="**/*.jar"/>
        </fileset>
      </classpath>
    </taskdef>
    <mkdir dir="${gen}"/>
    <dao-generation dest="${gen}" dir="${config}" modeldir="${src}" overrideMode="true"/>
  </target>

  <target name="compile-only" description="compiles the source">
    <antcall target="-compile"/>
  </target>

  <target name="compile" depends="update-version-class,generation" description="compiles the source">
    <antcall target="-compile"/>
  </target>

  <target name="-compile" description="compiles the source">
    <property environment="env" />
    <echo message="JAVA_HOME=${env.JAVA_HOME}"/>
    <mkdir dir="${classes}" />
    <mkdir dir="${gen}" />
    <javac srcdir="${src}:${gen}:${src.bridged}" destdir="${classes}" deprecation="true" debug="true" fork="true" memoryMaximumSize="256m">
      <classpath>
        <fileset dir="${cust.lib}">
          <include name="**/*.jar"/>
        </fileset>
        <fileset dir="${lib}">
          <include name="**/*.jar"/>
        </fileset>
        <pathelement location="${classes}" />
      </classpath>
    </javac>
  </target>

  <target name="-compile-reports" description="Internal target for report compilation">
    <copy file="${cust_config}/version1/FormatterClassMap.properties" todir="." failonerror="false"/>
    <mkdir dir="${temp.reports}" />
    <echo message="JAVA_HOME=${env.JAVA_HOME}"/>
    <echo message="lib=${lib}"/>
    <echo message="cust.lib=${cust.lib}"/>
    <echo message="cust_config=${cust_config}"/>
    <path id="reports.classpath">
      <pathelement location="bin"/>
      <pathelement location="${classes}"/>
      <pathelement location="${cust_config}"/>
      <fileset dir="${cust.lib}">
        <include name="**/*.jar"/>
      </fileset>
      <fileset dir="${lib}">
        <include name="**/*.jar"/>
      </fileset>
    </path>
    <taskdef classname="net.sf.jasperreports.ant.JRAntCompileTask" name="jrc">
      <classpath refid="reports.classpath" />
    </taskdef>
    <jrc keepjava="true" tempdir="${temp.reports}" xmlvalidation="true">
      <src>
        <fileset dir="${src.reports}">
          <include name="**/*.jrxml" />
        </fileset>
      </src>
      <classpath refid="reports.classpath" />
    </jrc>
    <delete file="./FormatterClassMap.properties" failonerror="false"/>
    <delete dir="${temp.reports}"/>
  </target>

  <target name="compile-reports" depends="compile" if="has.reports" description="compiles all Jasper reports">
    <antcall target="-compile-reports"/>
  </target>

  <target name="compile-reports-only" depends="clean-reports" description="compiles all Jasper reports without compiling regular source code">
    <antcall target="-compile-reports"/>
  </target>

  <target name="dtx-and-reports-dev">
    <antcall target="-generation-if"/>
    <antcall target="-compile-reports"/>
  </target>

  <target name="make" depends="jar" description="compiles and makes a zip file">
    <ant target="validate-configs" />
    <delete file="${zip.name}" failonerror="false"/>
    <delete dir="${dist}/xstore" failonerror="false"/>
    <mkdir dir="${dist}/xstore"/>
    <copy todir="${dist}/xstore">
      <fileset dir=".">
        <!-- avoid some directories by name -->
        <exclude name="${dist}/**"/>
        <exclude name="${src}/**"/>
        <exclude name="${src.bridged}/**"/>
        <exclude name="${src.test}/**"/>
        <exclude name="${classes}/**"/>
        <exclude name="${classes.bridged}/**"/>
        <exclude name="${classes.test}/**"/>
        <exclude name="res/keys/ccenc*.cip"/>
        <exclude name="res/ssl/**"/>
        <exclude name="bak/**"/>
        <exclude name="test/**"/>
        <exclude name="gen/**"/>
        <exclude name="config/**"/>
        <exclude name="${temp.reports}/**"/>
        <exclude name="${src.reports}/**/*.jrxml"/>
        <exclude name="bin/**"/>
        <exclude name="installx/builder/**"/>
        <exclude name="installx/dist/**"/>
        <exclude name="root/**"/>
        <exclude name="xstore/**"/>
        <exclude name="build/**"/>
        <exclude name="ecom/**"/>
        <!-- avoid directories off the root whose name starts with a dot -->
        <exclude name=".*/**"/>
        <!-- avoid files directly in the root of the project -->
        <exclude name="*"/>
        <!-- cust_config files will be placed inside the jar -->
        <exclude name="cust_config/**"/>
        <exclude name="reports/**"/>
        <exclude name="${download}/**" />
      </fileset>
    </copy>
    <ant target="manage-mnts" />
    <!-- copy the contents of the "root" folder into the root of the distro -->
    <copy todir="${dist}/xstore">
      <fileset dir="root" />
    </copy>
    <copy file="${jar.name}" todir="${dist}/xstore/lib" failonerror="false" />
    <zip destfile="${zip.name}">
      <zipfileset dir="${dist}"/>
    </zip>
  </target>

  <target name="manage-mnts">
    <ant target="-copy-mnts-unless" />
    <ant target="-zip-mnts-if" />
  </target>
  <target name="-copy-mnts-unless" unless="${should.zip.mnts}">
    <copy todir="${dist}/xstore">
      <fileset dir="." includes="download/**" />
    </copy>
  </target>
  <target name="-zip-mnts-if" if="${should.zip.mnts}">
    <zip destfile="${dist}/xstore/download/download.zip">
      <fileset dir="${download}" includes="**/*.mnt" />
    </zip>
  </target>

  <target name="check-configs-wellformed">
    <xmlvalidate classname="org.apache.xerces.parsers.SAXParser" failonerror="yes" lenient="yes" warn="yes">
      <fileset dir="cust_config" includes="**/*.xml" />
      <classpath>
        <fileset dir="${lib}">
          <include name="**/xercesImpl.jar" />
        </fileset>
      </classpath>
    </xmlvalidate>
  </target>

  <target name="jar" depends="compile-reports,check-configs-wellformed">
    <antcall target="-jar"/>
  </target>

  <target name="jar-only">
    <antcall target="-jar"/>
  </target>

  <target name="-jar">
    <tstamp>
      <format property="now.long" pattern="EEEE, MMMM d, yyyy 'at' HH:mm aa"/>
    </tstamp>
    <jar jarfile="${jar.name}" compress="true">
      <manifest>
        <attribute name="Built-By" value="${user.name}" />
        <attribute name="Build-Time" value="${env.BUILD_ID}" />
        <attribute name="Build-VM-Vendor" value="${java.vm.vendor}" />
        <attribute name="Build-VM-Name" value="${java.vm.name}" />
        <attribute name="Build-VM-Version" value="${java.vm.version}" />
        <attribute name="Build-OS" value="${os.name} (${os.arch}) version ${os.version}" />
        <attribute name="Hudson-Job-Name" value="${env.JOB_NAME}"/>
        <attribute name="Hudson-Build-Number" value="${env.BUILD_NUMBER}"/>
        <attribute name="SVN-Revision" value="${env.SVN_REVISION}"/>
        <attribute name="Xstore-Target-Version" value="${xstore.version}" />
      </manifest>
      <fileset dir="${classes}"/>
      <fileset dir="cust_config">
        <include name="**"/>
      </fileset>
    </jar>
    <jar jarfile="${jar.name}" compress="true" update="true">
      <fileset dir="reports">
        <include name="**/*.jasper"/>
      </fileset>
    </jar>
  </target>

  <target name="validate-configs">
    <taskdef name="validateXstoreConfigs" classname="com.micros_retail.xstore.validation.ValidateXstoreConfigsTask" classpathref="config.validation.classpath"/>
    <validateXstoreConfigs classpathref="config.validation.classpath">
      <fileset dir="${basedir}">
        <include name="**/*Config.xml"/>
        <exclude name="cust_config/version1/xcenterdb/**"/>
      </fileset>
    </validateXstoreConfigs>
  </target>

  <target name="installx">
    <delete dir="${distro}"/>
    <antcall target="installx-refresh" />
    <antcall target="installx-install" />
    <antcall target="installx-update" />
    <antcall target="installx-upgrade" />
    <antcall target="installx-default" />
    <antcall target="custom-restructure" />
  </target>
  
  <target name="installx-refresh">
  <echo>This is nck ixwork  ${ixwork}</echo>
    <mkdir dir="${ixwork}" />
    <!-- Remove old installx.jar -->
    <delete>
      <fileset dir="${ixwork}" includes="installx*.jar" />
    </delete>
    <!-- Copy in the latest InstallX release and the builder ant.install.properties file from the customer project -->
<echo>This is nck installx-dist-dir ::::: ${installx-dist-dir}</echo>   
   <copy tofile="${ixwork}/installx.jar">
      <fileset dir="${installx-dist-dir}">
        <include name="installx-*.jar"/>
      </fileset>
    </copy>
  </target>

  <target name="installx-install" if="installx.INSTALL">
    <antcall target="-installx-launch">
      <param name="-installx-launch.type" value="install" />
    </antcall>
  </target>

  <target name="installx-update" if="installx.UPDATE">
    <antcall target="-installx-launch">
      <param name="-installx-launch.type" value="update" />
    </antcall>
  </target>

  <target name="installx-upgrade" if="installx.UPGRADE">
    <antcall target="-installx-launch">
      <param name="-installx-launch.type" value="upgrade" />
    </antcall>
  </target>

  <target name="installx-default" if="installx.DEFAULT">
    <antcall target="-installx-launch">
      <param name="-installx-launch.type" value="${x.type}" />
    </antcall>
  </target>

  <target name="-installx-launch" if="-installx-launch.type">
    <copy file="installx/builder/${env.PROPS_PREFIX}ant.install.properties" tofile="${ixwork}/ant.install.properties" overwrite="true" />
    <propertyfile file="${ixwork}/ant.install.properties">
      <entry key="x.type" value="${-installx-launch.type}" />
    </propertyfile>
    <java 
        dir="${ixwork}"
        jar="${ixwork}/installx.jar"
        fork="true"
        failonerror="true"
    />
	  <echo>This is nck distro.installx  ${distro.installx}</echo>
    <move todir="${distro}">
  
      <fileset dir="${distro.installx}">
        <include name="**" />
      </fileset>
    </move>
	  
  </target>

  <target name="distribute">
    <antcall target="zip-artifacts" />
    <antcall target="restructure-dist" />
    <antcall target="copy-to-qa" />
    <antcall target="publish-to-sftp" />
    <antcall target="copy-to-archive" />
  </target>

  <target name="copy-to-archive" unless="copy-to-archive.disabled">
    <move todir="\\dtvdev\xstore\${cust}\">
      <fileset dir="${distro}" includes="*-installx.zip" />
    </move>
  </target>

  <target name="restructure-dist">
    <move todir="${distro}/${full.version}">
      <fileset dir="${distro}" includes="*bldcomp.txt" />
    </move>
    <copy todir="${distro}/xcenter" failonerror="false">
      <fileset dir="database" includes="*xcenter*.sql" />
    </copy>
    <antcall target="custom-restructure"/>
  </target>

  <target name="copy-to-qa" unless="copy-to-qa.disabled">
    <mkdir dir="\\qalab-server01\customer\${cust}\xstore-dev\${full.version}" />
    <copy todir="\\qalab-server01\customer\${cust}\xstore-dev\${full.version}">
      <fileset dir="${distro}\${full.version}" />
    </copy>
    <antcall target="-email-with-log">
      <param name="email.subject" value="new build ${cust} ${full.version} in \\qalab-server01\Customer\${cust}\Xstore-Dev" />
    </antcall>
  </target>

  <target name="publish-to-sftp">
    <ant target="publish-to-sftp" antfile="../../../../releases/shared-build-steps/build-sftp.xml" />
  </target>

  <target name="-email-with-log">
    <fail unless="email.subject" message="email.subject is required"/>
    <ant target="email-with-log" antfile="../../../../releases/shared-build-steps/build-email.xml">
      <property name="email.to" value="XStoreUpdate@micros-retail.com,${hudson.dist.email}" />
      <property name="email.subject" value="${email.subject}" />
    </ant>
  </target>

<!-- customer-specific tasks -->
  <target name="custom-restructure">
    <!--
    <delete dir="${distro}/${full.version}/cryptutil"/>
    <delete dir="${distro}/${full.version}/posloggen"/>
    <delete dir="${distro}/${full.version}/genkeys"/>
    -->
    <!--
    <delete dir="${distro}/${full.version}/xcenterdtx"/>
    <delete dir="${distro}/${full.version}/xcenteradmin"/>

    <delete file="${distro}/${cust}_xstore_${full.version}-installx.zip"/>
    <zip destfile="${distro}/${cust}_xstore_${full.version}-installx.zip">
      <zipfileset dir="${distro}/${full.version}"/>
    </zip>
    <copy todir="${distro}/xcenterdb" failonerror="false">
      <fileset dir="database" includes="*xcenter*.sql" />
    </copy>
    -->

    <!-- Update jar files in the Xcenter ear. This is a multi-step process due to needing to replace, not add, one jar file.
         Ant does not provide a way to overwrite files inside of a zip/jar/war/ear. Adding a file will result in two files
         with the same name, which is perfectly legal according to the zip standard. -->
    <property name="xcenter.ear" value="xcenter-${x.version}-${CUST}-${cust.version}.ear"/>
    <property name="xcenter.ear.temp" value="xc-ear"/>

    <!-- Extract the Xcenter ear file. -->
    <echo>Extracting Xcenter EAR file...</echo>
    <unzip src="${distro}/${full.version}/xcenter/${xcenter.ear}" dest="${xcenter.ear.temp}"/>

    <!-- Delete the old Serenade file. -->
    <echo>Deleting base Serenade JAR file...</echo>
    <delete failonerror="false">
      <fileset dir="${xcenter.ear.temp}" includes="xcenter-serenade-*.jar"/>
    </delete>

    <!-- Copy in required jars. -->
    <echo>Copying customer JAR files from overlay to Xcenter temp...</echo>
    <copy todir="${xcenter.ear.temp}" overwrite="true">
      <fileset dir="${ecom.lib}" includes="*.jar"/>
    </copy>

    <!-- Delete the old ear file. -->
    <echo>Deleting old Xcenter EAR file...</echo>
    <delete file="${distro}/${full.version}/xcenter/${xcenter.ear}"/>

    <!-- Rebuild the Xcenter ear file. -->
    <echo>Rebuilding Xcenter EAR file...</echo>
    <zip destfile="${distro}/${full.version}/xcenter/${xcenter.ear}" basedir="${xcenter.ear.temp}"/>

    <!-- Delete temp. -->
    <echo>Deleting Xcenter temp files...</echo>
    <delete dir="${xcenter.ear.temp}"/>

    <!-- Do the same thing, but with the InstallX zip file. -->
    <property name="ix.zip" value="${CUST}_xstore_${x.version}_${cust.version}-installx.zip"/>
    <property name="ix.redist.temp" value="ix-temp"/>

    <!-- Extract the InstallX zip file. -->
    <echo>Extracting InstallX JAR file...</echo>
    <unzip src="${distro}/${ix.zip}" dest="${ix.redist.temp}"/>

    <!-- Delete the old Xcenter ear file. -->
    <echo>Deleting existing Xcenter EAR file...</echo>
    <delete file="${ix.redist.temp}/${full.version}/xcenter/${xcenter.ear}"/>

    <!-- Copy in the new Xcenter ear file. -->
    <echo>Copying new Xcenter EAR into InstallX temp...</echo>
    <copy todir="${ix.redist.temp}/${full.version}/xcenter" file="${distro}/${full.version}/xcenter/${xcenter.ear}"/>

    <!-- Delete the old InstallX zip file. -->
    <echo>Deleting old InstallX ZIP file...</echo>
    <delete file="${distro}/${ix.zip}"/>

    <!-- Rebuild the InstallX zip file. -->
    <echo>Rebuilding InstallX ZIP file...</echo>
    <zip destfile="${distro}/${ix.zip}" basedir="${ix.redist.temp}"/>

    <!-- Delete temp. -->
    <echo>Deleting InstallX temp files...</echo>
    <delete dir="${ix.redist.temp}"/>

  </target>
<!-- modifeed by:  rblosch
     modified date:  1/25/2019
     notes:  this is the original snippet from OLR
  <target name="zip-artifacts">
    <zip destfile="${dist}/artifacts-installx.zip"
      basedir="${basedirinstallx-dist-dir}"
    />
  </target>
  -->
  <target name="zip-artifacts">
    <zip destfile="${dist}/artifacts-installx.zip"
      basedir="dist"
    />
  </target>


</project>
