//$Id: TlogGenerator.java 55414 2010-12-07 23:00:05Z dtvdomain\jweiss $
package dtv.pos.tools;

import java.io.File;
import java.util.*;

import org.apache.log4j.Logger;

import dtv.data2.access.IQueryResultList;
import dtv.data2.access.ObjectNotFoundException;
import dtv.logbuilder.ILogBuilder;
import dtv.logbuilder.LogBuilder;
import dtv.logbuilder.writers.LogEntryFileWriter;
import dtv.pos.ejournal.*;
import dtv.util.DateUtils;
import dtv.util.TypeSafeMapKey;
import dtv.util.config.SystemPropertiesLoader;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.query.results.DefaultTransactionSummaryModel;

/**
 * Utility class that creates transaction logs. May be run from the command line or accessed as a
 * regular class from another program.<br>
 * 
 * Copyright (c) 2004 Datavantage Corporation
 * 
 * @author mmichalek
 * @author jgaughan
 * @created October 11, 2004
 * @version $Revision: 55414 $
 * 
 * @see TransactionSearchHelper
 */
public class TlogGenerator {
  private static final String USAGE_ = "Usage: "
      + "TlogGenerator <-d<YYYY-MM-DD-YYYY-MM-DD> | -s<#-#> | -r<#-#>>\n"
      + "              | -ofile.xml | -?\n" + "-------------------------------\n" + "where options are:\n"
      + "-dYYYY-MM-DD Business Date (-d2004-10-12)\n"
      + "-dYYYY-MM-DD-YYYY-MM-DD Business Date RANGE (-d2004-10-11-2004-10-12)\n"
      + "-s# Store number  ( -s300)\n" + "-s#-# Store number RANGE (-s11-13)\n"
      + "-r# Register number (-r1)\n" + "-r#-# Register number RANGE (-r1-3)\n"
      + "-n# Transaction Sequence (-n1309)\n" + "-n#-# Transaction Sequence Number RANGE (-n1309-1399)\n"
      + "-ofile.xml Output file (e.g. \"file.xml\"), \"PosLog.xml\" by default\n" + "-? This help.\n\n"
      + "EXAMPLE: TlogGenerator -dYYYY-MM-DD -s311 -r5-10";

  public static final String EXEC_SUCCESS_ = "Transaction log generation succeeded.";
  public static final String EXEC_NO_RESULTS_ = "No data returned for the filters entered.";

  public static final String DATE_FORMAT = "YYYY-MM-DD";

  private static final Logger logger_ = Logger.getLogger(TlogGenerator.class);

  private static final char ARG_START_ = '-';
  private static final char ARG_DATE_ = 'd';
  private static final char ARG_STORE_ = 's';
  private static final char ARG_REGISTER_ = 'r';
  private static final char ARG_HELP1_ = 'h';
  private static final char ARG_HELP2_ = '?';
  private static final char ARG_RANGE_ = '-';
  private static final char ARG_SEQUENCE_ = 'n';
  private static final char ARG_FILENAME_ = 'o';

  /**
   * Command line entry point.
   * 
   * This method simply creates a new object and gives it the command line arguments.
   * 
   * @param args Command line arguments passed in from the user.
   */
  public static void main(String[] args) {
    try {
      new SystemPropertiesLoader().loadSystemProperties();
      logger_.info(new TlogGenerator().exec(args));
      System.exit(0);
    }
    catch (Exception e) {
      logger_.error(e);
      logger_.error(USAGE_);
      System.exit(-1);
    }
  }

  /**
   * Convenience method to run the program.
   * 
   * @param criteria Search {@linkplain dtv.pos.ejournal.ITransactionSearchCriteria criteria}.
   * @return results of the execution.
   */
  public String exec(ITransactionSearchCriteria criteria) {
    return exec(criteria, null);
  }

  /**
   * Main program logic.
   * 
   * Logic is contained here to facilitate executing this program in multiple ways, e.g. the
   * built-in {@link dtv.pos.tools.TlogGenerator#main(String[]) main}, JMX console, or as a regular
   * class in a running application.
   * 
   * @param criteria Search {@linkplain dtv.pos.ejournal.ITransactionSearchCriteria criteria}.
   * @param filename Output file to write, null for default.
   * @return results of the execution.
   */
  public String exec(ITransactionSearchCriteria criteria, String filename) {
    if (criteria == null) {
      throw new NullPointerException("No criteria specified.");
    }
    TransactionSearchHelper access = null;
    IQueryResultList<DefaultTransactionSummaryModel> queryResults = null;

    // Load up transactions based on criteria given
    access = TransactionSearchHelper.getInstance();
    try {
      queryResults = access.runQuery(criteria);
      logger_.debug(new StringBuilder().append("Transaction search query returned [")
          .append(queryResults.size()).append("] results."));
    }
    catch (ObjectNotFoundException e) {
      logger_.debug(new StringBuilder().append("Transaction search query returned [0] results."));
      queryResults = null;
    }

    // nothing to work with...
    if (queryResults == null) {
      return EXEC_NO_RESULTS_;
    }

    /* Most transaction search use cases desire a result set sorted by most-recent-to-least-recent,
     * but in this case we want to preserve the data's chronological order.  Perform a post-query 
     * sorting to ensure this required ordering, first reversing the original results to hopefully 
     * improve that sorting's performance. */
    Collections.reverse(queryResults);
    Collections.sort(queryResults, new Comparator<DefaultTransactionSummaryModel>() {
      /** {@inheritDoc} */
      @Override
      public int compare(DefaultTransactionSummaryModel argO1, DefaultTransactionSummaryModel argO2) {
        long date1 = (argO1.getBeginDatetimestamp() == null) //
            ? Long.MAX_VALUE : argO1.getBeginDatetimestamp().getTime();
        long date2 = (argO2.getBeginDatetimestamp() == null) //
            ? Long.MAX_VALUE : argO2.getBeginDatetimestamp().getTime();

        return (date1 == date2) ? 0 : (date1 < date2) ? -1 : 1;
      }
    });

    // There are results, create a list and add the transactions.
    List<IPosTransaction> transactions = new LinkedList<IPosTransaction>();
    for (DefaultTransactionSummaryModel queryResult : queryResults) {
      transactions.add(access.getTransaction(queryResult.getTransactionId()));
    }

    // Write to the default log file.
    if (filename == null) {
      ILogBuilder builder = LogBuilder.getInstance();
      for (IPosTransaction transaction : transactions) {
        if (!builder.saveLogEntry(transaction)) {
          throw new RuntimeException("Failed to save default log file.");
        }
      }
    }

    // Use a different file.
    else {
      Map<TypeSafeMapKey<?>, Object> settings = new HashMap<TypeSafeMapKey<?>, Object>();
      settings.put(LogEntryFileWriter.TRAINING_FILE_PATH, null);
      settings.put(LogEntryFileWriter.FILE_PATH, new File(filename));
      for (IPosTransaction transaction : transactions) {
        if (!LogBuilder.getInstance().saveLogEntry(transaction, settings)) {
          throw new RuntimeException("Failed to save log file: " + filename);
        }
      }
    }
    return EXEC_SUCCESS_;
  }

  /**
   * Convenience method to run with string arguments instead of search criteria.
   * 
   * @param argStartDate start date
   * @param argEndDate end date
   * @param argStartLoc start location ID
   * @param argEndLoc end location ID
   * @param argStartReg start register ID
   * @param argEndReg end register ID
   * @param argStartTran start transaction sequence number
   * @param argEndTran end transaction sequence number
   * @return results of the execution.
   */
  public String exec(String argStartDate, String argEndDate, String argStartLoc, String argEndLoc,
      String argStartReg, String argEndReg, String argStartTran, String argEndTran) {
    return exec(argStartDate, argEndDate, argStartLoc, argEndLoc, argStartReg, argEndReg, argStartTran,
        argEndTran, null);
  }

  /**
   * Convenience method to run with string arguments instead of search criteria.
   * 
   * @param argStartDate start date
   * @param argEndDate end date
   * @param argStartLoc start location ID
   * @param argEndLoc end location ID
   * @param argStartReg start register ID
   * @param argEndReg end register ID
   * @param argStartTran start transaction sequence number
   * @param argEndTran end transaction sequence number
   * @param filename filename, null for default
   * @return results of the execution.
   */
  public String exec(String argStartDate, String argEndDate, String argStartLoc, String argEndLoc,
      String argStartReg, String argEndReg, String argStartTran, String argEndTran, String filename) {
    TransactionSearchCriteria criteria = new TransactionSearchCriteria();

    Date startDate = null, endDate = null;
    Long startLoc = null, endLoc = null;
    Long startReg = null, endReg = null;
    Long startTran = null, endTran = null;

    // Set dates
    try {
      if (argStartDate != null) {
        startDate = DateUtils.parseDate(argStartDate);
      }
      if (argEndDate != null) {
        endDate = DateUtils.parseDate(argEndDate);
      }
    }
    catch (Exception e) {
      logger_.warn("Error parsing dates.", e);
      startDate = endDate = null;
    }
    if ((startDate == null) && (endDate != null)) {
      startDate = endDate;
    }
    else if ((startDate != null) && (endDate == null)) {
      endDate = startDate;
    }

    // Set retail location.
    try {
      if (argStartLoc != null) {
        startLoc = Long.valueOf(argStartLoc);
      }
      if (argEndLoc != null) {
        endLoc = Long.valueOf(argEndLoc);
      }
    }
    catch (NumberFormatException nfe) {
      logger_.warn("Error parsing retail location IDs.", nfe);
      startLoc = endLoc = null;
    }
    if ((startLoc == null) && (endLoc != null)) {
      startLoc = endLoc;
    }
    else if ((startLoc != null) && (endLoc == null)) {
      endLoc = startLoc;
    }
    else if ((startLoc == null) && (endLoc == null)) {
      startLoc = new Long(0);
      endLoc = Long.MAX_VALUE;
    }

    // Set register.
    try {
      if (argStartReg != null) {
        startReg = Long.valueOf(argStartReg);
      }
      if (argEndReg != null) {
        endReg = Long.valueOf(argEndReg);
      }
    }
    catch (NumberFormatException nfe) {
      logger_.warn("Error parsing retail register IDs.", nfe);
      startReg = endReg = null;
    }
    if ((startReg == null) && (endReg != null)) {
      startReg = endReg;
    }
    else if ((startReg != null) && (endReg == null)) {
      endReg = startReg;
    }
    else if ((startReg == null) && (endReg == null)) {
      startReg = new Long(0);
      endReg = Long.MAX_VALUE;
    }

    // Set transaction.
    try {
      if (argStartTran != null) {
        startTran = Long.valueOf(argStartTran);
      }
      if (argEndTran != null) {
        endTran = Long.valueOf(argEndTran);
      }
    }
    catch (NumberFormatException nfe) {
      logger_.warn("Error parsing retail transaction IDs.", nfe);
      startTran = endTran = null;
    }
    if ((startTran == null) && (endTran != null)) {
      startTran = endTran;
    }
    else if ((startTran != null) && (endTran == null)) {
      endTran = startTran;
    }
    else if ((startTran == null) && (endTran == null)) {
      startTran = new Long(0);
      endTran = Long.MAX_VALUE;
    }

    // Set the criteria and run.
    criteria.setStartDate(startDate);
    criteria.setEndDate(endDate);
    criteria.setRetailLocationIdStart(startLoc);
    criteria.setRetailLocationIdEnd(endLoc);
    criteria.setRegisterIdStartAsLong(startReg);
    criteria.setRegisterIdEndAsLong(endReg);
    criteria.setTranIdStartAsLong(startTran);
    criteria.setTranIdEndAsLong(endTran);
    return exec(criteria, filename);
  }

  /**
   * Convenience method to run with command line arguments instead of search criteria.
   * 
   * @param args Command line arguments.
   * @return results of the execution.
   */
  public String exec(String[] args) {
    String startDate = null, endDate = null;
    String startLoc = null, endLoc = null;
    String startReg = null, endReg = null;
    String startTran = null, endTran = null;
    String filename = null;
    CmdLineArg arg = null;

    // Loop over all the command line arguments.
    if (args != null) {
      for (String currentArg : args) {
        arg = new CmdLineArg(currentArg);
        switch (arg.command_) {
          case ARG_DATE_:
            startDate = arg.start_;
            endDate = arg.end_;
            break;
          case ARG_STORE_:
            startLoc = arg.start_;
            endLoc = arg.end_;
            break;
          case ARG_REGISTER_:
            startReg = arg.start_;
            endReg = arg.end_;
            break;
          case ARG_SEQUENCE_:
            startTran = arg.start_;
            endTran = arg.end_;
            break;
          case ARG_FILENAME_:
            filename = arg.filename_;
            break;
          case ARG_HELP1_:
          case ARG_HELP2_:
          default:
            return USAGE_;
        }
      }
    }
    return exec(startDate, endDate, startLoc, endLoc, startReg, endReg, startTran, endTran, filename);
  }

  /**
   * Private inner class representing a command-line argument.
   * 
   * This is a convenience class used to contain information about a single argument. It stores the
   * command, which instructs any users of this class as to what it contains, e.g. a
   * {@link java.util.Date Date} range, register number, etc. It stores start and end values in
   * string format without interpreting them, because these values may be of multiple types.
   */
  private class CmdLineArg {
    final char command_;
    final String arg_;
    String start_ = null;
    String end_ = null;
    String filename_ = null;

    /**
     * The sole constructor for this class. Parses a command line argument.
     * 
     * This is where all the logic is. Given an argument, parse it and figure out its important
     * pieces. Store the results in the object state.
     * 
     * Valid arguments:
     * <ul>
     * <li>Cannot be null.</li>
     * <li>Must have 3 or more characters: one for the start delimiter, one for the command, one or
     * more for a value.</li>
     * <li>Must start with the proper delimiter (e.g. '-').</li>
     * </ul>
     * 
     * @param arg Single command line argument.
     */
    public CmdLineArg(String arg) {
      if ((arg == null) || (arg.length() < 3) || !arg.startsWith(String.valueOf(ARG_START_))) {
        throw new IllegalArgumentException("Could not process arg: " + arg);
      }

      arg_ = arg;

      arg = arg.substring(1).toLowerCase(); // trim the '-'
      command_ = arg.charAt(0);
      arg = arg.substring(1); // trim the command

      // parse dates specially, since they have dashes all through
      if (command_ == ARG_DATE_) {
        try {
          start_ = arg.substring(0, DATE_FORMAT.length());
        }
        catch (StringIndexOutOfBoundsException ee) {
          throw new IllegalArgumentException("Invalid start date: " + arg);
        }
        try {
          end_ = arg.substring(DATE_FORMAT.length() + 1, arg.length());
        }
        catch (StringIndexOutOfBoundsException ee) {
          end_ = null;
        }

        // only 1 date supplied, use for start & end
        if ((end_ == null) || (end_.trim().length() == 0)) {
          end_ = start_;
        }
      }

      // Parse filename, since it does not have a range
      else if (command_ == ARG_FILENAME_) {
        filename_ = arg;
      }

      // Parse all other types, which must be numerical.
      else {
        // The range character (e.g. '-') does not exist in the string. The
        // entire argument should be treated as a single numerical value.
        if (arg.indexOf(ARG_RANGE_) == -1) {
          start_ = end_ = arg;
        }

        // This argument has a range. Parse out the range delimiter and treat as
        // two separate numbers.
        else {
          StringTokenizer parser = new StringTokenizer(arg, String.valueOf(ARG_RANGE_));

          // A range should only have 2 tokens.
          if (parser.countTokens() != 2) {
            throw new IllegalArgumentException("Arguments with a range "
                + "delimiter must have exactly two values. " + "Error processing arg: " + arg_);
          }
          start_ = parser.nextToken();
          end_ = parser.nextToken();
        }
      }
    }
  }
}