// $Id: TaxModifierCalculator.java 57381 2011-12-12 14:13:49Z dtvdomain\jweiss $
package dtv.pos.register.tax;

import static dtv.pos.register.returns.ReturnType.VERIFIED;

import static dtv.util.NumberUtils.getRequiredFractionalDigits;
import static dtv.util.NumberUtils.isZeroOrNull;
import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.HALF_UP;

import java.math.BigDecimal;
import java.util.*;

import dtv.pos.common.ConfigurationMgr;
import dtv.pos.common.TransactionHelper;
import dtv.pos.pricing.AbstractCalculator;
import dtv.pos.register.returns.ReturnType;
import dtv.pos.register.type.LineItemType;
import dtv.util.NumberUtils;
import dtv.util.StringUtils;
import dtv.xst.dao.tax.ITaxExemption;
import dtv.xst.dao.tax.ITaxGroupRule;
import dtv.xst.dao.trl.*;

/**
 * This calculator updates the tax modifiers on a sale line item.<br>
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author dberkland
 * @created January 12, 2007
 * @version $Revision: 57381 $
 */
public class TaxModifierCalculator
    extends AbstractCalculator {

  private static final SalesTaxStrategy SALES_TAX_STRATEGY = new SalesTaxStrategy();
  private static final VatTaxStrategy VAT_STRATEGY = new VatTaxStrategy();

  private static BigDecimal getExtendedAmount(ISaleReturnLineItem argLine, Currency argCurrency) {
    BigDecimal extendedAmount = argLine.getExtendedAmount();
    // If its a zero extended amount sale line item, need to get the extended
    // amount from the customer item account modifier.
    if (argLine.getForceZeroExtendedAmt()) {
      ICustomerItemAccountModifier custAcctMod = argLine.getCustomerAccountModifier();

      if (custAcctMod != null) {
        if (custAcctMod.getItemAccountExtendedPrice() != null) {
          extendedAmount = custAcctMod.getItemAccountExtendedPrice();
        }
      }
      else {
        // Unless there is no modifier, then just calculate it like it normally would be calculated
        extendedAmount = argLine.getUnitPrice().multiply(argLine.getQuantity());
        // round to the appropriate number of decimal places
        extendedAmount = extendedAmount.setScale(argCurrency.getDefaultFractionDigits(), HALF_UP);
      }
    }

    return extendedAmount;
  }

  private static BigDecimal getSubTotal(ISaleTaxModifier argModifier, Currency argCurrency) {
    List<ISaleReturnLineItem> lines =
        argModifier.getParentLine().getParentTransaction()
            .getLineItemsByTypeCode(LineItemType.ITEM.getName(), ISaleReturnLineItem.class);

    if ((lines == null) || lines.isEmpty()) {
      return BigDecimal.ZERO;
    }

    BigDecimal subTotal = BigDecimal.ZERO;

    for (ISaleReturnLineItem line : lines) {
      if (line.getVoid()) {
        continue;
      }

      List<ISaleTaxModifier> modifiers = line.getTaxModifiers();
      if ((modifiers == null) || modifiers.isEmpty()) {
        continue;
      }

      for (ISaleTaxModifier mod : modifiers) {
        if (mod.getVoid()) {
          continue;
        }

        if (mod.getSaleTaxGroupRule().getTaxAuthorityId()
            .equals(argModifier.getSaleTaxGroupRule().getTaxAuthorityId())) {

          subTotal = subTotal.add(getExtendedAmount(line, argCurrency).abs());
          break;
        }
      }
    }
    return subTotal;
  }

  @Override
  public IRetailTransactionLineItem[] handleLineItemEvent(IRetailTransactionLineItem[] argLineItems) {
    // jump out of this calculator if configured to use an alternate tax system
    if (!(TaxSystemType.XSTORE.equals(TaxHelper.getInstance().getTaxSystemType()))) {
      return argLineItems;
    }

    for (IRetailTransactionLineItem line : argLineItems) {
      if (line.getVoid()) {
        continue;
      }

      if (line instanceof ISaleReturnLineItem) {
        
        ISaleReturnLineItem lineItem = (ISaleReturnLineItem) line;

        this.handleLineItemEvent(lineItem);
        updateForTaxExemption(lineItem);
      }
    }

    return argLineItems;
  }

  /**
   * Loop throught all sale return line item of the current retail transaction and calculate the taxable and
   * tax amount of the sale tax modifier that attached to the sale return line item.
   * 
   * @param argLineItem the line item that has been changed
   * @param argType the type of change to the line item
   */
  protected void handleLineItemEvent(ISaleReturnLineItem argLineItem) {
    TaxInfo ti = new TaxInfo();
    ti.rawTaxableAmount = getExtendedAmount(argLineItem, getCurrency());
    ti.taxTime = argLineItem.getOriginalBusinessDate();
    ti.itemQuantity = argLineItem.getQuantity();
    
    if (!argLineItem.getReturn()
        && !VERIFIED.matches(argLineItem.getReturnTypeCode())) {
      ti.transTaxExempt = ((IRetailTransaction) argLineItem.getParentTransaction()).getTaxExemption() != null;
    } 
    

    BigDecimal vatAmount = ZERO;
    BigDecimal compoundTaxAmount = ZERO;

    // Need to separate the compound and regular tax modifiers first
    List<ISaleTaxModifier> compoundTaxMods = new ArrayList<ISaleTaxModifier>();
    List<ISaleTaxModifier> regularTaxMods = new ArrayList<ISaleTaxModifier>();

    for (ISaleTaxModifier modifier : argLineItem.getTaxModifiers()) {
      if (modifier.getVoid()) {
        continue;
      }

      if (modifier.getSaleTaxGroupRule().getCompound()) {
        compoundTaxMods.add(modifier);
      }
      else {
        regularTaxMods.add(modifier);
      }
    }

    boolean compoundTaxIncludeOtherCompTaxAmt = ConfigurationMgr.getCompoundTaxIncludeOtherCompTaxAmt();
    // Calculate compound tax first
    for (ISaleTaxModifier mod : compoundTaxMods) {
      ti.modifier = mod;
      ti.taxableAmount = mod.getTaxableAmount();

      if (compoundTaxIncludeOtherCompTaxAmt) {
        // If configure to include other compound tax amount as taxable amount
        ti.taxableAmount = ti.taxableAmount.add(compoundTaxAmount);
        ti.rawTaxableAmount = ti.rawTaxableAmount.add(compoundTaxAmount);
      }

      if (isVatTax(mod)) {
        vatAmount = vatAmount.add(calculateModifierTax(VAT_STRATEGY, ti));
      }
      else {
        compoundTaxAmount = compoundTaxAmount.add(calculateModifierTax(SALES_TAX_STRATEGY, ti));
      }
    }

    // Calculate regular tax
    for (ISaleTaxModifier mod : regularTaxMods) {
      ti.modifier = mod;
      ti.taxableAmount = mod.getTaxableAmount().add(compoundTaxAmount);

      if (isVatTax(mod)) {
        vatAmount = vatAmount.add(calculateModifierTax(VAT_STRATEGY, ti));
      }
      else {
        calculateModifierTax(SALES_TAX_STRATEGY, ti);
      }
    }

    if (!NumberUtils.isZero(vatAmount)) {
      argLineItem.setVatAmount(vatAmount);
    }
  }

  protected boolean reverseCalculation(TaxInfo ti) {
    if (ti.modifier.getTaxOverride()) {
      return true;
    }
    ISaleReturnLineItem parentLine = ti.modifier.getParentLine();

    return TransactionHelper.isVerifiedReturn(parentLine) //
        || TransactionHelper.isRemoteSendRefund(parentLine)//
        || TransactionHelper.isRemoteSendPickup(parentLine);
  }

  private BigDecimal calculateModifierTax(ITaxStrategy argTaxStrategy, TaxInfo argTi) {
    BigDecimal rawTaxAmount = ZERO;

    if (!isZeroOrNull(argTi.itemQuantity)) {
      rawTaxAmount =
          argTaxStrategy.calculateRawAmount(argTi.rawTaxableAmount, argTi.itemQuantity, argTi.modifier,
              argTi.taxTime, getSubTotal(argTi.modifier, getCurrency()));
    }

    /* To prevent tax from being represented in scientific notation in certain scenarios, scale the result to
     * the greatest precision necessary to avoid data loss or to two digits, whichever is greater. */
    rawTaxAmount = rawTaxAmount.setScale(//
        BigDecimal.valueOf(getRequiredFractionalDigits(rawTaxAmount)).max(BigDecimal.valueOf(2)).intValue());

    // check if exempted
    if (!StringUtils.isEmpty(argTi.modifier.getTaxExemptionId()) || argTi.transTaxExempt) {
      // Raw tax amount and exempted tax amount for exempted VAT taxes are all set in the
      // UnitPriceResetCalculator for reasons explained there.
      if (!isVatTax(argTi.modifier)) {
        argTi.modifier.setRawTaxAmount(rawTaxAmount);
        argTi.modifier.setExemptTaxAmount(rawTaxAmount);
      }

      argTi.modifier.setTaxableAmount(ZERO);
      argTi.modifier.setTaxAmount(ZERO);
      argTi.modifier.setTaxPercentage(ZERO);
      return ZERO;
    }
    else if ((reverseCalculation(argTi) && isTransLevelTaxing(argTi)) || argTi.modifier.getTaxOverride()) {
      /* 2007-02-09 John Gaughan Activity 192369 If the user changes the tax location, the operations that
       * feed into this calculator set the override flag but do not calculate the new amount or percentage.
       * They do not have enough information to do so and it is not their job anyway; that is what we have
       * calculators for. This next block of code accounts for a tax location override where the amount and
       * percentage are both null. Simply take the pre-calculated amount/percentage from the normal
       * calculations and apply it to the override amounts before continuing the override calculations. */
      if ((argTi.modifier.getTaxOverrideAmount() == null)
          && (argTi.modifier.getTaxOverridePercentage() == null)) {

        argTi.modifier.setTaxOverrideAmount(rawTaxAmount);
        argTi.modifier.setTaxOverridePercentage(argTi.modifier.getRawTaxPercentage());
      }

      BigDecimal overriddenAmt =
          argTaxStrategy.calculateOverrideAmt(argTi.modifier, argTi.taxableAmount, argTi.itemQuantity);

      BigDecimal overriddenPct =
          argTaxStrategy.calculateOverridePct(argTi.modifier, argTi.taxableAmount, argTi.itemQuantity);

      argTi.modifier.setRawTaxAmount(rawTaxAmount);
      argTi.modifier.setTaxableAmount(argTi.taxableAmount);
      argTi.modifier.setTaxAmount(overriddenAmt);
      argTi.modifier.setTaxPercentage(overriddenPct);
      argTi.modifier.setTaxExemptAmount(ZERO);

      return overriddenAmt;
    }
    else {
      BigDecimal taxAmount =
          argTaxStrategy.calculateRawAmount(argTi, getSubTotal(argTi.modifier, getCurrency()));

      /* To prevent tax from being represented in scientific notation in certain scenarios, scale the result
       * to the greatest precision necessary to avoid data loss or to two digits, whichever is greater. */
      taxAmount = taxAmount.setScale(//
          BigDecimal.valueOf(getRequiredFractionalDigits(taxAmount)).max(BigDecimal.valueOf(2)).intValue());

      BigDecimal taxPercentage = argTaxStrategy.reverseCalculatePercentage(argTi.taxableAmount, taxAmount);
      argTi.modifier.setRawTaxAmount(rawTaxAmount);
      argTi.modifier.setTaxExemptAmount(ZERO);
      argTi.modifier.setTaxableAmount(argTi.taxableAmount);
      argTi.modifier.setTaxAmount(taxAmount);
      argTi.modifier.setTaxPercentage(taxPercentage);

      return taxAmount;
    }
  }

  /* Returns whether the specified tax info indicates that taxation is performed at the transaction level as
   * opposed to the item level. */
  private boolean isTransLevelTaxing(TaxInfo argTaxInfo) {
    ITaxGroupRule groupRule = argTaxInfo.modifier.getSaleTaxGroupRule();
    return ((groupRule != null) && groupRule.getTaxedAtTransLevel());
  }

  /**
   * Returns whether or not a sale tax modifier represents a VAT tax.
   * 
   * @param argTaxModifier the sale tax modifier to check
   * @return whether or not <code>argTaxModifier</code> represents a VAT tax
   */
  private boolean isVatTax(ISaleTaxModifier argTaxModifier) {
    TaxType taxType = TaxType.forName(argTaxModifier.getSaleTaxGroupRule().getTaxTypeCode());
    return TaxType.VAT.equals(taxType);
  }

  private void updateForTaxExemption(ISaleReturnLineItem item) {
    
    if (item.getReturn()
        && VERIFIED.matches(item.getReturnTypeCode())) {
      return;
    } 
    
    ITaxExemption taxExemption = ((IRetailTransaction) item.getParentTransaction()).getTaxExemption();

    if (taxExemption != null) {
      List<ISaleTaxModifier> list = item.getTaxModifiers();
      for (ISaleTaxModifier mod : list) {
        if (StringUtils.isEmpty(mod.getTaxExemptionId())) {
          mod.setTaxExemptionId(taxExemption.getTaxExemptionId());
        }
      }
    }
  }

  /**
   * Structure-type class for passing a group of data between methods internally.
   */
  static class TaxInfo {
    BigDecimal taxableAmount;
    BigDecimal itemQuantity;
    ISaleTaxModifier modifier;
    Date taxTime;
    BigDecimal rawTaxableAmount;
    boolean transTaxExempt;
  }
}