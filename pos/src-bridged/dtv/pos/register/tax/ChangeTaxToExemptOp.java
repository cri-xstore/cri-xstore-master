// $Id: ChangeTaxToExemptOp.java 55880 2011-03-31 16:33:12Z dtvdomain\twhite $
package dtv.pos.register.tax;

import static dtv.pos.register.returns.ReturnType.VERIFIED;

import static dtv.xst.daocommon.LocaleInfo.currencyFractionDigits;
import static java.math.RoundingMode.HALF_UP;

import java.math.BigDecimal;
import java.util.*;

import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.register.type.LineItemType;
import dtv.util.NumberUtils;
import dtv.xst.dao.tax.ITaxExemption;
import dtv.xst.dao.trl.*;

/**
 * Operation that changes a tax to be exempt,<br>
 * <br>
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author dberkland
 * @created July 21, 2003
 * @version $Revision: 55880 $
 */
public class ChangeTaxToExemptOp
    extends AbstractChangeTaxOp {

  private static final long serialVersionUID = 1L;

  private static BigDecimal getExtendedAmount(ISaleReturnLineItem argLine) {
    BigDecimal extendedAmount = argLine.getExtendedAmount();
    // If its a zero extended amount sale line item, need to get the extended
    // amount from the customer item account modifier.
    if (argLine.getForceZeroExtendedAmt()) {
      ICustomerItemAccountModifier custAcctMod = argLine.getCustomerAccountModifier();

      if (custAcctMod != null) {
        if (custAcctMod.getItemAccountExtendedPrice() != null) {
          extendedAmount = custAcctMod.getItemAccountExtendedPrice();
        }
      }
      else {
        // Unless there is no modifier, then just calculate it like it normally would be calculated
        extendedAmount = argLine.getUnitPrice().multiply(argLine.getQuantity());
        // round to the appropriate number of decimal places
        extendedAmount = extendedAmount.setScale(currencyFractionDigits(), HALF_UP);
      }
    }

    return extendedAmount;
  }

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IChangeTaxCmd cmd = (IChangeTaxCmd) argCmd;

    // if only one, use it as normal
    if (cmd.getChangeItem() != null) {
      return super.handleOpExec(argCmd, argEvent);
    }

    // note: this op may run in a context where there is only a change item but
    // no current transaction.

    IRetailTransaction tran = cmd.getRetailTransaction();
    List<ITaxLineItem> taxLines = tran.getLineItemsByTypeCode(LineItemType.TAX.getName(), ITaxLineItem.class);
    taxLines = TaxHelper.getInstance().removeVoidedTaxLines(taxLines);

    if (taxLines.size() == 1) {
      cmd.setChangeItem(taxLines.get(0));
      return super.handleOpExec(argCmd, argEvent);
    }
    else {
      for (ITaxLineItem tli : taxLines) {
        cmd.setChangeItem(tli);
        handleChangeTaxLine(cmd, tli);
      }
      return HELPER.completeResponse();
    }
  }

  /** {@inheritDoc} */
  @Override
  protected void changeTax(IChangeTaxCmd argCmd, ITaxLineItem argTaxLineItem) {}

  /** {@inheritDoc} */
  @Override
  protected void changeTax(IChangeTaxCmd argCmd, Map<Integer, List<ISaleTaxModifier>> argHash) {
    Set<Integer> keys = argHash.keySet();
    if ((keys == null) || keys.isEmpty()) {
      return;
    }

    for (Integer key : keys) {

      List<ISaleTaxModifier> mods = argHash.get(key);
      if ((mods == null) || mods.isEmpty()) {
        continue;
      }

      ITaxExemption exemption = argCmd.getTaxExemption();
      String exemptId = null;
      if (exemption != null) {
        exemptId = exemption.getTaxExemptionId();
      }

      /* Look if there are any tax authorities configured for selected tax exemption reason code. If
       * configured, exempt tax for configured tax authoritoes only. If not configured, exempt tax for all tax
       * autorities. */
      String[] exemptTaxAuthorities = null;

      for (ISaleTaxModifier mod : mods) {
        boolean exempt = false;
        if (exemptTaxAuthorities != null) {
          for (String exemptTaxAuthoritie : exemptTaxAuthorities) {
            if (!exempt & (mod.getSaleTaxGroupRule().getTaxAuthorityId().equals(exemptTaxAuthoritie))) {
              exempt = true;

            }

          }
        }
        else {
          exempt = true;
        }
        if (exempt) {
          
          if (mod.getParentLine().getReturn()
              && VERIFIED.matches(mod.getParentLine().getReturnTypeCode())) {
            return;
          } 
          
          BigDecimal extendedPrice = getExtendedAmount(mod.getParentLine()).abs();

          mod.setTaxExemptAmount(extendedPrice);
          mod.setTaxAmount(NumberUtils.ZERO);
          mod.setTaxableAmount(NumberUtils.ZERO);
          mod.setTaxExemptionId(exemptId);
          mod.setTaxOverrideBracket(null);
        }
      }
    }
  }
}
