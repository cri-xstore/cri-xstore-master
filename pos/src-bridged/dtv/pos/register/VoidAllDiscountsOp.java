//$Id: VoidAllDiscountsOp.java 732 2013-07-02 14:25:53Z dtvdomain\bli $
package dtv.pos.register;

import java.util.List;

import dtv.pos.common.PromptKey;
import dtv.pos.framework.ui.op.AbstractPromptOp;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.xst.dao.trl.*;

/**
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author attributed
 * @version $Revision: 732 $
 */
public class VoidAllDiscountsOp
    extends AbstractPromptOp {

  private static final long serialVersionUID = 1L;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IOpState state = argCmd.getOpState();

    if (state != null) {
      return super.handleOpExec(argCmd, argEvent);
    }

    // convert the command
    ISaleReturnLineItemCmd cmd = (ISaleReturnLineItemCmd) argCmd;

    // get the sale line
    ISaleReturnLineItem saleLine = cmd.getLineItem();

    // get the price modifiers
    List<IRetailPriceModifier> priceModifiers = saleLine.getRetailPriceModifiers();

    // if the list is empty then return
    if (priceModifiers.isEmpty()) {
      return HELPER.completeResponse();
    }

    // loop through the price modifiers and void them
    RetailPriceModifierReasonCode reason;
    boolean showPrompt = false;
    IRetailPriceModifier modifier = null;

    for(int i=0;i < priceModifiers.size();i++){
      //System.out.println("length = "+priceModifiers.size()+" - "+Integer.valueOf(++count).intValue());
      modifier = priceModifiers.get(i);

      // if the modifier is void then continue
      if (modifier.getVoid()) {
        continue;
      }

      reason = RetailPriceModifierReasonCode.forName(modifier.getRetailPriceModifierReasonCode());

      if ((reason != RetailPriceModifierReasonCode.LINE_ITEM_DISCOUNT)
          && (reason != RetailPriceModifierReasonCode.PRICE_OVERRIDE)
          && (reason != RetailPriceModifierReasonCode.PROMPT_PRICE_CHANGE)) {
        continue;
      }

      modifier.setVoid(true);
      if (reason == RetailPriceModifierReasonCode.LINE_ITEM_DISCOUNT) {
        showPrompt = true;
      }
    }
    
   /*    
    for (IRetailPriceModifier modifier : priceModifiers) {
      System.out.println("length = "+priceModifiers.size()+" - "+Integer.valueOf(++count).intValue());

      // if the modifier is void then continue
      if (modifier.getVoid()) {
        continue;
      }

      reason = RetailPriceModifierReasonCode.forName(modifier.getRetailPriceModifierReasonCode());

      if ((reason != RetailPriceModifierReasonCode.LINE_ITEM_DISCOUNT)
          && (reason != RetailPriceModifierReasonCode.PRICE_OVERRIDE)
          && (reason != RetailPriceModifierReasonCode.PROMPT_PRICE_CHANGE)) {
        continue;
      }

      modifier.setVoid(true);
      if (reason == RetailPriceModifierReasonCode.LINE_ITEM_DISCOUNT) {
        showPrompt = true;
      }
    }*/
    
    saleLine.setRetailPriceModifiers(saleLine.getRetailPriceModifiers());

    if (showPrompt) {
      return super.handleOpExec(argCmd, argEvent);
    }

    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  protected IPromptKey getPromptKey(IXstCommand argCmd) {
    return PromptKey.valueOf("DISCOUNT_AUTO_REMOVE");
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    // do nothing... we don't care about the response
    return HELPER.completeResponse();
  }
}
