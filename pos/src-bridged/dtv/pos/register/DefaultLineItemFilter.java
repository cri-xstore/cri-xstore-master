//$Id: DefaultLineItemFilter.java 753 2013-10-17 14:40:32Z dtvdomain\bli $
package dtv.pos.register;

import static dtv.pos.common.ConfigurationMgr.getDisplayCommissionedAssociatesPerReturnItem;
import static dtv.pos.common.ConfigurationMgr.getDisplayCommissionedAssociatesPerSaleItem;
import static dtv.util.NumberUtils.*;

import java.util.*;

import org.apache.log4j.Logger;

import dtv.data2.access.IDataModel;
import dtv.pos.commission.CommissionModifierFilterDelegate;
import dtv.pos.common.ConfigurationMgr;
import dtv.pos.register.returns.ReturnManager;
import dtv.pos.register.returns.ReturnType;
import dtv.pricing2.DealLineItemComparator;
import dtv.pricing2.PricingDeal;
import dtv.util.StringUtils;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.ttr.IIdentityVerification;
import dtv.xst.dao.ttr.ITenderLineItem;
import dtv.xst.daocommon.ILineItemFilter;
import dtv.xst.daocommon.ILineItemFilterDelegate;
import dtv.xst.pricing.XSTPricingDescriptor;

/**
 * A class that filters a list of line items down to what should be displayed.<br>
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author dberkland
 * @created November 18, 2003
 * @version $Revision: 753 $
 */
public class DefaultLineItemFilter
    implements ILineItemFilter {

  private static final Logger _logger = Logger.getLogger(DefaultLineItemFilter.class);
  private static final String IMPL_KEY = DefaultLineItemFilter.class.getName();

  /**
   * Creates a delegate for use by this line-item filter.
   * @return a delegate for use by this line-item filter
   */
  private static ILineItemFilter createDelegateFilter() {
    ILineItemFilter delegate = null;

    final String delegateClass = System.getProperty(IMPL_KEY);
    if (!StringUtils.isEmpty(delegateClass)) {
      try {
        delegate = (ILineItemFilter) Class.forName(delegateClass).newInstance();
      }
      catch (Exception ex) {
        _logger.error("Invalid line-item filter mapped to key [" + IMPL_KEY + "]!", ex);
      }
    }
    return (delegate != null) ? delegate : new Filter();
  }

  private final ILineItemFilter _impl;

  public DefaultLineItemFilter() {
    this(createDelegateFilter());
  }

  public DefaultLineItemFilter(ILineItemFilter argDelegate) {
    super();
    _impl = (argDelegate != null) ? argDelegate : new Filter();
  }

  /** {@inheritDoc} */
  @Override
  public List<? extends IDataModel> filter(List<? extends IDataModel> argTranLineItems) {
    return _impl.filter(argTranLineItems);
  }

  /** {@inheritDoc} */
  @Override
  public void setItemSortComparator(Comparator<IDataModel> argNewComparator) {
    _impl.setItemSortComparator(argNewComparator);
  }

  /** A default line-item filter delegate. */
  public static class Filter
      implements ILineItemFilter {

    protected static final boolean DISPLAY_VOIDED_LINE_ITEMS = ConfigurationMgr.displayVoidedLineItems();

    private static final Set<RetailPriceModifierReasonCode> DISCOUNT_TYPES_TO_DISPLAY;
    private static final boolean DISPLAY_NEGATIVE_DEALS = ConfigurationMgr.getDisplayNegativeDeals();
    private static final boolean DISPLAY_LAYAWAY_ITEMS_ON_TRANSACTION_LIST = ConfigurationMgr
        .getDisplayLayawayItemOnTransactionList();

    static {
      Set<RetailPriceModifierReasonCode> s = new HashSet<RetailPriceModifierReasonCode>(5);
      s.add(RetailPriceModifierReasonCode.LINE_ITEM_DISCOUNT);
      s.add(RetailPriceModifierReasonCode.GROUP_DISCOUNT);
      s.add(RetailPriceModifierReasonCode.TRANSACTION_DISCOUNT);
      s.add(RetailPriceModifierReasonCode.DEAL);
      s.add(RetailPriceModifierReasonCode.MANUFACTURER_COUPON);
      s.add(RetailPriceModifierReasonCode.NEW_PRICE_RULE);
      DISCOUNT_TYPES_TO_DISPLAY = Collections.unmodifiableSet(s);
    }

    @SuppressWarnings("unchecked")
    private static List<IRetailTransactionLineItem> cast(List<? extends IDataModel> l) {
      return (List<IRetailTransactionLineItem>) l;
    }

    private static final boolean isDealModifier(IRetailPriceModifier modifier) {
      return RetailPriceModifierReasonCode.DEAL.getName().equals(modifier.getRetailPriceModifierReasonCode());
    }

    @SuppressWarnings("rawtypes")
    private final List<ILineItemFilterDelegate> _delegates;

    private Comparator<IDataModel> lineItemComparator_ = null;

    @SuppressWarnings("rawtypes")
    public Filter() {
      super();
      _delegates = new ArrayList<ILineItemFilterDelegate>();
      updateFilterDelegates(_delegates);
    }

    /** {@inheritDoc} */
    @Override
    public List<IDataModel> filter(List<? extends IDataModel> argIn) {
      List<IDataModel> result = new ArrayList<IDataModel>();

      for (IRetailTransactionLineItem line : cast(argIn)) {
        // check the line
        if (includeLine(line)) {
          result.add(line);

          if (line instanceof ISaleReturnLineItem) {
            addSaleLineDetails(result, (ISaleReturnLineItem) line);
          }
          else if (line instanceof ITenderLineItem) {
            addTenderLineDetails(result, (ITenderLineItem) line);
          }
        }
      }
      if (lineItemComparator_ != null) {
        Collections.sort(result, lineItemComparator_);
      }
      return result;
    }

    /** {@inheritDoc} */
    @Override
    public void setItemSortComparator(Comparator<IDataModel> argComparator) {
      lineItemComparator_ = argComparator;
    }

    /**
     * Adds identity verification info to a display line.
     * 
     * @param argResult the list of display lines
     * @param argLine the current line
     */
    protected void addIdentityVerifications(List<IDataModel> argResult, ITenderLineItem argLine) {
      for (IIdentityVerification id : argLine.getIdentityVerifications()) {
        argResult.add(id);
      }
    }

    /**
     * Adds line notes to display to the output list.
     * 
     * @param argResult the list of display lines
     * @param argLine the current line
     */
    protected final void addLineNotes(List<IDataModel> argResult, ISaleReturnLineItem argLine) {
      if (includeLineNotes()) {
        for (IRetailTransactionLineItemNotes note : argLine.getNoteSeq()) {
          argResult.add(note);
        }
      }
    }

    /**
     * Adds price modifiers to display to the output list.
     * 
     * @param argResult the list of display lines
     * @param argLine the current line
     */
    protected final void addPriceModifiers(List<IDataModel> argResult, ISaleReturnLineItem argLine) {
      List<IRetailPriceModifier> itemPriceModifierList = new ArrayList<IRetailPriceModifier>();
      List<IDataModel> transPriceModifierList = new ArrayList<IDataModel>();
      
      if (includePriceModifiers()) {
        for (IRetailPriceModifier modifier : argLine.getRetailPriceModifiers()) {
          if (includePriceModifier(modifier)) {
            //argResult.add(modifier);
            transPriceModifierList.add(modifier);
          }else{
            itemPriceModifierList.add(modifier);
          }
        }
        //sort the deal price modifier in transaction object to make sure the order is the same as the deal applied 
        orderDealPriceModifiers(transPriceModifierList,argLine);
        argResult.addAll(transPriceModifierList);
        
        //sort the deal price modifier in saleReturnLineItem object to make sure the order is the same as the deal applied 
        for(IDataModel mdf :transPriceModifierList){
          itemPriceModifierList.add((IRetailPriceModifier)mdf);
        }
        argLine.setRetailPriceModifiers(itemPriceModifierList);
      }
    }

    /**
     * Add sale line details to a display line.
     * @param argResult the list of display lines.
     * @param argLine the current line.
     */
    protected void addSaleLineDetails(List<IDataModel> argResult, ISaleReturnLineItem argLine) {
      /* 2/26/06 JRW - Previously, the following block was outside of the includeLine() check.  In 
       * other words, it was possible for price modifiers on unincluded line items to be included in 
       * the list.  For example, if you sold two items, voided the first, and then added a 
       * transaction discount, the pro-rated discount modifier attached to the first, now-voided 
       * line would display (though it would not be included in total calculations).  It's possible 
       * that this is not the right place to address this bug; maybe price modifiers shouldn't be 
       * added to voided line items, as they currently are in 
       * dtv.pos.pricing.discount.AddTransDiscountPriceModifiersOp */
      addPriceModifiers(argResult, argLine);
      addLineNotes(argResult, argLine);
      
      for (ILineItemFilterDelegate<ISaleReturnLineItem> delegate : getFilterDelegates()) {
        delegate.addDetails(argResult, argLine);
      }
    }

    /**
     * Adds tender line details to a display line.
     * 
     * @param argResult the list of display lines
     * @param argLine the current line
     */
    protected void addTenderLineDetails(List<IDataModel> argResult, ITenderLineItem argLine) {
      if (includeIdentityVerifications()) {
        addIdentityVerifications(argResult, argLine);
      }
    }

    /**
     * Returns the filter delegates assigned to this filter.
     * @return the filter delegates assigned to this filter
     */
    @SuppressWarnings("rawtypes")
    protected List<ILineItemFilterDelegate> getFilterDelegates() {
      return _delegates;
    }

    /**
     * Gets whether to include identity verification info.
     * @return whether to include identity verification info
     */
    protected boolean includeIdentityVerifications() {
      return false;
    }

    /**
     * Indicates whether to include a line.
     * 
     * @param argLine the line to check
     * @return whether to include the line
     */
    protected boolean includeLine(IRetailTransactionLineItem argLine) {
      // remove voided lines
      if (argLine.getVoid() && !DISPLAY_VOIDED_LINE_ITEMS) {
        return false;
      }

      // Any line items annotated as undisplayable should be filtered out
      // of the display list.
      if (argLine instanceof UndisplayableLineItem) {
        return false;
      }

      if (argLine instanceof ISaleReturnLineItem) {
        // Configurably remove layaway items
        if (!DISPLAY_LAYAWAY_ITEMS_ON_TRANSACTION_LIST) {
          ISaleReturnLineItem saleLine = (ISaleReturnLineItem) argLine;
          String type = saleLine.getSaleReturnLineItemTypeCode();

          if (SaleItemType.LAYAWAY.matches(type)) {
            return false;
          }
        }
      }
      return true;
    }

    /**
     * Indicates whether to include line notes.
     * @return <code>true</code> if line notes should be included; <code>false</code> otherwise
     */
    protected boolean includeLineNotes() {
      return true;
    }

    /**
     * Gets whether to include a modifier.
     * 
     * @param modifier the modifier to check
     * @return whether to include the modifier
     */
    protected boolean includePriceModifier(IRetailPriceModifier modifier) {
      if (modifier.getVoid()) {
        return false;
      }
      RetailPriceModifierReasonCode reason =
          RetailPriceModifierReasonCode.forName(modifier.getRetailPriceModifierReasonCode());

      if (!DISCOUNT_TYPES_TO_DISPLAY.contains(reason)) {
        return false;
      }

      if (isDealModifier(modifier)) {
        if (isZeroOrNull(modifier.getAmount())) {
          return false;
        }
        else if (!DISPLAY_NEGATIVE_DEALS) {
          /* Suppress the deal if its amount adds to the "magnitude" of the transaction total; 
           * i.e. if it has the same sign as the merchandise line to which it is attached. But 
           * note that the signs of deal modifiers have been inverted at this point such that a
           * deal which reduces the cost of a sale line has a positive sign, so the boolean 
           * logic needs to be counter-intuitive. */
          return (modifier.getParentLine().getReturn()) //
              ? isNegative(modifier.getAmount()) //
              : isPositive(modifier.getAmount());
        }
      }
      return true;
    }

    /**
     * Indicates whether to include any price modifiers.
     * @return whether to include any price modifiers
     */
    protected boolean includePriceModifiers() {
      return true;
    }

    /**
     * Updates the set of filter delegates assigned to this filter.
     */
    @SuppressWarnings("rawtypes")
    protected void updateFilterDelegates(List<ILineItemFilterDelegate> argDelegates) {
      if (getDisplayCommissionedAssociatesPerSaleItem() || getDisplayCommissionedAssociatesPerReturnItem()) {
        argDelegates.add(new CommissionModifierFilterDelegate());
      }
    }
    
    
   //sort the deal price modifier in transaction object to make sure the order is the same as the deal applied 
   protected void orderDealPriceModifiers(List<IDataModel> argResult, ISaleReturnLineItem argLine){
     int idx=0;
     XSTPricingDescriptor xstpd = null;
     DealLineItemComparator comparator = DealLineItemComparator.getInstance();
     List<PricingDeal<XSTPricingDescriptor>> dealList= argLine.getParentTransaction().getCurrentPossibleDeals();
     List<PricingDeal<XSTPricingDescriptor>> newOrderDealList = new LinkedList<PricingDeal<XSTPricingDescriptor>>();
     List<IDataModel> priceModifierList = new ArrayList<IDataModel>();
     ReturnType returnType = ReturnManager.getInstance().getCurrentReturnType();
     if(!ReturnType.VERIFIED.equals(returnType)){
       for (IDataModel modifier : argResult) {
         if (isDealModifier((IRetailPriceModifier)modifier)) {
           for(PricingDeal<XSTPricingDescriptor> prcDeal : dealList){
             xstpd = prcDeal.getNativeDeal();
             if(xstpd !=null && StringUtils.nonNull(xstpd.getDealId()).equals(StringUtils.nonNull(((IRetailPriceModifier)modifier).getDealId()))){
               idx = Math.abs(iteratorBinarySearch(newOrderDealList, prcDeal, comparator) + 1);
               newOrderDealList.add(idx, prcDeal);
               priceModifierList.add(idx,modifier);
             }
           }
         }else{
           priceModifierList.add(modifier);
         } 
       }
       
       if(priceModifierList.size() > 0){
         argResult.clear();
         argResult.addAll(priceModifierList);
       }
       
       //orderItemDealPriceModifiers(argLine,comparator,dealList);
       comparator = null;
       dealList = null;
       newOrderDealList = null;
       priceModifierList = null;
   }
 }
   
/*   
   protected void orderItemDealPriceModifiers(ISaleReturnLineItem argLine,DealLineItemComparator comparator,List<PricingDeal<XSTPricingDescriptor>> dealList){
     int idx=0;
     XSTPricingDescriptor xstpd = null;
     List<PricingDeal<XSTPricingDescriptor>> newOrderDealList = new LinkedList<PricingDeal<XSTPricingDescriptor>>();
     List<IRetailPriceModifier> priceModifierList = new ArrayList<IRetailPriceModifier>();
    
       for (IRetailPriceModifier modifier : argLine.getRetailPriceModifiers()) {
         if (isDealModifier(modifier)) {
           for(PricingDeal<XSTPricingDescriptor> prcDeal : dealList){
             xstpd = prcDeal.getNativeDeal();
             if(xstpd !=null && StringUtils.nonNull(xstpd.getDealId()).equals(StringUtils.nonNull((modifier.getDealId())))){
               idx = Math.abs(iteratorBinarySearch(newOrderDealList, prcDeal, comparator) + 1);
               newOrderDealList.add(idx, prcDeal);
               priceModifierList.add(idx,modifier);
             }
           }
         }else{
           priceModifierList.add(modifier);
         } 
       }
       
       if(priceModifierList.size() == argLine.getRetailPriceModifiers().size()){
         argLine.setRetailPriceModifiers(priceModifierList);
       }
       
       newOrderDealList = null;
   } 
   */
   
   private  <T> int iteratorBinarySearch(List<? extends T> l, T key, Comparator<? super T> c) {
     int low = 0;
     int high = l.size()-1;
     ListIterator<? extends T> i = l.listIterator();

     while (low <= high) {
       int mid = (low + high) >>> 1;
       T midVal = get(i, mid);
       int cmp = c.compare(midVal, key);

       if (cmp < 0)
         low = mid + 1;
       else if (cmp > 0)
         high = mid - 1;
       else
         return mid; // key found
     }
     return -(low + 1);  // key not found
   }
   
   
   private  <T> T get(ListIterator<? extends T> i, int index) {
     T obj = null;
     int pos = i.nextIndex();
     if (pos <= index) {
       do {
         obj = i.next();
       } while (pos++ < index);
     } else {
       do {
         obj = i.previous();
       } while (--pos > index);
     }
     return obj;
   }
   
   
  }
}
