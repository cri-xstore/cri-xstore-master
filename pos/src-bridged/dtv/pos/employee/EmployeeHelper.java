// $Id: EmployeeHelper.java 823 2015-01-29 14:32:52Z dtvdomain\vraman $
package dtv.pos.employee;

import static dtv.pos.common.ConfigurationMgr.getDefaultEmployeeGroup;
import static dtv.pos.storecalendar.StoreCalendar.getCurrentBusinessDate;

import java.io.IOException;
import java.util.*;

import org.apache.log4j.Logger;

import dtv.data2.access.*;
import dtv.data2.access.pm.PersistenceManagerType;
import dtv.i18n.LocaleManager;
import dtv.i18n.OutputContextType;
import dtv.pos.assistance.AssistanceHelper;
import dtv.pos.assistance.typesafe.AssistanceTypes;
import dtv.pos.commission.ClockedInComparator;
import dtv.pos.common.*;
import dtv.pos.customer.CustomerHelper;
import dtv.pos.framework.StationModelMgr;
import dtv.pos.iframework.type.*;
import dtv.pos.storecalendar.StoreCalendar;
import dtv.util.*;
import dtv.util.password.PasswordHash;
import dtv.util.sequence.SequenceFactory;
import dtv.xst.dao.com.CodeLocator;
import dtv.xst.dao.crm.ICustomerAffiliation;
import dtv.xst.dao.crm.IParty;
import dtv.xst.dao.hrs.*;
import dtv.xst.dao.loc.IRetailLocation;
import dtv.xst.dao.sec.GroupId;
import dtv.xst.dao.sec.IGroup;
import dtv.xst.query.results.CommAssocSearchResult;
import dtv.xst.query.results.EmployeeSearchResult;

/**
 * This class provides all of the data store related logic for employee maintenance.<br>
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author dberkland
 * @created October 8, 2003
 * @version $Revision: 823 $
 */
// Tom - A general question is whether or not the StoreCalendar.getCurrentBusinessDate should be used (as
// opposed to the system date).
// This impacts functions like isTerminated(), where I could have been terminated, but since the store isn't
// open yet, I'm able to
// use the system.
public class EmployeeHelper
    implements IEmployeeHelper {

  private static final Logger logger_ = Logger.getLogger(EmployeeHelper.class);
  private static IEmployeeHelper INSTANCE;

  public static final String SYSTEM_PROPERTY = EmployeeHelper.class.getName();
  public static final String SEQ_TYPE_EMPLOYEE = "EMPLOYEE";

  protected static final String DEFAULT_EMPLOYEE_GROUP = "EMPLOYEE";

  private static final IQueryKey<IEmployee> ALL_EMPLOYEES = new QueryKey<IEmployee>("ALL_EMPLOYEES",
      IEmployee.class);
  private static final IQueryKey<IEmployee> ALL_STORE_EMPLOYEES = new QueryKey<IEmployee>(
      "ALL_STORE_EMPLOYEES", IEmployee.class);
  private static final IQueryKey<IGroup> SECURITY_GROUPS = new QueryKey<IGroup>("SECURITY_GROUPS",
      IGroup.class);
  private static final IQueryKey<IEmployeeFingerprint> EMPLOYEE_FINGERPRINTS =
      new QueryKey<IEmployeeFingerprint>("EMPLOYEE_FINGERPRINTS", IEmployeeFingerprint.class);
  private static final IQueryKey<CommAssocSearchResult> COMM_ASSOC_LOOKUP_SQL =
      new QueryKey<CommAssocSearchResult>("COMM_ASSOC_LOOKUP_SQL", CommAssocSearchResult.class);
  private static final IQueryKey<EmployeeSearchResult> EMPLOYEE_SEARCH = new QueryKey<EmployeeSearchResult>(
      "EMPLOYEE_SEARCH", EmployeeSearchResult.class);
  private static final IQueryKey<IEmployee> EMPLOYEE_BY_LOGIN_ID = new QueryKey<IEmployee>(
      "EMPLOYEE_BY_LOGIN_ID", IEmployee.class);
  private static final IQueryKey<IEmployee> EMPLOYEE_BY_PARTY_ID = new QueryKey<IEmployee>(
      "EMPLOYEE_BY_PARTY_ID", IEmployee.class);
  private static final String EMPLOYEE_GROUP_CATEGORY = "EMPLOYEE_GROUP";
  private static final IAttributeKey<IEmployee> editedEmployeeKey_ = new AttributeKey<IEmployee>(
      "EDITED_EMPLOYEE", ScopeType.PROCESS, IEmployee.class);

  private static final Comparator<CommAssocSearchResult> COMPARATOR = new ClockedInComparator();
  private static final String EMPLOYEE_PARTY_TYPE = "EMPLOYEE";
  private static final String ACTIVE_EMP_STATUS_CODE = "A";
  private static final String TERMINATED_EMP_STATUS_CODE = "T";
  private static final String INACTIVE_EMP_STATUS_CODE = "I";
  private static final String DEFAULT_TRAINING_STATUS = AssistanceTypes.EXEMPT.getName();

  static {
    // Check the system configured factory to use...
    String className = System.getProperty(SYSTEM_PROPERTY);
    try {
      INSTANCE = (IEmployeeHelper) Class.forName(className).newInstance();
    }
    catch (Exception ex) {
      INSTANCE = new EmployeeHelper();
    }
    // TODO: extract an Initializable interface???
    if (INSTANCE instanceof EmployeeHelper) {
      ((EmployeeHelper) INSTANCE).init();
    }
  }

  public static IEmployee getEditedEmployee() {
    return StationModelMgr.getInstance().getStationModel().getAttribute(editedEmployeeKey_);
  }

  public static final IEmployeeHelper getInstance() {
    return INSTANCE;
  }

  public static void setEditedEmployee(IEmployee argEditedEmployee) {
    StationModelMgr.getInstance().getStationModel().setAttribute(editedEmployeeKey_, argEditedEmployee);
  }

  protected final Map<String, String> partyFields_;

  protected IRetailLocation THIS_STORE;
  protected IGroup DEFAULT_GROUP;
  protected byte[] DEFAULT_GROUP_MEMBERSHIP;

  private final int retailLocId_;
  private final Long orgId_;

  public EmployeeHelper(Long argOrgId, int argRetailLocId) {
    // create a hash to allow search object to flatten the locale fields
    Map<String, String> m = new HashMap<String, String>();
    m.put("lastName", "party.lastName");
    m.put("firstName", "party.firstName");
    partyFields_ = Collections.unmodifiableMap(m);
    orgId_ = argOrgId;
    retailLocId_ = argRetailLocId;
  }

  protected EmployeeHelper() {
    this(ConfigurationMgr.getOrganizationId(), ConfigurationMgr.getRetailLocationId());
  }

  @Override
  public IEmployee createEmployee(String argCustomerId, String argEmployeeId,
      Collection<IKeyedValue<String, ?>> argKnownFields) {
    if (ConfigurationMgr.autoGenerateEmployeeId()) {
      argEmployeeId = SequenceFactory.getNextStringValue(SEQ_TYPE_EMPLOYEE);
    }
    if (argEmployeeId == null) {
      throw new IllegalArgumentException("new employee id not captured");
    }

    // Create a new Party data access object to add this new customer.
    EmployeeId employeeId = new EmployeeId();
    // Get a new emp id from the sequence manager
    employeeId.setEmployeeId(argEmployeeId);
    IEmployee employee = DataFactory.createObject(employeeId, IEmployee.class);
    employee.setTrainingStatusEnum(DEFAULT_TRAINING_STATUS);

    IParty party =
        CustomerHelper.getInstance().createParty(argKnownFields, /* autoGenCustId= */(argCustomerId == null));

    party.setPartyTypeCode(EMPLOYEE_PARTY_TYPE);
    party.setAllegianceRetailLocationId(THIS_STORE.getRetailLocationId());
    party.setSignUpRetailLocationId(THIS_STORE.getRetailLocationId());
    party.setEmployeeId(argEmployeeId);
    party.setPreferredLocale(LocaleManager.getInstance().getLocale(OutputContextType.VIEW).toString());

    if (argCustomerId != null) {
      party.setCustomerId(argCustomerId);
    }

    employee.setParty(party);
    employee.setEmployeeStatusCode(ACTIVE_EMP_STATUS_CODE);
    employee.setAddDate(getCurrentBusinessDate());
    employee.setPrimaryGroup(DEFAULT_GROUP);
    employee.setGroupMembership(DEFAULT_GROUP_MEMBERSHIP);
    employee.setOvertimeEligible(true);
    employee.setClockInNotRequired(ConfigurationMgr.isClockInNotRequiredOnCreateEmployee());

    return employee;
  }

  @Override
  public IEmployeeStore createStoreAssignment(IEmployee argEmp) {
    EmployeeStoreId id = new EmployeeStoreId();
    id.setEmployeeId(argEmp.getEmployeeId());
    id.setRetailLocationId(new Long(retailLocId_));
    id.setEmployeeStoreSequence(new Integer(1));

    IEmployeeStore assignment = DataFactory.createObject(id, IEmployeeStore.class);

    assignment.setBeginDate(getCurrentBusinessDate());
    return assignment;
  }

  @Override
  public IEmployeeStore createTempStoreAssignment(IEmployee argEmp, Date argStartDate, Date argEndDate,
      boolean argTempAssignmentFlag) {
    EmployeeStoreId id = new EmployeeStoreId();
    id.setEmployeeId(argEmp.getEmployeeId());
    id.setRetailLocationId(new Long(retailLocId_));

    IEmployeeStore assignment = DataFactory.createObject(id, IEmployeeStore.class);
    assignment.setBeginDate(argStartDate);
    assignment.setEndDate(argEndDate);
    assignment.setTemporaryAssignment(argTempAssignmentFlag);
    argEmp.addEmployeeStore(assignment);

    return assignment;
  }

  @Override
  public String encryptPassword(String argValue) {
    return PasswordHash.hash(EmployeeConstants.getNewPasswordHashType(), argValue);
  }

  @Override
  public String getActiveCode() {
    return ACTIVE_EMP_STATUS_CODE;
  }

  /** {@inheritDoc} */
  @Override
  public List<? extends IEmployee> getAllEmployees() {
    List<? extends IEmployee> allEmployees = Collections.emptyList();
    try {
      allEmployees = DataFactory.getObjectByQuery(ALL_EMPLOYEES, new HashMap<String, Object>());
    }
    catch (ObjectNotFoundException ex) {
      logger_.debug(ex);
    }
    catch (Exception ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }
    return allEmployees;
  }

  /** {@inheritDoc} */
  @Override
  public List<? extends IEmployee> getAllStoreEmployees() {
    List<? extends IEmployee> allStoreEmployees = Collections.emptyList();
    try {
      Map<String, Object> params = new HashMap<String, Object>();
      params.put("argRetailLocationId", LocationFactory.getInstance().getMyLocation().getRetailLocationId());
      params.put("argBusinessDate", getCurrentBusinessDate());

      allStoreEmployees = DataFactory.getObjectByQuery(ALL_STORE_EMPLOYEES, params);
    }
    catch (ObjectNotFoundException ex) {
      logger_.debug(ex);
    }
    catch (Exception ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }
    return allStoreEmployees;
  }

  @Override
  public List<CommAssocSearchResult> getCommissionableEmployees() {
    List<CommAssocSearchResult> emps = Collections.emptyList();

    try {
      Map<String, Object> params = new HashMap<String, Object>();
      params.put("argRetailLocationId", retailLocId_);
      params.put("argBusinessDate", getCurrentBusinessDate());
      params.put("argDeleteFlag", Boolean.FALSE);
      params.put("argOpenRecordFlag", Boolean.TRUE);

      emps = DataFactory.getObjectByQuery(COMM_ASSOC_LOOKUP_SQL, params);
      Collections.sort(emps, COMPARATOR);

    }
    catch (Exception ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }
    return emps;
  }

  @Override
  public IEmployeePassword getCurrentPassword(IEmployee employee) {
    List<IEmployeePassword> passwds = employee.getEmployeePasswords();
    
    Collections.sort(passwds, new Comparator<IEmployeePassword>() {

      @Override
      public int compare(IEmployeePassword argPassword1, IEmployeePassword argPassword2) {
        if(argPassword1.getSequence() > argPassword2.getSequence()) {
          return -1;
        }
        else if(argPassword1.getSequence() < argPassword2.getSequence()) {
          return 1;
        }
        else {
          return 0;
        }
      }});
    
    for (IEmployeePassword pwd : passwds) {
      if (pwd.getCurrent()) {
        return pwd;
      }
    }
    return null;
  }
  
  
  protected boolean checkExpired(IEmployeePassword empPass) {
    if (empPass.getTemporary()) {
      return true;
    }

    if (ConfigurationMgr.isPasswordExpirationEnabled()) {
      Date effectiveDate = empPass.getEffectiveDate();
      if (effectiveDate == null) {
        return true;
      }

      Date expDate = DateUtils.dateAdd(CalendarField.DAY, ConfigurationMgr.getPasswordExpirationDays(), effectiveDate);
      if (expDate.before(StoreCalendar.getCurrentBusinessDate())) {
        return true;
      }
    }

    return false;
  }
  
  @Override
  public List<IEmployeeStore> getEmployeeActiveStores(IEmployee argEmployee) {
    List<IEmployeeStore> empStores = new ArrayList<IEmployeeStore>();
    List<IEmployeeStore> allEmpStores = argEmployee.getEmployeeStores();
    Date today = getCurrentBusinessDate();

    for (IEmployeeStore es : allEmpStores) {
      Date endDate = es.getEndDate();
      Date beginDate = es.getBeginDate();
      if (es.getRetailLocationId() != retailLocId_) {
        continue;
      }
      if ((endDate != null) && endDate.before(today)) {
        continue;
      }
      if ((beginDate != null) && beginDate.after(today)) {
        continue;
      }
      empStores.add(es);
    }
    return empStores;
  }

  @Override
  public IEmployee getEmployeeById(String argEmployeeId) {
    if (StringUtils.isEmpty(argEmployeeId)) {
      return null;
    }
    try {
      EmployeeId id = new EmployeeId();
      id.setEmployeeId(argEmployeeId);
      if (AssistanceHelper.getInstance().isTrainingMode()) {
        id.setOrganizationId(ConfigurationMgr.getOrganizationId());
      }
      else {
        id.setOrganizationId(orgId_);
      }
      return (IEmployee) DataFactory.getObjectById(id);
    }
    catch (Exception ex) {
      logger_.info("CAUGHT EXCEPTION", ex);
      return null;
    }
  }

  @Override
  public IEmployee getEmployeeByLoginId(String argLoginId) {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("argLoginId", argLoginId);
    List<IEmployee> found;
    try {
      found = DataFactory.getObjectByQuery(EMPLOYEE_BY_LOGIN_ID, params);
    }
    catch (ObjectNotFoundException ex) {
      logger_.info("No employee found", ex);
      return null;
    }
    catch (Exception ex) {
      logger_.error("An unexpected exception occurred while looking " + "up employee with login id: "
          + argLoginId, ex);
      return null;
    }

    if ((found == null) || found.isEmpty()) {
      return null;
    }
    else {
      IEmployee emp = found.get(0);
      if (found.size() > 1) {
        logger_.warn("more than one employee with partyId " + argLoginId + " using the one with employeeId="
            + emp.getEmployeeId());
      }
      return emp;
    }
  }

  /** {@inheritDoc} */
  @Override
  public IEmployee getEmployeeByPartyId(long argPartyId) {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("argPartyId", new Long(argPartyId));
    List<IEmployee> found;

    try {
      found = DataFactory.getObjectByQuery(EMPLOYEE_BY_PARTY_ID, params);
    }
    catch (ObjectNotFoundException ex) {
      logger_.info("No employee found", ex);
      return null;
    }

    if (found.size() > 1) {
      logger_.warn("More than one employee with party Id [" + argPartyId
          + "] was found.  Using the one with employeeId [" + found.get(0).getEmployeeId() + "]");
    }

    IEmployee emp = found.get(0);
    return emp;
  }

  /** {@inheritDoc} */
  @Override
  public List<? extends ICodeInterface> getEmployeeGroups() {
    return CodeLocator.getCodeValues(EMPLOYEE_GROUP_CATEGORY);
  }

  @Override
  public String getEmployeeIdFieldName() {
    return EmployeeSearchModel.EMPLOYEE_NUMBER_FIELD;
  }

  @Override
  public String getEmployeePartyTypeCode() {
    return EMPLOYEE_PARTY_TYPE;
  }

  @Override
  public List<IEmployeeFingerprint> getFingerprints(IEmployee argEmployee, boolean argLocalOnly) {
    return getFingerprints(argEmployee.getEmployeeId(), argLocalOnly);
  }

  @Override
  public List<IEmployeeFingerprint> getFingerprints(String argEmployee, boolean argLocalOnly) {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("argEmployeeId", argEmployee);
    try {
      if (argLocalOnly) {
        return DataFactory.getObjectByQuery(EMPLOYEE_FINGERPRINTS, params,
            PersistenceManagerType.forName("EMPLOYEE_FINGERPRINT"));
      }
      return DataFactory.getObjectByQuery(EMPLOYEE_FINGERPRINTS, params);
    }
    catch (ObjectNotFoundException ex) {
      if (logger_.isDebugEnabled()) {
        logger_.debug("No employee fingerprints found for Employee ID " + argEmployee, ex);
      }

      return new ArrayList<IEmployeeFingerprint>();
    }
  }

  /** {@inheritDoc} */
  @Override
  public List<IEmployeeNote> getNotes(IEmployee employee) {
    List<IEmployeeNote> notes = new ArrayList<IEmployeeNote>();

    if (employee != null) {
      notes.addAll(employee.getEmployeeNotes());
    }

    // sort in descending order
    Collections.sort(notes, new Comparator<IEmployeeNote>() {

      @Override
      public int compare(IEmployeeNote argObjectA, IEmployeeNote argObjectB) {
        Long sequenceA = argObjectA.getNoteSequence();
        Long sequenceB = argObjectB.getNoteSequence();
        return sequenceB.compareTo(sequenceA);
      }
    });

    return notes;
  }

  @Override
  public IEmployeePassword getPassword(IEmployee employee) {
    IEmployeePassword pwd = getCurrentPassword(employee);
    if (pwd == null) {
      pwd = createEmployeePassword(employee);
    }

    return pwd;
  }

  @Override
  public IGroup getSecurityGroup(String argGroupId) {
    GroupId id = new GroupId();
    id.setGroupId(argGroupId);
    IGroup group = (IGroup) DataFactory.getObjectById(id);
    return group;
  }

  @Override
  public IGroup[] getSortedSecurityGroups() {
    Map<String, Object> params = new HashMap<String, Object>();
    List<IGroup> results = DataFactory.getObjectByQuery(SECURITY_GROUPS, params);
    return results.toArray(new IGroup[results.size()]);
  }

  @Override
  public String getTerminatedCode() {
    return TERMINATED_EMP_STATUS_CODE;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isEmployee(IParty argCustomer) {
    boolean isEmployee = false;

    /* A customer will be considered an employee if: (1) They have an "EMPLOYEE" party type code OR (2) They
     * are a member of the reserved "EMPLOYEE" group and have no corresponding employee record. Ideally,
     * because this determination will be used to drive employee discount availability, we would first check
     * to see if the customer has an associated employee record with a non- terminated status. The consensus,
     * however, is that the remote round-trip to establish this record isn't worth the potential fraud
     * savings. */
    if (argCustomer != null) {
      /* If a customer is a member of the reserved "EMPLOYEE" group, assume that they can be considered an
       * employee, even if they do not have a corresponding employee record. This is a potentially
       * over-generous interpretation, but apparently there are many organizations that don't want to model
       * non-store personnel in the DB but still want to afford them employee discounts, etc. */
      isEmployee = getEmployeePartyTypeCode().equalsIgnoreCase(argCustomer.getPartyTypeCode()) //
          || isInEmployeeGroup(argCustomer);
    }
    return isEmployee;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isInactive(IEmployee argEmployee) {
    boolean inactive =
        (argEmployee == null || INACTIVE_EMP_STATUS_CODE.equals(argEmployee.getEmployeeStatusCode()));
    return inactive;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isTerminated(IEmployee argEmployee) {
    boolean terminated = false;

    /* An employee is considered terminated if: (1) they have a status code matching the one reserved for
     * terminated employees (2) they have an in-the-past termination date OR an unspecified termination date. */
    if (argEmployee != null) {
      terminated = getTerminatedCode().equalsIgnoreCase(argEmployee.getEmployeeStatusCode());

      if (terminated && (argEmployee.getTerminatedDate() != null)) {
        terminated = !argEmployee.getTerminatedDate().after(getCurrentBusinessDate());
      }
    }

    return terminated;
  }

  /**
   * Verifies that the passed in employee id is unique.
   * 
   * @param argEmployeeId the employee id to check for uniqueness
   * @return true if the employee id is unique, otherwise false
   * @throws NullPointerException if <tt>argEmployeeId</tt> is null
   */
  @Override
  public boolean isUniqueEmployeeId(String argEmployeeId) {
    if (argEmployeeId.length() == 0) {
      throw new NullPointerException("employee id cannot be zero length");
    }
    boolean rtn = false;
    IEmployee e = null;

    EmployeeId id = new EmployeeId();
    id.setEmployeeId(argEmployeeId);
    try {
      e = (IEmployee) DataFactory.getObjectById(id);
    }
    catch (ObjectNotFoundException ex) {
      logger_.info("CAUGHT EXCEPTION", ex);
      rtn = true;
    }
    if (e == null) {
      rtn = true;
    }
    return rtn;
  }

  @Override
  public IEmployeeFingerprint makeFingerprint(IEmployee argEmployee, int argSequence, Object argBiometricData)
      throws IOException {
    EmployeeFingerprintId id = new EmployeeFingerprintId();

    id.setEmployeeId(argEmployee.getEmployeeId());
    id.setSequence(new Integer(argSequence));

    IEmployeeFingerprint fp = DataFactory.createObject(id, IEmployeeFingerprint.class);
    fp.setBiometricData(argBiometricData);
    return fp;
  }

  @Override
  public EmployeeSearchResult[] runQuery(Collection<IKeyedValue<String, ?>> argFields) {
    List<EmployeeSearchResult> col = runQueryWrapResults(argFields);
    return col.toArray(new EmployeeSearchResult[col.size()]);
  }

  @Override
  public IQueryResultList<EmployeeSearchResult> runQueryWrapResults(
      Collection<IKeyedValue<String, ?>> argFields) {

    Map<String, Object> params = new HashMap<String, Object>();

    if (EmployeeConstants.searchCurrentLocationOnly()) {
      params.put("argretailLocationId", retailLocId_);
    }
    params.put("argBusinessDate", getCurrentBusinessDate());
    params.put("argPrimaryAddress", true);

    Object searchValue = null;
    String fieldName = null;

    for (IKeyedValue<String, ?> change : argFields) {
      searchValue = change.getValue();
      fieldName = change.getKey();
      if (searchValue != null) {
        if (EmployeeSearchModel.TERMINATED.equalsIgnoreCase(fieldName)) {
          if (Boolean.FALSE.equals(searchValue)) {
            fieldName = "not" + fieldName;
          }
          searchValue = TERMINATED_EMP_STATUS_CODE;
        }
        params.put("arg" + fieldName, searchValue);
      }
    }
    // Datafactory should THROW if no info available.
    return DataFactory.getObjectByQuery(EMPLOYEE_SEARCH, params);
  }

  @Override
  public void setPassword(IEmployee argEmployee, String argPassword, boolean argTemporary) {
    IEmployeePassword pwdOld = getPassword(argEmployee);
    IEmployeePassword pwd = null;
    if (pwdOld.getTemporary()) {
      // If temporary just reuse pwdOld
      pwd = pwdOld;
    }
    else {
      // Otherwise create a new password and cleanup the old password.
      pwdOld.setCurrent(false);
      pwd = createEmployeePassword(argEmployee);
      pwd.setSequence(pwdOld.getSequence() + 1);
    }

    pwd.setTemporary(argTemporary);
    pwd.setCurrent(true);
    pwd.setPassword(encryptPassword(argPassword));

    if (argEmployee.isNew() && ConfigurationMgr.isPasswordExpirationEnabled()) {
      pwd.setEffectiveDate(DateUtils.getFarPastDate());
    }
    else {
      pwd.setEffectiveDate(getCurrentBusinessDate());
    }

    try {
      DataFactory.makePersistent(pwdOld);
      DataFactory.makePersistent(pwd);
    }
    catch (dtv.data2.access.exception.PersistenceException ex) {
      logger_.error("Exception caught trying to persist object when setting employee password.", ex);
      throw new OpExecutionException(ex);
    }
  }

  /**
   * Returns a new temporary password for the given employee.
   * @param argEmployee The employee to create the password for.
   */
  protected IEmployeePassword createEmployeePassword(IEmployee argEmployee) {
    EmployeePasswordId id = new EmployeePasswordId();
    id.setEmployeeId(argEmployee.getEmployeeId());
    IEmployeePassword pwd = DataFactory.createObject(id, IEmployeePassword.class);
    if (pwd.getEffectiveDate() == null) {
      pwd.setEffectiveDate(new Date());
    }
    pwd.setTemporary(true);

    int seq = argEmployee.getEmployeePasswords() == null ? 1 : argEmployee.getEmployeePasswords().size() + 1;
    pwd.setSequence(seq);

    pwd.setCurrent(true);
    argEmployee.addEmployeePassword(pwd);
    return pwd;
  }

  protected void init() {
    if (THIS_STORE == null) {
      THIS_STORE = LocationFactory.getInstance().getMyLocation();

      GroupId id = new GroupId();
      id.setGroupId(getDefaultEmployeeGroup());
      DEFAULT_GROUP = (IGroup) DataFactory.getObjectByIdNoThrow(id);

      if (DEFAULT_GROUP != null) {
        int pos = DEFAULT_GROUP.getBitmapPosition();
        boolean[] groupFlags = new boolean[pos];
        groupFlags[pos - 1] = true;
        DEFAULT_GROUP_MEMBERSHIP = ByteUtils.toByteArray(groupFlags);
      }
      else {
        logger_.warn("No sec_groups row for employee group [" + getDefaultEmployeeGroup()
            + "].  This may be symptomatic of a much larger problem that will hopefully be reported as a "
            + "PFC, so rather than fail fast here an unsatisfactory default will be used for the security "
            + "group membership assigned to newly-created employees.");
        DEFAULT_GROUP_MEMBERSHIP = ByteUtils.toByteArray(new boolean[] {false});
      }
    }
  }

  /**
   * Indicates whether the specified customer belongs to any customer group designated as representing
   * employees.
   * 
   * @param argCustomer the customer to interrogate
   * @return <code>true</code> if <code>argCustomer</code> belongs to an employee customer group;
   * <code>false</code> otherwise
   */
  protected boolean isInEmployeeGroup(IParty argCustomer) {
    boolean inEmployeeGroup = false;

    for (ICustomerAffiliation custGroup : argCustomer.getCustomerGroups()) {
      if (DEFAULT_EMPLOYEE_GROUP.equalsIgnoreCase(custGroup.getCustomerGroupId())) {
        inEmployeeGroup = true;
        break;
      }
    }
    return inEmployeeGroup;
  }
}