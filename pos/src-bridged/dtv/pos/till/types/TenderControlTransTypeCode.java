//$Id: TenderControlTransTypeCode.java 55414 2010-12-07 23:00:05Z dtvdomain\jweiss $
package dtv.pos.till.types;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import dtv.pos.iframework.type.AbstractCodeEnum;

/**
 * A type-safe enumeration for tender control transaction type code values<br>
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author jhsiao
 * @created September 15, 2003
 * @version $Revision: 55414 $
 */
public class TenderControlTransTypeCode
    extends AbstractCodeEnum {

  private static final Logger logger_ = Logger.getLogger(TenderControlTransTypeCode.class);

  /* * * * * * * * * * START OF INSTANCES * * * * * * * * * */
  public static TenderControlTransTypeCode ISSUE_TILL_STARTING_CASH = new TenderControlTransTypeCode(
      "ISSUE_TILL_STARTING_CASH");
  public static TenderControlTransTypeCode BEGINCOUNT = new TenderControlTransTypeCode("BEGINCOUNT");
  public static TenderControlTransTypeCode ENDCOUNT = new TenderControlTransTypeCode("ENDCOUNT");
  public static TenderControlTransTypeCode MIDCOUNT = new TenderControlTransTypeCode("MIDCOUNT");
  public static TenderControlTransTypeCode CASH_PICKUP = new TenderControlTransTypeCode("CASH_PICKUP");
  public static TenderControlTransTypeCode CASH_TRANSFER = new TenderControlTransTypeCode("CASH_TRANSFER");
  public static TenderControlTransTypeCode STORE_BANK_CASH_DEPOSIT = new TenderControlTransTypeCode(
      "STORE_BANK_CASH_DEPOSIT");
  public static TenderControlTransTypeCode BANK_DEPOSIT = new TenderControlTransTypeCode("BANK_DEPOSIT");
  public static TenderControlTransTypeCode RECONCILE = new TenderControlTransTypeCode("RECONCILE");
  public static TenderControlTransTypeCode PAID_IN = new TenderControlTransTypeCode("PAID_IN");
  public static TenderControlTransTypeCode PAID_OUT = new TenderControlTransTypeCode("PAID_OUT");
  public static TenderControlTransTypeCode TENDER_EXCHANGE =
      new TenderControlTransTypeCode("TENDER_EXCHANGE");
  public static TenderControlTransTypeCode ASSOCIATE_ADVANCE = new TenderControlTransTypeCode(
      "ASSOCIATE_ADVANCE");
  public static TenderControlTransTypeCode STORE_BANK_AUDIT = new TenderControlTransTypeCode(
      "STORE_BANK_AUDIT");
  public static TenderControlTransTypeCode TILL_AUDIT = new TenderControlTransTypeCode("TILL_AUDIT");
  public static TenderControlTransTypeCode START_REMOTE_COUNT = new TenderControlTransTypeCode(
      "START_REMOTE_COUNT");
  public static TenderControlTransTypeCode END_REMOTE_COUNT = new TenderControlTransTypeCode(
      "END_REMOTE_COUNT");
  public static TenderControlTransTypeCode DEPOSIT_BAG_CONVEYANCE = new TenderControlTransTypeCode(
      "DEPOSIT_BAG_CONVEYANCE");
  
  /* * * * * * * * * *  END OF INSTANCES  * * * * * * * * * */

  /** searchable storage for instances of the class */
  private static Map<String, TenderControlTransTypeCode> values_;

  /**
   * retrieval method to get the tender control transaction value for the specified name
   * 
   * @param argName the name of the value to get
   * @return null if an invalid name is selected
   */
  public static TenderControlTransTypeCode forName(String argName) {
    if (argName == null) {
      return null;
    }
    TenderControlTransTypeCode t = values_.get(argName.trim().toUpperCase());
    if (t == null) {
      logger_.warn("There is no instance of [" + TenderControlTransTypeCode.class.getName() + "] named ["
          + argName + "].", new Throwable("STACK TRACE"));
    }
    return t;
  }

  private TenderControlTransTypeCode(String argName) {
    super(TenderControlTransTypeCode.class, argName);
    if (values_ == null) {
      values_ = new HashMap<String, TenderControlTransTypeCode>();
    }
    values_.put(getName(), this);
  }

}
