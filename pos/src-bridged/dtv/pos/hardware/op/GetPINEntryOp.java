//$Id: GetPINEntryOp.java 52 2012-06-12 15:42:42Z dtvdomain\aabdul $
package dtv.pos.hardware.op;

import static dtv.pos.framework.form.FormConstants.EXIT;

import org.apache.log4j.Logger;

import dtv.hardware.HardwareMgr;
import dtv.hardware.events.IHardwareInputEvent;
import dtv.hardware.ppad.*;
import dtv.hardware.types.DtvHardwareEventType;
import dtv.pos.common.PromptKey;
import dtv.pos.framework.op.OpState;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.action.IXstActionKey;
import dtv.pos.iframework.action.IXstDataAction;
import dtv.pos.iframework.event.*;
import dtv.pos.iframework.op.*;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.TenderHelper;
import dtv.util.ObjectUtils;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * An operation that gets a Pin for a debit card via a Pin pad.<br>
 * Copyright (c) 2003 Datavantage Corporation<br>
 * src-bridged: On selection of cancel at PINPAD, processing the combo-card as credit
 * @author dberkland
 * @created December 25, 2003
 * @version $Revision: 52 $
 */
public class GetPINEntryOp
    extends Operation
    implements IXstEventObserver {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(GetPINEntryOp.class);

  private final IOpState WAITING_FOR_PIN = new OpState(this, "WAITING_FOR_PIN");
  private final IOpState SHOWING_ERROR = new OpState(this, "SHOWING_ERROR");
  private static final IXstEventType[] EVENTS = new IXstEventType[] {DtvHardwareEventType.PIN_ENTRY_COMPLETE,
      DtvHardwareEventType.PIN_ENTRY_CANCELED};

  @Override
  public IXstEventType[] getObservedEvents() {
    return EVENTS;
  }

  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    if (argEvent instanceof IXstDataAction) {
      IOpResponse r = handleDataAction(argCmd, (IXstDataAction) argEvent);
      if (r != null) {
        return r;
      }
    }
    if (argEvent != null) {
      IXstEventType eventType = argEvent.getType();
      if (DtvHardwareEventType.PIN_ENTRY_COMPLETE.equals(eventType)) {
        ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
        PinEntryEvent event = (PinEntryEvent) argEvent;

        ICreditDebitTenderLineItem debitLine = (ICreditDebitTenderLineItem) cmd.getTenderLineItem();

        debitLine.setEncryptedPin(event.getEncryptedPin());
        debitLine.setAdditionalPinSecurityInfo(event.getAdditionalSecurityInformation());

        return HELPER.completeResponse();
      }
      if (DtvHardwareEventType.PIN_ENTRY_CANCELED.equals(eventType)) {

        /*CUSTOMIZATION*/
        ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
        ITenderLineItem tenderLine = cmd.getTenderLineItem();
        Object inputEvent = tenderLine.getInputEvent();

        if ((inputEvent instanceof IHardwareInputEvent)
            && TenderHelper.getInstance().isCreditDebitAmbiguous((IHardwareInputEvent<?>) inputEvent)) {

          final String tenderId;
          tenderId = TenderHelper.getInstance().getNonDebitTenderId((IHardwareInputEvent) inputEvent);

          if (tenderId != null) {
            logger_.info("Processing as credit card: [" + tenderId + "]");
            TenderHelper.getInstance().setTenderIdType(tenderLine, tenderId);
            return HELPER.completeResponse();
          }
        }
        /*<END> CUSTOMIZATION*/
        argCmd.setOpState(SHOWING_ERROR);
        return HELPER.getPromptResponse(PromptKey.valueOf("PIN_ENTRY_CUST_CANCELED"));
      }
    }
    if (WAITING_FOR_PIN == argCmd.getOpState()) {
      // do nothing
    }
    else {
      ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
      ITenderLineItem tenderLine = cmd.getTenderLineItem();

      if (!(tenderLine instanceof ICreditDebitTenderLineItem)) {
        logger_.error("tender line is not [" + ICreditDebitTenderLineItem.class.getName() + "] instead is "
            + ObjectUtils.getClassNameFromObject(tenderLine));
        return HELPER.errorNotifyResponse();
      }

      IDtvPinPad pp = HardwareMgr.getCurrentHardwareMgr().getPinPad();
      try {
        pp.startPinEntry((ICreditDebitTenderLineItem) tenderLine);
      }
      catch (PinPadException ex) {
        logger_.error("CAUGHT EXCEPTION", ex);
        pp.abortPinEntry();
        return HELPER.silentErrorResponse();
      }
      argCmd.setOpState(WAITING_FOR_PIN);
    }
    return HELPER.getPromptResponse(PromptKey.valueOf("PIN_ENTRY_IN_PROGRESS"));
  }

  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    // check command class
    if (!(argCmd instanceof ISaleTenderCmd)) {
      logger_.warn("command [" + argCmd + "] does not implement " + ISaleTenderCmd.class.getName());
      return false;
    }
    // check tender on command
    ITender tender = ((ISaleTenderCmd) argCmd).getTender();
    if (tender == null) {
      logger_.info("no tender on command; ergo nothing to do");
      return false;
    }
    // see if Pin entry is required
    if (!tender.getPinRequired()) {
      return false;
    }
    // see if a Pin pad is available
    if (!HardwareMgr.getCurrentHardwareMgr().getPinPad().isPresent()) {
      logger_.warn("Pin entry required, but not Pin device available");
      return false;
    }
    // passed all checks
    return true;
  }

  private IOpResponse handleDataAction(IXstCommand argCmd, IXstDataAction argAction) {
    IXstActionKey key = argAction.getActionKey();

    if (EXIT.equals(key)) {
      IDtvPinPad pp = HardwareMgr.getCurrentHardwareMgr().getPinPad();
      pp.abortPinEntry();
      return HELPER.silentErrorResponse();
    }
    else if (argCmd.getOpState() == SHOWING_ERROR) {
      return HELPER.silentErrorResponse();
    }
    return null;
  }
}
