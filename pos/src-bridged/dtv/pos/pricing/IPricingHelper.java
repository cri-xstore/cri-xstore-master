// $Id: IPricingHelper.java 772 2014-03-18 09:33:33Z suz.dhu $
package dtv.pos.pricing;

import java.math.BigDecimal;

import dtv.xst.dao.itm.IItem;
import dtv.xst.dao.trl.ISaleReturnLineItem;

/**
 * Interface representing a helper class for getting various pricing based attributes for items.<br>
 * Copyright (c) 2007 Datavantage Corporation
 * 
 * @author jculp
 * @version $Revision: 772 $
 * @created Mar 6, 2007
 */
public interface IPricingHelper {
  /**
   * Returns the current base price of <code>argItemId</code> as determined by the current state of the
   * current transaction.
   * 
   * @param saleItem
   * @param argCurrency
   * @param argReturn
   * @return BigDecimal
   */
  public BigDecimal getCurrentItemPrice(ISaleReturnLineItem saleItem, boolean argReturn);

  /**
   * Calculates the price of an item including any stand alone deals that are currently active for that item.
   * 
   * @param argItem the item for which the price should be calculated
   * @return the deal price of the item
   */
  public BigDecimal getStandAloneDealPrice(IItem argItem);
  
  public BigDecimal getPriceExcludingMultipleQuantityDeals(IItem argItem);
}
