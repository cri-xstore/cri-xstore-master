// $Id: ValidateDiscountsOnCustomerChangeOp.java 437 2012-09-27 17:58:32Z suz.sxie $
package dtv.pos.pricing.discount;

import static org.apache.commons.collections.CollectionUtils.containsAny;

import java.util.*;

import dtv.pos.register.returns.ReturnType;
import org.apache.log4j.Logger;

import dtv.pos.common.PromptKey;
import dtv.pos.customer.ICustAssociationCmd;
import dtv.pos.customer.loyalty.LoyaltyMgr;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.xst.dao.dsc.IDiscountGroupMapping;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trn.IPosTransaction;

/**
 * Operation to remove discounts that no longer apply when a customer is changed. This involves alot of
 * looping. There needs to be a better way of doing this.<br>
 * <br>
 * Copyright (c) 2003 Datavantage Corporation
 *
 * @author Jeff Sheldon
 * @created June 5, 2003
 * @version $Revision: 437 $
 */
public class ValidateDiscountsOnCustomerChangeOp
  extends Operation {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(ValidateDiscountsOnCustomerChangeOp.class);

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    ICustAssociationCmd cmd = (ICustAssociationCmd) argCmd;
    IRetailTransaction trans = (IRetailTransaction) cmd.getTransaction();

    List<ISaleReturnLineItem> lineItems = trans.getLineItems(ISaleReturnLineItem.class);

    if ((lineItems == null) || lineItems.isEmpty()) {
      return HELPER.completeResponse();
    }

    // check the transaction for a customer
    Collection<? extends String> customerGroups = LoyaltyMgr.getInstance().getEffectiveCustomerGroups();
    boolean prompt = false;

    for (ISaleReturnLineItem saleLine : lineItems) {
      // CRI feedback #381235 don't change the verified return item's discount
      if (saleLine.getVoid() || (saleLine.getReturn() && ReturnType.valueOf(saleLine.getReturnTypeCode()).isVerified())) {
        continue;
      }

      List<IRetailPriceModifier> modifiers = saleLine.getRetailPriceModifiers();
      if ((modifiers == null) || modifiers.isEmpty()) {
        continue;
      }

      for (IRetailPriceModifier modifier : modifiers) {
        if (modifier.getVoid() || (modifier.getDiscount() == null)) {
          continue;
        }

        RetailPriceModifierReasonCode type =
          RetailPriceModifierReasonCode.forName(modifier.getRetailPriceModifierReasonCode());

        if ((type == RetailPriceModifierReasonCode.LINE_ITEM_DISCOUNT)
          || (type == RetailPriceModifierReasonCode.TRANSACTION_DISCOUNT)
          || (type == RetailPriceModifierReasonCode.GROUP_DISCOUNT)) {

          Collection<String> discountGroups = new HashSet<String>();
          for (IDiscountGroupMapping discountGroup : modifier.getDiscount().getCustomerGroups()) {
            discountGroups.add(discountGroup.getCustomerGroupId());
          }

          boolean anyMatches = containsAny(customerGroups, discountGroups);

          if (!anyMatches) {
            modifier.setVoid(true);
            prompt = true;

            if (modifier.getParentLine() != null) {
              modifier.getParentLine().setRetailPriceModifiers(
                modifier.getParentLine().getRetailPriceModifiers());

            }
            IRetailTransactionLineItem reasonLineItem = modifier.getReasonLineItem();
            if (reasonLineItem != null) {
              reasonLineItem.setVoid(true);
            }
          }
        }
      }
    }
    return (prompt) ? HELPER.getPromptResponse(PromptKey.valueOf("DISCOUNT_AUTO_REMOVE")) : HELPER
      .completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    if (!(argCmd instanceof ICustAssociationCmd)) {
      logger_.warn("Invalid command type [" + (argCmd == null ? "null" : argCmd.getClass().getName()) + "]");
      return false;
    }
    IPosTransaction trans = ((ICustAssociationCmd) argCmd).getTransaction();
    return trans instanceof IRetailTransaction;
  }
}
