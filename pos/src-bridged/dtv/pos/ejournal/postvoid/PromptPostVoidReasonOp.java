// $Id: PromptPostVoidReasonOp.java 622 2013-03-28 06:26:37Z suz.jliao $
package dtv.pos.ejournal.postvoid;

import dtv.pos.common.*;
import dtv.pos.common.op.AbstractPromptReasonCodeOp;
import dtv.pos.framework.StationModelMgr;
import dtv.pos.framework.action.type.XstChainActionType;
import dtv.pos.framework.op.OpResponseHelper;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.xst.dao.com.IReasonCode;

/**
 * Copyright (c) 2006 Datavantage Corporation
 * 
 * @author unattributed
 * @version $Revision: 622 $
 */
public class PromptPostVoidReasonOp
    extends AbstractPromptReasonCodeOp
    implements IReversibleOp {

  private static final long serialVersionUID = 1L;

  public PromptPostVoidReasonOp() {
    super(PostVoidConstants.POST_VOID);
  }

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpReverse(IXstCommand argCmd, IXstEvent argEvent) {
    IPostVoidCmd cmd = (IPostVoidCmd) argCmd;

    // We do not want to persist transaction we attempted to void.
    cmd.setTransaction(cmd.getPostVoidTransaction());

    // Should not update the voided transaction.
    cmd.getPostVoidTransaction().getVoidedTransaction().setPostVoid(false);

    // Remove all persistables. Only persist Post Void transaction.
    argCmd.getPersistables().clear();

    // Ensure the transaction we attempted to void is not in Station Model.
    StationModelMgr.getInstance().getStationModel().setCurrentTransaction(cmd.getTransaction());

    return OpResponseHelper.getInstance().getRunChainResponse(OpStatus.COMPLETE_HALT,
        OpChainKey.valueOf("CANCEL_TRANSACTION"), cmd, XstChainActionType.START);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    return ConfigurationMgr.getPostVoidPromptForReason();
  }

  /** {@inheritDoc} */
  @Override
  protected IPromptKey getCommentPromptKey(IXstCommand argCmd) {
    return PostVoidConstants.POST_VOID_COMMENT_PROMPT;
  }

  @Override
  protected IPromptKey getReasonCodePromptKey(IXstCommand argCmd) {
    return PostVoidConstants.POST_VOID_REASON_PROMPT;
  }

  /** {@inheritDoc} */
  @Override
  protected void setComment(IXstCommand argCmd, String argComment) {
    IPostVoidCmd cmd = (IPostVoidCmd) argCmd;
    cmd.getPostVoidTransaction().addTransactionNotes(TransactionHelper.createNote(argComment));
  }

  @Override
  protected void setSelectedCode(IXstCommand argCmd, IReasonCode argCode) {
    /* Set the reason code on the transaction. If objects are of wrong type, class cast excpetions will filter
     * up. */
    IPostVoidCmd cmd = (IPostVoidCmd) argCmd;
    cmd.getPostVoidTransaction().setPostVoidReasonCode(argCode.getReasonCode());
  }

  @Override
  protected boolean showListIfOne(IXstCommand argCmd) {
    return true;
  }
}
