// $Id: TenderHelper.java 475 2012-10-11 20:40:41Z dtvdomain\aabdul $
package dtv.pos.tender;

import static dtv.pos.common.ConfigurationMgr.getLocalCurrencyId;
import static dtv.pos.tender.TenderAvailabilityCodeType.RETURN_WITH_RECEIPT;
import static dtv.pos.tender.TenderConstants.DEBIT_CARD;
import static dtv.pos.tender.TenderConstants.DEFAULT;
import static dtv.util.NumberUtils.*;
import static java.math.BigDecimal.ZERO;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import org.apache.log4j.Logger;

import dtv.data2.access.*;
import dtv.hardware.events.CreditCardEvent;
import dtv.hardware.events.IHardwareInputEvent;
import dtv.hardware.inputrules.EventDiscriminator;
import dtv.hardware.msr.MsrSwipe;
import dtv.hardware.sigcap.ISignature;
import dtv.hardware.sigcap.Signature;
import dtv.pos.common.*;
import dtv.pos.framework.security.SecurityMgr;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.security.IAuthenticationTicket;
import dtv.pos.iframework.security.ISecurityMgr;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.pos.iframework.type.VoucherActivityCodeType;
import dtv.pos.register.ItemLocator;
import dtv.pos.register.returns.ReturnManager;
import dtv.pos.register.type.LineItemType;
import dtv.pos.storecalendar.StoreCalendar;
import dtv.pos.tender.voucher.config.*;
import dtv.util.*;
import dtv.util.config.IHasSourceDescription;
import dtv.util.sequence.SequenceFactory;
import dtv.xst.dao.cat.ICustomerAccountPlan;
import dtv.xst.dao.sec.IGroup;
import dtv.xst.dao.tnd.*;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.tsn.IExchangeRateTransLineItem;
import dtv.xst.dao.tsn.IExchangeRateTransaction;
import dtv.xst.dao.ttr.*;
import dtv.xst.daocommon.*;

/**
 * The tender helper is designed to provide support for tender related configuration parameters and
 * general tender data access.<br>
 * src-bridged: To allow Debit card purchase displayed in the original transaction credit tender
 * line items. This is to preform a retunr on the credit side if it was Combo-card <br>
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author ttamulewicz
 * @created March 24, 2003
 * @version $Revision: 475 $
 */
public class TenderHelper {
  /** Sequence type for a voucher history sequence number. */
  public static final String SEQ_TYPE_VOUCHER_HISTORY_SEQ = "VOUCHER_HISTORY_SEQ";
  /** The system property to configure to customize the singleton instance. */
  public static final String SYSTEM_PROPERTY = TenderHelper.class.getName();

  private static final Logger _logger = Logger.getLogger(TenderHelper.class);
  private static final IQueryKey<ITenderUserSettings> TENDER_USER_SETTINGS_FOR_TENDER =
      new QueryKey<ITenderUserSettings>("TENDER_USER_SETTINGS_FOR_TENDER", ITenderUserSettings.class);
  private static final IQueryKey<ICustomerAccountPlan> CREDIT_PLANS = new QueryKey<ICustomerAccountPlan>(
      "CREDIT_PLANS", ICustomerAccountPlan.class);
  private static final TenderHelper INSTANCE;
  private static final IQueryKey<ITender> TENDERS = new QueryKey<ITender>("TENDERS", ITender.class);

  static {
    // Check the system configured factory to use...
    String className = System.getProperty(SYSTEM_PROPERTY);
    TenderHelper tempInstance = null;

    try {
      tempInstance = (TenderHelper) Class.forName(className).newInstance();
    }
    catch (Exception ex) {
      tempInstance = new TenderHelper();
    }

    INSTANCE = tempInstance;
  }

  /**
   * Get the configured <code>TenderHelper</code> object.
   * 
   * @return an instance of <code>TenderHelper</code> or a subclass.
   */
  public static TenderHelper getInstance() {
    return INSTANCE;
  }

  private final ITender _localCurrencyTender;
  private final ITender _localTravelersCheck;
  private final VoucherConfigHelper _voucherHelper;

  /**
   * Constructs a <code>TenderHelper</code>.
   */
  protected TenderHelper() {
    _localCurrencyTender = searchSingleLocalTender(TenderCategory.CURRENCY);
    if (_localCurrencyTender == null) {
      /* The system is not configured with any local currency cash tender. This will result in the collapse of
       * this class's loading and, consequently, the collapse of the entire application. Log the details of
       * the failure here to provide better troubleshooting support than the nebulous NoClassDefFoundError
       * that will bubble out of this trainwreck. */
      _logger.fatal("No local currency record found! "
          + " (No rows in tnd_tndr WHERE tnd_tndr.tndr_typcode = '" + TenderCategory.CURRENCY
          + "' AND tnd_tndr.currency_id = '" + ConfigurationMgr.getLocalCurrencyId() + "')");
    }
    _localTravelersCheck = searchSingleLocalTender(TenderCategory.TRAVELERS_CHECK);

    _voucherHelper = new VoucherConfigHelper();
    _voucherHelper.initialize();
  }

  /**
   * Copy credit/debit tender line item properties from one line to another.
   * 
   * @param newLine the destination line
   * @param oldLine the source line
   * @param argResume whether this is a resume line or not
   */
  public void copyCreditDebitTenderLineItem(ICreditDebitTenderLineItem newLine,
      ICreditDebitTenderLineItem oldLine, boolean argResume) {

    newLine.setAuthorizationToken(oldLine.getAuthorizationToken());
    newLine.setAccountNumber(oldLine.getAccountNumber());
    newLine.setMaskAccountNumberDao(oldLine.getMaskAccountNumberDao());
    newLine.setAccountNumberHash(oldLine.getAccountNumberHash());
    newLine.setSerialNumber(oldLine.getSerialNumber());
    newLine.setExpirationDateString(oldLine.getExpirationDateString());
    newLine.setAdjudicationCode(oldLine.getAdjudicationCode());
    newLine.setAmount(oldLine.getAmount());
    newLine.setCustomerName(oldLine.getCustomerName());
    newLine.setEntryMethodCode(oldLine.getEntryMethodCode());
    newLine.setExchangeRate(oldLine.getExchangeRate());
    newLine.setForeignAmount(oldLine.getForeignAmount());
    newLine.setMediaIssuerId(oldLine.getMediaIssuerId());

    // some data should be copied for a resume, but not for a verified return
    if (argResume) {
      newLine.setNotes(oldLine.getNotes());
      newLine.setAdditionalPinSecurityInfo(oldLine.getAdditionalPinSecurityInfo());
      newLine.setAuthorizationCode(oldLine.getAuthorizationCode());
      newLine.setAuthorizationMethodCode(oldLine.getAuthorizationMethodCode());
      newLine.setBankReferenceNumber(oldLine.getBankReferenceNumber());
      newLine.setEncryptedPin(oldLine.getEncryptedPin());
      newLine.setHostValidation(oldLine.getHostValidation());
      newLine.setImprint(oldLine.getImprint());
      newLine.setPersonalIdReferenceNumber(oldLine.getPersonalIdReferenceNumber());
      newLine.setPersonalIdRequiredTypeCode(oldLine.getPersonalIdRequiredTypeCode());
      newLine.setPs2000(oldLine.getPs2000());
      copySignature(newLine, oldLine);
      copyIdentityVerifications(newLine, oldLine);
    }

    ItemLocator.getLocator().copyLineItemProperties(newLine, oldLine);
  }

  /**
   * Create and return the exchange rate transaction data object.
   * @return the exchange rate transaction data object
   */
  public IExchangeRateTransaction createExchangeRateTransaction() {
    return TransactionHelper.createTransaction(TransactionType.EXCHANGE_RATE);
  }

  /**
   * Factory method for creating an exchange rate transaction line item.
   * @return a new line item
   */
  public IExchangeRateTransLineItem createExchangeRateTransLine() {
    return DataFactory.createObject(IExchangeRateTransLineItem.class);
  }

  /**
   * Create and return the new IdentityVerification data access object.
   * @return the new IdentityVerification data access object
   */
  public IIdentityVerification createIdentityVerification() {
    return DataFactory.createObject(IIdentityVerification.class);
  }

  /**
   * Create a resume line.
   * 
   * @param oldLine the old line item
   * @param newTransaction the new transaction
   * @return the created line
   */
  public ICreditDebitTenderLineItem createResumeLine(ICreditDebitTenderLineItem oldLine,
      IPosTransaction newTransaction) {

    ICreditDebitTenderLineItem newLine =
        (ICreditDebitTenderLineItem) createTenderLineItem(oldLine.getTender(),
            TenderStatus.forName(oldLine.getTenderStatusCode()), null);

    newTransaction.addRetailTransactionLineItem(newLine);
    copyCreditDebitTenderLineItem(newLine, oldLine, /* argResume */true);

    return newLine;
  }

  /**
   * Create and return a tender line item for the specified tender id and amount value.
   * 
   * @param argTender tender
   * @param argTenderCategory tender category
   * @param argTenderStatus tender status
   * @param argEvent the current event
   * @return a tender line item of the specified type
   */
  public ITenderLineItem createTenderLineItem(ITender argTender, TenderCategory argTenderCategory,
      TenderStatus argTenderStatus, IXstEvent argEvent) {

    ITenderLineItem result =
        createTenderLineItem(argTender, argTenderCategory.getInterfaceClass(), argTenderStatus);

    if (argTender != null && result instanceof IAuthorizableLineItem) {
      ((IAuthorizableLineItem) result).setAuthorizationMethodCode(argTender.getAuthMethodCode());
    }

    return setInputObject(result, argEvent);
  }

  /**
   * Create and return a tender line item for the specified tender.
   * 
   * @param argTender the tender to create a line item for
   * @param argTenderStatus tender status
   * @param argEvent the current event
   * @return a tender line item with the specified tender
   */
  public ITenderLineItem createTenderLineItem(ITender argTender, TenderStatus argTenderStatus,
      IXstEvent argEvent) {

    Class<? extends ITenderLineItem> tenderClass = null;

    String tenderTypeCode = argTender.getTenderType().getTenderTypecode();
    TenderCategory tenderCategory = TenderCategory.forName(tenderTypeCode);

    if (tenderCategory != null) {
      tenderClass = tenderCategory.getInterfaceClass();
    }

    if (tenderClass == null) {
      tenderClass = ITenderLineItem.class;
    }

    return setInputObject(createTenderLineItem(argTender, tenderClass, argTenderStatus), argEvent);
  }

  /**
   * Create a tender signature and add it to a line item.
   * @param argLine the line item to receive the signature.
   * @param argSignature <code>ISignature</code> object containing raw signature data.
   * @return the created tender signature.
   */
  public ITenderSignature createTenderSignature(IRetailTransactionLineItem argLine, ISignature argSignature) {
    return createTenderSignature(argLine, argSignature.getData());
  }

  /**
   * Create a new Voucher data access object with the voucher type and voucher serial number passed
   * in.
   * 
   * @param argVoucherType the tender line item
   * @param argSerial the voucher serial number
   * @return The newly created Voucher data access object
   */
  public IVoucher createVoucher(String argVoucherType, String argSerial) {
    VoucherId voucherId = new VoucherId();
    voucherId.setSerialNumber(argSerial);
    voucherId.setVoucherTypeCode(argVoucherType);

    return DataFactory.createObject(voucherId, IVoucher.class);
  }

  /**
   * Return true if the tender identified by the specified tender id is set to default to balance
   * due, false otherwise. This means that whenever this tender is selected, the amount will be set
   * to the remaining amount needed to fully tender the transaction.
   * 
   * @param argTenderId the tender id to check on
   * @return true, if the tender amount should default to the balance due. false otherwise
   */
  public boolean defaultToBalanceDue(TenderId argTenderId) {
    ITender result = getTender(argTenderId);
    return (result != null) ? result.getDfltToAmountDue() : false;
  }

  /**
   * Get a list of the foreign currency tenders that are currently available.
   * @return a list of the foreign currency tenders that are currently available
   */
  public List<ITender> getAllAvailableForeignCurrencyTenders() {
    List<ITender> availableForeignTenders = new ArrayList<ITender>();

    for (ITender tender : getAllTenders()) {
      if (isForeignTender(tender) && isCurrencyTender(tender, true) && isAvailableTender(tender)) {
        availableForeignTenders.add(tender);
      }
    }
    return availableForeignTenders;
  }

  /**
   * Get a list of the tenders configured to be available.
   * @return a list of the tenders configured to be available
   */
  public List<ITender> getAllAvailableTenders() {
    List<ITender> availableTenders = new ArrayList<ITender>();

    for (ITender tender : getAllTenders()) {
      if (isAvailableTender(tender)) {
        availableTenders.add(tender);
      }
    }
    return availableTenders;
  }

  /**
   * Returns all tenders in the system.
   * @return all tenders in the system
   */
  public List<? extends ITender> getAllTenders() {
    return DataFactory.getObjectByQueryNoThrow(TENDERS, null);
  }

  /**
   * Get a list of tenders in a category available for inclusion in a type count.
   * 
   * @param argCategory the category to look up
   * @return the list of categories
   */
  public List<ITender> getAvailableIncludeInTypeCountTendersForCategory(TenderCategory argCategory) {
    List<ITender> validTenders = new ArrayList<ITender>();
    ITender[] tenders = getAvailableTendersForCategory(argCategory);

    if ((tenders == null) || (tenders.length == 0)) {
      return null;
    }

    for (ITender tender : tenders) {
      if (tender.getIncludeInTypeCount()) {
        validTenders.add(tender);
      }
    }
    return (validTenders.isEmpty()) ? null : validTenders;
  }

  /**
   * Get all available tenders for the specified categories.
   * @param argCategory the categories to look up.
   * @return an array of tenders available for the specified categories.
   */
  public ITender[] getAvailableTendersForCategories(Collection<TenderCategory> argCategory) {
    return getAvailableTendersForCategories(argCategory.toArray(new TenderCategory[0]));
  }

  /**
   * Get all available tenders for the specified categories.
   * @param argCategory the categories to look up.
   * @return an array of tenders available for the specified categories.
   */
  public ITender[] getAvailableTendersForCategories(TenderCategory[] argCategory) {
    List<ITender> validTenders = new ArrayList<ITender>();

    for (TenderCategory element : argCategory) {
      for (ITender tender : getTendersForCategory(element)) {
        if (!tender.getTenderAvailabilityCodes().isEmpty() && validateEffectiveTender(tender)) {
          validTenders.add(tender);
        }
      }
    }
    return (validTenders.isEmpty()) ? null : validTenders.toArray(new ITender[validTenders.size()]);
  }

  /**
   * Get all available tenders for the specified category.
   * @param argCategory the category to look up.
   * @return an array of tenders available for the specified category.
   */
  public ITender[] getAvailableTendersForCategory(TenderCategory argCategory) {
    return getAvailableTendersForCategories(new TenderCategory[] {argCategory});
  }

  /**
   * Return the CHANGE tender line item of the retail transaction. We should assume that every
   * transaction can only have one CHANGE tender line item.
   * 
   * @param argTrans the retail transaction we want to search the change tender from
   * @return the CHANGE tender line item of the retail transaction
   */
  public ITenderLineItem getChangeTenderLineItem(IPosTransaction argTrans) {
    ITenderLineItem changeTender = null;
    List<ITenderLineItem> lineItems =
        argTrans.getLineItemsByTypeCode(LineItemType.TENDER.getName(), ITenderLineItem.class);

    // Need to loop through all line item to find the change tender line item
    for (ITenderLineItem tenderLine : lineItems) {
      // Check the tender status for CHANGE tender
      if (TenderStatus.CHANGE.matches(tenderLine) && !tenderLine.getVoid()) {
        changeTender = tenderLine;
        break;
      }
    }
    return changeTender;
  }

  /**
   * Get customer account plans.
   * @param argCustAccountCode the account code to check.
   * @return an array of all account plans that meet the code. Array will be empty if none are
   * found.
   */
  public List<ICustomerAccountPlan> getCustomerAccountPlans(String argCustAccountCode) {
    SortedMap<String, ICustomerAccountPlan> allPlans = new TreeMap<String, ICustomerAccountPlan>();

    getCustomerAccountPlans(allPlans, argCustAccountCode);

    // process the expiration and effective dates
    List<ICustomerAccountPlan> results = new LinkedList<ICustomerAccountPlan>();
    Date today = StoreCalendar.getBusinessDateTimeStamp();

    for (ICustomerAccountPlan plan : allPlans.values()) {
      // check effective date
      Date effectiveDate = plan.getEffectiveDate();
      if ((effectiveDate != null) && (!today.before(effectiveDate))) {
        // check expiration date
        Date expirationDate = plan.getExpirationDate();

        if ((expirationDate == null) || (!today.after(expirationDate))) {
          results.add(plan);
        }
      }
    }
    return results;
  }

  /**
   * Get the rounding amount by default cash tender.
   * @param value the amount needed rounding.
   * @return the calculated amount.
   */
  public BigDecimal getDefaultRoundingAmount(BigDecimal value) {
    return getRoundingAmountByTender(_localCurrencyTender, value);
  }

  /**
   * Get the exchange rate between the base currency and a target currency.
   * @param argTargetCurrency the target currency to check.
   * @return the exchange rate.
   */
  public BigDecimal getExchangeRate(String argTargetCurrency) {
    return ExchangeRateHelper.getExchangeRate(argTargetCurrency);
  }

  /**
   * Get the foreign currency tender that matches a given ID.
   * @param argCurrencyId the ID to look up.
   * @return the tender if found, or null if not.
   */
  public ITender getForeignCurrencyTender(String argCurrencyId) {
    ITender matched = null;

    for (ITender tender : getAllTenders()) {
      if (isCurrencyTender(tender, false) && tender.getCurrencyId().equalsIgnoreCase(argCurrencyId)) {
        matched = tender;
      }
    }
    return matched;
  }

  /**
   * Return the tender for local currency.
   * @return the tender.
   */
  public ITender getLocalCurrency() {
    return _localCurrencyTender;
  }

  /**
   * Get the ID for the local currency tender.
   * @return the ID.
   */
  public String getLocalCurrencyTenderId() {
    ITender localCurrency = getLocalCurrency();
    return (localCurrency == null) ? null : localCurrency.getTenderId();
  }

  /**
   * Get a list of the foreign currency tenders that are currently available.
   * @return a list of the foreign currency tenders that are currently available
   */
  public List<? extends IExchangeRate> getLocalExchangeRates() {
    return ExchangeRateHelper.getLocalExchangeRates();
  }

  /**
   * Return the tender for travelers' checks.
   * @return the tender.
   */
  public ITender getLocalTravelersCheck() {
    return _localTravelersCheck;
  }

  /**
   * Get the ID for the local travelers' checks tender.
   * @return the ID.
   */
  public String getLocalTravelersCheckTenderId() {
    ITender travCheckCurrency = getLocalTravelersCheck();
    return (travCheckCurrency == null) ? null : travCheckCurrency.getTenderId();
  }

  /**
   * Get the maximum amount allowed to prompt for a tender.
   * @param argTender the tender to check.
   * @param argTenderUsageCodeType the usage code.
   * @param argTransaction the transaction against which to check.
   * @return the calculated amount.
   */
  public BigDecimal getMaxPromptAmount(ITender argTender, TenderUsageCodeType argTenderUsageCodeType,
      IPosTransaction argTransaction) {

    return getMaxPromptAmount(argTender, argTenderUsageCodeType, argTransaction, true);
  }

  /**
   * Get the maximum amount allowed to prompt for a tender.
   * @param argTender the tender to check.
   * @param argTenderUsageCodeType the usage code.
   * @param argTransaction the transaction against which to check.
   * @param argNeedRounding the need rounding flag.
   * @return the calculated amount.
   */
  public BigDecimal getMaxPromptAmount(ITender argTender, TenderUsageCodeType argTenderUsageCodeType,
      IPosTransaction argTransaction, boolean argNeedRounding) {
    BigDecimal maxAllowed = argTransaction.getAmountDue();
    // TenderId tid = TenderId.forName(tender.getTenderId());
    ITenderUserSettings settings = getTenderSettings(argTender.getTenderId(), argTenderUsageCodeType);

    if (settings != null) {
      maxAllowed = settings.getMaximumAcceptAmount();
      BigDecimal totalForTender = getTotalForTender(argTender, argTransaction);
      maxAllowed = maxAllowed.subtract(totalForTender);
    }

    BigDecimal amountDue = argTransaction.getAmountDue().abs();
    maxAllowed = maxAllowed.min(amountDue);

    // If foreign tender, ignore rounding
    if (argNeedRounding) {
      maxAllowed = getRoundingAmountByTender(argTender, amountDue);
    }
    maxAllowed = maxAllowed.max(NumberUtils.ZERO);

    return maxAllowed;
  }

  /**
   * Get the account type from the event or one of its chained alternate events.
   * 
   * @param argCreditCardEvent
   * @return the first tender ID from the event chain that is not DEBIT_CARD
   * @see TenderConstants#DEBIT_CARD
   */
  public String getNonDebitTenderId(IHardwareInputEvent argCreditCardEvent) {
    CreditCardEvent[] events =
        (CreditCardEvent[]) getInputEventChain(argCreditCardEvent, CreditCardEvent.class);

    if (events.length == 0) {
      _logger.warn("no credit card events found");
      return null;
    }

    for (CreditCardEvent element : events) {
      String accountType = element.getAccountType();

      if (!DEBIT_CARD.equals(accountType)) {
        return accountType;
      }
    }

    _logger.warn("all " + events.length + " event(s) has tender ID of '" + DEBIT_CARD + "'");
    return null;
  }

  /**
   * Get whether a transaction is a return with a receipt.
   * @param argTrans the transaction to check.
   * @return true if the transaction is a return with a receipt, false if any returned item does not
   * have a receipt.
   */
  public boolean getReturnWithReceipt(IPosTransaction argTrans) {
    boolean returnValue = true;
    List<ISaleReturnLineItem> lineItems =
        argTrans.getLineItemsByTypeCode(LineItemType.ITEM.getName(), ISaleReturnLineItem.class);

    if ((lineItems == null) || lineItems.isEmpty()) {
      return returnValue;
    }

    for (ISaleReturnLineItem lineItem : lineItems) {
      // For now, if there is any return item return without receipt, treat
      // the whole transaction as return without receipt.
      if (lineItem.getReturn() && (lineItem.getOriginalBusinessDate() == null)) {
        returnValue = false;
        break;
      }
    }

    return returnValue;
  }

  /**
   * Get the rounding amount by tender.
   * @param argTender the tender.
   * @param orgValue amount needed rounding.
   * @return the calculated amount.
   */
  public BigDecimal getRoundingAmountByTender(ITender argTender, BigDecimal orgValue) {
    BigDecimal mda = null;

    if (argTender != null) {
      mda = getMinDenominationByTender(argTender);
    }
    else {
      return getDefaultRoundingAmount(orgValue);
    }

    BigDecimal roundValue = orgValue;

    if (mda != null) {
      RoundingMode roundingMode = ConfigurationMgr.getLocalCurrencyRoundingMode();
      BigDecimal quotient = orgValue.divide(mda, 0, roundingMode);
      roundValue = quotient.multiply(mda);
    }

    return roundValue;
  }

  /**
   * Get a rounding tender line item from a transaction, if present.
   * @param argTrans the transaction to look up.
   * @return the rounding tender if present, <code>null</code> otherwise.
   */
  public ITenderLineItem getRoundingTenderLineItem(IPosTransaction argTrans) {
    ITenderLineItem roundingTender = null;
    List<ITenderLineItem> tenderLineItems =
        argTrans.getLineItemsByTypeCode(LineItemType.TENDER.getName(), ITenderLineItem.class);

    if ((tenderLineItems != null) && !tenderLineItems.isEmpty()) {
      // Need to loop through all line item to find the change tender line item
      for (ITenderLineItem tenderItem : tenderLineItems) {
        // Check the tender status for CHANGE tender
        if ("LOCAL_CURRENCY_ROUNDING".equalsIgnoreCase(tenderItem.getTender().getTenderId())
            && !tenderItem.getVoid()) {

          roundingTender = tenderItem;
          break;
        }
      }
    }

    return roundingTender;
  }

  /**
   * Get a new signature from a tender signature.
   * @param argTenderSignature
   * @return the new signature.
   */
  public ISignature getSignature(ITenderSignature argTenderSignature) {
    return new Signature(argTenderSignature.getSignature());
  }

  /**
   * Gets the tender for the specified tender ID.
   * 
   * @param argTenderId the tender ID for which to find a tender
   * @param argCallingInfo information to include for debugging if the tender is not found; should
   * be the name and line number of the configuration file containing the tender ID configuration
   * @return the tender or <code>null</code> if not found
   */
  public ITender getTender(String argTenderId, Object argCallingInfo) {
    if (argTenderId == null) {
      return null;
    }

    ITender tender = getTender(createTenderId(argTenderId));

    if ((tender == null) && _logger.isDebugEnabled()) {
      if ((argCallingInfo == null) || (argCallingInfo.toString() == null)) {
        _logger.debug(argTenderId + " is not a valid tender ID.", new Throwable("STACK TRACE"));
      }
      else {
        _logger.debug(argTenderId + " is not a valid tender ID @@ " + argCallingInfo);
      }
    }
    return tender;
  }

  /**
   * Return the list of tenders that contains specified tender availability code.
   * 
   * @param argCode availability type code
   * @return the list of tenders that contains specified tender availability code.
   */
  public List<ITender> getTendersByAvailabilityCode(TenderAvailabilityCodeType argCode) {
    List<ITender> available = new ArrayList<ITender>(5);

    for (ITender tender : getAllTenders()) {
      if (tender.containsAvailCode(argCode.getName())) {
        available.add(tender);
      }
    }

    if (available.isEmpty()) {
      _logger.warn("Cannot find any tenders with the availability code of " + argCode);
      return null;
    }
    return available;
  }

  /**
   * Return the tender settings for the specified tender id, user group, and entry method.
   * 
   * @param argGroup group of interest
   * @param argTenderId ID for the tender of interest
   * @param argUsageCode usage of interest
   * @return the tender settings
   */
  public ITenderUserSettings getTenderSettings(IGroup argGroup, String argTenderId,
      TenderUsageCodeType argUsageCode) {

    return getTenderSettings(argGroup, argTenderId, argUsageCode, DEFAULT);
  }

  /**
   * Return the tender settings for the specified tender id, user group, and entry method.
   * 
   * @param argGroup group of interest
   * @param argTenderId ID for the tender of interest
   * @param argUsageCode usage of interest
   * @param argEntryMethod the entry method used for the tender
   * @return the tender settings
   */
  public ITenderUserSettings getTenderSettings(IGroup argGroup, String argTenderId,
      TenderUsageCodeType argUsageCode, String argEntryMethod) {

    if ((argUsageCode == null) || (argTenderId == null) || (argGroup == null)) {
      return null;
    }

    String groupId = argGroup.getGroupId();

    ITenderUserSettings[] results = null;
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("argGroupId", groupId);
    params.put("argTenderId", argTenderId);
    params.put("argUsageCode", argUsageCode.getName());
    params.put("argEntryMthdCode", argEntryMethod);

    List<ITenderUserSettings> settings =
        DataFactory.getObjectByQueryNoThrow(TENDER_USER_SETTINGS_FOR_TENDER, params);

    results = settings.toArray(new ITenderUserSettings[settings.size()]);

    // If there are more than 1, take the one that has the same usage code.
    switch (results.length) {
      case 0:
        _logger.warn("no tender user settings for group='" + groupId + "', tenderId='" + argTenderId
            + "', usageCode='" + argUsageCode.getName() + "'");
        return null;

      case 1:
        // If there is only one found... return it
        return results[0];

      default:
        if (results.length > 1) {
          // First search if any user setting that match both the usage code and entry method.
          for (ITenderUserSettings element : results) {
            if (argUsageCode.getCode().equalsIgnoreCase(element.getUsageCode())
                && argEntryMethod.equalsIgnoreCase(element.getEntryMethodCode())) {
              return element;
            }
          }

          // Second search if any user setting at least match the usage code
          for (ITenderUserSettings element : results) {
            if (argUsageCode.getCode().equalsIgnoreCase(element.getUsageCode())) {
              return element;
            }
          }
        }
        _logger.warn("no tender user settings for group='" + groupId + "', tenderId='" + argTenderId
            + "', usageCode='" + argUsageCode.getName() + "'");
    }
    return null;
  }

  /**
   * Return the tender settings for the specified tender id and user group.
   * 
   * @param argTenderId ID for tender of interest
   * @param argUsageCode usage of interest
   * @return the tender settings for the specified tender id and user group.
   */
  public ITenderUserSettings getTenderSettings(String argTenderId, TenderUsageCodeType argUsageCode) {
    return getTenderSettings(argTenderId, argUsageCode, DEFAULT);
  }

  /**
   * Return the tender settings for the specified tender id, usage code, and entry method.
   * 
   * @param argTenderId ID for tender of interest
   * @param argUsageCode usage of interest
   * @param argEntryMethod the entry method used for the tender
   * @return the tender settings for the specified tender id and user group.
   */
  public ITenderUserSettings getTenderSettings(String argTenderId, TenderUsageCodeType argUsageCode,
      String argEntryMethod) {

    if ((argTenderId == null) || (argUsageCode == null)) {
      return null;
    }

    ISecurityMgr sm = SecurityMgr.getInstance();
    IAuthenticationTicket au = sm.getAuthenticatedUser();
    ISystemUser su = au.getSystemUser();
    IGroup group = su.getPrimaryGroup();

    if (group == null) {
      _logger.warn("system user (operatorId='" + su.getOperatorId() + "') has no primary group");
    }

    return getTenderSettings(group, argTenderId, argUsageCode, argEntryMethod);
  }

  /**
   * Return an array of tenders that exist for the specified category
   * 
   * @param argCategory the category to find tenders by
   * @return an array of tenders that exist for the specified category
   */
  public List<? extends ITender> getTendersForCategory(TenderCategory argCategory) {
    if (argCategory == null) {
      return getAllTenders();
    }
    else if (argCategory.equals(TenderCategory.FOREIGN_CURRENCY)) {
      return getAllAvailableForeignCurrencyTenders();
    }
    else {
      List<ITender> tendersForCategory = new ArrayList<ITender>(4);

      for (ITender tender : getAllTenders()) {
        if (argCategory.matches(tender)) {
          tendersForCategory.add(tender);
        }
      }
      return tendersForCategory;
    }
  }

  /**
   * Get the tender signature from a line item.
   * @param argLine the line item to look up.
   * @return the tender signature.
   */
  public ITenderSignature getTenderSignature(IRetailTransactionLineItem argLine) {
    return argLine.getSignature();
  }

  /**
   * Get the tender type from an ID.
   * @param argTenderType the ID to look up.
   * @return the tender type, or null if not found.
   */
  public ITenderType getTenderType(String argTenderType) {
    TenderTypeId id = new TenderTypeId();
    id.setTenderTypecode(argTenderType);

    return DataFactory.getObjectByIdNoThrow(id);
  }

  /**
   * Get the tender type from a category.
   * @param argTenderType the category to look up.
   * @return the tender type, or null if not found.
   */
  public ITenderType getTenderType(TenderCategory argTenderType) {
    return getTenderType(argTenderType.getName());
  }

  /**
   * Return the total amount of the specified tender already issued in the specified transaction.
   * 
   * @param argTender tender of interest
   * @param argTran current transaction
   * @return the total amount of the specified tender
   */
  public BigDecimal getTotalForTender(ITender argTender, IPosTransaction argTran) {
    // Changed this from IRetailTransaction to IPosTransaction to work with Tender Control Transactions.
    BigDecimal result = ZERO;
    String tenderId = argTender.getTenderId();

    for (ITenderLineItem tenderLine : argTran.getLineItems(ITenderLineItem.class)) {
      if (!tenderLine.getVoid()
          && !tenderLine.getTenderStatusCode().equalsIgnoreCase(TenderStatus.CHANGE.getName())) {

        if (tenderLine.getTender().getTenderId().equalsIgnoreCase(tenderId)) {
          if (tenderLine.getAmount() != null) {
            result = result.add(tenderLine.getAmount());
          }
        }
      }
    }
    return result;
  }

  /**
   * Returns a collection of all tenders which:<br>
   * <ul>
   * <li>were originally used to tender any transactions represented in verified returns in the
   * current transaction.</li>
   * <li>may be credited with an amount owed to the customer in the current transaction.</li>
   * <li>have not already been used in the current transaction.</li>
   * </ul>
   * @param argCurrentTrans the current transaction whose valid set of refund tenders is to be
   * derived from the verified returns included in it
   * @return all tenders which were originally used in transactions represented by verified returns
   * in <code>argCurrentTrans</code> and which may be used in <code>argCurrentTrans</code> to
   * receive a refund
   */
  public Collection<? extends ICreditDebitTenderLineItem> getUsableTendersFromOriginalTransactions(
      IRetailTransaction argCurrentTrans) {

    /* Build a set of all credit/debit tender lines from all verified returns represented by verified returns
     * in the current transaction. */
    Collection<? extends ICreditDebitTenderLineItem> origTransTenderLines =
        searchOriginalTransCreditTenderLineItems();

    // No qualifying tender lines, so nothing to choose from.
    if ((origTransTenderLines == null) || (origTransTenderLines.isEmpty())) {
      return Collections.emptyList();
    }

    /* Build a set of all credit/debit tender lines already used in the current transaction and which will
     * therefore be excluded from the return set. */
    List<ICreditDebitTenderLineItem> returnTransTenderLines =
        argCurrentTrans.getLineItems(ICreditDebitTenderLineItem.class);

    /* Initialize a set of return tenders. At most, this will contain all credit/debit tenders from all
     * verified returns represented in the current transaction. */
    List<ICreditDebitTenderLineItem> validTenderLines =
        new ArrayList<ICreditDebitTenderLineItem>(origTransTenderLines.size());

    /* Loop over all tender lines from the original transactions and add to the return set any that pass all
     * qualifying checks. */
    for (ICreditDebitTenderLineItem origTransTenderLine : origTransTenderLines) {
      // Exclude all voided tender lines and anything not available for return.
      boolean stillValid = !origTransTenderLine.getVoid() //
          && origTransTenderLine.getTender().containsAvailCode(RETURN_WITH_RECEIPT.getName());

      if (stillValid) {
        /* Exclude all tenders for negative amounts. This may be possible, for example, if a verified return
         * is being performed against a mixed transaction in which a refund was issued to the customer. Say
         * Joe bought a $20 item and returned an $80 item at the same time; the transaction would capture a
         * tender with a -$60 amount. We don't want that tender considered as a refund store when Joe comes
         * back and returns the $20 item. */
        if (isNonPositive(origTransTenderLine.getAmount())) {
          stillValid = false;
        }
        else {
          // Exclude all tenders that have already been used in the current transaction.
          String tenderAccountNbr = origTransTenderLine.getAccountNumberHash();

          /* See if a tender with the same account number has already been used in the current transaction. If
           * it has, the user can't use it again without first voiding the original. */
          for (ICreditDebitTenderLineItem returnTransTenderLine : returnTransTenderLines) {
            if (!returnTransTenderLine.getVoid()
                && returnTransTenderLine.getAccountNumberHash().equalsIgnoreCase(tenderAccountNbr)) {

              stillValid = false;
              break;
            }
          }
        }
      }

      /* Exclude all tenders representing verified returns included in the current transaction but for which
       * no actual items from those transactions have been returned. This may be possible if the user
       * initiates a verified return and then exits without performing any line-level return activity, or if
       * such activity was subsequently voided. */
      if (stillValid) {
        stillValid = false;
        final long origTransTenderLineSeq = origTransTenderLine.getTransactionSequence();

        for (ISaleReturnLineItem returnedSaleLine : argCurrentTrans.getLineItems(ISaleReturnLineItem.class)) {
          /* Comparing transaction sequences isn't bulletproof -- it's possible that a tender line and sale
           * line have the same transaction sequence but were not part of the same transaction (e.g. they were
           * used in two different stores). But that would be a freak coincidence and not worth sweating over;
           * this entire test is already working against the fringes of frequency. */
          if (!returnedSaleLine.getVoid() && !returnedSaleLine.getReturnedWithGiftReceipt()
              && (returnedSaleLine.getOriginalTransactionSequence() == origTransTenderLineSeq)) {

            stillValid = true;
            break;
          }
        }
      }

      if (stillValid) {
        validTenderLines.add(origTransTenderLine);
      }
    }
    return validTenderLines;
  }

  /**
   * Get a voucher from a transaction that has a given type and serial number.
   * @param tran the current transaction on which to find the line items to process.
   * @param argVoucherType the voucher type code to look up.
   * @param argSerial the serial number to look up
   * @return the voucher line item if found, <code>null</code> if not found or if the transaction
   * object is null.
   */
  public IVoucherLineItem getVoucherFromCurrentTransaction(IPosTransaction tran, String argVoucherType,
      String argSerial) {
    // Also need to search for current transaction line item since they are not
    // persisted into database yet.
    if (tran != null) {
      List<IVoucherLineItem> lineItems = tran.getLineItems(IVoucherLineItem.class);

      if (lineItems != null) {
        for (IVoucherLineItem voucherLineItem : lineItems) {
          if (!voucherLineItem.getVoid() && voucherLineItem.getSerialNumber().equalsIgnoreCase(argSerial)
              && voucherLineItem.getVoucherTypeCode().equalsIgnoreCase(argVoucherType)) {
            return voucherLineItem;
          }
        }
      }
    }

    return null;
  }

  /**
   * Search and return the voucher based on the voucher type and voucher serial number passed in.
   * 
   * @param argVoucherType the voucher type
   * @param argSerial the voucher serial
   * @return the voucher based on the voucher type and voucher serial number passed in.
   */
  public IVoucher getVoucherFromDatabase(String argVoucherType, String argSerial) {
    VoucherId id = new VoucherId();
    id.setSerialNumber(argSerial);
    id.setVoucherTypeCode(argVoucherType);

    return DataFactory.getObjectByIdNoThrow(id);
  }

  /**
   * Get the root config for vouchers.
   * @return the config.
   */
  public VoucherRootConfig getVoucherRootConfig() {
    return _voucherHelper.getRootConfig();
  }

  /**
   * Get whether any tender on a transaction uses local currency.
   * @param argTrans the transaction to look up.
   * @return <code>true</code> if any signature uses local currency, <code>false</code> if none do.
   */
  public boolean isAnyLocalCurrencyTender(IRetailTransaction argTrans) {
    boolean result = false;

    if (argTrans != null) {
      List<ITenderLineItem> tenderLineItems =
          argTrans.getLineItemsByTypeCode(LineItemType.TENDER.getName(), ITenderLineItem.class);

      if (tenderLineItems != null) {
        for (ITenderLineItem tndrLineItem : tenderLineItems) {
          if (isLocalCurrency(tndrLineItem.getTender())) {
            result = true;
            break;
          }
        }
      }
    }

    return result;
  }

  /**
   * Examines the event and its alternates to determine if it could represent either a credit card
   * or a debit card, but we can't tell which yet.
   * 
   * @param argInputEvent the event to examine
   * @return <code>false</code> if the event could be neither credit nor debit, <code>false</code>
   * if the event must be credit or must be debit, or <code>true</code> if the tender line could be
   * either credit or debit, but we can't tell which for sure
   * @see TenderConstants#DEBIT_CARD
   */
  public boolean isCreditDebitAmbiguous(IHardwareInputEvent argInputEvent) {
    CreditCardEvent[] events = (CreditCardEvent[]) getInputEventChain(argInputEvent, CreditCardEvent.class);
    boolean foundDebit = false;
    boolean foundCredit = false;
    for (CreditCardEvent element : events) {
      if (DEBIT_CARD.equals(element.getAccountType())) {
        foundDebit = true;
      }
      else {
        foundCredit = true;
      }
    }

    if (foundDebit && foundCredit) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Check a tender to see if it uses foreign currency.
   * 
   * @param argTndr the tender to check
   * @return <code>true</code> if the tender uses foreign currency, <code>false</code> if not
   */
  public boolean isForeignCurrency(ITender argTndr) {
    return isForeignTender(argTndr) && isCurrencyTender(argTndr, false);
  }

  /**
   * Check a tender to see if it is a foreign tender.
   * 
   * @param argTndr the tender to check
   * @return <code>true</code> if the tender is a foreign tender, <code>false</code> if not
   */
  public boolean isForeignTender(ITender argTndr) {
    return !getLocalCurrencyId().equalsIgnoreCase(argTndr.getCurrencyId());
  }

  /**
   * Check a tender to see if it uses local currency.
   * 
   * @param argTndr the tender to check.
   * @return <code>true</code> if the tender uses local currency, <code>false</code> if not.
   */
  public boolean isLocalCurrency(ITender argTndr) {
    String tenderTypecode = argTndr.getTenderType().getTenderTypecode();

    if (!tenderTypecode.equalsIgnoreCase(TenderCategory.CURRENCY.getName())) {
      return false;
    }

    return getLocalCurrencyId().equalsIgnoreCase(argTndr.getCurrencyId());
  }

  /**
   * Determine whether or not a retail transaction line item contains a non-voidable voucher
   * 
   * @param argLineItem the line item to check.
   * @return true if it does, false if not.
   */
  public boolean isNotVoidableVoucher(IRetailTransactionLineItem argLineItem) {
    VoucherRootConfig rootConfig = TenderHelper.getInstance().getVoucherRootConfig();
    ActivityConfig actConfig = null;
    IVoucherSaleLineItem saleLine = null;

    if (argLineItem instanceof IVoucherLineItem) {

      if (argLineItem instanceof IVoucherSaleLineItem) {
        saleLine = (IVoucherSaleLineItem) argLineItem;

        if (isGreaterThan(saleLine.getFaceValueAmount(), saleLine.getUnspentBalanceAmount())) {
          return true;
        }
      }

      IVoucherLineItem vli = (IVoucherLineItem) argLineItem;
      VoucherConfig vc = rootConfig.getVoucherConfig(vli.getVoucherTypeCode());
      VoucherActivityCodeType act = VoucherActivityCodeType.valueOf(vli.getActivityCode());

      if ((vc != null) && (act != null)) {
        actConfig = vc.getActivityConfig(act);
      }

      if (actConfig == null) {
        if (argLineItem instanceof IVoucherSaleLineItem) {
          actConfig =
              rootConfig.getActivityConfigForItemIdNActivity(saleLine.getActivityCode().trim(), saleLine
                  .getItem().getItemId().trim());
        }
        else if (argLineItem instanceof IVoucherTenderLineItem) {
          IVoucherTenderLineItem tndrLine = (IVoucherTenderLineItem) argLineItem;
          actConfig = rootConfig.getActivityConfigForTenderId(tndrLine.getTenderId());
        }
      }
    }

    if ((actConfig != null) && !actConfig.getVoidable()) {
      return true;
    }

    return false;
  }

  /**
   * Return whether or not the serial number is required for this specific tender.
   * 
   * @param argTenderId the tender id in String value
   * @return whether or not the serial number is required for this specific tender.
   */
  public boolean isSerialNumberRequired(String argTenderId) {
    TenderId id = new TenderId();
    id.setTenderId(argTenderId);

    ITender result = getTender(id);
    return (result != null) ? result.getSerialIdentificationNbrRequired() : false;
  }

  /**
   * Determine whether or not a signature is required for a tender line item.
   * @param argLine the tender line item to check.
   * @return true if the line item requires a signature, false if not.
   */
  public boolean isSignatureRequired(ITenderLineItem argLine) {
    if ((argLine == null) || (argLine.getTender() == null)) {
      return false;
    }

    // we always look at the flag on the tender first
    if (!argLine.getTender().getCustomerSignatureRequired()) {
      return false;
    }

    if (argLine.getAmount() == null) {
      // if not amount, can't check the levels
      // (this check was in the previous implementation, so leaving it in to prevent introducing a bug)
      return true;
    }

    String entryMethod = null;

    if (argLine instanceof IAuthorizableLineItem) {
      String code = ((IAuthorizableLineItem) argLine).getEntryMethodCode();
      /* split the method which will be something like "KEYBOARD.KEYBOARD", "MSR.MAIN_MSR", "MICR.MAIN_MICR",
       * "MSR.CUST_MSR", or "MSR.RFID" into it's components and return the last component (e.g. "KEYBOARD",
       * "MAIN_MSR", "MAIN_MICR", "CUST_MSR", or "RFID".) */
      if (!StringUtils.isEmpty(code)) {
        String[] parts = StringUtils.split(code, '.');
        entryMethod = parts[parts.length - 1];
      }
    }

    if (StringUtils.isEmpty(entryMethod)) {
      entryMethod = DEFAULT;
    }

    final TenderUsageCodeType sigUsage;

    if (isNegative(argLine.getAmount())) {
      sigUsage = TenderUsageCodeType.SIGNATURE_REQ_FLOOR_LIMIT_NEG;
    }
    else {
      sigUsage = TenderUsageCodeType.SIGNATURE_REQ_FLOOR_LIMIT;
    }

    ITenderUserSettings settings =
        getTenderSettings(argLine.getTender().getTenderId(), sigUsage, entryMethod);

    if (settings == null) {
      // no fine tuning configured
      _logger.warn("no tnd_tndr_user_settings entry for tndr_id='" + argLine.getTender().getTenderId()
          + "' and usage_code='" + sigUsage.getCode() + "' or '" + DEFAULT + "'");
      return true;
    }

    if (settings.getMinimumAcceptAmount() == null) {
      _logger.warn("unexpected NULL found in 'tnd_tndr_user_settings'.'min_accept_amt' for ["
          + settings.getObjectIdAsString() + "]...ignoring settings");
      return true;
    }

    if (!settings.getUsageCode().equalsIgnoreCase(sigUsage.getName())
        && !settings.getUsageCode().equalsIgnoreCase(DEFAULT)) {
      _logger.warn("ignoring 'tnd_tndr_user_settings' entry for [" + settings.getObjectIdAsString()
          + "] ...does not match '" + sigUsage.getName() + "' nor '" + DEFAULT + "'");
      return true;
    }

    return !isGreaterThan(settings.getMinimumAcceptAmount(), argLine.getAmount().abs());
  }

  /**
   * Search and return the voucher based on the voucher type and voucher serial number passed in.
   * 
   * @param argVoucherType the voucher type
   * @param argSerial the voucher serial
   * @return the voucher based on the voucher type and voucher serial number passed in.
   */
  public IVoucher lookupVoucher(String argVoucherType, String argSerial) {
    return getVoucherFromDatabase(argVoucherType, argSerial);
  }

  /**
   * Persist all the voucher history of the retail transaction into the database.
   * 
   * @param argTrans The retail transaction that cotains the voucher history.
   */
  public void persistVoucherHistory(IPosTransaction argTrans) {
    // Get all the retail transaction line items from the transaction
    List<IVoucherLineItem> lineItems = argTrans.getLineItems(IVoucherLineItem.class);

    for (IVoucherLineItem voucherLineItem : lineItems) {
      if (voucherLineItem.getVoucher() != null) {
        createVoucherHistory(voucherLineItem, argTrans.getPostVoid());
      }
    }
  }

  /**
   * Get a list of local tenders.
   * @param argTndrTypeCode the type code for which to search.
   * @return a list of local tenders with the requested type code.
   */
  public List<ITender> searchLocalTender(String argTndrTypeCode) {
    List<ITender> localTenders = new ArrayList<ITender>();

    final String LOCAL_CURRENCY = getLocalCurrencyId();

    for (ITender tender : getAllTenders()) {
      if (isAvailableTender(tender) && LOCAL_CURRENCY.equals(tender.getCurrencyId())
          && tender.getTenderTypecode().equals(argTndrTypeCode)) {

        localTenders.add(tender);
      }
    }
    return localTenders;
  }

  /**
   * Search for original credit/debit tender line items.
   * @param argClass unused.
   * @return a list of credit/debit tender line items.
   */
  public Collection<? extends ICreditDebitTenderLineItem> searchOriginalTransCreditTenderLineItems() {
    List<ICreditDebitTenderLineItem> ccTenderLineItems = new ArrayList<ICreditDebitTenderLineItem>();
    List<IRetailTransaction> origTransactions = ReturnManager.getInstance().getAllOrigTransaction();

    /* If there is no original transaction in current retail transaction, return false; */
    if ((origTransactions == null) || origTransactions.isEmpty()) {
      return ccTenderLineItems;
    }

    for (IRetailTransaction transaction : origTransactions) {
      List<ICreditDebitTenderLineItem> tenderLineItems =
          transaction.getLineItems(ICreditDebitTenderLineItem.class);

      if ((tenderLineItems == null) || tenderLineItems.isEmpty()) {
        continue;
      }

      for (ICreditDebitTenderLineItem creditDebitLine : tenderLineItems) {
        if (!creditDebitLine.getVoid()) {
          ITender tender = creditDebitLine.getTender();
          /*DEBIT_CARD purchases should be shown in the prompt list to be able to perform a 
          return on the credit side if it is a combo-card*/
          if ("DEBIT_CARD".equalsIgnoreCase(tender.getTenderId())) {

            final MsrSwipe swipe =
                MsrSwipe.makeFromTrack2(creditDebitLine.getAccountNumber() + "="
                    + creditDebitLine.getExpirationDateString());
            IXstEvent event = EventDiscriminator.getInstance().translateEvent(swipe);

            if (!isCreditDebitAmbiguous((IHardwareInputEvent<?>) event)) {
              continue;
            }
          }

          if (!tender.containsAvailCode(TenderAvailabilityCodeType.RETURN_WITH_RECEIPT.getName())) {
            continue;
          }

          /* Now that Tokenization has been added in 4.0, there may not be an account number stored, so check
           * for the masked account number istead. */
          if (!StringUtils.isEmpty(creditDebitLine.getMaskAccountNumberDao())) {
            ccTenderLineItems.add(creditDebitLine);
          }
        }
      }
    }
    return ccTenderLineItems;
  }

  /**
   * Get a single local tender.
   * @param argTndrCategory the category to look up.
   * @return the single tender if there is one, null if zero or more than one.
   */
  public ITender searchSingleLocalTender(String argTndrCategory) {
    List<ITender> list = searchLocalTender(argTndrCategory);
    return (list.isEmpty()) ? null : list.get(0);
  }

  /**
   * Get a single local tender.
   * @param argTndrCategory the category to look up.
   * @return the single tender if there is one, null if zero or more than one.
   */
  public ITender searchSingleLocalTender(TenderCategory argTndrCategory) {
    return searchSingleLocalTender(argTndrCategory.getName());
  }

  /**
   * Search tender line items for all tenders that match a given tender ID.
   * @param argTrans the transaction containing the line items to search.
   * @param argTenderId the tender ID for which to search.
   * @return a list of all line items that match, or null if the transaction is null.
   */
  public List<ITenderLineItem> searchTenderLineItemsByTenderId(IRetailTransaction argTrans, String argTenderId) {
    if (argTrans == null) {
      return null;
    }

    List<IRetailTransaction> trans = new ArrayList<IRetailTransaction>();
    trans.add(argTrans);
    return searchTenderLineItemsByTenderId(trans, argTenderId);
  }

  /**
   * Search tender line items for all tenders that match a given tender ID.
   * @param argTrans a list of transactions containing the line items to search.
   * @param argTenderId the tender ID for which to search.
   * @return a list of all line items that match.
   */
  public List<ITenderLineItem> searchTenderLineItemsByTenderId(List<IRetailTransaction> argTrans,
      String argTenderId) {
    List<ITenderLineItem> tenderLineItems = new ArrayList<ITenderLineItem>();

    // If there is no original transaction in current retail transaction, return false;
    if ((argTrans == null) || argTrans.isEmpty()) {
      return tenderLineItems;
    }

    for (IRetailTransaction transaction : argTrans) {
      List<ITenderLineItem> lineItems = transaction.getLineItems(ITenderLineItem.class);

      if ((lineItems == null) || lineItems.isEmpty()) {
        continue;
      }

      for (ITenderLineItem tndrLineItem : lineItems) {
        if (!tndrLineItem.getVoid()) {
          ITender tender = tndrLineItem.getTender();
          if ((tender != null) && tender.getTenderId().equalsIgnoreCase(argTenderId)) {
            tenderLineItems.add(tndrLineItem);
          }
        }
      }
    }

    return tenderLineItems;
  }

  /**
   * Set the tender line item's tender id field value. For certain tenders like credit card, the
   * tender id is unknown until the user swipes or enters the credit card number.
   * 
   * @param argTenderLineItem the tender line item
   * @param argTenderId the tender id needs to be updated to the tender line item
   */
  public void setTenderIdType(ITenderLineItem argTenderLineItem, String argTenderId) {
    ITender tender = getTender(argTenderId, null);
    argTenderLineItem.setTender(tender);

    if (argTenderLineItem instanceof IAuthorizableTenderLineItem) {
      String authMethod = tender.getAuthMethodCode();
      ((IAuthorizableTenderLineItem) argTenderLineItem).setAuthorizationMethodCode(authMethod);
    }
  }

  /**
   * Return the list of tenders that contains specified tender availability code.
   * 
   * @param argCode availability type code
   * @param argTenderId tender ID
   * @return the list of tenders that contains specified tender availability code.
   */
  public ITender tenderAvailable(TenderAvailabilityCodeType argCode, String argTenderId) {
    return tenderAvailable(argCode, argTenderId, null);
  }

  /**
   * Return the list of tenders that contains specified tender availability code.
   * 
   * @param argCode availability type code
   * @param argTenderId tender ID
   * @param argCallingInfo from where the method was called (used for logging)
   * @return the list of tenders that contains specified tender availability code.
   */
  public ITender tenderAvailable(TenderAvailabilityCodeType argCode, String argTenderId, Object argCallingInfo) {
    if ((argCode == null) || (argTenderId == null)) {
      return null;
    }

    ITender tender = getTender(createTenderId(argTenderId));

    if ((tender != null) && tender.containsAvailCode(argCode.getName())) {
      return tender;
    }
    else {
      if (_logger.isDebugEnabled()) {
        if (argCallingInfo == null) {
          _logger.debug("Tender " + argTenderId + " does not have requested availability code: " + argCode,
              new Throwable("STACK TRACE"));
        }
        else {
          _logger.debug("Tender " + argTenderId + " does not have requested availability code: " + argCode
              + " @@ " + argCallingInfo);
        }
      }
      return null;
    }
  }

  /**
   * Validate that a tender is able to be used on the current date.
   * 
   * @param tender Tender to be checked.
   * @return True if the effective and expiration dates are valid for today's date.
   */
  public boolean validateEffectiveTender(ITender tender) {
    return validateTenderEffectiveDate(tender) && validateTenderExpirationDate(tender);
  }

  /**
   * Validate that a tender's effective date is on or before today.
   * @param tender
   * @return whether the validation passed
   */
  public boolean validateTenderEffectiveDate(ITender tender) {
    return !((tender.getEffectiveDate() != null) && tender.getEffectiveDate().after(
        DateUtils.clearTime(StoreCalendar.getCurrentBusinessDate())));
  }

  /**
   * Validate that a tender's expiration date is on or after today.
   * @param tender
   * @return whether the validation passed
   */
  public boolean validateTenderExpirationDate(ITender tender) {
    return !((tender.getExpirationDate() != null) && tender.getExpirationDate().before(
        DateUtils.clearTime(StoreCalendar.getCurrentBusinessDate())));
  }

  /**
   * Copy identity verification between two line items.
   * @param newLine the destination line.
   * @param oldLine the source line.
   */
  protected void copyIdentityVerifications(ICreditDebitTenderLineItem newLine,
      ICreditDebitTenderLineItem oldLine) {

    List<IIdentityVerification> newVerifications = new LinkedList<IIdentityVerification>();

    for (IIdentityVerification oldVeri : oldLine.getIdentityVerifications()) {
      IIdentityVerification newVeri = createIdentityVerification();
      newVeri.setIdNumber(oldVeri.getIdNumber());
      newVeri.setIdTypeCode(oldVeri.getIdTypeCode());
      newVeri.setIssuingAuthority(oldVeri.getIssuingAuthority());
      newVerifications.add(newVeri);
    }

    newLine.setIdentityVerifications(newVerifications);
  }

  /**
   * Copy a signature between two line items.
   * @param argNewLine the destination line.
   * @param argOldLine the source line.
   */
  protected void copySignature(ICreditDebitTenderLineItem argNewLine, ICreditDebitTenderLineItem argOldLine) {
    ITenderSignature signature = argOldLine.getSignature();

    if (signature != null) {
      createTenderSignature(argNewLine, signature.getSignature());
    }
  }

  /**
   * Returns a DTX tender ID extracted from the specified tender model.
   * 
   * @param argTender the tender whose ID to extract
   * @return the DTX tender ID associated with <code>argTender</code>
   */
  protected TenderId createTenderId(ITender argTender) {
    return (TenderId) argTender.getObjectId();
  }

  /**
   * Returns a DTX tender ID generated from the specified string.
   * 
   * @param argTenderId a string identifying a tender
   * @return a DTX tender ID derived from <code>argTenderId</code>
   */
  protected TenderId createTenderId(String argTenderId) {
    TenderId tenderId = new TenderId();
    tenderId.setTenderId(argTenderId);

    return tenderId;
  }

  /**
   * Create and return a tender line item for the specified tender
   * 
   * @param argTender the tender to create a line item for
   * @param argClass class for line to create
   * @param argTenderStatus tender status
   * @return a tender line item with the specified tender
   */
  protected ITenderLineItem createTenderLineItem(ITender argTender,
      Class<? extends ITenderLineItem> argClass, TenderStatus argTenderStatus) {

    ITenderLineItem result = null;
    try {
      RetailTransactionLineItemId id = new RetailTransactionLineItemId();

      if (argClass == null) {
        argClass = ITenderLineItem.class;
      }

      result = DataFactory.createObject(id, argClass);
      result.setTender(argTender);
      result.setBeginDateTimestamp(DateUtils.getNewDate());
      result.setLineItemTypeCode(LineItemType.TENDER.getName());
      result.setTenderStatusCode(argTenderStatus.getName());
    }
    catch (Exception ex) {
      _logger.warn("Exception caught trying to set create tender line item", ex);
    }

    return result;
  }

  /**
   * Create a tender signature and add it to a line item.
   * @param argLine the line item to receive the signature.
   * @param argSignatureData the raw signature data.
   * @return the created tender signature.
   */
  protected ITenderSignature createTenderSignature(IRetailTransactionLineItem argLine, String argSignatureData) {
    final ITenderSignature sig;

    if (argLine.getSignature() != null) {
      sig = argLine.getSignature();
    }
    else {
      // create an ID object for a signature on the passed in tender line
      TenderSignatureId id = getTenderSignatureIdForLine(argLine);
      sig = DataFactory.createObject(id, ITenderSignature.class);
    }

    sig.setSignature(argSignatureData);
    argLine.setSignature(sig);
    return sig;
  }

  /**
   * Create the voucher history data access object based on the retail transaction line item and
   * voucher serial number and voucher type
   * 
   * @param argLineItem the voucher line item
   * @param argVoided flag indicating if this voucher line item was part of a voided transaction.
   */
  protected void createVoucherHistory(IVoucherLineItem argLineItem, boolean argVoided) {
    final String voucherType = argLineItem.getVoucherTypeCode();
    final String voucherSerial = argLineItem.getSerialNumber();

    if ((voucherSerial == null) || (voucherType == null)) {
      _logger.warn("There is no serial number or voucher type for this voucher. "
          + "No voucher history will be created for this voucher");
      return;
    }

    VoucherHistoryId voucherId = new VoucherHistoryId();
    voucherId.setSerialNumber(voucherSerial);
    voucherId.setVoucherTypeCode(voucherType);
    long seq = SequenceFactory.getNextLongValue(SEQ_TYPE_VOUCHER_HISTORY_SEQ);
    voucherId.setHistorySequence(seq);
    IVoucherHistory newRecord = DataFactory.createObject(voucherId, IVoucherHistory.class);
    newRecord.setRetailTransactionLineItem(argLineItem);
    String activityCode =
        (argVoided ? "VOID_" + argLineItem.getActivityCode() : argLineItem.getActivityCode());
    newRecord.setActivityCode(activityCode);
    BigDecimal amount = argLineItem.getAmount();

    if (amount != null) {
      newRecord.setAmount(amount.abs());
    }

    try {
      DataFactory.makePersistent(newRecord);
    }
    catch (dtv.data2.access.exception.PersistenceException ex) {
      _logger.error("Exception caught trying to persist voucher history data for " + voucherType + ":"
          + voucherSerial + ".", ex);
      throw new OpExecutionException(ex);
    }
  }

  /**
   * Get customer account plans that match criteria.
   * 
   * @param argAllPlans the plans to search
   * @param argCustAccountCode the account code for which to search
   */
  protected void getCustomerAccountPlans(SortedMap<String, ICustomerAccountPlan> argAllPlans,
      String argCustAccountCode) {

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("argCustAccountCode", argCustAccountCode);
    params.put("argTargetDate", StoreCalendar.getBusinessDateTimeStamp());

    List<ICustomerAccountPlan> results = DataFactory.getObjectByQueryNoThrow(CREDIT_PLANS, params);

    for (ICustomerAccountPlan plan : results) {
      argAllPlans.put(plan.getPlanId(), plan);
    }
  }

  /**
   * Gets the credit card events and any chained alternate credit card events from
   * <code>argInputEvent</code>.
   * 
   * @param argInputEvent the event to examine
   * @return all credit card events found before the chain of alternates was broken
   */
  protected IHardwareInputEvent[] getInputEventChain(IHardwareInputEvent argInputEvent, Class<?> argClass) {
    if (!IHardwareInputEvent.class.isAssignableFrom(argClass)) {
      throw new ClassCastException("cannot assign " + argClass + " to " + IHardwareInputEvent.class);
    }

    List<IHardwareInputEvent> l = new ArrayList<IHardwareInputEvent>();
    IXstEvent evt = argInputEvent;

    while ((evt instanceof IHardwareInputEvent) && argClass.isAssignableFrom(evt.getClass())) {
      l.add((IHardwareInputEvent) evt);
      evt = ((IHardwareInputEvent) evt).getAlternateEvent();
    }

    IHardwareInputEvent[] results = (IHardwareInputEvent[]) Array.newInstance(argClass, l.size());
    results = l.toArray(results);
    return results;
  }

  /**
   * Get the min denomination amount by tender.
   * @param argTender the tender needed rounding.
   * @return the calculated amount.
   */
  protected BigDecimal getMinDenominationByTender(ITender argTender) {
    ITender result = getTender((TenderId) argTender.getObjectId());
    return (result != null) ? result.getMinimumDenominationAmount() : BigDecimal.ONE;
  }

  /**
   * Returns the tender for the specified ID.
   * 
   * @param argTenderId the ID of the tender to return
   * @return the tender uniquely identified by <code>argTenderId</code> if one exists;
   * <code>null</code> otherwise
   */
  protected ITender getTender(TenderId argTenderId) {
    try {
      return DataFactory.getObjectById(argTenderId);
    }
    catch (ObjectNotFoundException ex) {
      _logger.debug("No tender exists for ID [" + argTenderId + "]", ex);
      return null;
    }
  }

  /**
   * Get a tender signature ID for a line item.
   * @param argLine the line item for which to create an ID.
   * @return an ID object containing the new ID.
   */
  protected TenderSignatureId getTenderSignatureIdForLine(IRetailTransactionLineItem argLine) {
    TenderSignatureId id = new TenderSignatureId();

    id.setOrganizationId(new Long(argLine.getOrganizationId()));
    id.setRetailLocationId(new Long(argLine.getRetailLocationId()));
    id.setBusinessDate(argLine.getBusinessDate());
    id.setWorkstationId(new Long(argLine.getWorkstationId()));
    id.setTransactionSequence(new Long(argLine.getTransactionSequence()));
    id.setRetailTransactionLineItemSequence(new Integer(argLine.getRetailTransactionLineItemSequence()));

    return id;
  }

  /**
   * Indicates whether the specified tender is available for use in at least one retail context.
   * 
   * @param argTender the reference tender
   * @return <code>true</code> if <code>argTender</code> is available in at least one retail
   * context; <code>false</code> otherwise
   */
  protected boolean isAvailableTender(ITender argTender) {
    return !argTender.getTenderAvailabilityCodes().isEmpty();
  }

  /**
   * Indicates whether the specified tender is a currency tender.
   * 
   * @param argTender the tender to check
   * @param argIncludeTravCheck <code>true</code> if travelers checks should be treated as a
   * currency tender; <code>false</code> if only paper currency should be treated as such
   * @return <code>true</code> if <code>argTender</code> is a currency tender; <code>false</code>
   * otherwise
   */
  protected boolean isCurrencyTender(ITender argTender, boolean argIncludeTravCheck) {
    return TenderCategory.CURRENCY.matches(argTender)
        || (argIncludeTravCheck && TenderCategory.TRAVELERS_CHECK.matches(argTender));
  }

  /**
   * Set the input event on a tender line.
   * @param argTenderLine the tender line on which to set the event.
   * @param argEvent the event to set.
   * @return the passed in tender line.
   */
  protected ITenderLineItem setInputObject(ITenderLineItem argTenderLine, IXstEvent argEvent) {
    if (argEvent instanceof IHardwareInputEvent) {
      argTenderLine.setInputEvent(argEvent);
    }
    return argTenderLine;
  }

  /**
   * Nested class that wraps an immutable <code>Object</code> array.
   */
  public static class CallingInformation {
    private final Object[] obj_;

    /**
     * Constructor
     * @param o the info
     */
    public CallingInformation(Object... o) {
      obj_ = o;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
      if ((obj_ == null) || (obj_.length == 0)) {
        return "null";
      }

      for (Object o : obj_) {
        if (o instanceof IHasSourceDescription) {
          return ((IHasSourceDescription) o).getSourceDescription();
        }
      }

      return obj_[obj_.length - 1].toString();
    }
  }
}
