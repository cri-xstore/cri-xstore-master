//$Id: ResultStore.java 730 2013-06-24 19:45:19Z dtvdomain\bli $
package dtv.pricing2;

class ResultStore<I, D> {
  protected final int _dealCount, _itemCount, _maxMatches;

  /**
   * Best dealset. This is the storage for the description of the combination of deals which give
   * the best deal during testing.
   */
  protected final PricingDeal<D>[] _bestDealSet;

  /**
   * Best dealset iteration count. This is the storage for the descriptions of the optimal number of
   * times to apply each deal during testing.
   */
  protected final int[] _bestDealIterationCount;

  /**
   * Store the item-applications used by each deal. This is used to tell the external implementation
   * to which items a deal applied, and to what quantity of the item.
   * <tt>[{@link #dealCount}][{@link #itemCount}][{@link #maxMatches}]
   */
  protected final long[][][] _applicationLevels;

  /**
   * Store the price changes that the items got at in each level. This can be used to determine
   * which deals applied how much of a discount to which items.
   */
  protected final long[][] _priceDeltas;

  /**
   * Store the subtotal differences that were applicable at each level.
   */
  protected final long[] _subDiff;

  /**
   * Total discount applied by a deal. (useful for blind deal application.)
   */
  protected final long[] _dealAmountLevels;

  /**
   * This is the best deal subtotal found thus far.
   */
  protected long _bestDealAmount;

  /**
   * This is the best deferred amount (bouncebacks/rebates) found thus far.
   */
  protected long bestDeferredAmount;
  protected long[] deferredAmounts;
  

  protected final long[] _latestItemPrice;


  @SuppressWarnings("unchecked")
  public ResultStore(int argDealCount, int argItemCount, int argMaxMatches) {
    _bestDealSet = new PricingDeal[argDealCount];
    _bestDealIterationCount = new int[argDealCount];
    _bestDealAmount = Long.MAX_VALUE;
    _applicationLevels = new long[argDealCount][argItemCount][argMaxMatches];
    _priceDeltas = new long[argDealCount][argItemCount];
    _subDiff = new long[argDealCount];
    _dealAmountLevels = new long[argDealCount];
    deferredAmounts = new long[argDealCount];
    this._dealCount = argDealCount;
    this._itemCount = argItemCount;
    this._maxMatches = argMaxMatches;
    _latestItemPrice = new long[argItemCount];
  }

  public void fillInfo(boolean[] dealGroup, ResultStore<I, D> right) {
    _bestDealAmount = right._bestDealAmount;
    bestDeferredAmount = right.bestDeferredAmount;

    for (int d = 0; d < _dealCount; d++ ) {
      if (dealGroup[d]) {
        // if (right.bestDealIterationCount[d] > 0) {
        _bestDealSet[d] = right._bestDealSet[d];
        _bestDealIterationCount[d] = right._bestDealIterationCount[d];
        _subDiff[d] = right._subDiff[d];
        deferredAmounts[d] = right.deferredAmounts[d];
        System.arraycopy(right._applicationLevels[d], 0, _applicationLevels[d], 0, _itemCount);
        System.arraycopy(right._priceDeltas[d], 0, _priceDeltas[d], 0, _itemCount);
        System.arraycopy(right._dealAmountLevels, 0, _dealAmountLevels, 0, _dealCount);
      }
    }
  }
}
