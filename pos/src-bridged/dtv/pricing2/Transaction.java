//$Id: Transaction.java 777 2014-03-27 02:15:29Z suz.jliu $
package dtv.pricing2;

import java.io.Serializable;
import java.util.List;

/**
 * A transaction from the perspective of the deal pricing engine. This should be maintained with the
 * native transaction type, since it will contain the properly stateful information regarding that
 * transaction.<br>
 * This transaction is not inherently threadsafe.
 * 
 * @author Greyson Fischer<gfischer@datavantagecorp.com>
 * @version $Revision: 777 $
 * @param <L> Native item type
 * @param <D> Native deal type
 * @param <T> Native transaction type.
 */
public interface Transaction<L, D, T>
    extends Serializable {
  /**
   * Add a line item to the transaction. This method is to be called in tandem with the addition of
   * a line item to the native transaction, depending on the implementation used, this may do some
   * minor front-loading of calculation; as a result, calling this method serially with the entire
   * contents of the transaction at each item addition may cause deal-time calculation to be
   * undually slow. <br>
   * 
   * @param item Item to add to the transaction.
   */
  public void addItem(L item);

  /**
   * Adds a target to this transaction. A target provides access to deals which would otherwise be
   * excluded from the dealspace. Specifically, deals which declare targets will only be considered
   * members of this transaction's dealspace if a matching target has been identified via this
   * method.
   * 
   * @param argTarget the target to add to this transaction and identifying matching deals which
   * will subsequently be included in the dealspace
   */
  public void addTarget(String argTarget);

  public void addTransactionDeal(Object argDealKey, PricingDeal<D> argDeal);

  /**
   * Add a trigger to the transaction. Triggers are used to activate deals into the transaction
   * whose conditions are determinable only based on criteria which cannot be expressed by the
   * pricing engine without introducing inefficiency. This may be used for coupon activation,
   * loyalty membership, or any other trigger deemed appropriate by the integrator.<br>
   * In order to be useful, this trigger must have the same string value as the trigger for deals
   * which have been added to the dealspace.
   * 
   * @param trigger Name of the trigger to activate.
   */
  public void addTrigger(String trigger);

  /**
   * Removes all targets from this transaction. All targeted deals will subsequently be excluded
   * from membership in the corresponding dealspace.
   */
  public void clearTargets();

  public void debug(StringBuffer argSb);

  /**
   * Calculate the optimal deal combination for the transaction in its current state. The collection
   * returned contains pairs containing the native deals to be applied, and the number of times
   * those deals should be applied. Note that depending on the implementation used, some of the
   * deals may indicate <tt>0</tt> applications; it is important to ensure that the count of
   * applications is not ignored.
   * 
   * @return Collection of native deals, and number of times they should be applied.
   */
  public Result<L, D> getBestDeals();

  /**
   * Retrieve native transaction. In addition to shadowing the transaction, the pricing engine
   * maintains its connection to the native transaction itself.
   * 
   * @return Native Transaction type.
   */
  public T getNativeTransaction();

  /**
   * Returns a list of the deals that are active and may apply for this transaction, if the
   * necessary criteria are met. It is important to note that this method returns all of the deals
   * that <b>may</b> apply, not the deals that have been applied.
   * @return a list of the deals that are may apply for this transaction
   */
  public List<PricingDeal<D>> getPossibleDeals();

  public Result<L, D> getSubstituteDeals();

  public Result<L, D> getThresholdDeals();

  public boolean isDirty();

  /**
   * Reestablish lost link to the pricing engine. In order to avoid huge cascades of time-sensitive
   * objects following a native transaction through serialization, this method should be used to
   * re-establish the connection between a serialized transaction's shadow (the implementor of this
   * interface) and the engine as appropriate.
   * 
   * @param adapter The pricing adapter with which this transaction should be paired.
   */
  public void reestablishPricingAdapter(PricingAdapter<L, D, T> adapter);

  public void refreshItem(L item);

  /**
   * Remove a line item from the transaction. This method is to be callin tandem with the removal of
   * a line item from the native transaction, depending on the implementation used, this may do some
   * minor front-loading of calculation; as a result, calling this method serially with the entire
   * contents of the transaction at each change may cause deal-time calculation to be undually slow.<br>
   * 
   * @param item Item to remove from the transaction.
   */
  public void removeItem(L item);

  /**
   * Removes a target from this transaction. Deals declaring matching targets will no longer be
   * considered members of the corresponding dealspace.
   * 
   * @param argTarget the target to remove from this transaction and identifying matching deals
   * which will subsequently be excluded from the dealspace
   */
  public void removeTarget(String argTarget);

  public void removeTransactionDeal(Object argDealKey);

  /**
   * Remove a trigger from the transaction. This will have the reverse effect on the dealspace that
   * {@link #addTrigger(String)} effects.
   * 
   * @param trigger Name of the trigger to de-activate.
   */
  public void removeTrigger(String trigger);
  
  /**
   * Get all conditional deals for item.
   * 
   * @return
   */
  public PricingDeal<D>[] getEligibleConditionalDeals();
}
