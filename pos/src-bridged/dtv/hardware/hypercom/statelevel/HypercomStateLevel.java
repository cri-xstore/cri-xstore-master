//$Id: HypercomStateLevel.java 216 2012-08-06 14:51:10Z dtvdomain\aabdul $
package dtv.hardware.hypercom.statelevel;

import dtv.hardware.hypercom.forms.*;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2009 MICROS Retail
 * 
 * @author czhou
 * @created Jul 9, 2009
 * @version $Revision: 216 $
 */
public class HypercomStateLevel<F extends IDeviceForm>
    extends dtv.hardware.statelevel.StateLevel {

  private final F form_;

  protected HypercomStateLevel(String argName, int argLevel) {
    this(argName, argLevel, false, null);
  }

  protected HypercomStateLevel(String argName, int argLevel, boolean argBreakpoint, F argForm) {
    super(argName, argLevel, argBreakpoint);
    form_ = argForm;
  }

  protected F getForm() {
    return form_;
  }

  public static final HypercomStateLevel<IdleForm> IDLE = new HypercomStateLevel<IdleForm>("IDLE", 100, true,
      new IdleForm());

  public static final HypercomStateLevel<TransactionInfoForm> ITEMS = new ItemsStateLevel("ITEMS", 201);

  public static final HypercomStateLevel<SignatureCaptureForm> CAPTURING_SIGNATURE =
      new HypercomStateLevel<SignatureCaptureForm>("CAPTURING_SIGNATURE", 301, false,
          new SignatureCaptureForm());

  public static final HypercomStateLevel<WaitForm> WAIT = new HypercomStateLevel<WaitForm>("WAIT", 303,
      false, new WaitForm());

  public static final HypercomStateLevel<PinEntryForm> CAPTURING_PIN = new PinEntryStateLevel(
      "CAPTURING_PIN", 501);

  public static final HypercomStateLevel<CreditDebitForm> CREDIT_DEBIT =
      new HypercomStateLevel<CreditDebitForm>("CREDIT_DEBIT", 503, false, new CreditDebitForm());

  /** {@inheritDoc} */
  @Override
  public final boolean enterState(dtv.hardware.statelevel.IStateLevelCallback argCallback,
      dtv.hardware.statelevel.StateLevel argPrevious) {
    IDeviceForm form = getForm();
    if (form != null) {
      form.setCallback((IHypercomStateLevelCallback) argCallback);
    }
    if (doEnterState(argCallback, argPrevious)) {
      return true;
    }
    else {
      return false;
    }
  }

  /** {@inheritDoc} */
  @Override
  public final boolean exitingState(dtv.hardware.statelevel.IStateLevelCallback argCallback,
      dtv.hardware.statelevel.StateLevel argNext) {
    IDeviceForm form = getForm();
    if (form != null) {
      form.exiting((IHypercomStateLevelCallback) argCallback, argNext);
    }
    return doExitingState(argCallback, argNext);
  }

  protected boolean doEnterState(dtv.hardware.statelevel.IStateLevelCallback argCallback,
      dtv.hardware.statelevel.StateLevel argPrevious) {
    IDeviceForm form = getForm();
    if (form != null) {
      ((IHypercomStateLevelCallback) argCallback).setCurrentForm(form, null, argPrevious);
    }
    return true;
  }

  protected boolean doExitingState(dtv.hardware.statelevel.IStateLevelCallback argCallback,
      dtv.hardware.statelevel.StateLevel argNext) {
    return true;
  }
}
