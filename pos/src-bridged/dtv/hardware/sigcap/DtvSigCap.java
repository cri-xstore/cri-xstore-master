//$Id: DtvSigCap.java 216 2012-08-06 14:51:10Z dtvdomain\aabdul $
package dtv.hardware.sigcap;

import java.awt.Point;
import java.util.Map;

import org.apache.log4j.Logger;

import jpos.JposException;
import jpos.SignatureCapture;
import jpos.events.DataEvent;
import jpos.events.ErrorEvent;

import dtv.event.EventDescriptor;
import dtv.event.Eventor;
import dtv.event.eventor.DefaultEventor;
import dtv.hardware.*;
import dtv.hardware.events.SignatureEvent;
import dtv.hardware.statelevel.IStateLevelCallback;
import dtv.hardware.types.HardwareFamilyType;
import dtv.hardware.types.HardwareType;
import dtv.i18n.*;
import dtv.pos.iframework.event.IXstEvent;
import dtv.util.TypeSafeMapKey;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * Implementation of a signature capture device.<br>
 * Copyright (c) 2003 MICROS-Retail
 * 
 * @author Doug Berkland
 * @created March 20, 2003
 * @version $Revision: 216 $
 */
public class DtvSigCap
    extends AbstractDtvJposDevice<IStateLevelCallback>
    implements IDtvJposSigCap {

  private static final Logger logger_ = Logger.getLogger(DtvSigCap.class);
  private static final String DEFAULT_SIGCAP_TENDER_KEY = "_sigcapTenderText";
  private static final int DEFAULT_MINIMUM_POINT_RESOLUTION = 6;

  private static Point[] getPoints(jpos.SignatureCapture argSigCap) {
    try {
      return argSigCap.getPointArray();
    }
    catch (JposException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
      return new Point[0];
    }
  }

  protected final SignatureCapture control_;

  private final Eventor events_ = new DefaultEventor(new EventDescriptor(SIGCAP_EVENTS, this));

  private String sigcapTenderKey_ = DEFAULT_SIGCAP_TENDER_KEY;
  private boolean flipHorizontal_ = false;
  private boolean flipVertical_ = false;
  private Integer signatureMessageDirectIOCommand_ = null;
  private int minimumPointResolution_ = DEFAULT_MINIMUM_POINT_RESOLUTION;

  /**
   * constructor for the DtvSigCap object
   * 
   * @param argParent parent of this device
   * @param argType the use type of this device
   */
  public DtvSigCap(IHardwareMgr argParent, HardwareType<?> argType) {
    super(argParent, argType, "signature capture");
    control_ = new SignatureCapture();
  }

  /** {@inheritDoc} */
  @Override
  public void beginCapture(Map<TypeSafeMapKey<?>, ?> argParameters)
      throws SigCapException {

    try {
      getJposControl().beginCapture("signature");
      displaySignatureMessage(argParameters);
    }
    catch (Throwable t) {
      throw SigCapException.toSigCapException(t);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void dataOccurred(DataEvent argDataEvent) {
    Point[] points = getPoints(getJposControl());
    if ((points == null) || (points.length < minimumPointResolution_)) {
      errorOccurred(null);
    }
    else {
      Signature data = new Signature(true, getType(), points, flipHorizontal_, flipVertical_);
      final IXstEvent event = new SignatureEvent(data);
      processInputNotificationWorker(new Runnable() {
        @Override
        public void run() {
          getEventor().post(IDtvInputDevice.EVENT_ROUTED_EVENT, event);
        }
      });
      logger_.info(data);
      getEventor().post(IDtvInputDevice.INPUT_COMPLETE_EVENT, data);
      enableDataEvents();
    }
  }

  /** {@inheritDoc} */
  @Override
  public void endCapture(boolean argWait) {
    try {
      getJposControl().endCapture();
    }
    catch (Throwable t) {
      JposException ex = toJposException(t);
      hardwareMgr_.logJposException("problem ending signature capture", this, ex);
    }
    getEventor().post(IDtvInputDevice.INPUT_CANCELED_EVENT);
  }

  /** {@inheritDoc} */
  @Override
  public void errorOccurred(ErrorEvent argErrorEvent) {
    final Signature data = Signature.forError(getType());
    if (argErrorEvent != null) {
      hardwareMgr_.handleErrorEvent(data, this, argErrorEvent);
    }
    processInputNotificationWorker(new Runnable() {
      @Override
      public void run() {
        getEventor().post(IDtvInputDevice.EVENT_ROUTED_EVENT, new SignatureEvent(data));
      }
    });
    getEventor().post(IDtvInputDevice.INPUT_CANCELED_EVENT, data);
    enableDataEvents();
  }

  /** {@inheritDoc} */
  @Override
  public Eventor getEventor() {
    return events_;
  }

  public OutputContextType getOutputContextType() {
    return OutputContextType.POLE_DISPLAY;
  }

  /**
   * @param argParameters
   * @throws JposException
   */
  protected void displaySignatureMessage(Map<TypeSafeMapKey<?>, ?> argParameters)
      throws JposException {

    if (signatureMessageDirectIOCommand_ != null) {
      final String msg;
      IFormattable displayMessage_ = FORMATTABLE_MESSAGE_KEY.retrieve(argParameters);
      if (displayMessage_ != null) {
        msg = displayMessage_.toString(getOutputContextType());
      }
      else {
        ICreditDebitTenderLineItem line = CREDIT_DEBIT_LINE_KEY.retrieve(argParameters);
        if (line == null) {
          msg = null;
        }
        else {
          FormattableFactory FF = FormattableFactory.getInstance();
          msg =
              HardwareI18nHelper.getInstance().translate(getOutputContextType(), sigcapTenderKey_,
                  FF.getSimpleFormattable(line.getCustomerName()),
                  FF.getSimpleFormattable(line.getAmount(), FormatterType.MONEY));
        }
      }
      if (msg != null) {
        getJposControl().directIO(signatureMessageDirectIOCommand_, new int[0], msg);
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  protected HardwareFamilyType<?> getHardwareFamilyType() {
    return HardwareFamilyType.SIG_CAP;
  }

  /** {@inheritDoc} */
  @Override
  protected SignatureCapture getJposControl() {
    return control_;
  }

  /** {@inheritDoc} */
  @Override
  protected void onEnable() {
    JposException ex;
    ex = enableDataEvents();
    if (ex != null) {
      throw new RuntimeException("problem enabling data events", ex);
    }

    ex = clearInput();
    if (ex != null) {
      throw new RuntimeException("problem clearing input", ex);
    }
  }

  /** {@inheritDoc} */
  @Override
  protected void onOpen() {
    flipHorizontal_ = getProperty("dtvFlipHorizontal", false, false);
    flipVertical_ = getProperty("dtvFlipVertical", false, false);
    signatureMessageDirectIOCommand_ =
        getProperty("dtvSignatureMessageDirectIOCommand", (Integer) null, false);
    sigcapTenderKey_ = getProperty("dtvSigcapTenderKey", DEFAULT_SIGCAP_TENDER_KEY, false);
    minimumPointResolution_ =
        getProperty("dtvMinimumPointResolution", DEFAULT_MINIMUM_POINT_RESOLUTION, false);
  }

  /**
   * Clears all device input that has been buffered.
   * 
   * @param argMessages an object to append any error messages to
   * @return <tt>false</tt> if not able to clear input
   */
  private JposException clearInput() {
    try {
      getJposControl().clearInput();
      return null;
    }
    catch (Throwable t) {
      JposException ex = toJposException(t);
      appendMessage("_errorClearingInput", ex);
      hardwareMgr_.logJposException("problem clearing signature capture input", this, ex);
      return ex;
    }
  }

  /**
   * enable data events from the signature capture
   * 
   * @param argMessages an object to append any error messages to
   * @return <code>null</code> if success
   */
  private JposException enableDataEvents() {
    try {
      getJposControl().setDataEventEnabled(true);
      return null;
    }
    catch (Throwable t) {
      JposException ex = toJposException(t);
      appendMessage("_errorEnablingDataEvents", ex);
      hardwareMgr_.logJposException(
          "problem enabling signature capture events (" + getType().getName() + ")", this, ex);
      return ex;
    }
  }
}
