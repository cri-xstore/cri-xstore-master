//$Id: TenderAuthHelper.java 107 2012-07-10 15:30:25Z dtvdomain\aabdul $
package dtv.tenderauth;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

import org.apache.log4j.Logger;

import dtv.hardware.inputrules.CheckDigitRule;
import dtv.pos.iframework.hardware.IHardwareType;
import dtv.pos.iframework.hardware.IInput;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.tenderauth.binfile.BinFile;
import dtv.tenderauth.binfile.BinFileEntry;
import dtv.tenderauth.event.IAuthResponse;
import dtv.util.CompositeObject;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.tnd.ITenderUserSettings;
import dtv.xst.dao.trl.IAuthorizableLineItem;

/**
 * Helper functions for tender authorization.<br>
 * Copyright (c) 2004 Datavantage Corporation<br>
 * src-bridged: To check if the amount in the tender line item is null during Floor and ceiling
 * limit check. The amount in tender line item can be null in case of CASH OUT request
 * @author dberkland
 * @created February 16, 2004
 * @version $Revision: 107 $
 */
public class TenderAuthHelper
    implements ITenderAuthHelper {

  private static BigInteger DTV_KEY = BigInteger.valueOf(0x555);

  private static final Logger logger_ = Logger.getLogger(TenderAuthHelper.class);

  /** The property in <code>System.getProperties()</code> to check for an override to this class. */
  public static final String SYSTEM_PROPERTY = TenderAuthHelper.class.getName();
  private static ITenderAuthHelper INSTANCE;
  static {
    // Check the system configured factory to use...
    String className = System.getProperty(SYSTEM_PROPERTY);
    try {
      INSTANCE = (dtv.tenderauth.ITenderAuthHelper) Class.forName(className).newInstance();
    }
    catch (Exception ex) {
      INSTANCE = new dtv.tenderauth.TenderAuthHelper();
    }
  }

  /**
   * Gets the single instance of a class implementing {@link dtv.tenderauth.ITenderAuthHelper}.
   * {@link #SYSTEM_PROPERTY} is checked for an override to this class.
   * 
   * @return the single instance of an {@link dtv.tenderauth.ITenderAuthHelper}
   */
  public static dtv.tenderauth.ITenderAuthHelper getInstance() {
    return INSTANCE;
  }

  /** constructor for the TenderAuthHelper object */
  protected TenderAuthHelper() {
    // singleton -- prevent instantiation
  }

  /** {@inheritDoc} */
  @Override
  public boolean areCeilingLimitsExceeded(IAuthorizableLineItem argLineItem, String argTenderId,
      TenderUsageCodeType argUsage, boolean argOffline) {

    if (argUsage == null) {
      return false;
    }
    ITenderUserSettings usageSettings = getTenderUserSettings(argTenderId, argUsage);
    if (usageSettings == null) {
      return false;
    }
    final BigDecimal ceiling;
    final BigDecimal tenderAmount = argLineItem.getAmount();
    if (argOffline) {
      ceiling = usageSettings.getOfflineCeilingApprovalAmount();
    }
    else {
      ceiling = usageSettings.getOnlineCeilingApprovalAmount();
    }
    // see if a ceiling limit is defined
    if (ceiling == null || tenderAmount == null) {
      return false;
    }
    // see if the ceiling is below the amount
    if (ceiling.compareTo(tenderAmount.abs()) < 0) {
      return true;
    }
    else {
      // is at or below the ceiling
      return false;
    }
  }

  /** {@inheritDoc} */
  @Override
  public boolean areFloorLimitsMet(IAuthorizableLineItem argLineItem, String argTenderId,
      TenderUsageCodeType argUsage, boolean argOffline) {

    if (argUsage == null) {
      return false;
    }
    ITenderUserSettings usageSettings = getTenderUserSettings(argTenderId, argUsage);
    if (usageSettings == null) {
      return false;
    }
    final BigDecimal floor;
    final BigDecimal tenderAmount = argLineItem.getAmount();
    if (argOffline) {
      floor = usageSettings.getOfflineFloorApprovalAmount();
    }
    else {
      floor = usageSettings.getOnlineFloorApprovalAmount();
    }
    // see if a floor limit is defined
    if (floor == null || tenderAmount == null) {
      return false;
    }
    // see if the floor is below the amount
    if (floor.compareTo(tenderAmount.abs()) < 0) {
      return false;
    }
    // passed checks... floor limits are met
    return true;
  }

  /** {@inheritDoc} */
  @Override
  public CompositeObject.TwoPiece<String, BigDecimal> decodeAuthNumber(String argEnteredAuthCode) {
    String authCodeResult = null;
    BigDecimal amount = null;
    try {
      // Tom -> This code will lazily catch exceptions instead of checking
      // string length ahead of time.
      String block1 = argEnteredAuthCode.substring(0, 8);
      String block2 = argEnteredAuthCode.substring(8, 16);
      String block3 = argEnteredAuthCode.substring(16);
      int b1 = new BigInteger(block1).xor(DTV_KEY).intValue();
      int b2 = new BigInteger(block2).xor(DTV_KEY).intValue();
      double b3 = new BigInteger(block3).xor(DTV_KEY).intValue() / 100.0;

      String authCode = String.valueOf(b1) + String.valueOf(b2);
      amount = new BigDecimal(b3).setScale(2, BigDecimal.ROUND_HALF_EVEN);

      // now perform mod 10 on the auth code
      CheckDigitRule rule = new CheckDigitRule(new Integer(10));
      boolean valid = rule.checkRule(new AuthCodeInput(authCode));
      // If the auth code passes the mod check, then assign to results!
      if (valid) {
        authCodeResult = authCode;
      }
    }
    catch (Exception ex) {
      logger_.info("Exception caught decoding manual auth code " + argEnteredAuthCode, ex);
    }

    return CompositeObject.make(authCodeResult, amount);
  }

  /** {@inheritDoc} */
  @Override
  public IAuthProcess getAuthProcess(ITender argTender) {
    return getAuthProcess(argTender.getAuthMethodCode());
  }

  /** {@inheritDoc} */
  @Override
  public IAuthProcess getAuthProcess(String argAuthMethodCode) {
    return AuthFactory.getInstance().getAuthProcess(argAuthMethodCode);
  }

  /** {@inheritDoc} */
  @Override
  public BinFileEntry getBinFileEntry(String argBinFile, String argAccountNumber)
      throws IOException {

    return BinFile.get(argBinFile).getEntry(argAccountNumber);
  }

  /** {@inheritDoc} */
  @Override
  public boolean matchesBinFile(String argBinFile, String argAccountNumber)
      throws IOException {

    return getBinFileEntry(argBinFile, argAccountNumber) != null;
  }

  /** {@inheritDoc} */
  @Override
  public boolean needsMoreInfo(IAuthResponse argResponse) {
    AuthFailedActionType[] actions = argResponse.getAvailableActions();
    for (AuthFailedActionType action : actions) {
      if (action == AuthFailedActionType.MORE_INFO_REQUIRED) {
        return true;
      }
    }
    return false;
  }

  protected ITenderUserSettings getTenderUserSettings(String argTenderId, TenderUsageCodeType argUsage) {
    logger_.warn("returning null tender user setting");
    return null;
  }

  private static class AuthCodeInput
      implements IInput {
    private static final long serialVersionUID = 1L;
    private final String authCode_;

    AuthCodeInput(String argAuthCode) {
      authCode_ = argAuthCode;
    }

    /** {@inheritDoc} */
    @Override
    public Object getAdditionalInformation(String argInformationKey) {
      return null;
    }

    /** {@inheritDoc} */
    @Override
    public String getData() {
      return authCode_;
    }

    /** {@inheritDoc} */
    @Override
    public IHardwareType<?> getSourceType() {
      return null;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isError() {
      return false;
    }
  }

}
