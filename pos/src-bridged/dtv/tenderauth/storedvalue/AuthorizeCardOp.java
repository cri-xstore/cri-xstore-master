//$Id: AuthorizeCardOp.java 1167 2017-09-26 16:41:46Z johgaug $
package dtv.tenderauth.storedvalue;

import static dtv.pos.iframework.type.VoucherActivityCodeType.*;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;

import cri.ajb.fipay.reqeust.ISupportsManualEntry;

import dtv.data2.access.DaoUtils;
import dtv.i18n.*;
import dtv.pos.common.ConfigurationMgr;
import dtv.pos.common.PromptKey;
import dtv.pos.framework.op.OpState;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.iframework.type.*;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.pos.pricing.discount.IDiscountCmd;
import dtv.pos.register.*;
import dtv.pos.register.type.LineItemType;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.TenderHelper;
import dtv.pos.tender.voucher.IVoucherCmd;
import dtv.pos.tender.voucher.config.ActivityConfig;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.op.AbstractAuthorizeOp;
import dtv.tenderauth.op.IAuthorizationCmd;
import dtv.util.*;
import dtv.util.config.IConfigObject;
import dtv.xst.dao.dsc.IDiscount;
import dtv.xst.dao.itm.INonPhysicalItem;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.tnd.TenderStatus;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.IAuthorizableTenderLineItem;
import dtv.xst.dao.ttr.IVoucherTenderLineItem;

/**
 * Operation that handles all gift card authorizations except voids.<br>
 * <br>
 * Copyright (c) 2004 Datavantage Corporation
 *
 * @author jdrummond
 * @created February 20, 2004
 * @version $Revision: 1167 $
 * @todo This op should be refactored so that it does not repeatedly check and cast the item type so
 * much - Jeff Sheldon 03/01/04
 */
public class AuthorizeCardOp
  extends AbstractAuthorizeOp {
  private static final long serialVersionUID = 1L;

  private static final ITenderAuthHelper AUTH_HELPER = TenderAuthHelper.getInstance();
  private static final Logger logger_ = Logger.getLogger(AuthorizeCardOp.class);
  private static final IAuthFactory AUTH_FACTORY = AuthFactory.getInstance();
  protected static final FormattableFactory FF = FormattableFactory.getInstance();

  protected IPromptKey amountChangedPromptKey_ = PromptKey.valueOf("GIFT_CARD_CHANGE_AMT");

  protected final IOpState SHOWING_NEW_AMOUNT = new OpState(this, "SHOWING_NEW_AMOUNT");

  /**
   * Return true if this operation is applicable for the current state of the command (and system).
   *
   * @param argCmd - the current command
   * @return true if this operation is applicable for the current state of the command (and system).
   */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    IVoucherCmd vcmd = (IVoucherCmd) argCmd;
    IVoucherLineItem lineItem = vcmd.getVoucherLineItem();

    if (lineItem instanceof IVoucherTenderLineItem) {
      ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
      ITender tender = cmd.getTender();

      if (tender == null) {
        logger_.warn("null tender?!?!?!");
        return false;
      }
      if (cmd.getTenderLineItem() == null) {
        logger_.warn("null tender line item ?!?!?");
        return false;
      }
      if (!tender.getAuthRequired()) {
        return false;
      }
    }
    else if (!((lineItem instanceof IVoucherSaleLineItem) || (lineItem instanceof IVoucherDiscountLineItem))) {
      logger_.warn("unexpected line item " + ObjectUtils.getClassNameFromObject(lineItem));
      return false;
    }
    else {
      String authMethod = determineAuthMethodCode((IAuthorizationCmd) argCmd);
      if (authMethod == null) {
        return false;
      }
      if (lineItem instanceof IVoucherDiscountLineItem) {
        ISaleReturnLineItemCmd cmd = (ISaleReturnLineItemCmd) argCmd;
        ISaleReturnLineItem itemLine = cmd.getLineItem();
        if (itemLine != null) {
          if (itemLine.getReturn()) {
            return false;
          }
        }
      }
    }
    return true;
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("amountChangedPromptKey".equalsIgnoreCase(argName)) {
      amountChangedPromptKey_ = PromptKey.valueOf(argValue.toString());
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

  @Override
  protected IAuthProcess getAuthProcessor(IAuthorizationCmd argCmd) {
    String authMethodCode = determineAuthMethodCode(argCmd);
    return AUTH_FACTORY.getAuthProcess(authMethodCode);
  }

  @Override
  protected IAuthRequest getAuthRequest(IAuthorizationCmd argCmd) {
    IVoucherCmd cmd = (IVoucherCmd) argCmd;
    IVoucherLineItem lineItem = cmd.getVoucherLineItem();

    String serialNumber = cmd.getSerialNumber();
    if (serialNumber == null) {
      serialNumber = lineItem.getSerialNumber();
    }
    if ((serialNumber == null) && (lineItem != null)) {
      serialNumber = lineItem.getSerialNumber();
    }
    if (cmd.getAuthRequest() instanceof IStoredValueAuthRequest) {
      IStoredValueAuthRequest req = (IStoredValueAuthRequest) cmd.getAuthRequest();
      if (serialNumber.equals(req.getAccountId())) {
        IAuthResponse[] responses = req.getResponses();
        IAuthResponse lastResponse = responses[responses.length - 1];
        if (lastResponse.isSuccess()) {
          return req;
        }
      }
    }
    if (lineItem.getParentTransaction() == null) {
      throw new NullPointerException("parent transaction is null");
    }

    IStoredValueAuthRequest request = makeRequest(cmd);
    request.setAccountId(serialNumber);
    request.setAmount(getLineAmt(cmd).abs());
    request.setTransactionSequence(lineItem.getTransactionSequence());
    request.setCurrencyId(ConfigurationMgr.getCurrency().getCurrencyCode());
    request.setPIN(cmd.getPINNumber());

    return request;
  }

  protected IVoucherLineItem getVoucherLineItem(IVoucherCmd cmd) {
    return cmd.getVoucherLineItem();
  }

  @Override
  protected IOpResponse handleAuthResponse(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    if (argResponse.isSuccess()) {
      updateVoucher((IVoucherCmd) argCmd, argResponse);

      return handleNewAmount(argCmd, argResponse);
    }
    return super.handleAuthResponse(argCmd, argResponse);
  }

  @Override
  protected IOpResponse handleInitialState(IAuthorizationCmd argCmd) {
    IAuthProcess p = getAuthProcessor(argCmd);
    argCmd.setAuthProcess(p);

    IVoucherCmd cmd = (IVoucherCmd) argCmd;

    if (cmd.getAuthRequest() instanceof IStoredValueAuthRequest) {
      IStoredValueAuthRequest req = (IStoredValueAuthRequest) cmd.getAuthRequest();

      if ((cmd.getSerialNumber() != null) && cmd.getSerialNumber().equals(req.getAccountId())) {
        IAuthResponse[] responses = req.getResponses();
        IAuthResponse lastResponse = responses[responses.length - 1];
        if (lastResponse.isSuccess()) {
          argCmd.setAuthRequest(req);
          return handleAuthResponse(cmd, lastResponse);
        }
      }
    }
    IAuthRequest authRequest = getAuthRequest(argCmd);
    argCmd.setAuthRequest(authRequest);

    return handleProcess(argCmd, authRequest);
  }

  protected IOpResponse handleNewAmount(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    IVoucherCmd cmd = (IVoucherCmd) argCmd;
    IStoredValueAuthRequest request = (IStoredValueAuthRequest) argResponse.getRequest();

    IVoucherLineItem vli = cmd.getVoucherLineItem();
    BigDecimal requestedAmount = request.getAmount();
    BigDecimal authorizedAmount = argResponse.getAmount();

    String activityCode = "";

    if (authorizedAmount == null) {
      authorizedAmount = requestedAmount;
    }
    if (vli instanceof IVoucherTenderLineItem) {
      IVoucherTenderLineItem t = ((IVoucherTenderLineItem) vli);

      activityCode = t.getActivityCode();
      BigDecimal amt = authorizedAmount.abs();

      if (!t.getTenderStatusCode().equals(TenderStatus.TENDER.getName())) {
        amt = amt.negate();
      }
      t.setAmount(amt);

      if (ISSUED.toString().equals(t.getActivityCode())) {
        t.setFaceValueAmount(amt);
      }
    }
    else if (vli instanceof IVoucherSaleLineItem) {
      ((IPriceCmd) cmd).setNewPrice(authorizedAmount);

      activityCode = ((IVoucherSaleLineItem) vli).getActivityCode();
    }
    else if (vli instanceof IVoucherDiscountLineItem) {
      ((IVoucherDiscountLineItem) vli).setAmount(authorizedAmount);
      activityCode = ((IVoucherDiscountLineItem) vli).getActivityCode();

      if (cmd instanceof IDiscountCmd) {
        ((IDiscountCmd) cmd).setAmount(authorizedAmount);
      }
    }
    else {
      String message = "Unknown line type [" + ObjectUtils.getClassNameFromObject(vli) + "]";
      logger_.error(message);
      throw new RuntimeException(message);
    }

    if ((!NumberUtils.equivalent(authorizedAmount.abs(), requestedAmount.abs()))
      && (!NumberUtils.isZeroOrNull(requestedAmount))
      && (!activityCode.equalsIgnoreCase(CASHOUT.toString()))) {

      IFormattable requestedAmountFormattable = FF.getSimpleFormattable(requestedAmount, FormatterType.MONEY);
      IFormattable authorizedAmountFormattable =
        FF.getSimpleFormattable(authorizedAmount, FormatterType.MONEY);
      cmd.setOpState(SHOWING_NEW_AMOUNT);

      return HELPER.getPromptResponse(amountChangedPromptKey_, new IFormattable[] {
        requestedAmountFormattable, authorizedAmountFormattable});
    }
    else {
      return super.handleAuthResponse(argCmd, argResponse);
    }
  }

  @Override
  protected IOpResponse handlePostPrompt(IAuthorizationCmd argCmd, IXstEvent argEvent, IOpState argState) {
    if (argState == SHOWING_NEW_AMOUNT) {
      IAuthRequest req = argCmd.getAuthRequest();
      IAuthResponse[] responses = req.getResponses();
      IAuthResponse lastResponse = responses[responses.length - 1];

      return super.handleAuthResponse(argCmd, lastResponse);
    }
    else {
      return super.handlePostPrompt(argCmd, argEvent, argState);
    }
  }

  @Override
  protected IOpResponse handleSuccess(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    IFormattable msg = argResponse.getMessage();
    if (msg != null) {
      return HELPER.getPromptResponse(OpStatus.COMPLETE_HALT, PromptKey.valueOf("AUTH_SUCCESS_MESSAGE"), msg);
    }
    else {
      getAttributeModel(argCmd).setAttribute(AttributeKey.TENDER_INPUT_EVENT, null);
      return HELPER.completeResponse();
    }
  }

  @Override
  protected IOpResponse handleTrainingMode(IAuthorizationCmd argCmd) {
    return HELPER.completeResponse();
  }

  @Override
  protected IOpResponse handleVoid(IAuthorizationCmd argCmd) {
    IVoucherCmd cmd = (IVoucherCmd) argCmd;
    IVoucherLineItem voucherLine = cmd.getVoucherLineItem();

    voucherLine.setVoid(true);
    voucherLine.setVoucher(null);

    if (voucherLine instanceof IVoucherDiscountLineItem) {
      IPosTransaction tran = voucherLine.getParentTransaction();
      List<? extends IRetailTransactionLineItem> lines =
        tran.getLineItemsByTypeCode(LineItemType.ITEM.getName());

      for (IRetailTransactionLineItem retailTransactionLineItem : lines) {
        ISaleReturnLineItem line = (ISaleReturnLineItem) retailTransactionLineItem;

        for (IRetailPriceModifier mod : line.getRetailPriceModifiers()) {
          if (DaoUtils.equivalent(mod.getReasonLineItem(), voucherLine)) {
            mod.setVoid(true);
          }
        }
      }
    }
    return HELPER.silentErrorResponse();
  }

  @Override
  protected boolean isNeedingMoreInfo(IAuthResponse argResponse) {
    return AUTH_HELPER.needsMoreInfo(argResponse);
  }

  protected void updateVoucher(IVoucherCmd cmd, IAuthResponse response) {
    IVoucherLineItem voucherLineItem = getVoucherLineItem(cmd);

    if ((response.getBalance() != null) && (voucherLineItem != null)) {
      voucherLineItem.setUnspentBalanceAmount(response.getBalance());
    }
    IAuthorizableLineItem line = cmd.getVoucherLineItem();
    line.setAuthorizationCode(response.getAuthorizationCode());
  }

  private String determineAuthMethodCode(IAuthorizationCmd argCmd) {
    IVoucherCmd cmd = (IVoucherCmd) argCmd;

    final ITender tender;

    if (cmd.getVoucherLineItem() instanceof IVoucherSaleLineItem) {
      IVoucherSaleLineItem lineItem = (IVoucherSaleLineItem) cmd.getVoucherLineItem();
      return lineItem.getAuthorizationMethodCode();
    }
    else if (cmd.getVoucherLineItem() instanceof IVoucherDiscountLineItem) {
      IVoucherDiscountLineItem lineItem = (IVoucherDiscountLineItem) cmd.getVoucherLineItem();
      IDiscount discount = lineItem.getLineItemDiscount();
      if (discount != null) {
        TenderHelper TH = TenderHelper.getInstance();
        ActivityConfig ac =
          TH.getVoucherRootConfig().getActivityConfigForDiscountCode(discount.getDiscountCode());
        tender = TH.getTender(ac.getTenderId(), ac.getSourceDescription());
      }
      else {
        tender = null;
      }
    }
    else {
      IVoucherTenderLineItem lineItem = (IVoucherTenderLineItem) cmd.getVoucherLineItem();
      tender = lineItem.getTender();
    }
    if (tender == null) {
      return null;
    }
    else {
      return tender.getAuthMethodCode();
    }
  }

  private AuthRequestType determineAuthRequestType(IVoucherLineItem lineItem) {
    VoucherActivityCodeType type = VoucherActivityCodeType.valueOf(lineItem.getActivityCode());
    if (ISSUED.equals(type)) {
      return AuthRequestType.ISSUE;
    }
    if (REDEEMED.equals(type)) {
      return AuthRequestType.TENDER;
    }
    if (RELOAD.equals(type)) {
      return AuthRequestType.RELOAD;
    }
    if (CASHOUT.equals(type)) {
      return AuthRequestType.CASH_OUT;
    }
    if (INQUIRE_BALANCE.equals(type)) {
      return AuthRequestType.INQUIRE_BALANCE;
    }

    // the following is older logic left only to serve as failover
    if (lineItem instanceof IVoucherSaleLineItem) {
      IVoucherSaleLineItem saleItem = (IVoucherSaleLineItem) lineItem;
      if (saleItem.getReturn()) {
        return AuthRequestType.CASH_OUT;
      }
      else if (ItemLocator.getLocator().isReloadItem((INonPhysicalItem) saleItem.getItem())) {
        return AuthRequestType.RELOAD;
      }
      else {
        return AuthRequestType.ISSUE;
      }
    }
    else {
      if (lineItem.getAmount().signum() != -1) {
        return AuthRequestType.TENDER;
      }
      else if (lineItem.getActivityCode().equals(RELOAD.toString())) {
        return AuthRequestType.RELOAD;
      }
      else {
        return AuthRequestType.ISSUE;
      }
    }
  }

  private BigDecimal getLineAmt(IVoucherCmd cmd) {
    IVoucherLineItem voucherLineItem = cmd.getVoucherLineItem();
    BigDecimal amount;
    if (voucherLineItem instanceof IVoucherSaleLineItem) {
      amount = ((IPriceCmd) cmd).getNewPrice();
      if (amount == null) {
        amount = ((IVoucherSaleLineItem) voucherLineItem).getItem().getCurrentShelfPrice();
      }
    }
    else {
      amount = voucherLineItem.getAmount();
    }
    if (amount == null) {
      amount = voucherLineItem.getParentTransaction().getAmountDue();
    }
    return amount;
  }

  protected IStoredValueAuthRequest makeRequest(IVoucherCmd argCmd) {
    IVoucherLineItem lineItem = argCmd.getVoucherLineItem();
    final AuthRequestType type = determineAuthRequestType(lineItem);

    if ((argCmd instanceof ISaleTenderCmd)
      && (((ISaleTenderCmd) argCmd).getTenderLineItem() instanceof IAuthorizableTenderLineItem)) {

      TenderUsageCodeType tenderUsageCode = ((ISaleTenderCmd) argCmd).getTenderUsageCodeType();
      IAuthRequest req = AUTH_FACTORY.makeAuthRequest(type, lineItem, tenderUsageCode, true);
      String vType = StringUtils.nonNull(((ISaleTenderCmd) argCmd).getVoucherType());
      if (vType.endsWith("ECARD") && (req instanceof ISupportsManualEntry)) {
        ((ISupportsManualEntry)req).setManual();
      }
      return (IStoredValueAuthRequest) req;
    }
    else {
      return (IStoredValueAuthRequest) AUTH_FACTORY.makeAuthRequest(determineAuthMethodCode(argCmd), type,
        lineItem, true);
    }
  }
}
