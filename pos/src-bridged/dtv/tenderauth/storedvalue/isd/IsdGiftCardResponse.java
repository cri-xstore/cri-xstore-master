//$Id: IsdGiftCardResponse.java 318 2012-08-31 15:06:31Z dtvdomain\aabdul $
package dtv.tenderauth.storedvalue.isd;

import java.math.BigDecimal;
import java.util.Map;

import imsformat.sv.SVResponse;

import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.impl.isd.IsdResponse;

/**
 * An authorization response for gift cards via ISD.<br>
 * <br>
 * 
 * Copyright (c) 2007 Datavantage Corporation
 * 
 * @author mshields
 * @version $Revision: 318 $
 * @created Feb 28, 2007
 */
public class IsdGiftCardResponse
    extends IsdResponse {
  private static final long serialVersionUID = 1L;

  /**
   * Constructs an <code>IsdGiftCardResponse</code>.
   * 
   * @param argRequest the request corresponding to this response
   * @param argMap a map of named fields to response values
   */
  public IsdGiftCardResponse(IAuthRequest argRequest, Map<String, Object> argMap) {
    super(argRequest, argMap);
    setAmount(getAuthorizationAmt());
  }

  /**
   * Gets the authorization amount.
   * @return the authorization amount
   */
  public BigDecimal getAuthorizationAmt() {
    String amt = getAuthorizationAmtString();
    return toBigDecimal(amt);
  }

  /**
   * Gets the authorization amount as a string.
   * @return the authorization amount as a string.
   */
  public String getAuthorizationAmtString() {
    return get(SVResponse.AUTHORIZATION_AMOUNT);
  }

  /**
   * Gets the remaining balance of the stored value card.
   * @return the remaining balance of the stored value card.
   */
  @Override
  public BigDecimal getBalance() {
    return getCardBalance();
  }

  /**
   * Gets the card balance; if &lt; 0, this indicates card balance is now zero, and the negative
   * amount is added to the requested authorization amount to obtain the amount actually authorized.
   * 
   * @return the card balance
   */
  public BigDecimal getCardBalance() {
    String amt = checkForNegativeAmount(getCardBalanceString());
    return toBigDecimal(amt);
  }

  /**
   * Gets the card balance as a string.
   * @return the card balance as a string.
   */
  public String getCardBalanceString() {
    return get(SVResponse.NEW_BALANCE);
  }

  @Override
  public String getBankReferenceNumber() {
    return get(SVResponse.RETRIEVAL_REFERENCE_NUMBER);
  }

  private String checkForNegativeAmount(String argAmt) {
    String amt = argAmt;
    if (argAmt.length() == 11) {
      //must be negative, anything less than 11 digits is considered positive
      //poor assumption, but not all values are 11 characters long
      //positive value are not left zero padded out to 11 characters
      if (argAmt.charAt(0) == '1') {
        amt = '-' + argAmt.substring(1);
      }
    }
    return amt;
  }
}
