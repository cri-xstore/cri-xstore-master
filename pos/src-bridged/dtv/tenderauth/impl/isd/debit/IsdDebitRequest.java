// $Id: IsdDebitRequest.java 143 2012-07-24 13:19:24Z dtvdomain\aabdul $
package dtv.tenderauth.impl.isd.debit;

import static dtv.util.StringUtils.nonNull;

import imsformat.credit.CCRequest;

import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.impl.isd.credit.IsdCreditRequest;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * Debit card request for ISD.<br>
 * Copyright (c) 2007 Datavantage Corporation
 * 
 * @author dberkland
 * @version $Revision: 143 $
 * @created Feb 6, 2007
 */
public class IsdDebitRequest
    extends IsdCreditRequest {

  /**
   * Constructor
   * 
   * @param argType the type of authorization request
   * @param argLine the line item this request is related to, or <code>null</code> is there is no
   * related transaction line item
   * @param argTenderUsageCode tender usage
   */
  public IsdDebitRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
      TenderUsageCodeType argTenderUsageCode) {
    super(argType, argLine, argTenderUsageCode);
    setCancelAllowed(false);
  }

  /** {@inheritDoc} */
  @Override
  public void loadSensitiveData() {
    if (getLineItem() instanceof ICreditDebitTenderLineItem) {
      ICreditDebitTenderLineItem creditLineItem = (ICreditDebitTenderLineItem) getLineItem();

      StringBuilder track1 = new StringBuilder(nonNull(creditLineItem.getTrack1()));
      StringBuilder track2 = new StringBuilder(nonNull(creditLineItem.getTrack2()));

      setParameter(CCRequest.ACCOUNT_NUMBER, creditLineItem.getAccountNumber());
      // -------------------------------------
      // : CUSTOMIZATIONS
      // --------------------------------------
      // ISD requires only Track2 Data for Debit purchases
      if (track2.length() > 0) {
        setParameter(CCRequest.TRACK_DATA, track2.toString());
        setParameter(CCRequest.TRACK_NUMBER, "2");
      }else {
        setParameter(CCRequest.TRACK_DATA, null);
        setParameter(CCRequest.TRACK_NUMBER, null);
      }
      // -------------------------------------
      // : END CUSTOMIZATIONS
      // --------------------------------------
      track1.delete(0, track1.length());
      track2.delete(0, track2.length());
    }
  }

}
