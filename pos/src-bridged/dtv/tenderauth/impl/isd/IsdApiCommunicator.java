//$Id: IsdApiCommunicator.java 570 2013-01-08 18:11:22Z dtvdomain\aabdul $
package dtv.tenderauth.impl.isd;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import imsformat.km.KMResponse;

import imsformat.credit.CCRequest;
import msgsentry.common.*;
import msgsentry.common.ids.LoggingIdIface;
import mspcomm.ctrl.MspCommController;
import mspcomm.ctrl.MspControl;
import mspcomm.shared.MspModuleIds;
import mspcomm.shared.MspQueue;

import dtv.hardware.auth.UserCancelledException;
import dtv.i18n.FormattableFactory;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.AbstractCommunicator;
import dtv.util.*;

/**
 * Authorization Communicator that uses the ISD Java API.<br>
 * Copyright (c) 2007 Datavantage Corporation
 * 
 * @author dberkland
 * @version $Revision: 570 $
 * @created Feb 6, 2007
 */
public class IsdApiCommunicator
    extends AbstractCommunicator {

  private static final Logger logger_ = Logger.getLogger(IsdApiCommunicator.class);
  private static final FormattableFactory FF = FormattableFactory.getInstance();

  private static MspQueue requestQ_ = null;
  private static MspQueue responseQ_ = null;

  private static int toLogLevel(Object argValue) {
    if (argValue instanceof Integer) {
      return ((Integer) argValue).intValue();
    }
    if (argValue instanceof String) {
      if (NumberUtils.isInteger((String) argValue)) {
        return Integer.valueOf((String) argValue);
      }
      if ("ERROR".equalsIgnoreCase((String) argValue)) {
        return LoggingIdIface.LEVEL_ERROR;
      }
      if ("INFO".equalsIgnoreCase((String) argValue)) {
        return LoggingIdIface.LEVEL_INFO;
      }
      if ("DEBUG".equalsIgnoreCase((String) argValue)) {
        return LoggingIdIface.LEVEL_DEBUG;
      }
      if ("TRACE".equalsIgnoreCase((String) argValue)) {
        return LoggingIdIface.LEVEL_TRACE;
      }
    }
    logger_.warn("unexpected log level:" + argValue);
    return LoggingIdIface.LEVEL_TRACE;
  }

  private int mspCommModuleState_ = MgrStateInterface.INACTIVE_STATE;

  private CountDownLatch completed_ = new CountDownLatch(1);

  private final ControlInterface controlCallback_ = new ControlInterface() {
    /**
     * This method is used to inform this class when an error condition has occurred within another
     * controller that it is monitoring. Modules are identified by module ID.
     * 
     * @param errorCondition An int identifying the error condition that occurred.
     * @param moduleId An int identifying the module or controller perfoming this callback.
     */
    @Override
    public void errorCallback(int errorCondition, int moduleId) {
      log("errorCallback(" + errorCondition + "," + moduleId);
      switch (moduleId) {
        case MspModuleIds.MSP_COMM_CONTROL:
          if (errorCondition > ErrorConditionKeys.NO_ERROR) {
            mspCommModuleState_ = MgrStateInterface.ERROR_STATE;
          }
          break;

        default:
          break;
      }
    }

    /**
     * This method is called to inform the implementing class when a task has been completed.
     */
    @Override
    public void notifyCompletion() {
      log("notifyCompletion()");
      completed_.countDown();
    }

    /**
     * This method is called to inform the implementing class when a task has been completed.
     * @param moduleId An int identifying the module or controller perfoming this callback.
     */
    @Override
    public void notifyCompletion(int moduleId) {
      log("notifyCompletion(" + moduleId + ")");
      notifyCompletion();
    }
  };
  private int startTimeoutSeconds_ = 10;
  private int stopTimeoutSeconds_ = 10;
  private String logFilePath_ = ".";
  private int logLevel_ = LoggingIdIface.LEVEL_TRACE;
  private int transactionTimeoutSeconds_ = 30;

  private boolean allowRequestTurnAround_ = false;

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, Object argValue) {
    if ("startTimeoutSeconds".equals(argName)) {
      startTimeoutSeconds_ = ((Integer) argValue).intValue();
    }
    else if ("stopTimeoutSeconds".equals(argName)) {
      stopTimeoutSeconds_ = ((Integer) argValue).intValue();
    }
    if ("allowRequestTurnAround".equals(argName)) {
      allowRequestTurnAround_ = toBoolean(argValue);
    }
    else if ("transactionTimeoutSeconds".equals(argName)) {
      transactionTimeoutSeconds_ = toInteger(argValue);
    }
    else if ("logFilePath".equals(argName)) {
      logFilePath_ = argValue.toString();
    }
    else if ("logLevel".equals(argName)) {
      logLevel_ = toLogLevel(argValue);
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthResponse sendRequestImpl()
      throws UserCancelledException, ReceiveTimeoutException {

    log("begin sendRequest");
    try {
      start();

      checkUserCancelled();

      // drain the response queue...just in case we had previous failures
      //    (might not be a good idea, but we'll try it for now)
      while (responseQ_.getQueueSize() > 0) {
        responseQ_.getMessage();
      }

      // toss the request across the wall
      IIsdRequest req = (IIsdRequest) getRequest();

      setStatus(req, FF.getTranslatable("_xpayCommunicatorConnecting"), getProgressConnectMillis(), 0,
          getProgressConnectPercent());

      HashMap<String, Object> requestMap = convert(req);
      logMap("REQUEST", requestMap);

      setStatus(req, FF.getTranslatable("_xpayCommunicatorSending"), getProgressSendingMillis(),
          getProgressConnectPercent(), getProgressSendingPercent());

      requestQ_.addMessage(requestMap);

      setStatus(req, FF.getTranslatable("_xpayCommunicatorWaiting"), getProgressWaitMillis(),
          getProgressSendingPercent(), getProgressWaitPercent());

      // poll for response availability
      long timeoutTime = System.currentTimeMillis() + getReceiveTimeoutMillis();
      while ((responseQ_.getQueueSize() < 1) && (System.currentTimeMillis() < timeoutTime)) {
        calculateProgressPercents();
        sleep();

        try {
          checkUserCancelled();
        }
        catch (UserCancelledException ex) {
          // stop the library if the user selects cancel
          try {
            stop();
          }
          catch (OfflineException e) {
            log("User cancelled - Offline", e);
          }
          throw ex;
        }
      }

      setStatus(req, FF.getTranslatable("_xpayCommunicatorReceiving"), getProgressReceiveMillis(),
          getProgressWaitPercent(), getProgressReceivePercent());

      // read the response
      HashMap<?, ?> respMessage;
      try {
        respMessage = responseQ_.getMessage();
      }
      catch (Exception ex) {
        stop();
        throw new ReceiveTimeoutException(ex);
      }
      logMap("RESPONSE", respMessage);
      IAuthResponse response = convert(req, respMessage);

      //stop();

      return response;
    }
    finally {
      getCommCallback().setNotCancelled();
      log("exit sendRequest");
    }
  }

  private IAuthResponse convert(IAuthRequest argRequest, HashMap<?, ?> responseMap) {
    return getResponseConverter().convertResponse(responseMap, argRequest);
  }

  @SuppressWarnings("unchecked")
  private HashMap<String, Object> convert(IIsdRequest req) {
    return (HashMap<String, Object>) getRequestConverter().convertRequest(req);
  }

  private void logMap(String argComment, Map<?, ?> argMap) {
    StringBuilder sb = new StringBuilder();
    sb.append(argComment);

    for (Map.Entry<?, ?> entry : new TreeMap<Object, Object>(argMap).entrySet()) {
      String key = entry.getKey().toString();
      Object value = entry.getValue();
      sb.append("\n  ");
      sb.append(key);
      sb.append("=");
      if (value instanceof byte[]) {
        sb.append(ByteUtils.toString((byte[]) value, ByteDumpFormat.PLAIN));
      }
      else if (value == null) {
        sb.append("null");
      }
      else {
        sb.append('\'').append(mask(key, value.toString())).append('\'');
      }
    }
    log(sb);
  }

  private String mask(String argKey, String argValue) {
    if (CCRequest.ACCOUNT_NUMBER.equals(argKey) //
        || CCRequest.CVV2_VALUE.equals(argKey) //
        || CCRequest.TRACK_DATA.equals(argKey)
        || CCRequest.EXPIRATION_DATE.equals(argKey)
        || KMResponse.CURR_KEY.equals(argKey)) {
      return StringUtils.fill('*', argValue.length());
    }
    return argValue;
  }

  private void sleep() {
    try {
      Thread.sleep(100);
    }
    catch (InterruptedException ex) {
      log("CAUGHT EXCEPTION", ex);
    }
  }

  private void start() {
    log("starting");
    if (requestQ_ != null) {
      logger_.info("already started");
      return;
    }
    MspControl controller = MspCommController.getInstance();
    controller.setCallingControl(controlCallback_);
    try {
      controller.setLogPath(logFilePath_, logLevel_);
    }
    catch (ISDException ex) {
      log("CAUGHT EXCEPTION", ex);
    }
    //    if (getHostList().hasNext()) {
    //      String hostString = getHostList().get();
    //      log("Remote System: " + hostString);
    //      HostDescriptor host = new HostDescriptor(hostString);
    //      controller.setRemoteHostLocation(host.getHostNameOrAddr(), host.getPort());
    //    }
    controller.setTransactionTimeout(transactionTimeoutSeconds_);
    controller.allowRequestTurnAround(allowRequestTurnAround_);

    completed_ = new CountDownLatch(1);
    controller.start();
    try {
      if (!completed_.await(startTimeoutSeconds_, TimeUnit.SECONDS)) {
        throw new OfflineException("timeout starting");
      }
    }
    catch (InterruptedException ex) {
      log("Interrupted", ex);
    }

    log(this + " Services Started.");

    if (mspCommModuleState_ == MgrStateInterface.ERROR_STATE) {
      throw new OfflineException("MSP_COMM_MODULE_STARTUP_ERROR");
    }

    log("MSP Comm module started");
    requestQ_ = controller.getRequestQueue();
    responseQ_ = controller.getResponseQueue();

    ///TODO add shutdown hook to call stop???

    ///TODO add idle detection to bring down the library if unused for a period of time?
  }

  private void stop() {
    log("stopping");
    requestQ_ = null;
    responseQ_ = null;
    completed_ = new CountDownLatch(1);
    MspCommController.getInstance().stop();

    try {
      if (!completed_.await(stopTimeoutSeconds_, TimeUnit.SECONDS)) {
        throw new OfflineException("timeout stopping");
      }
    }
    catch (InterruptedException ex) {
      log("Interrupted", ex);
    }
  }
}
