//$Id: AbstractIsdAuthProcess.java 569 2013-01-08 17:44:34Z dtvdomain\aabdul $
package dtv.tenderauth.impl.isd;

import static dtv.util.StringUtils.nonNull;

import org.apache.log4j.Logger;
import imsformat.credit.CCRequest;
import dtv.hardware.auth.UserCancelledException;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.event.ICreditAuthResponse;
import dtv.tenderauth.impl.AbstractThreadedAuthProcess;
import dtv.tenderauth.impl.event.AuthTenderFailedResponse;
import dtv.tenderauth.impl.event.RepollResponse;
import dtv.util.config.*;
import dtv.util.sequence.SequenceFactory;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * Abstract authorization process for ISD.<br>
 * Copyright (c) 2007 Datavantage Corporation<br>
 * src-bridged: To send out a time out reversal
 * @author dberkland
 * @version $Revision: 569 $
 * @created Feb 26, 2007
 */
public abstract class AbstractIsdAuthProcess
    extends AbstractThreadedAuthProcess {

  private static final Logger logger_ = Logger.getLogger(AbstractIsdAuthProcess.class);
  private static final Logger adminLogger_ = Logger.getLogger("dtv.xstore.comm.paysys");

  private boolean _isReversalRequired = false;
  private static int MAX_REVERSAL_RETRY = 3;
  private static int ISD_TIMEOUT_RESPONSE_CODE = 04;
  private static int ISD_OFFLINE_RESPONSE_CODE = 05;

  @Override
  public void setParameter(ParameterConfig argConfig) {
    String name = argConfig.getName();
    IConfigObject value = argConfig.getValue();

    if ("ReversalRequired".equalsIgnoreCase(name)) {
      _isReversalRequired = ConfigUtils.toBoolean(value);
    }
    if ("MaxReversalAttempts".equalsIgnoreCase(name)) {
      MAX_REVERSAL_RETRY = ConfigUtils.toInt(value);
    }
    if ("IsdTimeOutResponseCode".equalsIgnoreCase(name)) {
      ISD_TIMEOUT_RESPONSE_CODE = ConfigUtils.toInt(value);
    }
    if ("IsdOfflineResponseCode".equalsIgnoreCase(name)) {
      ISD_OFFLINE_RESPONSE_CODE = ConfigUtils.toInt(value);
    }
    super.setParameter(argConfig);
  }

  /** {@inheritDoc} */
  @Override
  public void cancelRequest(IAuthRequest argRequest) {
    super.cancelRequest(argRequest);
    clearLock();
  }

  /**
   * Handle an authorization when the processor is online.
   * 
   * @param argRequest request to process
   * @return response
   */
  @Override
  protected IAuthResponse handleOnline(ITenderAuthRequest argRequest) {
    IAuthorizableLineItem lineItem = argRequest.getLineItem();
    ITender tender = argRequest.getTender();

    if (tender != null) {
      IAuthResponse response = null;

      if (areFloorLimitsMet(lineItem, tender.getTenderId(), argRequest.getTenderUsageCode(), /* offline= */
      false)) {

        logger_.info(getClass().getName() + ".handleFloorLimitMet(online)");
        response = handleFloorLimitMet(argRequest,/* offline= */false);
      }
      else if (areCeilingLimitsExceeded(lineItem, tender.getTenderId(), argRequest.getTenderUsageCode(),
      /* offline= */false)) {

        logger_.info(getClass().getName() + ".handleCeilingLimitExceeded(online)");
        response = handleCeilingLimitExceeded(argRequest, /* offline= */false);
      }
      if (response != null) {
        return response;
      }
    }
    try {
      logger_.info(getClass().getName() + ".handleOnline->sendRequest");
      IAuthResponse response = getAuthCommunicator().sendRequest(argRequest);

      if (ISD_TIMEOUT_RESPONSE_CODE == Integer.parseInt(response.getResponseCode())) {
        getAuthLog().warn("SafTor time out message. Response code: " + response.getResponseCode());
        adminLogger_.error("SafTor time out message. Response code: " + response.getResponseCode());
        handleReversalOnException(argRequest);
        IAuthResponse resp = handleReceiveTimeout(argRequest);

        if (!(resp instanceof AuthTenderFailedResponse)) {
          return resp;
        }
      }
      if (ISD_OFFLINE_RESPONSE_CODE == Integer.parseInt(response.getResponseCode())) {
        getAuthLog().warn("SafTor Offline message. Response code: " + response.getResponseCode());
        adminLogger_.error("SafTor Offline message. Response code: " + response.getResponseCode());
        handleReversalOnException(argRequest);
        IAuthResponse resp = handleOffline(argRequest);

        if (!(resp instanceof AuthTenderFailedResponse)) {
          return resp;
        }
      }

      if (argRequest instanceof ICreditAuthRequest) {
        if (response instanceof ICreditAuthResponse) {
          ICreditAuthResponse resp = (ICreditAuthResponse) response;
          ICreditDebitTenderLineItem tenderLine = (ICreditDebitTenderLineItem) lineItem;
          tenderLine.setPs2000(resp.getPs2000());

          // If there is any PosDataCode, create a line item property to store the value.
          if (resp.getPosDataCode() != null) {
            tenderLine.setStringProperty("POS_DATA_CODE_VALUE", resp.getPosDataCode());
          }

        }
      }
      // check if the response requires us to prompt for the cashier to try typing the CID again
      if (response.isReenteringCID()) {
        logger_.info(getClass().getName() + ".getCidReentryResponse");
        return getCidReentryResponse(argRequest);
      }

      // pass the response to updateLineItemForOnline; if we get a RepollResponse
      // send the request back to the communicator; if not, return the response
      while ((response = updateLineItemForOnline(argRequest, response, lineItem)) instanceof RepollResponse) {
        logger_.info(getClass().getName() + ".handleOnline->sendRequest(repoll)");
        response = getAuthCommunicator().sendRequest(((RepollResponse) response).getRepollRequest());
      }
      if (argRequest instanceof ISuspendableAuthRequest) {
        ((ISuspendableAuthRequest) argRequest).setPending(false);
      }
      return response;
    }
    catch (ReceiveTimeoutException ex) {
      handleReversalOnException(argRequest);
      // log problem
      getAuthLog().warn("Timeout waiting for response: " + ex);
      adminLogger_.error("Timeout Waiting For Response: " + ex);
      return handleReceiveTimeout(argRequest);
    }
    catch (OfflineException ex) {
      handleReversalOnException(argRequest);
      // log problem
      getAuthLog().warn("Host offline: " + ex);
      adminLogger_.error("Payment Systems Host Offline: " + ex);
      return handleOffline(argRequest);
    }
    catch (UserCancelledException ex) {
      return handleUserCancel(argRequest);
    }
  }

  //Added system generated reversal for timeout and communication error exceptions
  protected void handleReversalOnException(ITenderAuthRequest argRequest) {

    AbstractIsdRequest request = (AbstractIsdRequest) argRequest;
    if (!validTORRequest(request)) {
      return;
    }

    try {
      if (_isReversalRequired) {
        request.prepReversalRequest();
        int nRevAttempts = 0;
        boolean isReversalSuccess = false;

        while (!isReversalSuccess && ++nRevAttempts <= MAX_REVERSAL_RETRY) {

          try {
            request.setParameter(CCRequest.RETRY_INDICATOR, nRevAttempts);
            IAuthResponse response;
            response = getAuthCommunicator().sendRequest(request);
            if (response.isSuccess()) {
              isReversalSuccess = true;
            }
          }
          catch (Exception ex) {
            getAuthLog().error("Error sending reversal request. Attempt #" + nRevAttempts, ex);
          }
        }
      }
    }
    catch (Exception ex) {
      getAuthLog().error("Unable to generate a reversal for auth request ", ex);
    }
    finally {
      request.clearReversalRequest();
    }
  }

  public boolean validTORRequest(AbstractIsdRequest request) {
    if (AuthRequestType.INQUIRE_BALANCE.equals(request.getRequestType())) {
      return false;
    }
    return true;
  }

  @Override
  protected IAuthResponse handleFloorLimitMet(ITenderAuthRequest argRequest, boolean argOffline) {

    if (argRequest instanceof AbstractIsdRequest) {
      //Generating a new journal key for offline floor limit approved transactions
      AbstractIsdRequest req = (AbstractIsdRequest) argRequest;
      String terminalId = req.getRequestMap().get(CCRequest.TERMINAL_NUMBER).toString();
      String storeNumber = req.getRequestMap().get(CCRequest.LOCATION).toString();

      req.setParameter(CCRequest.JOURNAL_KEY, AbstractIsdRequest.generateJournalKey(storeNumber, terminalId));
      req.setParameter(CCRequest.SYSTEM_TRACE_AUDIT_NUMBER, SequenceFactory.getNextStringValue("STAN6"));
      if (req.getLineItem() != null) {
        req.getLineItem().setStringProperty(CCRequest.JOURNAL_KEY,
            nonNull(req.getRequestMap().get(CCRequest.JOURNAL_KEY)));
        req.getLineItem().setStringProperty(CCRequest.SYSTEM_TRACE_AUDIT_NUMBER,
            nonNull(req.getRequestMap().get(CCRequest.SYSTEM_TRACE_AUDIT_NUMBER)));
      }
    }
    return super.handleFloorLimitMet(argRequest, argOffline);
  }
}
