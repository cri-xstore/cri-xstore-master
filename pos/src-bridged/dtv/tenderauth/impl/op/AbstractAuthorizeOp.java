// $Id: AbstractAuthorizeOp.java 1167 2017-09-26 16:41:46Z johgaug $
package dtv.tenderauth.impl.op;

import java.util.*;

import org.apache.log4j.Logger;

import cri.ajb.fipay.reqeust.ISupportsManualEntry;

import dtv.event.EventDescriptor;
import dtv.event.Eventor;
import dtv.event.eventor.DefaultEventor;
import dtv.hardware.events.IHardwareInputEvent;
import dtv.hardware.rcptbuilding.IRcpt;
import dtv.hardware.rcptbuilding.RcptBuilder;
import dtv.i18n.IFormattable;
import dtv.i18n.config.IFormattableConfig;
import dtv.pos.assistance.AssistanceHelper;
import dtv.pos.common.*;
import dtv.pos.framework.action.type.XstChainActionType;
import dtv.pos.framework.action.type.XstDataActionKey;
import dtv.pos.framework.form.FormConstants;
import dtv.pos.framework.op.OpState;
import dtv.pos.framework.op.Operation;
import dtv.pos.hardware.op.IPrintReceiptsCmd;
import dtv.pos.iframework.action.*;
import dtv.pos.iframework.event.*;
import dtv.pos.iframework.op.*;
import dtv.pos.iframework.validation.IValidationResult;
import dtv.pos.register.DefaultCmd;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.event.AuthEventType;
import dtv.tenderauth.impl.form.MoreAuthInfoEditModel;
import dtv.tenderauth.impl.form.TenderAuthEditModel;
import dtv.tenderauth.op.IAuthorizationCmd;
import dtv.util.config.ConfigUtils;
import dtv.util.config.IConfigObject;

/**
 * Abstract operation to handle basic authorization logic.<br>
 * Copyright (c) 2004 Datavantage Corporation
 *
 * @created 02/23/04
 * @author Jeff Sheldon
 * @version $Revision: 1167 $
 */
public abstract class AbstractAuthorizeOp
  extends Operation
  implements IXstEventObserver, IReversibleOp {

  protected static final Class<?>[] EVENT_INTERFACES = new Class<?>[] {IHardwareInputEvent.class};
  protected static final IXstEventType[] EVENTS = new IXstEventType[] {AuthEventType.RESPONSE};
  protected static final IXstDataActionKey MANUAL_AUTH = XstDataActionKey.valueOf("MANUAL_AUTH");
  protected static final IXstDataActionKey RETRY = XstDataActionKey.valueOf("RETRY");

  private static final Logger logger_ = Logger.getLogger(AbstractAuthorizeOp.class);
  private static final long serialVersionUID = 1L;

  protected final IOpState ASKING_TO_CANCEL = new OpState(this, "ASKING_TO_CANCEL");
  protected final IOpState ASKING_TO_SUSPEND = new OpState(this, "ASKING_TO_SUSPEND");
  protected final IOpState COMPLETE = new OpState(this, "COMPLETE");
  protected final IOpState PRINTING_END_RCPTS = new OpState(this, "PRINTING_END_RCPTS");
  protected final IOpState PRINTING_PRE_RCPTS = new OpState(this, "PRINTING_PRE_RCPTS");
  protected final IOpState PROCESSING = new OpState(this, "PROCESSING");
  protected final IOpState SHOWING_FAILED = new OpState(this, "SHOWING_FAILED");
  protected final IOpState SHOWING_GET_MANUAL_AUTH = new OpState(this, "SHOWING_GET_MANUAL_AUTH");
  protected final IOpState SHOWING_NEED_MORE_INFO = new OpState(this, "SHOWING_NEED_MORE_INFO");

  private final IXstEventType[] eventTypes_;

  private IFormattableConfig additionalMessageTranslatable_ = null;
  private Eventor opChainProcessorEvents_ = null;
  private IOpChainKey suspendChainKey_ = OpChainKey.valueOf("SUSPEND_TRANSACTION");
  protected boolean manualEntry_ = false;

  /**
   * Constructs an <code>AbstractAuthorizeOp</code>.
   */
  public AbstractAuthorizeOp() {
    super();

    /* Build an array of event types to which this operation responds. This will be a default, mandatory set
     * plus any declared by concrete subclasses. */
    Collection<IXstEventType> evtTypes = new HashSet<IXstEventType>(Arrays.asList(EVENTS));
    evtTypes.addAll(getAdditionalObservedEvents());

    eventTypes_ = evtTypes.toArray(new IXstEventType[evtTypes.size()]);
  }

  /**
   * Handle a CANCEL action by calling {@link #handleVoid(IAuthorizationCmd)}
   * @param argCmd the current command
   */
  @Override
  public boolean canceling(IXstCommand argCmd) {
    argCmd.setOpState(ASKING_TO_CANCEL);
    handleVoid((IAuthorizationCmd) argCmd);
    return true;
  }

  /** {@inheritDoc} */
  @Override
  public Class<?>[] getObservedEventInterfaces() {
    return EVENT_INTERFACES;
  }

  /** {@inheritDoc} */
  /**
   * BaseBug [176207] If you need to override this method's behavior, declare additional event types in
   * {@link #getAdditionalObservedEvents()}.
   */
  @Override
  public final IXstEventType[] getObservedEvents() {
    return eventTypes_;
  }

  /** {@inheritDoc} */
  /**
   * BaseBug [176207] If you need to override this method's behavior, override any of the following methods as
   * appropriate:<br>
   * <ul>
   * <li>{@link #handleTrainingMode(IAuthorizationCmd)} -- if authorizing from training mode</li>
   * <li>{@link #handleAbort(IAuthorizationCmd)} -- if the EXIT action is invoked</li>
   * <li>{@link #handleAuthResponse(IAuthorizationCmd, IAuthResponse)} -- if an auth response is received</li>
   * <li>{@link #handleHardwareEvent(IXstCommand, IHardwareInputEvent)} -- if a hardware event is received</li>
   * <li>{@link #handleBasedOnState(IXstCommand, IXstEvent)} -- for all other cases</li>
   * </ul>
   */
  @Override
  public final IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IAuthorizationCmd cmd = (IAuthorizationCmd) argCmd;

    // handle training mode up front so we don't use the live processors
    if (AssistanceHelper.getInstance().isTrainingMode()) {
      return handleTrainingMode(cmd);
    }

    // handle any EXIT actions (on any form)
    if (argEvent instanceof IXstDataAction) {
      IXstDataAction action = (IXstDataAction) argEvent;
      IXstActionKey actionKey = action.getActionKey();

      if (FormConstants.EXIT == actionKey) {
        return handleAbort(cmd);
      }
    }

    // handle any responses from the auth process
    if (argEvent instanceof IAuthResponse) {
      return handleAuthResponse(cmd, (IAuthResponse) argEvent);
    }
    else if (argEvent instanceof IHardwareInputEvent<?>) {
      return handleHardwareEvent(cmd, (IHardwareInputEvent<?>) argEvent);
    }
    else {
      return handleBasedOnState(argCmd, argEvent);
    }
  }

  /** {@inheritDoc} */
  /**
   * BaseBug [176207] If you need to override this method's behavior, define additional void actions in
   * {@link #handleVoid(IAuthorizationCmd)}.
   */
  @Override
  public final IOpResponse handleOpReverse(IXstCommand argCmd, IXstEvent argEvent) {
    // if we are the currently active operation, and chain is reversing or
    // canceling, make sure the tenderline is voided
    handleVoid((IAuthorizationCmd) argCmd);
    if (opChainProcessorEvents_ == null) {
      opChainProcessorEvents_ = new DefaultEventor(new EventDescriptor(IOpChainProcessor.class));
    }
    opChainProcessorEvents_.post(IOpChain.OP_CHAIN_COMPLETE, "TransactionModified");
    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  /**
   * BaseBug [176207] If you need to override this method's behavior, define additional reversal applicability
   * conditions in {@link #isApplicableToReverseImpl(IXstCommand)}.
   */
  @Override
  public final boolean isApplicableToReverse(IXstCommand argCmd) {
    // only need to handle the reverse if we are the current op
    return !isComplete() && isApplicableToReverseImpl(argCmd);
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("SuspendChain".equalsIgnoreCase(argName)) {
      suspendChainKey_ = OpChainKey.valueOf(argValue.toString());
    }
    else if ("ManualEntry".equalsIgnoreCase(argName)) {
      manualEntry_ = ConfigUtils.toBoolean(argValue);
    }
    else if ((argValue instanceof IFormattableConfig)
      && "AdditionalMessageTranslatable".equalsIgnoreCase(argName)) {

      additionalMessageTranslatable_ = (IFormattableConfig) argValue;
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

  /**
   * Returns any types of events to which this operation responds in addition to those to which it absolutely
   * must respond.
   *
   * @return any additional types of events to which this operation responds; an empty set if the default set
   * of event types is sufficient
   */
  protected Set<? extends IXstEventType> getAdditionalObservedEvents() {
    return Collections.emptySet();
  }

  protected abstract IAuthProcess getAuthProcessor(IAuthorizationCmd argCmd);

  protected abstract IAuthRequest getAuthRequest(IAuthorizationCmd argCmd);

  protected DataActionGroupKey getFailedDataActionGroup(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    DataActionGroupKey actionGroup = argResponse.getDataActionGroup();
    if (actionGroup == null) {
      actionGroup = DataActionGroupKey.DEFAULT;
    }
    return actionGroup;
  }

  protected Object getPreReceiptTarget(IAuthorizationCmd argCmd) {
    return null;
  }

  /**
   * Shared method for how to show that we are processing a request.
   *
   * @param argCmd IAuthorizationCmd
   * @param authRequest IAuthRequest
   * @return IOpResponse
   */
  protected IOpResponse getProcessingResponse(IAuthorizationCmd argCmd, IAuthRequest authRequest) {
    argCmd.setOpState(PROCESSING);
    final DataActionGroupKey actionGroup;
    if (isCancelAllowed(argCmd, authRequest)) {
      actionGroup = DataActionGroupKey.DEFAULT;
    }
    else {
      actionGroup = DataActionGroupKey.NO_SKIP;
    }
    // display a prompt indicating we are authorizing
    return HELPER.getShowFormResponse(FormKey.valueOf("AUTH_PROCESSING"),
      new TenderAuthEditModel(authRequest), actionGroup);
  }

  protected IOpChainKey getSuspendChainKey() {
    return suspendChainKey_;
  }

  /**
   * handle aborting the auth process.
   *
   * @param argCmd IAuthorizationCmd
   * @return IOpResponse
   */
  protected IOpResponse handleAbort(IAuthorizationCmd argCmd) {
    IAuthProcess p = argCmd.getAuthProcess();
    IAuthRequest r = argCmd.getAuthRequest();
    if ((p != null) && (r != null)) {
      IOpState state = argCmd.getOpState();
      // If cancelAuthAllowed is false, don't allow the user to cancel when the auth is processing.
      // If the auth request is comoplete, the user should be allowed to cancel or retry.
      if (!p.isCancelAuthAllowed(r) && state.equals(PROCESSING)) {
        // ignore a cancel if cancel is not allowed
        return HELPER.waitResponse();
      }
      if ((r instanceof ISuspendableAuthRequest) && ((ISuspendableAuthRequest) r).isPending()) {
        argCmd.setOpState(ASKING_TO_SUSPEND);
        return HELPER.getPromptResponse(PromptKey.valueOf("CONFIRM_SUSPEND_TRANSACTION"));
      }
      p.cancelRequest(r);
    }
    return handleVoid(argCmd);
  }

  /**
   * Handle a response from auth process.
   *
   * @param argCmd current command
   * @param argResponse response to handle
   * @return IOpResponse
   */
  protected IOpResponse handleAuthResponse(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    if (PRINTING_END_RCPTS != argCmd.getOpState()) {
      // give the rcpt builder a chance to add receipts if any
      IRcpt[] rcpts = RcptBuilder.getInstance().getRcpts(argResponse);
      if ((rcpts != null) && (rcpts.length > 0)) {
        // store the information we are working with so we can resume after the receipts print
        argCmd.setOpState(PRINTING_END_RCPTS);
        argCmd.setCurrentResponse(argResponse);
        // set up a command to use while printing
        IPrintReceiptsCmd cmd = new DefaultCmd();
        cmd.setReceipts(rcpts);
        // stack the print chain
        return HELPER.getStackChainResponse(OpStatus.INCOMPLETE_HALT, OpChainKey.valueOf("PRINT_ITEMS"), cmd);
      }
    }

    if (argResponse.isSuccess()) {
      return handleSuccess(argCmd, argResponse);
    }
    if (isNeedingMoreInfo(argResponse)) {
      return handleNeedMoreInfo(argCmd, argResponse);
    }
    else {
      return handleFailed(argCmd, argResponse);
    }
  }

  protected IOpResponse handleBasedOnState(IXstCommand argCmd, IXstEvent argEvent) {
    IAuthorizationCmd cmd = (IAuthorizationCmd) argCmd;

    // handle based on current state
    IOpState state = argCmd.getOpState();

    if (state == null) {
      return handleInitialState(cmd);
    }
    else if (argCmd.getOpState() == COMPLETE) {
      return HELPER.completeResponse();
    }
    else if (PRINTING_END_RCPTS == state) {
      return handleAuthResponse(cmd, cmd.getCurrentResponse());
    }
    else if (PRINTING_PRE_RCPTS == state) {
      return handleInitialState(cmd);
    }
    else {
      return handlePostPrompt(cmd, argEvent, state);
    }
  }

  protected IOpResponse handleCancelQuestionResponse(IAuthorizationCmd argCmd, IXstEvent argEvent) {
    if (argEvent instanceof IXstDataAction) {
      IXstActionKey actionKey = ((IXstDataAction) argEvent).getActionKey();
      if (XstDataActionKey.YES.equals(actionKey)) {
        IAuthProcess p = argCmd.getAuthProcess();
        IAuthRequest r = argCmd.getAuthRequest();
        p.cancelRequest(r);
        return handleVoid(argCmd);
      }
      else {
        return getProcessingResponse(argCmd, argCmd.getAuthRequest());
      }
    }
    return HELPER.waitResponse();
  }

  /**
   * handle a FAILED response from auth process.
   *
   * @param argCmd IAuthorizationCmd
   * @param argResponse IAuthResponse
   * @return IOpResponse
   */
  protected IOpResponse handleFailed(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    if (Arrays.asList(argResponse.getAvailableActions()).contains(AuthFailedActionType.AUTO_MANUAL)) {
      return handleManualAuth(argCmd);
    }
    else {
      argCmd.setOpState(SHOWING_FAILED);
      DataActionGroupKey actionGroup = getFailedDataActionGroup(argCmd, argResponse);
      // handle displaying auth failed
      TenderAuthEditModel editModel = makeEditModel(argResponse);
      return HELPER.getShowFormResponse(FormKey.valueOf("AUTH_FAILED"), editModel, actionGroup);
    }
  }

  /**
   * Processes a hardware event.
   *
   * @param argCmd the currennt operational command
   * @param argEvent the hardware event triggering this call
   * @return an operational response to <code>argEvent</code>
   */
  protected IOpResponse handleHardwareEvent(IAuthorizationCmd argCmd, IHardwareInputEvent<?> argEvent) {
    return HELPER.waitResponse();
  }

  /**
   * Initiate the authorization.
   *
   * @param argCmd current command
   * @return IOpResponse
   */
  protected IOpResponse handleInitialState(IAuthorizationCmd argCmd) {
    if (PRINTING_PRE_RCPTS != argCmd.getOpState()) {
      Object preReceiptTarget = getPreReceiptTarget(argCmd);
      if (preReceiptTarget != null) {
        // give the rcpt builder a chance to add receipts if any
        IRcpt[] rcpts = RcptBuilder.getInstance().getRcpts(preReceiptTarget);
        if ((rcpts != null) && (rcpts.length > 0)) {
          // store the information we are working with so we can resume after the receipts print
          argCmd.setOpState(PRINTING_PRE_RCPTS);
          // set up a command to use while printing
          IPrintReceiptsCmd cmd = new DefaultCmd();
          cmd.setReceipts(rcpts);
          // stack the print chain
          return HELPER.getStackChainResponse(OpStatus.INCOMPLETE_HALT, OpChainKey.valueOf("PRINT_ITEMS"),
            cmd);
        }
      }
    }
    IAuthProcess p = getAuthProcessor(argCmd);
    argCmd.setAuthProcess(p);
    IAuthRequest authRequest = getAuthRequest(argCmd);
    if (manualEntry_ && (authRequest instanceof ISupportsManualEntry)) {
      ((ISupportsManualEntry) authRequest).setManual();
    }
    // when no appropriate authorization request exists then consider the op complete.
    if (authRequest == null) {
      return HELPER.completeResponse();
    }
    argCmd.setAuthRequest(authRequest);
    return handleProcess(argCmd, authRequest);
  }

  /**
   * handle user request to initiate manual auth.
   *
   * @param argCmd IAuthorizationCmd
   * @return IOpResponse
   */
  protected IOpResponse handleManualAuth(IAuthorizationCmd argCmd) {
    IAuthProcess p = argCmd.getAuthProcess();
    IAuthRequest request = argCmd.getAuthRequest();

    IValidationResult r = p.validateManualAuthAllowed(request);
    if (!r.isValid()) {
      argCmd.setOpState(SHOWING_FAILED);
      return HELPER.getPromptResponse(PromptKey.valueOf("MANUAL_AUTH_NOT_ALLOWED"), r.getMessage());
    }
    else {
      request = p.getManualAuthInfo(request);
      argCmd.setAuthRequest(request);
      argCmd.setOpState(SHOWING_GET_MANUAL_AUTH);

      IAuthInfo manualAuthInfo = request.getMoreAuthInfo();
      MoreAuthInfoEditModel model = new MoreAuthInfoEditModel(manualAuthInfo);
      // display a form to gather the additional information
      return HELPER.getShowFormResponse(FormKey.valueOf("AUTH_MORE_INFO"), model);
    }
  }

  /**
   * Handle user response to getting more auth info.
   *
   * @param argCmd IAuthorizationCmd
   * @param argEvent IXstEvent
   * @return IOpResponse
   */
  protected IOpResponse handleMoreInfoResponse(IAuthorizationCmd argCmd, IXstEvent argEvent) {
    MoreAuthInfoEditModel model = (MoreAuthInfoEditModel) argEvent.getData();

    IAuthRequest request = argCmd.getAuthRequest();
    request.setMoreAuthInfo(model.getMoreAuthInfo());
    argCmd.getAuthProcess().processRequest(request);

    return getProcessingResponse(argCmd, request);
  }

  /**
   * Handle a request for more info from the auth process.
   *
   * @param argCmd IAuthorizationCmd
   * @param argResponse IAuthFailedResponse
   * @return IOpResponse
   */
  protected IOpResponse handleNeedMoreInfo(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    argCmd.setOpState(SHOWING_NEED_MORE_INFO);

    IAuthInfo moreAuthInfo = argResponse.getRequiredInfo();
    MoreAuthInfoEditModel model = new MoreAuthInfoEditModel(moreAuthInfo);
    // display a form to gather the additional information
    return HELPER.getShowFormResponse(FormKey.valueOf("AUTH_MORE_INFO"), model);
  }

  protected IOpResponse handlePostPrompt(IAuthorizationCmd argCmd, IXstEvent argEvent, IOpState argState) {
    if (SHOWING_FAILED == argState) {
      return handleShowingFailedResponse(argCmd, argEvent);
    }
    else if (SHOWING_NEED_MORE_INFO == argState) {
      return handleMoreInfoResponse(argCmd, argEvent);
    }
    else if (SHOWING_GET_MANUAL_AUTH == argState) {
      return handleManualAuthInfoResponse(argCmd, argEvent);
    }
    else if (PROCESSING == argState) {
      logger_.warn("unexpected event " + argEvent);
      return HELPER.waitResponse();
    }
    else if (ASKING_TO_SUSPEND == argState) {
      return handleSuspendQuestionResponse(argCmd, argEvent);
    }
    else if (ASKING_TO_CANCEL == argState) {
      return handleCancelQuestionResponse(argCmd, argEvent);
    }
    else {
      logger_.warn("unexpected state " + argState);
      return HELPER.errorNotifyResponse();
    }
  }

  protected IOpResponse handleProcess(IAuthorizationCmd argCmd, IAuthRequest argAuthRequest) {
    IAuthProcess process = argCmd.getAuthProcess();
    // send a request to the auth process
    // note: this may need to be turned into be a custom op request
    process.processRequest(argAuthRequest);

    return getProcessingResponse(argCmd, argAuthRequest);
  }

  protected IOpResponse handleSuccess(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    IFormattable msg = argResponse.getMessage();
    if (msg != null) {
      argCmd.setOpState(COMPLETE);
      return HELPER.getPromptResponse(OpStatus.INCOMPLETE_HALT, PromptKey.valueOf("AUTH_SUCCESS_MESSAGE"),
        msg);
    }
    else {
      return HELPER.completeResponse();
    }
  }

  protected IOpResponse handleSuspend(IAuthorizationCmd argCmd, IXstEvent argEvent) {
    IAuthProcess p = argCmd.getAuthProcess();
    p.cancelRequest(argCmd.getAuthRequest());

    IAuthRequest r = argCmd.getAuthRequest();
    if (r instanceof ISuspendableAuthRequest) {
      ISuspendableAuthRequest request = (ISuspendableAuthRequest) r;
      request.suspend(argCmd.getPersistables());
    }
    return HELPER.getRunChainResponse(OpStatus.ERROR_HALT, getSuspendChainKey(), XstChainActionType.START);
  }

  protected IOpResponse handleSuspendQuestionResponse(IAuthorizationCmd argCmd, IXstEvent argEvent) {
    if (argEvent instanceof IXstDataAction) {
      IXstActionKey actionKey = ((IXstDataAction) argEvent).getActionKey();
      if (XstDataActionKey.YES.equals(actionKey)) {
        return handleSuspend(argCmd, argEvent);
      }
      else {
        return getCancelQuestionResponse(argCmd, argCmd.getAuthRequest());
      }
    }
    return HELPER.waitResponse();
  }

  protected abstract IOpResponse handleTrainingMode(IAuthorizationCmd argCmd);

  protected abstract IOpResponse handleVoid(IAuthorizationCmd argCmd);

  /**
   * Returns whether this operation may be reversed, assuming other baseline conditions for reversal have
   * already been met.
   *
   * @param argCmd the current operational command
   * @return <code>true</code> if this operation may be reversed if all baseline conditions for reversal have
   * been satisfied; <code>false</code> if this operation may not be reversed regardless of any other
   * considerations
   */
  protected boolean isApplicableToReverseImpl(IXstCommand argCmd) {
    return true;
  }

  /**
   * @param argCmd current command
   * @param argAuthRequest current auth request
   * @return <code>true</code> if OK to have a cancel button
   */
  protected boolean isCancelAllowed(IAuthorizationCmd argCmd, IAuthRequest argAuthRequest) {
    return argCmd.getAuthProcess().isCancelAuthAllowed(argAuthRequest);
  }

  protected boolean isNeedingMoreInfo(IAuthResponse argResponse) {
    return false;
  }

  protected TenderAuthEditModel makeEditModel(IAuthResponse argResponse) {
    return new TenderAuthEditModel(argResponse, additionalMessageTranslatable_);
  }

  /**
   * Shared method for how to show that we are processing a request.
   *
   * @param argCmd IAuthorizationCmd
   * @param authRequest IAuthRequest
   * @return IOpResponse
   */
  private IOpResponse getCancelQuestionResponse(IAuthorizationCmd argCmd, IAuthRequest authRequest) {
    if (isCancelAllowed(argCmd, authRequest)) {
      argCmd.setOpState(ASKING_TO_CANCEL);
      return HELPER.getPromptResponse(PromptKey.valueOf("CONFIRM_CANCEL_PENDING_AUTHORIZATION"));
    }
    else {
      return getProcessingResponse(argCmd, authRequest);
    }
  }

  /**
   * Handle user response to get manual auth info.
   *
   * @param argCmd IAuthorizationCmd
   * @param argEvent IXstEvent
   * @return IOpResponse
   */
  protected IOpResponse handleManualAuthInfoResponse(IAuthorizationCmd argCmd, IXstEvent argEvent) {
    if (argEvent == null) {
      return HELPER.waitResponse();
    }

    MoreAuthInfoEditModel model = (MoreAuthInfoEditModel) argEvent.getData();

    IAuthRequest request = argCmd.getAuthRequest();
    request.setMoreAuthInfo(model.getMoreAuthInfo());
    argCmd.getAuthProcess().processRequest(request);

    return getProcessingResponse(argCmd, request);
  }

  // Handles user response to FAILED prompt.
  private IOpResponse handleShowingFailedResponse(IAuthorizationCmd argCmd, IXstEvent argEvent) {
    if (argEvent instanceof IXstDataAction) {
      IXstDataAction action = (IXstDataAction) argEvent;
      IXstActionKey actionKey = action.getActionKey();

      if (RETRY == actionKey) {
        IAuthRequest request = argCmd.getAuthRequest();
        HostList hostList = request.getHostList();
        if (hostList != null) {
          hostList.reset();
        }
        return handleProcess(argCmd, request);
      }
      else if (MANUAL_AUTH == actionKey) {
        return handleManualAuth(argCmd);
      }
      else {
        logger_.warn("unexpected data action key [" + actionKey + "]");
        return handleAbort(argCmd);
      }
    }
    else {
      logger_.warn("unexpected event " + argEvent);
      return HELPER.silentErrorResponse();
    }
  }
}
