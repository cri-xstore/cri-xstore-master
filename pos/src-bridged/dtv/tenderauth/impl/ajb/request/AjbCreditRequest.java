// $Id: AjbCreditRequest.java 881 2015-07-21 15:07:53Z suz.sxie $
package dtv.tenderauth.impl.ajb.request;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import dtv.i18n.FormattableFactory;
import dtv.i18n.IFormattable;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.pos.tender.TenderConstants;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.ICreditAuthRequest;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.ajb.response.AjbCreditResponse;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * Request object for third-party credit card authorization through AJB.<br>
 * Copyright (c) 2005 Datavantage Corporation
 *
 * @author dberkland
 * @version $Revision: 881 $
 * @created Jun 14, 2005
 */
public class AjbCreditRequest
  extends AbstractAjbAuthRequest
  implements ICreditAuthRequest {

  protected static final int ACCOUNT_NBR = 13;
  private static final int EXP_DATE = 14;
  private static final int TRACK2 = 15;
  private static final int VERIFICATION = 31;

  private final DateFormat expDateFormat_ = new SimpleDateFormat("yyMM");

  private static final FormattableFactory FF = FormattableFactory.getInstance();

  /**
   * Constructor
   *
   * @param argType the type of authorization request
   * @param argLine the line item this request is related to, or <code>null</code> is there is no related
   * transaction line item
   * @param argTenderUsageCode tender usage
   */
  public AjbCreditRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
                          TenderUsageCodeType argTenderUsageCode) {

    this(argType, argLine, argTenderUsageCode, "Credit");
  }

  protected AjbCreditRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
                             TenderUsageCodeType argTenderUsageCode, String argReqTranType) {

    super(argType, argLine, argTenderUsageCode, "100", argReqTranType);
    ICreditDebitTenderLineItem line = (ICreditDebitTenderLineItem) argLine;
    setExpirationDate(line);
    setVerificationField(line);
  }

  protected void setExpirationDate(IAuthorizableLineItem argLine) {
    ICreditDebitTenderLineItem line = (ICreditDebitTenderLineItem) argLine;
    if (line != null && line.getExpirationDate() != null) {
      setField(EXP_DATE, expDateFormat_.format(line.getExpirationDate()));
    }
  }

  /**
   * @return the account number
   */
  public String getAccountNbr() {
    return nonNull(((ICreditDebitTenderLineItem) getLineItem()).getAccountNumber());
  }

  /**
   * Gets the message string that will be sent to AJB
   *
   * @return the message string that will be sent to AJB
   */
  @Override
  public String getMessageString() {
    StringBuffer buf = new StringBuffer(100);

    for (int i = 1; i <= getMaxField(); i++ ) {
      if (getField(i) != null) {
        if (i == ACCOUNT_NBR) {
          buf.append(getAccountNbr());
        }
        else if (i == TRACK2) {
          buf.append(getTrack2());
        }
        else {
          buf.append(getField(i));
        }
      }

      if (i < getMaxField()) {
        buf.append(",");
      }
    }

    return buf.toString();
  }

  /**
   * Returns an instance of the appropriate response class for this request
   *
   * @param argFields
   * @param argAuthCode
   * @return The appropriate response object
   */
  @Override
  public IAuthResponse getResponse(String[] argFields, String argAuthCode) {
    return new AjbCreditResponse(this, argFields, argAuthCode);
  }

  /**
   * @return the track 2 data
   */
  public String getTrack2() {
    return nonNull(((ICreditDebitTenderLineItem) getLineItem()).getTrack2());
  }

  /** {@inheritDoc} */
  @Override
  public void loadSensitiveData() {
    ICreditDebitTenderLineItem line = (ICreditDebitTenderLineItem) getLineItem();
    setField(ACCOUNT_NBR, line.getAccountNumber());
    setField(TRACK2, line.getTrack2());
  }

  /** {@inheritDoc} */
  @Override
  public void processReenteredCid(String argCid) {
    ICreditDebitTenderLineItem creditLine = (ICreditDebitTenderLineItem) getLineItem();
    creditLine.setCid(argCid);
    // the retry counter is incremented a couple of levels inside this call
    setVerificationField(creditLine);
    getHostList().reset();
  }

  /** {@inheritDoc} */
  @Override
  public void unloadSensitiveData() {
    setField(ACCOUNT_NBR, null);
    setField(TRACK2, null);
  }

  @Override
  protected IFormattable getSendingMessage() {
    return FF.getTranslatable("_ajbCommunicatorAuthorizing");
  }

  protected String getVerification(ICreditDebitTenderLineItem argLine) {
    String cid = argLine.getCid();
    String postalCode = argLine.getStringProperty(TenderConstants.PROP_CUST_POSTAL_CODE);

    return getVerification(cid, postalCode);
  }

  /** {@inheritDoc} */
  @Override
  protected void setVerificationField(IAuthorizableLineItem argLineItem) {
    ICreditDebitTenderLineItem ccLine = (ICreditDebitTenderLineItem) argLineItem;
    setField(VERIFICATION, getVerification(ccLine));
  }

  private String getVerification(String argCid, String argPostalCode) {
    // Card verification data;
    // PaymentTech (all 3rd party cards except Amex & Discover) requires a zip
    // code when a card number is manually entered, with the zip code sent as: "*AVS_" + zip code;
    // if a CID was entered (required unconditionally for Amex and by Discover
    // if manually keyed) this should be sent as "*CVV2_" + CID.
    //
    // If both zip and CID have been entered, this should be
    // sent as "*AVS_" + zip code + " " + "*CVV2_" + CID.
    if ((argCid != null) && (argCid.trim().length() != 0) && (argPostalCode != null)) {
      incrementCidEntryCount();
      return "*AVS_" + argPostalCode + " *CVV2_" + argCid;
    }
    else if ((argCid != null) && (argCid.trim().length() != 0)) {
      incrementCidEntryCount();
      return "*CVV2_" + argCid;
    }
    else if (argPostalCode != null) {
      return "*AVS_" + argPostalCode;
    }
    else {
      // neither was entered; both are null
      return "";
    }
  }

  /**
   * Returns
   * @param argString
   */
  private String nonNull(String argString) {
    if (argString == null) {
      return "";
    }
    return argString;
  }
}
