-------------------------------------------------------------------------------------------------------------------
--                                                                                                              
-- Script           : db-create.sql                                                                    
-- Description      : Creates the Tablespaces, Roles, Profiles and Users for the XTools application.               
-- Author           : Paul Hiles
-- DB platform:     : Oracle 10g/11g
-- Version          : 4.0.0                                                                                       
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                    
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                    
-------------------------------------------------------------------------------------------------------------------
-- ... .....     Initial Version
-------------------------------------------------------------------------------------------------------------------
SET SERVEROUTPUT ON;
SPOOL dbcreate.log;
EXEC DBMS_OUTPUT.PUT_LINE('db-create.sql');

--
-- Creating the items for Xtools schema objects
-- Create Roles
--

DECLARE
  l_cnt     number;
BEGIN

    SELECT COUNT(*) INTO l_cnt FROM DBA_ROLES
      WHERE ROLE = 'XTOOL_APP';
    
    IF l_cnt = 0 THEN
        EXECUTE IMMEDIATE 'CREATE ROLE xtool_app';
        DBMS_OUTPUT.PUT_LINE('Roles created');
    END IF;
END;
/

GRANT SELECT on dtv.ctl_version_history to xtool_app;
grant select on dtv.ctl_replication_queue to xtool_app;
grant select on dtv.ctl_event_log to xtool_app;

--
-- Create the XTools schema owner
--
DECLARE
  l_cnt     number;
BEGIN

    SELECT COUNT(*) INTO l_cnt FROM DBA_USERS
      WHERE USERNAME = 'XTOOL';
    
    IF l_cnt = 0 THEN
        EXECUTE IMMEDIATE '
            CREATE USER XTOOL
              IDENTIFIED BY password
              DEFAULT TABLESPACE USERS
              TEMPORARY TABLESPACE TEMP
              ACCOUNT UNLOCK';
        DBMS_OUTPUT.PUT_LINE('User created');
    END IF;
END;
/
    
GRANT CREATE SESSION TO XTOOL;
GRANT RESOURCE to XTOOL;
GRANT UNLIMITED TABLESPACE TO XTOOL;
GRANT CREATE TABLE TO XTOOL;
GRANT CREATE SYNONYM TO XTOOL;
GRANT CREATE PUBLIC SYNONYM TO XTOOL;
GRANT DROP PUBLIC SYNONYM TO XTOOL;
GRANT CREATE VIEW TO XTOOL;
GRANT SELECT_CATALOG_ROLE TO XTOOL with admin option;
GRANT SELECT ANY DICTIONARY to XTOOL with admin option; 

GRANT xtool_app to XTOOL  with admin option;

GRANT SELECT on dtv.ctl_version_history to xtool with grant option;
grant select on dtv.ctl_replication_queue to xtool with grant option;
grant select on dtv.ctl_event_log to xtool with grant option;

--
-- Create the XTools application user
--
DECLARE
  l_cnt     number;
BEGIN

    SELECT COUNT(*) INTO l_cnt FROM DBA_USERS
      WHERE USERNAME = 'XTOOLUSERS';
    
    IF l_cnt = 0 THEN
        EXECUTE IMMEDIATE '
            CREATE USER XTOOLUSERS
              IDENTIFIED BY password
              DEFAULT TABLESPACE USERS
              TEMPORARY TABLESPACE TEMP
              ACCOUNT UNLOCK';
        DBMS_OUTPUT.PUT_LINE('User created');
    END IF;
END;
/

GRANT CREATE SESSION TO XTOOLUSERS;
GRANT RESOURCE to XTOOLUSERS;

GRANT xtool_app to xtoolusers;

--
--
-- Creating this sequence for Hibernate.
--
DECLARE
  l_cnt     number;
BEGIN

    SELECT COUNT(*) INTO l_cnt FROM DBA_SEQUENCES
      WHERE SEQUENCE_NAME = 'HIBERNATE_SEQUENCE';
    
    IF l_cnt = 0 THEN
        EXECUTE IMMEDIATE '
            CREATE SEQUENCE XTOOL.HIBERNATE_SEQUENCE
              START WITH 10
              MAXVALUE 999999999999999999999999999
              MINVALUE 0
              NOCYCLE
              NOCACHE
              NOORDER';
        DBMS_OUTPUT.PUT_LINE('Sequence created');
    END IF;
END;
/


GRANT SELECT on XTOOL.HIBERNATE_SEQUENCE TO xtool_app;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'HIBERNATE_SEQUENCE'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM HIBERNATE_SEQUENCE';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM HIBERNATE_SEQUENCE FOR XTOOL.HIBERNATE_SEQUENCE;

SPOOL OFF;