-- ***************************************************************************
-- This script will create / update the database objects for the Xenvironment 
-- database monitor plug-in.
--
-- Product:			Xenvironment
-- Version:         2.5
-- DB platform:     Oracle 10g/11g
-- ***************************************************************************
SET SERVEROUTPUT ON;
SPOOL db-define.log

-------------------------------------------------------------------------------------------------------------------
--
-- View             : v_AllBlockedBlockingSessions 
-- Description      : List of sessions that are either bocking or being blocked. 
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
EXEC DBMS_OUTPUT.PUT_LINE('XTOOL.v_AllBlockedBlockingSessions');

CREATE OR REPLACE VIEW XTOOL.v_AllBlockedBlockingSessions (
	SPID,
    SessionStatus,
    Login,
    blocking_session_id,
    DBName,
    command,
    SQLStatus,
    wait_type,
    wait_resource,
    total_elapsed_time,         -- Total time in waiting
    cpu_time,
    READS,
    WRITES)
AS
select S.SID,
        S.STATUS,
        S.USERNAME,
        NVL(S.BLOCKING_SESSION,0),
        S.SERVICE_NAME,
        A.NAME,
        CASE BLOCKING_SESSION_STATUS
            WHEN 'VALID' THEN s.state
            ELSE ' ' 
        END SQLStatus,
        CASE BLOCKING_SESSION_STATUS
            WHEN 'VALID' THEN s.WAIT_CLASS
            ELSE ' ' 
        END WaitType,
        CASE BLOCKING_SESSION_STATUS
            WHEN 'VALID' THEN o.object_name
            ELSE ' '
        END Object,
        CASE BLOCKING_SESSION_STATUS
            WHEN 'VALID' THEN SECONDS_IN_WAIT
            ELSE 0 
        END SecondsInWait,
        0,
        0,
        0
  from v$session s
  LEFT JOIN dba_objects o on s.ROW_WAIT_OBJ# = o.object_id
  JOIN AUDIT_ACTIONS a on S.COMMAND = A.ACTION
  WHERE S.BLOCKING_SESSION IS NOT NULL
     OR EXISTS (select 1
                  from v$session s2
                  where s.sid = S2.BLOCKING_SESSION);

GRANT SELECT ON XTOOL.v_AllBlockedBlockingSessions TO xtool_app;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_ALLBLOCKEDBLOCKINGSESSIONS'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_ALLBLOCKEDBLOCKINGSESSIONS';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM V_ALLBLOCKEDBLOCKINGSESSIONS for XTOOL.V_ALLBLOCKEDBLOCKINGSESSIONS;
-------------------------------------------------------------------------------------------------------------------
--
-- View             : v_BlockedBlockingSessions 
-- Description      : Count of blocking and blocked sessions 
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
EXEC DBMS_OUTPUT.PUT_LINE('XTOOL.v_BACKUPINFO');

CREATE OR REPLACE VIEW xtool.v_BACKUPINFO
AS
select 1 as id, 
	0 as DAYS_SINCE_LAST_BACKUP,
	cast('' as VARCHAR2(1)) as BACKUP_TYPE, 
	0 AS BACKUP_SIZE, 
	cast('' as VARCHAR2(1)) as DATABASE_NAME, 
	cast('' as timestamp) as LAST_BACKUP
FROM dual;

GRANT SELECT ON xtool.v_BACKUPINFO TO xtool_app;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_BACKUPINFO'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM v_BACKUPINFO';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM v_BACKUPINFO for xtool.v_BACKUPINFO;

--
-- View             : v_BlockedBlockingSessions 
-- Description      : Count of blocking and blocked sessions 
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
EXEC DBMS_OUTPUT.PUT_LINE('XTOOL.v_BlockedBlockingSessions');

CREATE OR REPLACE VIEW XTOOL.v_BlockedBlockingSessions
(
    Blocking,
    Blocked,
    querydatetime)
AS
select count(distinct S.BLOCKING_SESSION),
       count(*),
       sysdate
  from v$session s
  where S.BLOCKING_SESSION IS NOT NULL
;

GRANT SELECT ON XTOOL.v_BlockedBlockingSessions TO xtool_app;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_BLOCKEDBLOCKINGSESSIONS'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_BLOCKEDBLOCKINGSESSIONS';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM V_BLOCKEDBLOCKINGSESSIONS for XTOOL.V_BLOCKEDBLOCKINGSESSIONS;
-------------------------------------------------------------------------------------------------------------------
--
-- View             : v_DATAINFO_DETAILS 
-- Description      : Display tables statistics for all tables not owned by:
--                      TSMSYS, OUTLN, SYSTEM, DBSNMP, SYS, WMSYS
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
EXEC DBMS_OUTPUT.PUT_LINE('XTOOL.v_DATAINFO_DETAILS');

CREATE OR REPLACE VIEW XTOOL.v_DATAINFO_DETAILS (
    dSchema,
    table_name,
    Owner,
    Columns,
    HasClusIdx,
    Row_Count,
    IndexKB,
    DataKB,
    create_date,
    modify_date,
    stats_date           -- Added this column to determine when the last time that stats where gathered
    )
AS
SELECT t.owner,
       T.TABLE_NAME,
       T.OWNER,
       (select count(*) from DBA_TAB_COLS c where T.OWNER = C.OWNER and T.TABLE_NAME = C.TABLE_NAME) Columns, 
	   0 HasClusIdx, 
	   NVL(T.NUM_ROWS,-1),
       --(select sum(bytes)/ 1024 from DBA_EXTENTS e join dba_indexes i on e.owner = I.OWNER and E.SEGMENT_NAME = I.INDEX_NAME where i.OWNER = t.OWNER and i.TABLE_NAME = t.table_name) IndexKB
       cast(0 as float),                   -- Currently returning zero for index size
       cast(NVL(T.BLOCKS,0) * (NVL(P.VALUE,0) / 1024) as float),
       cast(O.CREATED as timestamp),
       cast('' as timestamp),
       cast(T.LAST_ANALYZED as timestamp)
  FROM dba_tables t
  JOIN dba_objects o on T.OWNER = O.OWNER and T.TABLE_NAME = O.OBJECT_NAME
  JOIN v$parameter p on P.NAME = 'db_block_size' 
  where t.owner not in ('TSMSYS', 'OUTLN', 'SYSTEM', 'DBSNMP', 'SYS', 'WMSYS', 'EXFSYS', 'CTXSYS', 'XDB', 'ORDSYS', 
                        'MDSYS', 'OLAPSYS', 'WKSYS', 'SYSMAN', 'EXF$', 'ACCESS$', 'WK_TEST', 'SCOTT', 'TRAINING')  and 		
		t.owner not like 'APPLY$%' and 
		t.owner not like 'FLOWS%';
  
GRANT SELECT ON XTOOL.v_DATAINFO_DETAILS TO xtool_app;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_DATAINFO_DETAILS'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_DATAINFO_DETAILS';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM V_DATAINFO_DETAILS for XTOOL.V_DATAINFO_DETAILS;
-------------------------------------------------------------------------------------------------------------------
--
-- View             : v_DBINFO 
-- Description      :  
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
EXEC DBMS_OUTPUT.PUT_LINE('XTOOL.v_DBINFO');

CREATE OR REPLACE VIEW XTOOL.v_DBINFO (
    id,
    FileGroupName,             -- Added the Tablespace Name
    FileName,
    PhysicalName,
    SizeMB,                     -- Had to change the name of the column due to reserved word
    SpaceUsed,
    SpaceUsedPCT,
    AutoGrow,
    Growth,
    GrowthType,
    MaxSize,
    state_desc)
AS
select 1,
       DF.TABLESPACE_NAME,
       cast(DF.FILE_ID as VARCHAR2(2)),
       DF.FILE_NAME,
       cast(DF.BYTES / 1024 / 1024 as float) SizeMB,
       cast((DF.BYTES - free.FreeBytes) / 1024 / 1024 as float) SpaceUsed,
       round((((df.bytes - free.FreeBytes) / DF.BYTES) * 100), 0) SpaceUsedPCT,
       DF.AUTOEXTENSIBLE,
       DF.INCREMENT_BY / 128 Growth,
       cast('MB' as VARCHAR2(10)) GrowthType,
       DF.MAXBYTES / 1024 / 1024 MaxSize,
       DF.ONLINE_STATUS
  from dba_data_files df,
  (select file_id, sum(bytes) FreeBytes from dba_free_space fs group by file_id) free
  where df.file_id = free.file_id;

GRANT SELECT ON XTOOL.v_DBINFO TO xtool_app;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_DBINFO'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_DBINFO';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM V_DBINFO for XTOOL.V_DBINFO;
-------------------------------------------------------------------------------------------------------------------
--
-- View             : v_HeadBlockingSessions 
-- Description      :  
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
EXEC DBMS_OUTPUT.PUT_LINE('XTOOL.v_HeadBlockingSessions');

CREATE OR REPLACE VIEW XTOOL.v_HeadBlockingSessions
(
    SPID,
    SessionStatus,
    Login,
    DBName,
    Command,
    SQLStatus)
AS
select S.SID,
        S.STATUS,
        S.USERNAME,
        S.SERVICE_NAME,
        A.NAME,
        CASE BLOCKING_SESSION_STATUS
            WHEN 'VALID' THEN s.state
            ELSE ' '
        END SQLStatus
  from v$session s
  JOIN AUDIT_ACTIONS a on S.COMMAND = A.ACTION
  WHERE EXISTS (select 1
                  from v$session s2
                  where s.sid = S2.BLOCKING_SESSION)
;

GRANT SELECT ON XTOOL.V_HEADBLOCKINGSESSIONS TO xtool_app;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_HEADBLOCKINGSESSIONS'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_HEADBLOCKINGSESSIONS';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM V_HEADBLOCKINGSESSIONS for XTOOL.V_HEADBLOCKINGSESSIONS;
-------------------------------------------------------------------------------------------------------------------
--
-- View             : v_Parameters.sql 
-- Description      : List all of the Oracle Parameters 
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
--EXEC DBMS_OUTPUT.PUT_LINE('XTOOL.v_Parameters');

CREATE OR REPLACE VIEW XTOOL.v_Parameters (
    configuration_id,
    cfgname,
    value,
    value2,
    minimum,
    maximum,
    value_in_use,
    description,
    default_value,
    recommend_value)
AS
select cfg.NUM  configuration_id,     
    cfg.NAME as cfgname, 
    0 value,  
    COALESCE (SPCFG.VALUE, CFG.VALUE)  value2, 
    0  minimum, 
    0  maximum, 
    CFG.VALUE  value_in_use, 
    cfg.description,
    CFG.ISDEFAULT  default_value,
    cast('' as VARCHAR2(1))  recommend_value
from v$parameter cfg
JOIN v$spparameter spcfg on SPCFG.NAME = CFG.NAME;

GRANT SELECT ON XTOOL.V_PARAMETERS TO xtool_app;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_PARAMETERS'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_PARAMETERS';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM V_PARAMETERS for XTOOL.V_PARAMETERS;
-------------------------------------------------------------------------------------------------------------------
--
-- View             : v_SESSIONINFO 
-- Description      :  
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
EXEC DBMS_OUTPUT.PUT_LINE('XTOOL.v_SESSIONINFO');

CREATE OR REPLACE VIEW XTOOL.v_SESSIONINFO (
    ID,
    TOTAL_SESSIONS,
    ACTIVE_SESSIONS,
    QTY_BLOCKING_SESSIONS,
    QTY_BLOCKED_SESSIONS
)
AS
select  1,
        ts.value,
        s.value,
        blocking.value,
        blocked.value
  from (select count(*) value from v$session) ts,
  (select count(*) value from v$session s where S.STATUS = 'ACTIVE') s,
  (select count(distinct S.BLOCKING_SESSION) value from v$session s) blocking,
  (select count(*) value from v$session s where S.BLOCKING_SESSION is not NULL) blocked;

GRANT SELECT ON XTOOL.v_SESSIONINFO TO xtool_app;


BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_SESSIONINFO'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_SESSIONINFO';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM V_SESSIONINFO for XTOOL.V_SESSIONINFO;
-------------------------------------------------------------------------------------------------------------------
--
-- View             : v_SESSION_WHO3 
-- Description      :  
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
EXEC DBMS_OUTPUT.PUT_LINE('XTOOL.v_SESSION_WHO3');

CREATE OR REPLACE VIEW XTOOL.v_SESSION_WHO3 (
    SPID,
    Status,
    LoginName,
    Host,
    BlkBy,
    DBName,
    CommandType,
    SQLStatement,
    ObjectName,
    ElapsedMS,
    CPUTime,
    IOReads,
    IOWrites,
    LastWaitType,
    StartTime,
    Protocol,
    ConnectionReads,
    ConnectionWrites,
    ClientAddress,
    Authentication)
AS
SELECT  S.SID,
        S.STATUS,
        S.USERNAME,
        S.MACHINE,
        NVL(S.BLOCKING_SESSION,0),
        S.SERVICE_NAME,
        A.NAME,
        ST.SQL_TEXT,
        cast(' ' as VARCHAR2(2)),                   -- ObjectName 
        sts.value,                                  -- ElapsedMS
        ss.value,                                   -- CPUTime
        (SIO.BLOCK_GETS + SIO.PHYSICAL_READS),      -- IOReads
        SIO.BLOCK_CHANGES,                          -- IOWrites
        WAIT_CLASS,                                 -- LastWaitType
        S.SQL_EXEC_START,                           -- StartTime
        cast('' as VARCHAR2(2)),                    -- Protocol
        (SIO.BLOCK_GETS + SIO.PHYSICAL_READS),       -- Connection Reads
        SIO.BLOCK_CHANGES,                          -- Connection Writes
        cast('' as VARCHAR2(2)),                    -- Client Address
        SI.AUTHENTICATION_TYPE
  FROM v$session s
  JOIN v$sess_io sio on S.SID = SIO.SID
  JOIN (select sid, value, stat_name from v$sess_time_model where stat_name = 'sql execute elapsed time') sts on sts.sid = S.SID 
  JOIN (select sid, value from v$sesstat ss join v$statname sn on ss.STATISTIC# = SN.STATISTIC# where sn.name = 'CPU used by this session') ss on s.sid = ss.sid 
  LEFT JOIN (SELECT DISTINCT SID, AUTHENTICATION_TYPE FROM V$SESSION_CONNECT_INFO) SI ON SI.SID = S.SID
  JOIN AUDIT_ACTIONS a on S.COMMAND = A.ACTION
  --LEFT JOIN dba_objects o on s.ROW_WAIT_OBJ# = o.object_id
  LEFT JOIN v$sqlarea st on S.SQL_ID = ST.SQL_ID;


GRANT SELECT ON XTOOL.v_SESSION_WHO3 TO xtool_app;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_SESSION_WHO3'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_SESSION_WHO3';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM V_SESSION_WHO3 for XTOOL.V_SESSION_WHO3;
-------------------------------------------------------------------------------------------------------------------
--
-- View             : v_SYSCONNECT 
-- Description      :  
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
EXEC DBMS_OUTPUT.PUT_LINE('XTOOL.v_SYSCONNECT');

CREATE OR REPLACE VIEW XTOOL.v_SYSCONNECT (
    ID,
    dbrecovery_modeL_desc,
    USER_ACCESS,
    Connections,
    CPU_BUSY)
AS
SELECT 1,
        D.LOG_MODE,
        D.OPEN_MODE,
        (select distinct count(username) from v$session),
        0
  FROM v$database d;

GRANT SELECT ON XTOOL.v_SYSCONNECT TO xtool_app;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_SYSCONNECT'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_SYSCONNECT';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM V_SYSCONNECT for XTOOL.V_SYSCONNECT;
-------------------------------------------------------------------------------------------------------------------
--
-- View             : v_SYSINFO 
-- Description      : 
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
EXEC DBMS_OUTPUT.PUT_LINE('XTOOL.v_SYSINFO');

CREATE OR REPLACE VIEW XTOOL.v_SYSINFO (
    ID, 
    MachineName, 
    ServerName,
    DataBaseName,
    ProductVersion,        
    ServicePackLevel,
    ProductEdition,        
    LoginName,
    UserName,
    LanguageName,         
    dbcreatedate,
    dbcompatibility_level,
    dbcollation_name,
    dbis_read_only,
    dbis_auto_close_on,
    dbis_auto_shrink_on,
    dbstate,
    dbstate_desc,
    dbis_supplemental_logging,      -- Need to change the name because it was too long
    dbrecovery_model,
    dbrecovery_model_desc,
    starttime
)
AS
SELECT 1 as ID,
    I.HOST_NAME as MachineName,
    I.INSTANCE_NAME as ServerName,
    D.NAME as DataBaseName,
    I.VERSION as ProductVersion,
    cast('N/A' as VARCHAR2(5)) as  ServicePackLevel,
    cast('N/A'  as VARCHAR2(5)) as   ProductEdition,
    SYS_CONTEXT ('USERENV', 'SESSION_USER') as   LoginName,
    SYS_CONTEXT ('USERENV', 'SESSION_USER') as   UserName,
    lang.value as LanguageName,
    D.CREATED  as  dbcreatedate,
    c.value  as  dbcompatibility_level,
    cs.value  as  dbcollation_name,
    D.OPEN_MODE as   dbis_read_only,
    NULL as   dbis_auto_close_on,
    NULL as   dbis_auto_shrink_on,
    I.STATUS as   dbstate,
    i.status as   dbstate_desc,
    NULL  as  dbis_supplemental_logging,
    D.LOG_MODE as   dbrecovery_model,
    D.LOG_MODE as   dbrecovery_model_desc,
    I.STARTUP_TIME as   starttime
    from v$instance i,
         v$database d,
         (select value from v$parameter where name = 'compatible') c,
         (select value from NLS_DATABASE_PARAMETERS where parameter = 'NLS_LANGUAGE') lang,
         (select value from NLS_DATABASE_PARAMETERS where parameter = 'NLS_CHARACTERSET') cs;



GRANT SELECT ON XTOOL.v_SYSINFO TO xtool_app;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_SYSINFO'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_SYSINFO';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM V_SYSINFO for XTOOL.V_SYSINFO;
-------------------------------------------------------------------------------------------------------------------
--
-- View             : v_SYSINFO_CPUMEM 
-- Description      :  
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
EXEC DBMS_OUTPUT.PUT_LINE('XTOOL.v_SYSINFO_CPUMEM');

CREATE OR REPLACE VIEW XTOOL.v_SYSINFO_CPUMEM (
    ID,
    LogicalCPUCount,
    HyperthreadRatio,
    PhysicalCPUCount,
    PhysicalMemoryMB
)
AS
SELECT 1,
        Cores.value,
        (CPUs.value / Cores.value),
        CPUs.value,
        Mem.value
  FROM (select NVL(value,0) value from V$OSSTAT where stat_name = 'NUM_CPUS') Cores,
       (SELECT NVL((select value from V$OSSTAT where stat_name = 'num_cpu_sockets'), (select value from V$OSSTAT where stat_name = 'NUM_CPUS')) AS VALUE FROM DUAL) CPUs,
       (select NVL(value, 0) value from V$OSSTAT where stat_name = 'PHYSICAL_MEMORY_BYTES') Mem;

GRANT SELECT ON XTOOL.v_SYSINFO_CPUMEM TO xtool_app;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_SYSINFO_CPUMEM'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM v_SYSINFO_CPUMEM';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM v_SYSINFO_CPUMEM for XTOOL.v_SYSINFO_CPUMEM;

-------------------------------------------------------------------------------------------------------------------
--
-- View             : v_TableRowStats 
-- Description      :  
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
EXEC DBMS_OUTPUT.PUT_LINE('XTOOL.v_TableRowStats');

CREATE OR REPLACE VIEW XTOOL.v_TableRowStats
(
    Table_Name,
    Row_Count
)
AS
SELECT T.TABLE_NAME,
        NVL(T.NUM_ROWS, -1)
    FROM dba_tables t
    WHERE t.owner not in ('TSMSYS', 'OUTLN', 'SYSTEM', 'DBSNMP', 'SYS', 'WMSYS', 'SYSMAN', 'XDB')  
;

GRANT SELECT ON XTOOL.v_TableRowStats TO xtool_app;

BEGIN
    FOR l_rec IN (SELECT table_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND TABLE_NAME = 'V_TABLEROWSTATS'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_TABLEROWSTATS';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM V_TABLEROWSTATS for XTOOL.V_TABLEROWSTATS;
-------------------------------------------------------------------------------------------------------------------
--
-- View             : v_TopFiveSQLAvgCPU 
-- Description      :  
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
PROMPT XTOOL.v_TopFiveSQLAvgCPU

CREATE OR REPLACE VIEW XTOOL.v_TopFiveSQLAvgCPU (
    sql_id,
    total_cpu_time,
    Avg_CPU_Time,
    excutions,
    last_exec_time,
    first_load,
    last_load,
    buffer_gets,
    buffer_gets_per_exec,
    disk_reads,
    disk_writes,
    sorts,
    elapsed_time,
    query_text,
    total_phy_reads,
    last_phy_reads ,
    total_log_reads,
    last_log_reads,
    total_el_time
)
AS
    SELECT SA.SQL_ID,
            sa.CPU_TIME,
            CASE SA.EXECUTIONS          -- Returns 0 if SA.EXECUTIONS is zero.  This is to 
                WHEN 0 THEN 0           -- prevent divide by zero error. 
                ELSE (sa.CPU_TIME / SA.EXECUTIONS)
            END CASE,
            sa.EXECUTIONS,
            cast(SA.LAST_ACTIVE_TIME as timestamp),
            sa.FIRST_LOAD_TIME,
            SA.LAST_ACTIVE_TIME,
            SA.BUFFER_GETS,
            CASE SA.EXECUTIONS          -- Returns 0 if SA.EXECUTIONS is zero.  This is to 
                WHEN 0 THEN 0           -- prevent divide by zero error. 
                ELSE (SA.BUFFER_GETS / SA.EXECUTIONS)
            END CASE,
            sa.DISK_READS,
            SA.DIRECT_WRITES,
            SA.SORTS,
            SA.ELAPSED_TIME / 1000000,
            SA.SQL_TEXT,
            cast(SA.DISK_READS as integer)total_phy_reads,
            cast(0 as integer) last_phy_reads,
            cast(SA.BUFFER_GETS as integer) total_log_reads,
            cast(0 as integer) last_log_reads,
            cast(SA.ELAPSED_TIME as integer) total_el_time
      FROM v$sqlarea sa
      where SA.EXECUTIONS > 0
        and rownum <= 5
      order by sa.cpu_time desc;



GRANT SELECT ON XTOOL.v_TopFiveSQLAvgCPU TO xtoolusers;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_TOPFIVESQLAVGCPU'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_TOPFIVESQLAVGCPU';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM V_TOPFIVESQLAVGCPU for XTOOL.V_TOPFIVESQLAVGCPU;

GRANT SELECT on V_TOPFIVESQLAVGCPU to xtool_app;
-------------------------------------------------------------------------------------------------------------------
--
-- View             : ViewName 
-- Description      :  
-- Version          : 
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                     --
-------------------------------------------------------------------------------------------------------------------
-- ... .....         Initial Version
-------------------------------------------------------------------------------------------------------------------
PROMPT v_WAITTYPESTATS

CREATE OR REPLACE VIEW XTOOL.V_WAITTYPESTATS (
    wait_type,
    wait_category,
    num_waits,
    wait_time,
    wait_time_avg,
    PctTime
)
AS
        SELECT E.EVENT,
                E.WAIT_CLASS,
                E.TOTAL_WAITS,
                E.TIME_WAITED,
                E.AVERAGE_WAIT,                                                 -- Added this column
                cast((100. * E.TIME_WAITED / SUM(E.TIME_WAITED) OVER()) as float)   AS PctTime2     -- Added this column
          FROM V$SYSTEM_EVENT e
          WHERE WAIT_CLASS <> 'Idle'
            AND rownum < 20
          order by time_waited desc;

GRANT SELECT ON XTOOL.V_WAITTYPESTATS TO xtoolusers;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_WAITTYPESTATS'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_WAITTYPESTATS';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM V_WAITTYPESTATS for XTOOL.V_WAITTYPESTATS;
-- ****************************************************
/* List of ctl_event_log */
EXEC DBMS_OUTPUT.PUT_LINE('xtool.v_ctl_event_log.sql');

CREATE OR REPLACE VIEW xtool.v_ctl_event_log
AS 
select  organization_id, rtl_loc_id, wkstn_id, business_date, operator_party_id, 
		log_level, log_timestamp, source, thread_name, critical_to_Deliver,
		logger_category, log_message
from dtv.ctl_event_log;          

GRANT SELECT ON xtool.v_ctl_event_log TO xtool_app ;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_CTL_EVENT_LOG'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_CTL_EVENT_LOG';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM v_ctl_event_log FOR xtool.v_ctl_version_history;
GRANT SELECT ON xtool.v_ctl_event_log TO xtool_app;
-- ****************************************************
/* List of ctl_replication_queue */
EXEC DBMS_OUTPUT.PUT_LINE('xtool.v_ctl_replication_queue');


CREATE OR REPLACE VIEW xtool.v_ctl_replication_queue
AS 
select  organization_id, rtl_loc_id, wkstn_id, db_trans_id, service_name,
		date_time, expires_after, expires_immediately_flag,
		never_expires_flag, offline_failures, error_failures, replication_data
from repqueue.ctl_replication_queue;


GRANT SELECT ON xtool.v_ctl_replication_queue TO xtool_app ;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_CTL_REPLICATION_QUEUE'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_CTL_REPLICATION_QUEUE';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM v_ctl_replication_queue FOR xtool.v_ctl_replication_queue;
GRANT SELECT ON xtool.v_ctl_replication_queue TO xtool_app;
-- ****************************************************
/* List of ctl_version_history */
EXEC DBMS_OUTPUT.PUT_LINE('xtool.xtool.v_ctl_version_history.sql');

CREATE OR REPLACE VIEW xtool.v_ctl_version_history
AS 
select  organization_id, base_schema_version, customer_schema_version, customer
		base_schema_date, seq
from dtv.ctl_version_history;


GRANT SELECT ON xtool.v_ctl_version_history TO xtool_app ;

BEGIN
    FOR l_rec IN (SELECT synonym_name 
                      FROM all_synonyms
                      WHERE owner = 'PUBLIC'
                        AND table_owner = 'XTOOL'
                        AND SYNONYM_NAME = 'V_CTL_VERSION_HISTORY'
)
    loop
        EXECUTE IMMEDIATE 'DROP PUBLIC SYNONYM V_CTL_VERSION_HISTORY';
        DBMS_OUTPUT.PUT_LINE('Synonym dropped.');
    end loop;
end;
/

CREATE PUBLIC SYNONYM v_ctl_version_history FOR xtool.v_ctl_version_history;
GRANT SELECT ON xtool.v_ctl_version_history TO xtool_app;
