-- ***************************************************************************
-- This script will create / update the database objects for the Xenvironment 
-- database monitor plug-in.
--
-- Product:			Xenvironment
-- Version:         2.5
-- DB platform:     MS SQL Server
-- ***************************************************************************

print '************************************************************************';
print '* Assign actual database for USE statement before running this script! *';
print '************************************************************************';
use [xstore]
GO
-------------------------------------------------------------------------------------------------------------------
--
-- Table		: [xtool].[mssql_default_parms]
-- Description	: Holds the default server configuration values and Micros-Retail suggested values
--
-------------------------------------------------------------------------------------------------------------------

print 'mssql_default_parms';
GO

IF (object_id('[xtool].[mssql_default_parms]')) IS NOT NULL
	DROP TABLE [xtool].[mssql_default_parms]
GO

CREATE TABLE [xtool].[mssql_default_parms] (
	def_parm_id			integer			IDENTITY(1,1),
    parm_name			nvarchar(128)	NOT NULL,
	default_value		nvarchar(254)	NOT NULL,
	recommend_value		nvarchar(254),
	CONSTRAINT [pk_mssql_default_parms] PRIMARY KEY CLUSTERED ([def_parm_id])
);
GO

/* 
 * INDEX: [idx_mssql_default_parms01] 
 */

CREATE INDEX [idx_mssql_default_parms01] ON [xtool].[mssql_default_parms]([parm_name], [default_value])
go

/*
 * Loads the default parameters into the table
 */

GRANT SELECT ON [xtool].[mssql_default_parms] TO xtool_app ;

IF (object_id('[mssql_default_parms]', 'SN')) IS NOT NULL
      DROP SYNONYM mssql_default_parms;
GO

CREATE SYNONYM mssql_default_parms FOR [xtool].[mssql_default_parms] ;
GO
	  
insert into [xtool].[mssql_default_parms] values('Ad Hoc Distributed Queries',0,0);
insert into [xtool].[mssql_default_parms] values('affinity I/O mask',0,NULL);
insert into [xtool].[mssql_default_parms] values('affinity mask',0,NULL);
insert into [xtool].[mssql_default_parms] values('Agent XPs',0,NULL);
insert into [xtool].[mssql_default_parms] values('allow updates',0,NULL);
insert into [xtool].[mssql_default_parms] values('awe enabled',0,NULL);
insert into [xtool].[mssql_default_parms] values('blocked process threshold',0,NULL);
insert into [xtool].[mssql_default_parms] values('c2 audit mode',0,NULL);
insert into [xtool].[mssql_default_parms] values('clr enabled',0,NULL);
insert into [xtool].[mssql_default_parms] values('cost threshold for parallelism',5,NULL);
insert into [xtool].[mssql_default_parms] values('cross db ownership chaining',0,NULL);
insert into [xtool].[mssql_default_parms] values('cursor threshold',-1,NULL);
insert into [xtool].[mssql_default_parms] values('Database Mail XPs',0,NULL);
insert into [xtool].[mssql_default_parms] values('default full-text language',1033,NULL);
insert into [xtool].[mssql_default_parms] values('default language',0,NULL);
insert into [xtool].[mssql_default_parms] values('default trace enabled',1,NULL);
insert into [xtool].[mssql_default_parms] values('disallow results from triggers',0,NULL);
insert into [xtool].[mssql_default_parms] values('fill factor (%)',0,NULL);
insert into [xtool].[mssql_default_parms] values('ft crawl bandwidth (max)',100,NULL);
insert into [xtool].[mssql_default_parms] values('ft crawl bandwidth (min)',0,NULL);
insert into [xtool].[mssql_default_parms] values('ft notify bandwidth (max)',100,NULL);
insert into [xtool].[mssql_default_parms] values('ft notify bandwidth (min)',0,NULL);
insert into [xtool].[mssql_default_parms] values('index create memory (KB)',0,NULL);
insert into [xtool].[mssql_default_parms] values('in-doubt xact resolution',0,NULL);
insert into [xtool].[mssql_default_parms] values('lightweight pooling',0,NULL);
insert into [xtool].[mssql_default_parms] values('locks',0,NULL);
insert into [xtool].[mssql_default_parms] values('max degree of parallelism',0,NULL);
insert into [xtool].[mssql_default_parms] values('max full-text crawl range',4,NULL);
insert into [xtool].[mssql_default_parms] values('max server memory (MB)',2147483647,NULL);
insert into [xtool].[mssql_default_parms] values('max text repl size (B)',65536,NULL);
insert into [xtool].[mssql_default_parms] values('max worker threads',0,NULL);
insert into [xtool].[mssql_default_parms] values('media retention',0,NULL);
insert into [xtool].[mssql_default_parms] values('min memory per query (KB)',1024,NULL);
insert into [xtool].[mssql_default_parms] values('min server memory (MB)',0,NULL);
insert into [xtool].[mssql_default_parms] values('nested triggers',1,NULL);
insert into [xtool].[mssql_default_parms] values('network packet size (B)',4096,NULL);
insert into [xtool].[mssql_default_parms] values('Ole Automation Procedures',0,NULL);
insert into [xtool].[mssql_default_parms] values('open objects',0,NULL);
insert into [xtool].[mssql_default_parms] values('PH timeout (s)',60,NULL);
insert into [xtool].[mssql_default_parms] values('precompute rank',0,NULL);
insert into [xtool].[mssql_default_parms] values('priority boost',0,NULL);
insert into [xtool].[mssql_default_parms] values('query governor cost limit',0,NULL);
insert into [xtool].[mssql_default_parms] values('query wait (s)',-1,NULL);
insert into [xtool].[mssql_default_parms] values('recovery interval (min)',0,NULL);
insert into [xtool].[mssql_default_parms] values('remote access',1,NULL);
insert into [xtool].[mssql_default_parms] values('remote admin connections',0,NULL);
insert into [xtool].[mssql_default_parms] values('remote login timeout (s)',20,NULL);
insert into [xtool].[mssql_default_parms] values('remote proc trans',0,NULL);
insert into [xtool].[mssql_default_parms] values('remote query timeout (s)',600,NULL);
insert into [xtool].[mssql_default_parms] values('Replication XPs',0,NULL);
insert into [xtool].[mssql_default_parms] values('RPC parameter data validation',0,NULL);
insert into [xtool].[mssql_default_parms] values('scan for startup procs',0,NULL);
insert into [xtool].[mssql_default_parms] values('server trigger recursion',1,NULL);
insert into [xtool].[mssql_default_parms] values('set working set size',0,NULL);
insert into [xtool].[mssql_default_parms] values('show advanced options',0,NULL);
insert into [xtool].[mssql_default_parms] values('SMO and DMO XPs',1,NULL);
insert into [xtool].[mssql_default_parms] values('SQL Mail XPs',0,NULL);
insert into [xtool].[mssql_default_parms] values('transform noise words',0,NULL);
insert into [xtool].[mssql_default_parms] values('two digit year cutoff',2049,NULL);
insert into [xtool].[mssql_default_parms] values('user connections',0,NULL);
insert into [xtool].[mssql_default_parms] values('user options',0,NULL);
insert into [xtool].[mssql_default_parms] values('Web Assistant Procedures',0,NULL);
insert into [xtool].[mssql_default_parms] values('xp_cmdshell',0,0);
GO
-- ****************************************************
/* List of Head Blockers */
print 'v_AllBlockedBlockingSessions';
GO

IF (object_id('[xtool].[v_AllBlockedBlockingSessions]')) IS NOT NULL
      DROP VIEW [xtool].[v_AllBlockedBlockingSessions]
GO

CREATE VIEW [xtool].[v_AllBlockedBlockingSessions]
AS 
select      CAST(s.session_id AS int) as 'SPID',
            s.status 'SessionStatus',
            s.login_name 'Login',
            CAST(isNull(r.blocking_session_id,0) AS int) as 'blocking_session_id',
            DB_Name(r.database_id) 'DBName',
            r.command,
            r.status 'SQLStatus',
            r.wait_type,
            r.wait_resource,
            isNull(r.total_elapsed_time,0) total_elapsed_time,
            isNull(r.cpu_time,0) cpu_time,
            isNull(r.logical_reads,0) + isNull(r.reads,0) reads,
            isNull(r.writes,0) writes
  from sys.dm_exec_sessions s
  left join sys.dm_exec_requests r on r.session_id = s.session_id
  where (r.blocking_session_id <> 0)
     or exists (select 1
                  from sys.dm_exec_requests r2
                  where s.session_id = r2.blocking_session_id);
GO            

GRANT SELECT ON xtool.v_AllBlockedBlockingSessions TO xtool_app ;

IF (object_id('[v_AllBlockedBlockingSessions]', 'SN')) IS NOT NULL
      DROP SYNONYM v_AllBlockedBlockingSessions;
GO

CREATE SYNONYM v_AllBlockedBlockingSessions FOR xtool.v_AllBlockedBlockingSessions;
GRANT SELECT ON v_AllBlockedBlockingSessions TO xtool_app;
GO
-- DB and Log backup details returns 2 rows normally for 1 db and log
-- ****************************************************
/* UI View 7 */
print 'v_BACKUPINFO';
GO

IF (object_id('[xtool].[v_BACKUPINFO]')) IS NOT NULL
      DROP VIEW [xtool].[v_BACKUPINFO]
GO

CREATE VIEW [xtool].[v_BACKUPINFO]
AS
select 1 as id, max(isnull(datediff(dd,b.backup_start_date,getdate()),0)) as 'DAYS_SINCE_LAST_BACKUP',
	CONVERT( Varchar, isnull(b.type, '')) as  'BACKUP_TYPE', isnull(b.backup_size, 0) AS BACKUP_SIZE, d.name as DATABASE_NAME, 
	CONVERT( DateTime , MAX(isnull(b.Backup_Finish_Date, ''))) as LAST_BACKUP
	
from  master..sysdatabases d with (nolock) left join msdb..backupset b  with (nolock) on d.name = b.database_name 
	and b.backup_start_date = (select max(backup_start_date) 
                                   from msdb..backupset b2 
                                   where b.database_name = b2.database_name
                                  and b2.type = 'D')
where d.name = (SELECT DB_NAME() AS DataBaseName)
group by d.name, b.type, b.backup_size

union all

select 2 as id, max(isnull(datediff(dd,b.backup_start_date,getdate()),0))  as 'DAYS_SINCE_LAST_BACKUP',
	CONVERT( Varchar, isnull(b.type, '')) as 'BACKUP_TYPE', isnull(b.backup_size, 0) AS BACKUP_SIZE, d.name as DATABASE_NAME, 
	CONVERT( DateTime , MAX(isnull(b.Backup_Finish_Date, ''))) as LAST_BACKUP

from  master..sysdatabases d with (nolock) join msdb..backupset b  with (nolock) on d.name = b.database_name
	and b.backup_start_date = (select max(backup_start_date) 
                                   from msdb..backupset b2 
                                   where b.database_name = b2.database_name
                                   and b2.type = 'L')
where d.name = (SELECT DB_NAME() AS DataBaseName)
group by d.name, b.type, b.backup_size;
GO            

GRANT SELECT ON xtool.v_BACKUPINFO TO xtool_app ;

IF (object_id('[v_BACKUPINFO]', 'SN')) IS NOT NULL
      DROP SYNONYM v_BACKUPINFO;
GO

CREATE SYNONYM v_BACKUPINFO FOR xtool.v_BACKUPINFO;
GRANT SELECT ON v_BACKUPINFO TO xtool_app;
GO
-- DB and Log backup details returns 2 rows normally for 1 db and log
-- ****************************************************
/* Count of blocking and blocked sessions */
print 'v_BlockedBlockingSessions';
GO

IF (object_id('[xtool].[v_BlockedBlockingSessions]')) IS NOT NULL
      DROP VIEW [xtool].[v_BlockedBlockingSessions]
GO

CREATE VIEW [xtool].[v_BlockedBlockingSessions]
AS

SELECT  count(DISTINCT blocking_session_id) 'Blocking',
        count(*) 'Blocked',
        getdate() querydatetime
  FROM    sys.dm_exec_requests
  WHERE   blocking_session_id <> 0;
GO            

GRANT SELECT ON xtool.v_BlockedBlockingSessions TO xtool_app ;

IF (object_id('[v_BlockedBlockingSessions]', 'SN')) IS NOT NULL
      DROP SYNONYM v_BlockedBlockingSessions;
GO

CREATE SYNONYM v_BlockedBlockingSessions FOR xtool.v_BlockedBlockingSessions;
GRANT SELECT ON v_BlockedBlockingSessions TO xtool_app;
GO
/* SpaceUsed type information for every table in a SQL Server 2005 database*/
print 'v_DATAINFO_DETAILS';
GO

IF (object_id('[xtool].[v_DATAINFO_DETAILS]')) IS NOT NULL
      DROP VIEW [xtool].[v_DATAINFO_DETAILS]
GO

CREATE VIEW [xtool].[v_DATAINFO_DETAILS]
AS
SELECT  SCHEMA_NAME(tbl.schema_id) AS [dSchema],
        tbl.name as table_name,
        COALESCE(( SELECT  name
                   FROM    sys.database_principals AS pr
                   WHERE   ( principal_id = tbl.principal_id ) ), SCHEMA_NAME(tbl.schema_id)) AS Owner,
        tbl.max_column_id_used AS Columns,
        CAST(CASE idx.index_id
               WHEN 1 THEN 1
               ELSE 0
             END AS bit) AS HasClusIdx,
        COALESCE(( SELECT  SUM(rows) AS Expr1
                   FROM    sys.partitions AS spart
                   WHERE   ( object_id = tbl.object_id )
                           AND ( index_id < 2 ) ), 0) AS [Row_Count],
        COALESCE(( SELECT  CAST(v.low / 1024.0 AS float) * SUM(a.used_pages - CASE WHEN a.type <> 1 THEN a.used_pages
                                                                                   WHEN p.index_id < 2 THEN a.data_pages
                                                                                   ELSE 0
                                                                              END) AS Expr1
                   FROM    sys.indexes AS i
                   INNER JOIN sys.partitions AS p
                   ON      p.object_id = i.object_id
                           AND p.index_id = i.index_id
                   INNER JOIN sys.allocation_units AS a
                   ON      a.container_id = p.partition_id
                   WHERE   ( i.object_id = tbl.object_id ) ), 0.0) AS IndexKB,
        COALESCE(( SELECT  CAST(v.low / 1024.0 AS float) * SUM(CASE WHEN a.type <> 1 THEN a.used_pages
                                                                    WHEN p.index_id < 2 THEN a.data_pages
                                                                    ELSE 0
                                                               END) AS Expr1
                   FROM    sys.indexes AS i
                   INNER JOIN sys.partitions AS p
                   ON      p.object_id = i.object_id
                           AND p.index_id = i.index_id
                   INNER JOIN sys.allocation_units AS a
                   ON      a.container_id = p.partition_id
                   WHERE   ( i.object_id = tbl.object_id ) ), 0.0) AS DataKB,
        tbl.create_date,
        tbl.modify_date,
        isNull(stats_date(tbl.object_id, 1), '') as stats_date
FROM    sys.tables AS tbl
INNER JOIN sys.indexes AS idx
	ON      idx.object_id = tbl.object_id
    	    AND idx.index_id < 2
INNER JOIN master.dbo.spt_values AS v
	ON      v.number = 1
    	    AND v.type = 'E'
	where tbl.type = 'u'
;
GO            

GRANT SELECT ON xtool.v_DATAINFO_DETAILS TO xtool_app ;

IF (object_id('[v_DATAINFO_DETAILS]', 'SN')) IS NOT NULL
      DROP SYNONYM v_DATAINFO_DETAILS;
GO

CREATE SYNONYM v_DATAINFO_DETAILS FOR xtool.v_DATAINFO_DETAILS;
GRANT SELECT ON v_DATAINFO_DETAILS TO xtool_app;
GO
/* UI View 4 */
/* Individual File Sizes and space available for current database
 Look at how large and how full the files are and where they are located
 Make sure the transaction log is not full!!*/
print 'v_DBINFO';
GO 

IF (object_id('[xtool].[v_DBINFO]')) IS NOT NULL
      DROP VIEW [xtool].[v_DBINFO]
GO

CREATE VIEW [xtool].[v_DBINFO]
AS
SELECT  1 as id ,
		CASE df.data_space_id
			WHEN 0 THEN 'LOG'
			ELSE  ds.name
		END AS [FileGroupName],								-- File Group Name
		df.name AS [FileName], 
		df.physical_name AS [PhysicalName], 
		round((cast(df.size as float) / 128), 2) AS [SizeMB], 
		round(cast((FILEPROPERTY(df.name, 'SpaceUsed') / 128.0)as float) ,2) AS [SpaceUsed],	--Changed from Available Space to Used Space
		cast(ROUND(((FILEPROPERTY(df.name, 'SpaceUsed')/ 128.0) / (cast(df.size as decimal) / 128)) * 100, 0) as int)
			AS [SpaceUsedPCT],								-- Space Used %
		' ' AS [AutoGrow],
		CASE is_percent_growth								-- Growth by parameter
			WHEN 0 THEN growth / 128
			ELSE growth
		END AS [Growth],
		CASE is_percent_growth 								-- Growth by Percent or MB
			WHEN 0 THEN 'MB'
			ELSE 'PCT'
		END AS [GrowthType],								-- Data file max size
		CASE df.max_size
			WHEN -1 THEN df.max_size
			ELSE max_size / 128
		END as [MaxSize],					
		state_desc											-- Added data file state
FROM sys.database_files df
LEFT JOIN sys.data_spaces ds on ds.data_space_id = df.data_space_id;
GO
          
GRANT SELECT ON xtool.v_DBINFO TO xtool_app ;

IF (object_id('[v_DBINFO]', 'SN')) IS NOT NULL
      DROP SYNONYM v_DBINFO;
GO

CREATE SYNONYM v_DBINFO FOR xtool.v_DBINFO;
GRANT SELECT ON v_DBINFO TO xtool_app;
GO
-- ****************************************************
/* List of Head Blockers */
print 'v_HeadBlockingSessions';
GO

IF (object_id('[xtool].[v_HeadBlockingSessions]')) IS NOT NULL
      DROP VIEW [xtool].[v_HeadBlockingSessions]
GO

CREATE VIEW [xtool].[v_HeadBlockingSessions]
AS

select      CAST(s.session_id AS int) as 'SPID',
            s.status 'SessionStatus',
            s.login_name 'Login',
            DB_Name(r.database_id) 'DBName',
            r.command 'Command',
            r.status 'SQLStatus'
  from sys.dm_exec_sessions s
  left join sys.dm_exec_requests r on r.session_id = s.session_id
  where (r.blocking_session_id is NULL or blocking_session_id = 0)
    and exists (select 1
                  from sys.dm_exec_requests r2
                  where s.session_id = r2.blocking_session_id);
GO            

GRANT SELECT ON xtool.v_HeadBlockingSessions TO xtool_app ;

IF (object_id('[v_HeadBlockingSessions]', 'SN')) IS NOT NULL
      DROP SYNONYM v_HeadBlockingSessions;
GO

CREATE SYNONYM v_HeadBlockingSessions FOR xtool.v_HeadBlockingSessions;
GRANT SELECT ON v_HeadBlockingSessions TO xtool_app;
GO
print 'v_Parameters';
GO

if (object_id('[xtool].[v_Parameters]')) IS NOT NULL
      DROP VIEW [xtool].[v_Parameters]
GO

CREATE VIEW [xtool].[v_Parameters]
AS
select CAST(cfg.configuration_id as int) as configuration_id, 
	cfg.name as cfgname, 
	CAST(cfg.value as int) as value, 
	CAST(cfg.minimum as int) as minimum, 
	CAST(cfg.maximum as int) as maximum, 
	CAST(cfg.value_in_use as varchar(254)) as value_in_use, 
	cfg.description,
	isnull(dflt.default_value, '') as default_value,
	isnull(dflt.recommend_value, '') as recommend_value
from sys.configurations as cfg, mssql_default_parms dflt
where (cfg.name = dflt.parm_name)
GO

GRANT SELECT ON xtool.v_Parameters TO xtool_app ;

if (object_id('[v_Parameters]', 'SN')) IS NOT NULL
      DROP SYNONYM v_Parameters;
GO

CREATE SYNONYM v_Parameters FOR xtool.v_Parameters;
GRANT SELECT ON v_Parameters TO xtool_app;
GO
/* UI View 6 */
/* This is used by the overview tab to display
   simple session stats */
print 'v_SESSIONINFO';
GO

IF (object_id('[xtool].[v_SESSIONINFO]')) IS NOT NULL
      DROP VIEW [xtool].[v_SESSIONINFO]
GO

CREATE VIEW [xtool].[v_SESSIONINFO]
AS
select 1 as id,
	-- total sessions
	(select count(*) from sys.dm_exec_sessions) AS TOTAL_SESSIONS,
	--Active Sessions
	(select count(*) from sys.dm_exec_requests) AS ACTIVE_SESSIONS,
	-- qty blocking sessions
	(select count(distinct blocking_session_id) from sys.dm_exec_requests where blocking_session_id <> 0) AS QTY_BLOCKING_SESSIONS,
	-- qty blocked sessions
	(select count(blocking_session_id) from sys.dm_exec_requests where blocking_session_id <> 0) AS QTY_BLOCKED_SESSIONS;
GO            

GRANT SELECT ON xtool.v_SESSIONINFO TO xtool_app ;

IF (object_id('[v_SESSIONINFO]', 'SN')) IS NOT NULL
      DROP SYNONYM v_SESSIONINFO;
GO

CREATE SYNONYM v_SESSIONINFO FOR xtool.v_SESSIONINFO;
GRANT SELECT ON v_SESSIONINFO TO xtool_app;
GO
/* UI View 11 */
/* This view is used by the session details tab similar to sp_who and sp_who2 in MS-SQL */
print 'v_SESSION_WHO3';
GO 

if (object_id('[xtool].[v_SESSION_WHO3]')) IS NOT NULL
      DROP VIEW [xtool].[v_SESSION_WHO3]
GO

CREATE VIEW [xtool].[v_SESSION_WHO3] AS
select CAST(isnull(ses.session_id,0) as int) as SPID
	,Status             = ses.status 
	,LoginName          = ses.login_name 
	,Host               = ses.host_name 
	,CAST(isnull(er.blocking_session_id,0) as int) as BlkBy
	,DBName             = DB_Name(er.database_id) 
	,CommandType        = er.command 
	,CAST(st.text as varchar(254)) as SQLStatement
	,ObjectName         = OBJECT_NAME(st.objectid) 
	,ElapsedMS          = isnull(er.total_elapsed_time,0) 
	,CPUTime            = isnull(er.cpu_time, 0)
	,CAST(isnull(er.logical_reads, 0) + isnull(er.reads, 0) as numeric(19,0)) as IOReads
	,CAST(isnull(er.writes, 0) as numeric(19,0)) as IOWrites
	,LastWaitType       = er.last_wait_type 
	,StartTime          = er.start_time 
	,Protocol           = con.net_transport 
	,ConnectionWrites   = isnull(con.num_writes,0)   
	,ConnectionReads    = isnull(con.num_reads,0) 
	,ClientAddress      = con.client_net_address 
	,Authentication     = con.auth_scheme 

	FROM sys.dm_exec_sessions ses 	
	LEFT JOIN sys.dm_exec_requests er ON ses.session_id = er.session_id 
		OUTER APPLY sys.dm_exec_sql_text(er.sql_handle) st 
	LEFT JOIN sys.dm_exec_connections con ON con.session_id = ses.session_id 

GO
          
GRANT SELECT ON xtool.v_SESSION_WHO3 TO xtool_app ;

if (object_id('[v_SESSION_WHO3]', 'SN')) IS NOT NULL
      DROP SYNONYM v_SESSION_WHO3;
GO

CREATE SYNONYM v_SESSION_WHO3 FOR xtool.v_SESSION_WHO3;
GRANT SELECT ON v_SESSION_WHO3 TO xtool_app;
GO
/* UI View 5 */
print 'v_SYSCONNECT';
GO

IF (object_id('[xtool].[v_SYSCONNECT]')) IS NOT NULL
      DROP VIEW [xtool].[v_SYSCONNECT]
GO

CREATE VIEW [xtool].[v_SYSCONNECT]
AS
SELECT  1 as id,
(select recovery_model_desc FROM SYS.DATABASES WHERE NAME =DB_NAME()) AS dbrecovery_model_desc,
(select user_access_desc from sys.databases where name = (SELECT DB_NAME() AS DataBaseName)) as USER_ACCESS,
(SELECT @@Connections ) as CONNECTIONS,
(select @@CPU_BUSY) AS CPU_BUSY;
GO            

GRANT SELECT ON xtool.v_SYSCONNECT TO xtool_app ;

IF (object_id('[v_SYSCONNECT]', 'SN')) IS NOT NULL
      DROP SYNONYM v_SYSCONNECT;
GO

CREATE SYNONYM v_SYSCONNECT FOR xtool.v_SYSCONNECT;
GRANT SELECT ON v_SYSCONNECT TO xtool_app
GO
/* UI View 1 & 3 */
print 'v_SYSINFO';
GO

IF (object_id('[xtool].[v_SYSINFO]')) IS NOT NULL
      DROP VIEW [xtool].[v_SYSINFO]
GO

CREATE VIEW [xtool].[v_SYSINFO]
AS
SELECT  1 as id, 
		CONVERT(varchar(100), SERVERPROPERTY('MachineName')) as MachineName, 
		@@SERVERNAME AS ServerName,
        DB_NAME() AS DataBaseName,
        convert(varchar(100), SERVERPROPERTY('productversion')) AS ProductVersion,		
		convert(varchar(100), SERVERPROPERTY('productlevel')) AS ServicePackLevel,
		convert(varchar(100), SERVERPROPERTY('edition')) AS ProductEdition,		
        SYSTEM_USER AS LoginName,
        USER AS UserName,
        @@LANGUAGE AS LanguageName, 		
		(select CREATE_DATE FROM SYS.DATABASES WHERE NAME = DB_NAME()) AS dbcreatedate,
		(select compatibility_level FROM SYS.DATABASES WHERE NAME =DB_NAME()) AS dbcompatibility_level,
		(select collation_name FROM SYS.DATABASES WHERE NAME =DB_NAME()) AS dbcollation_name,
		(select is_read_only FROM SYS.DATABASES WHERE NAME =DB_NAME()) AS dbis_read_only,
		(select is_auto_close_on FROM SYS.DATABASES WHERE NAME =DB_NAME()) AS dbis_auto_close_on,
		(select is_auto_shrink_on FROM SYS.DATABASES WHERE NAME =DB_NAME()) AS dbis_auto_shrink_on,
		(select "state" FROM SYS.DATABASES WHERE NAME =DB_NAME()) AS dbstate,
		(select state_desc FROM SYS.DATABASES WHERE NAME =DB_NAME()) AS dbstate_desc,
		(select is_supplemental_logging_enabled FROM SYS.DATABASES WHERE NAME =DB_NAME()) AS dbis_supplemental_logging_enabled,
		(select recovery_model FROM SYS.DATABASES WHERE NAME =DB_NAME()) AS dbrecovery_model,
		(select recovery_model_desc FROM SYS.DATABASES WHERE NAME =DB_NAME()) AS dbrecovery_model_desc,
		(select login_time FROM sys.dm_exec_sessions WHERE session_id = 1) as starttime;
GO            

GRANT SELECT ON xtool.v_SYSINFO TO xtool_app ;

IF (object_id('[v_SYSINFO]', 'SN')) IS NOT NULL
      DROP SYNONYM v_SYSINFO;
GO

CREATE SYNONYM v_SYSINFO FOR xtool.v_SYSINFO ;
GRANT SELECT ON v_SYSINFO TO xtool_app;
GO
/* UI View 2 */
/* log/phy cpu qty, thread ratio, physical mem*/
print 'v_SYSINFO_CPUMEM';
GO

IF (object_id('[xtool].[v_SYSINFO_CPUMEM]')) IS NOT NULL
      DROP VIEW [xtool].[v_SYSINFO_CPUMEM]
GO

CREATE VIEW [xtool].[v_SYSINFO_CPUMEM]
AS
SELECT  1 as id, 
		cpu_count AS LogicalCPUCount, 
		hyperthread_ratio AS HyperthreadRatio, 
		cpu_count / hyperthread_ratio AS PhysicalCPUCount, 
		physical_memory_in_bytes / 1048576 AS PhysicalMemoryMB
FROM	sys.dm_os_sys_info;
GO            

GRANT SELECT ON xtool.v_SYSINFO_CPUMEM TO xtool_app ;

IF (object_id('[v_SYSINFO_CPUMEM]', 'SN')) IS NOT NULL
      DROP SYNONYM v_SYSINFO_CPUMEM;
GO

CREATE SYNONYM v_SYSINFO_CPUMEM FOR xtool.v_SYSINFO_CPUMEM ;
GRANT SELECT ON v_SYSINFO_CPUMEM TO xtool_app;
GO
-- Stats view for table/row counts
print 'v_TableRowStats';
GO

IF (object_id('[xtool].[v_TableRowStats]')) IS NOT NULL
      DROP VIEW [xtool].[v_TableRowStats]
GO

CREATE VIEW [xtool].[v_TableRowStats]
AS
-- Get Table names, row counts
-- This view should be access with the clause
-- ORDER BY SUM(Rows) DESC;

	SELECT OBJECT_NAME(object_id) AS Table_Name, 
		   SUM(Rows) AS Row_Count
	FROM sys.partitions 
	WHERE index_id < 2 --ignore the partitions from the non-clustered index if any
		AND OBJECT_NAME(object_id) NOT LIKE 'sys%'
		AND OBJECT_NAME(object_id) NOT LIKE 'queue_%' 
		AND OBJECT_NAME(object_id) NOT LIKE 'filestream_tombstone%' 
	GROUP BY object_id;
-- Which tables are largest, which maybe candidates for data cleanup?
GO            

GRANT SELECT ON xtool.v_TableRowStats TO xtool_app ;

IF (object_id('[v_TableRowStats]', 'SN')) IS NOT NULL
	DROP SYNONYM v_TableRowStats;
GO	

CREATE SYNONYM v_TableRowStats FOR xtool.v_TableRowStats ;
GRANT SELECT ON v_TableRowStats TO xtool_app;
GO
print'v_TopFiveSQLAvgCPU';
GO

IF (object_id('[xtool].[v_TopFiveSQLAvgCPU]')) IS NOT NULL
      DROP VIEW [xtool].[v_TopFiveSQLAvgCPU]
GO

CREATE VIEW [xtool].[v_TopFiveSQLAvgCPU]
AS
--The following query shows the top 5 SQL statements with high average CPU consumption.
SELECT TOP 5
	total_worker_time/execution_count AS Avg_CPU_Time,
	last_execution_time as last_exec_time,
	total_physical_reads as total_phy_reads,
	last_physical_reads as last_phy_reads,
	total_logical_reads as total_log_reads,
	last_logical_reads as last_log_reads,
	total_elapsed_time as total_el_time,
	CONVERT (Varchar(255), (SELECT SUBSTRING(text,statement_start_offset/2,(CASE WHEN statement_end_offset = -1 then LEN(CONVERT(nvarchar(max), text)) * 2 ELSE statement_end_offset end -statement_start_offset)/2) FROM sys.dm_exec_sql_text(sql_handle))) AS query_text
FROM sys.dm_exec_query_stats 
ORDER BY Avg_CPU_Time DESC;
GO

GRANT SELECT ON xtool.v_TopFiveSQLAvgCPU TO xtool_app ;

IF (object_id('[v_TopFiveSQLAvgCPU]', 'SN')) IS NOT NULL
      DROP SYNONYM v_TopFiveSQLAvgCPU;
GO

CREATE SYNONYM v_TopFiveSQLAvgCPU FOR xtool.v_TopFiveSQLAvgCPU;
GRANT SELECT ON v_TopFiveSQLAvgCPU TO xtool_app;
GO
print'v_TopFiveSQLAvgCPU2';
GO

IF (object_id('[xtool].[v_TopFiveSQLAvgCPU2]')) IS NOT NULL
      DROP VIEW [xtool].[v_TopFiveSQLAvgCPU2]
GO

CREATE VIEW [xtool].[v_TopFiveSQLAvgCPU2] (
    sql_id, 
	time_cpu_total,
    executions,
    first_load,
    last_load,
    buffer_gets,
    buffer_gets_per_exec,
    disk_reads,
    disk_writes,
    sorts,
    elapsed_time,
    query_text
)
AS
--The following query shows the top 5 SQL statements with high average CPU consumption.
SELECT TOP 5
	-1,	
	total_worker_time, 
	execution_count, 
	creation_time, 
	last_execution_time, 
	total_logical_reads,
	(total_logical_reads / execution_count),
	total_physical_reads, 
	total_logical_writes, 
	-1,
	total_elapsed_time as total_el_time,
	CONVERT (Varchar(1024), (SELECT SUBSTRING(text,statement_start_offset/2,(CASE WHEN statement_end_offset = -1 then LEN(CONVERT(nvarchar(max), text)) * 2 ELSE statement_end_offset end -statement_start_offset)/2) FROM sys.dm_exec_sql_text(sql_handle))) AS query_text
FROM sys.dm_exec_query_stats 
ORDER BY total_worker_time DESC;
GO

GRANT SELECT ON xtool.v_TopFiveSQLAvgCPU2 TO xtool_app ;

IF (object_id('[v_TopFiveSQLAvgCPU2]', 'SN')) IS NOT NULL
      DROP SYNONYM v_TopFiveSQLAvgCPU2;
GO

CREATE SYNONYM v_TopFiveSQLAvgCPU2 FOR xtool.v_TopFiveSQLAvgCPU2;
GRANT SELECT ON v_TopFiveSQLAvgCPU2 TO xtool_app;
GO
/* UI View 1 & 3 */
print 'v_WAITTYPESTATS';
GO

IF (object_id('[xtool].[v_WAITTYPESTATS]')) IS NOT NULL
      DROP VIEW [xtool].[v_WAITTYPESTATS]
GO

CREATE VIEW [xtool].[v_WAITTYPESTATS]
AS
SELECT TOP 20 wait_type,
        case 
                  when wait_type like N'LCK_M_%' then N'Lock'
                  when wait_type like N'LATCH_%' then N'Latch'
                  when wait_type like N'PAGELATCH_%' then N'Buffer Latch'
                  when wait_type like N'PAGEIOLATCH_%' then N'Buffer IO'
                  when wait_type like N'RESOURCE_SEMAPHORE_%' then N'Compilation'
                  when wait_type = N'SOS_SCHEDULER_YIELD' then N'Scheduler Yield'
                  when wait_type in (N'LOGMGR', N'LOGBUFFER', N'LOGMGR_RESERVE_APPEND', N'LOGMGR_FLUSH', N'WRITELOG') then N'Logging'
                  when wait_type in (N'ASYNC_NETWORK_IO', N'NET_WAITFOR_PACKET') then N'Network IO'
                  when wait_type in (N'CXPACKET', N'EXCHANGE') then N'Parallelism'
                  when wait_type in (N'RESOURCE_SEMAPHORE', N'CMEMTHREAD', N'SOS_RESERVEDMEMBLOCKLIST') then N'Memory'
                  when wait_type like N'CLR_%' 
                    or wait_type like N'SQLCLR%' then N'CLR'
                  when wait_type like N'DBMIRROR%' 
                    or wait_type = N'MIRROR_SEND_MESSAGE' then N'Mirroring'
                  when wait_type like N'XACT%' 
                    or wait_type like N'DTC_%' 
                    or wait_type like N'TRAN_MARKLATCH_%' 
                    or wait_type like N'MSQL_XACT_%' 
                    or wait_type = N'TRANSACTION_MUTEX' then N'Transaction'
                  when wait_type like N'SLEEP_%' 
                    or wait_type in(N'LAZYWRITER_SLEEP', N'SQLTRACE_BUFFER_FLUSH', N'WAITFOR', N'WAIT_FOR_RESULTS') then N'Sleep'
                  else N'Other'
            end AS wait_category,
        waiting_tasks_count AS num_waits,
        wait_time_ms AS wait_time,
        (wait_time_ms / waiting_tasks_count) as wait_time_avg,
        cast(100. * wait_time_ms / SUM(wait_time_ms) OVER() as float) AS PctTime     -- Added this column
FROM          sys.dm_os_wait_stats e
WHERE         waiting_tasks_count > 0
              AND (wait_type NOT like N'SLEEP_%' 
                     AND wait_type NOT in(N'LAZYWRITER_SLEEP', N'SQLTRACE_BUFFER_FLUSH', N'WAITFOR', N'WAIT_FOR_RESULTS'))
ORDER BY      wait_time_ms DESC;
GO            

GRANT SELECT ON xtool.v_WAITTYPESTATS TO xtool_app ;

IF (object_id('[v_WAITTYPESTATS]', 'SN')) IS NOT NULL
      DROP SYNONYM v_WAITTYPESTATS;
GO

CREATE SYNONYM v_WAITTYPESTATS FOR xtool.v_WAITTYPESTATS ;
GRANT SELECT ON v_WAITTYPESTATS TO xtool_app;
GO
-- ****************************************************
/* List of ctl_event_log */
print 'v_ctl_event_log';
GO

IF (object_id('[xtool].[v_ctl_event_log]')) IS NOT NULL
      DROP VIEW [xtool].[v_ctl_event_log]
GO

CREATE VIEW [xtool].[v_ctl_event_log]
AS 
select 	1 as id, 
		isNull(organization_id, 0) as organization_id, 
		isNull(rtl_loc_id, 0) as rtl_loc_id, 
		isNull(wkstn_id, 0) as wkstn_id, 
		isNull(business_date, '01-01-1970 00:00:01.000') as business_date, 
		isNull(operator_party_id, 0) as operator_party_id, 
		isNull(log_level, '') as log_level, 
		log_timestamp, 
		isNull(source, '') as source, 
		isNull(thread_name, '') as thread_name, critical_to_Deliver,
		logger_category, log_message
from ctl_event_log;
GO            

GRANT SELECT ON xtool.v_ctl_event_log TO xtool_app ;

IF (object_id('[v_ctl_event_log]', 'SN')) IS NOT NULL
      DROP SYNONYM v_ctl_event_log;
GO

CREATE SYNONYM v_ctl_event_log FOR xtool.v_ctl_event_log;
GRANT SELECT ON v_ctl_event_log TO xtool_app;
GO
-- ****************************************************
/* List of ctl_replication_queue */
print 'v_ctl_replication_queue';
GO

IF (object_id('[xtool].[v_ctl_replication_queue]')) IS NOT NULL
      DROP VIEW [xtool].[v_ctl_replication_queue]
GO

CREATE VIEW [xtool].[v_ctl_replication_queue]
AS 
select 1 as id, 
		isNull(organization_id, 0) as organization_id, 
		isNull(rtl_loc_id, 0) as rtl_loc_id, 
		isNull(wkstn_id, 0) as wkstn_id, 
		isNull(db_trans_id, 0) as db_trans_id, 
		isNull(service_name, '') as service_name,
		isNull(date_time, 0) as date_time, 
		isNull(expires_after, 0) as expires_after, 
		isNull(expires_immediately_flag, 0) as expires_immediately_flag,
		isNull(never_expires_flag, 0) as never_expires_flag, 
		isNull(offline_failures, 0) as offline_failures, 
		isNull(error_failures, 0) as error_failures, 
		isNull(replication_data, '') as replication_data
from ctl_replication_queue;
GO            

GRANT SELECT ON xtool.v_ctl_replication_queue TO xtool_app ;

IF (object_id('[v_ctl_replication_queue]', 'SN')) IS NOT NULL
      DROP SYNONYM v_ctl_replication_queue;
GO

CREATE SYNONYM v_ctl_replication_queue FOR xtool.v_ctl_replication_queue;
GRANT SELECT ON v_ctl_replication_queue TO xtool_app;
GO
-- ****************************************************
/* List of ctl_version_history */
print 'v_ctl_version_history';
GO

IF (object_id('[xtool].[v_ctl_version_history]')) IS NOT NULL
      DROP VIEW [xtool].[v_ctl_version_history]
GO

CREATE VIEW [xtool].[v_ctl_version_history]
AS 
select 1 as id, 
	isNull(organization_id, 0) as organization_id, 
	isNull(base_schema_version, '') as base_schema_version, 
	isNull(customer_schema_version, '') as customer_schema_version, 
	isNull(customer, '') as customer ,
	isNull(base_schema_date, '') as base_schema_date, 
	isNull(seq, 0) as seq
from ctl_version_history;
GO            

GRANT SELECT ON xtool.v_ctl_version_history TO xtool_app ;

IF (object_id('[v_ctl_version_history]', 'SN')) IS NOT NULL
      DROP SYNONYM v_ctl_version_history;
GO

CREATE SYNONYM v_ctl_version_history FOR xtool.v_ctl_version_history;
GRANT SELECT ON v_ctl_version_history TO xtool_app;
GO
