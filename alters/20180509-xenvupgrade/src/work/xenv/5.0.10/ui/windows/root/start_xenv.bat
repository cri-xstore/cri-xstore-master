@echo off
setlocal

:: Copyright (c) 1999, 2009 Tanuki Software, Ltd.
:: http://www.tanukisoftware.com
:: All rights reserved.
::
:: This software is the proprietary information of Tanuki Software.
:: You shall use it only in accordance with the terms of the
:: license agreement you entered into with Tanuki Software.
:: http://wrapper.tanukisoftware.org/doc/english/licenseOverview.html
::
:: Java Service Wrapper general startup script.
:: Optimized for use with version 3.3.6 of the Wrapper.
::

::
:: Resolve the real path of the wrapper.exe
::  For non NT systems, the _REALPATH and _WRAPPER_CONF values
::  can be hard-coded below and the following test ::oved.
::
if "%OS%"=="Windows_NT" goto nt
echo This script only works with NT-based versions of Windows.
goto :eof

:nt
::
:: Find the application home.
::
:: %~dp0 is location of current script under NT
:: set _REALPATH=%~dp0

:: Decide on the wrapper binary.
set _WRAPPER_BASE=wrapper
set _REALPATH=%~dp0%
set _REALPATH=%_REALPATH%%_WRAPPER_BASE%
::echo Real path is %_REALPATH%

set _WRAPPER_EXE=%_REALPATH%%_WRAPPER_BASE%-windows-x86-32.exe
if exist "%_WRAPPER_EXE%" goto conf
set _WRAPPER_EXE=%_REALPATH%\bin\%_WRAPPER_BASE%-windows-x86-32.exe
if exist "%_WRAPPER_EXE%" goto conf
set _WRAPPER_EXE=%_REALPATH%\bin\%_WRAPPER_BASE%-windows-x86-64.exe
if exist "%_WRAPPER_EXE%" goto conf
set _WRAPPER_EXE=%_REALPATH%\bin\%_WRAPPER_BASE%.exe
if exist "%_WRAPPER_EXE%" goto conf
echo Unable to locate a Wrapper executable using any of the following names:
echo %_REALPATH%\bin\%_WRAPPER_BASE%-windows-x86-32.exe
echo %_REALPATH%\bin\%_WRAPPER_BASE%-windows-x86-64.exe
echo %_REALPATH%\bin\%_WRAPPER_BASE%.exe
goto :eof

::
:: Find the wrapper.conf
::
:conf

if "%1" == "cfg" goto CFG_WRAPPER
:ENV_WRAPPER
set _WRAPPER_CONF="%_REALPATH%\conf\wrapper.conf"
goto END_WRAPPER

:CFG_WRAPPER
set _WRAPPER_CONF="%_REALPATH%\conf\wrapper_cfg.conf"
:END_WRAPPER

:copyexe
IF NOT EXIST ..\runtime\win32\jre\bin\envui.exe. (
    COPY /Y ..\runtime\win32\jre\bin\java.exe ..\runtime\win32\jre\bin\envui.exe
)

::
:: Start the Wrapper
::
:startup
echo Startup
"%_WRAPPER_EXE%" -c %_WRAPPER_CONF%
if not errorlevel 1 goto :eof
pause

