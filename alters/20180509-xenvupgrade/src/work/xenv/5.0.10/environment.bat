@echo off
TITLE xEnvironment

REM ===============================================================================
REM Environment.Bat
REM This is the base startup script for an Xstore environment. It configures the
REM local operating system environment and launches the environment application
REM
REM Create 2005/02/07 Todd M. Senauskas
REM
REM Updates:
REM 2008/07/26 - DAA - Added logging of stderr and stdout
REM 2009/10/27 - DAA - Added check for whether engine is actually running.
REM 2010/08/30 - DAA - Changed ENV_HOME variable to use the current working dir.
REM
REM Internal Build: 17
REM
REM $Id: environment.bat 155904 2014-08-21 13:28:52Z dtvdomain\dandrzejewski $
REM ===============================================================================

cls
echo xEnvironment Startup...

REM Determine the relative path of the current directory to
REM the script, and then change to that directory.
set REL_DIR=%~d0%~p0

set OS_HOME=c:\windows
set SYS_HOME=%OS_HOME%\system32
set ENV_HOME=%~dp0%
set PYTHON_HOME=%ENV_HOME%\runtime\win32\python
set LIBZIP=%PYTHON_HOME%\lib\library.zip
set PYTHONPATH=%LIBZIP%;%LIBZIP%\site-packages;%LIBZIP%\site-packages\M2Crypto;%PYTHON_HOME%\lib;%PYTHON_HOME%\lib\site-packages;%ENV_HOME%\custom;%PYTHON_HOME%\DLLs
set PYTHONPATH=%PYTHONPATH%;%PYTHON_HOME%\lib\site-packages\OpenSSL;%PYTHON_HOME%\lib\site-packages\M2Crypto;%PYTHON_HOME%\lib\site-packages\win32
set BATCH_HOME=%ENV_HOME%\cust_batch
set XST_HOME=c:\xstore
set DB_HOME=%XST_HOME%\windows\sybase\win32
set UTIL_HOME=c:\util
set ENV_UTIL=%ENV_HOME%\ext\win32\util
set UPDATES_HOME=c:\updates
set PYTHONCASEOK=

cd %ENV_HOME%

ERASE /F /Q tmp\envtmp_*

Path=%ENV_UTIL%;%Path%;%OS_HOME%;%SYS_HOME%;%XST_HOME%;%ENV_HOME%;%BATCH_HOME%;%PYTHON_HOME%;%UTIL_HOME%;%DB_HOME%;%ENV_HOME%\runtime\win32;%PYTHON_HOME%\Lib\site-packages\pywin32_system32

setlocal

set _REALPATH=%~dp0
set PIDFILE=engine.pid
set EXECUTABLES=environment.exe,python.exe,pythonw.exe
set PIDDIR=%_REALPATH%tmp\

set FOUNDPID=no

echo Real Path: %_REALPATH%
echo PID File: %PIDFILE%
echo Executables: %EXECUTABLES%
echo PID Dir: %PIDDIR%
echo.

if not exist %windir%\system32\tasklist.exe goto START
if not exist %PIDDIR%%PIDFILE% goto START
for /f "tokens=*" %%a in (%PIDDIR%%PIDFILE%) do set XPID=%%a

ECHO PID: %XPID%
echo.
::@ECHO ON

for %%i in (%EXECUTABLES%) DO CALL :FINDIT %%i
GOTO :NEXT

:FINDIT
echo ----- Looking for PID %XPID%, task %1
tasklist /FI "PID eq %XPID%" |find "%1"
if %ERRORLEVEL% == 0 (
       ECHO Another Instance of %1 is already running from %_REALPATH%.
       SET FOUNDPID=yes
)

goto :EOF

:NEXT
if "%FOUNDPID%"=="yes" (
    ECHO xEnvironment is already running!
	PAUSE
    goto END
) else (
    ECHO Stale PID file found, removing.
    ERASE /Q %PIDDIR%%PIDFILE%
)

:START
echo.
echo ------------------------
echo Starting xEnvironment...
echo ------------------------
echo.

IF EXIST bin\environment.exe.log. (
    ERASE /Q bin\environment.exe.log.
)

IF EXIST %ENV_HOME%\bin\environment.exe. (
    start /D %ENV_HOME% bin\environment.exe %*
) ELSE IF EXIST %ENV_HOME%\environment.py. (
    start %PYTHON_HOME%\pythonw.exe %ENV_HOME%\environment.py %* 2>&1 c:\environment\log\env_out.log
) ELSE IF EXIST %ENV_HOME%\environment.pyc. (
    start %PYTHON_HOME%\pythonw.exe %ENV_HOME%\environment.pyc %* 2>&1 c:\environment\log\env_out.log
) ELSE (
    echo Error: Unable to locate environment.exe, environment.py, or environment.pyc!
    PAUSE
    GOTO END
)

:END
exit
